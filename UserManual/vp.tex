\chapter{Elastic and Viscoplastic Deformation}
\LCHAPH{VP}
 \begin{figure}[h]
   \begin{center}
     \begin{tabular}{lr}
        \includegraphics[width=4cm]{figures/vpgmv000003.png} &
	\includegraphics[width=4cm]{figures/vprgmv000003.png}
     \end{tabular}
   \end{center}
 \end{figure}

In this chapter we set up a simple problem to demonstrate the solid
deformation capability of \truchas.  The problem uses heat conduction (see
\CHAPH{HC}) and introduces input for solid mechanics,
including new material property input, boundary conditions, and solver
input specific to the solid mechanics solution.

%% \clearpage

\section{\truchas\ Capabilities Demonstrated}
\LSECH{vp_capabilities}

\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item \hyperlink{sec:vp:physics}{Solid mechanics}
  \item \hyperlink{sec:vp:mat}{Elastic stress and strain}
  \item \hyperlink{sec:vp:mat}{Viscoplastic strain}
\end{itemize}

\section{Problem Description}
\LSECH{vp:problem:description}

The problem consists of a ring of aluminum alloy in contact with a
center plug of graphite.  The outer diameter of the aluminum ring is
101.6 mm and the diameter of the plug (and inner diameter of the
ring) is 50.8 mm.  The plug and ring are 12.7 mm thick.  Both
materials have their stress-free reference temperature set to
630 degrees and both bodies have an initial temperature of
455 degrees.  An initial elastic stress field is calculated, driven by
the difference in the coefficients of thermal expansion for the two
materials.  Initial conditions were chosen to induce stresses
that will produce plastic flow in the aluminum ring near the interface
with the graphite.

The ring/plug assembly is allowed to cool with convective heat transfer
boundary conditions on the top and bottom.  Stresses and strains evolve
with time due to the changing temperature field and viscoplastic
deformation in the aluminum.  There is no relative movement at the
interface between the aluminum and graphite.

\begin{tabular}{ll}
Units: & SI\\
Input file:& UMProblems/Chapter\_vp/vp.inp \\
Mesh file: & UMProblems/Chapter\_vp/vp\_mesh.txt\\
\end{tabular}

\section{Setup}
\LSECH{vp:setup}

The complete input file for this problem is presented in \APPH{vp}.
Here we discuss previously unseen sections of the input file in detail.

\subsection{Defining a mesh}
\LSECH{vp:mesh}

\begin{verbatim}
&MESH
  mesh_file          ='vp_mesh.txt'
  mesh_file_format   ='genesis'
/
\end{verbatim}

The \texttt{MESH} namelist reads a file created by CUBIT and
translated to Genesis text format.  In addition to element blocks,
this input file contains
``side~sets.'' As defined by CUBIT, these are
sets of contiguous or non-contiguous surfaces that can be grouped,
written into the mesh file, and read by \truchas.
Side sets can then be referred to by side~set number in the input BC
namelists as a convenient way to specify which surfaces shall acquire
a boundary condition.

For this problem the solution should be axisymmetric, and the domain
represented by
the mesh is only 1/8 of the full size ring and plug.  Symmetry planes
are modeled by specifying appropriate boundary conditions.  

\subsection{Specifying physics}
\LSECH{vp:physics}

This problem activates only heat conduction and solid mechanics.

\begin{verbatim}
&PHYSICS
  fluid_flow        = .false.
  heat_conduction   = .true.
  solid_mechanics   = .true.
  phase_change      = .false.
/
\end{verbatim}

\subsection{Defining materials}
\LSECH{vp:mat}

Now we must give our materials elastic-plastic physical properties.
The graphite material is elastic, while the aluminum is
elastic-viscoplastic.  Currently the Mechanical Threshold Stress (MTS)
model or a simple power law creep model are the only choices for a
plasticity model.  Other choices may be added in future releases. 
More information about the viscoplastic models model is in the \PA\ and \RM. 
In order to calculate
elastic stresses, it is necessary to specify elastic constants
(\verb|Lame1_constants| and \verb|Lame2_constants|), the
coefficient of thermal expansion, and the stress reference
temperature.  If \verb|viscoplastic| \verb|_model| is not equal to
\verb|'elastic_only'|, then viscoplastic model parameters must be
specified.

\begin{verbatim}
&MATERIAL
  immobile                       = .true.
  material_number                = 1
  priority                       = 1
  material_name                  = 'graphite',
  density                        = 1.7e+03
  Cp_constants                   = 1.925e+03
  conductivity_constants         = 1.95e+02
  Lame1_constants                = 3.4e+9
  Lame2_constants                = 2.76e+9
  CTE_constants                  = 7.0e-06
  stress_reference_temperature   = 6.30e+02
  material_feature               = 'background'
  viscoplastic_model             = 'elastic_only'
/

&MATERIAL
  immobile                       = .true.
  material_number                = 2
  priority                       = 2
  material_name                  = '5754 aluminum'
  density                        = 2.70e+03
  Cp_constants                   = 9.00e+02
  conductivity_constants         = 2.40e+02
  Lame1_constants                = 5.20e+10
  Lame2_constants                = 2.60e+10
  CTE_constants                  = 2.20e-05
  stress_reference_temperature   = 6.30e+02
  viscoplastic_model             = 'MTS'
  MTS_k                          = 1.38e-23
  MTS_mu_0                       = 28.815e9
  MTS_sig_a                      = 10.0e6
  MTS_d                          = 3.440e9
  MTS_temp_0                     = 215.0
  MTS_b                          = 2.86e-10
  MTS_edot_0i                    = 1.0e7
  MTS_g_0i                       = 3.6
  MTS_q_i                        = 1.5
  MTS_p_i                        = 0.5
  MTS_sig_i                      = 107.2e6
/
\end{verbatim}

\subsection{Initializing the domain}
\LSECH{vp:init}

The bodies are defined in the mesh file, as described in 
\CHAPH{flow}.  The temperature is also specified for
each body, and in this case we set the initial temperature different from
the reference temperatures specified for each material in the material
namelist, so that the material will begin out of equilibrium. 

When solid mechanics physics is activated, an initial calculation of the
displacement field and linear elastic stresses is always computed
before the first time step.  If temperatures of bodies are the
same as the stress reference temperature of the materials assigned to
those bodies, the displacements, stresses, and strains should be zero.
In this case the temperature difference and the difference in thermal
expansion between the two materials will result in non-zero stresses
and displacements.

\begin{verbatim}
&BODY
  material_number        = 1
  mesh_material_number   = 1
  surface_name           = 'from mesh file'
  temperature            = 4.55e+02
/

&BODY
  material_number        = 2
  mesh_material_number   = 2
  surface_name           = 'from mesh file'
  temperature            = 4.55e+02
/
\end{verbatim}

\subsection{Applying boundary conditions}
\LSECH{vp:bc}

This time we do not set our boundary conditions on plane surfaces
using degenerate conic sections. Instead we assign them to side~sets
in the mesh file.

\begin{verbatim}
&BC
  surface_name   = 'from mesh file'
  mesh_surface   = 6
  BC_variable    = 'temperature'
  BC_type        = 'HNeumann'
/

&BC
  surface_name   = 'from mesh file'
  mesh_surface   = 6
  BC_variable    = 'displacement'
  BC_type        = 'z-displacement'
  BC_value       = 0.0e0
/
\end{verbatim}

The first two BC namelists specify a symmetry boundary on the bottom
surface of the mesh ($z$ = 0).  The \verb|mesh_surface| entry
corresponds to the number of the side~set defined in CUBIT (in this
case, 6).  For a symmetry boundary the thermal boundary condition
must be zero flux (\verb|'HNeumann'|), and the displacement BC should
constrain the displacement normal to the surface
to be zero.  Also for solid mechanics the traction BC
tangential to the interface ($x$ and $y$ directions in this
case) must be zero, which is the default.

\begin{verbatim}
&BC
  surface_name    = 'from mesh file'
  mesh_surface    = 5
  BC_variable     = 'temperature'
  BC_type         = 'HTC'
  t0_convection   = 2.98e+02
  BC_value        = 1.0e3
/
\end{verbatim}

The third BC namelist specifies a convection boundary condition for
temperature on the top surface of the mesh.  Solid mechanics
boundary conditions default to zero traction in the $x$, $y$, and $z$
directions.

The fourth and fifth BC namelists specify symmetry conditions for the
plane defined by $x = 0$, in the same manner as for BC namelists 1 and
2.  This plane corresponds to side set number 3.

The sixth and seventh BC namelists specify symmetry conditions for the
plane defined by $y = 0$, in the same manner as for BC namelists 1 and
2.  This plane corresponds to side set number 2.

Finally, the eighth BC namelist applies a convection temperature condition to the outer radius of the mesh, corresponding to side set number 4.

\subsection{Selecting output formats}
\LSECH{vp:output}

Outputs are selected for this problem in familiar ways.
The problem is set to run for a short time (3 seconds) because the run
time on some platforms can be rather long to observe substantial
plastic flow.  The user may want to run the problem to ~100 seconds to
see more interesting results.

\subsection{Numerics}
\LSECH{vp:numerics}

For solid mechanics we add new options to the \verb|NUMERICS| namelist.
There is no well-defined stability criterion for the nonlinear
viscoplastic solution, so choosing appropriate time step parameters
may be difficult.  No time step control can be based on elastic
strain. 
For plastic strain integration the \verb|strain_limit| parameter
is used to control the accuracy as well as the method used.
This parameter should be set to a value that is
the minimum plastic strain \emph{increment} that is significant for a time step.
Lastly, a new nonlinear solver must be specified for the displacement
solution.

\begin{verbatim}
&NUMERICS
  dt_max                            = 5.0e-1
  dt_grow                           = 1.2
  dt_init                           = 2.0e-2
  dt_min                            = 1.0e-8
  strain_limit                      = 1.0e-12
  energy_nonlinear_solution         = 'conduction nonlin'
  displacement_nonlinear_solution   = 'displacement nonlin'
/
\end{verbatim}

\subsection{Nonlinear Solvers}

There are two nonlinear solvers to define.
Since the heat transfer solution involves conduction only, that solver's
convergence criterion can be fairly loose.  The solid mechanics solution
uses the accelerated inexact Newton (AIN) method.  In general the AIN
nonlinear solver is more robust than Newton-Krylov for solid mechanics
problems, especially for viscoplastic or contact problems.

\begin{verbatim}
&NONLINEAR_SOLVER 
  name                     ='conduction nonlin'
  method                   ='NK'
  linear_solver_name       ='conduction NK'
  convergence_criterion    =1e-02
  output_mode              ='none'
  use_damper               =.false.
  perturbation_parameter   =1e-06
/

&NONLINEAR_SOLVER
  name                     ='displacement nonlin'
  method                   ='AIN'
  linear_solver_name       ='displacement AIN'
  convergence_criterion    = 1e-8
  AIN_max_vectors          = 30
  AIN_vector_tolerance     = 30
  maximum_iterations       = 200
/
\end{verbatim}

\subsection{Linear Solvers}

There are also two linear solvers defined, one for the conduction
solution, and one for the nonlinear elastic-viscoplastic solution.
In this release the preconditioning method for the displacement
solvers must be \verb|'TM_SSOR'|, \verb|'TM_diag'| or \verb|'none'|.

\begin{verbatim}
&LINEAR_SOLVER
  name                     = 'conduction NK'
  method                   = 'FGMRES'
  preconditioning_method   = 'SSOR'
  preconditioning_steps    = 2
  stopping_criterion       = '$||r||/||r0||$'
  convergence_criterion    = 1.0e-4
/    

&LINEAR_SOLVER
  name                     = 'displacement AIN'
  method                   = 'none'
  preconditioning_steps    = 4
  relaxation_parameter     = 1.0
  preconditioning_method   = 'TM_SSOR'
\end{verbatim}

\section{Results}
As advertised, the results show that the aluminum ring shrinks and
flows plasitcally around the graphite plug.

\vspace{2cm}
\begin{figure}[h]
  \begin{center} \begin{tabular}{lll}
  \includegraphics[width=5cm]{figures/vpTgmv000003.png} &
  \includegraphics[width=5cm]{figures/vpsxxgmv000003.png} &
  \includegraphics[width=5cm]{figures/vpdzgmv000003.png} \end{tabular}
  \end{center} \caption{Plots of $T$, $\sigma_{xx}$ and $\Delta z$ are
  shown for times t = 3.0 sec}
\LFIGH{vpresults}
\end{figure}
