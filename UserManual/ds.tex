\chapter{Diffusion Solver}
\LCHAPH{IH}
 \begin{figure}[h]
   \begin{center}
     \includegraphics[width=4in]{figures/ds-title.png}
   \end{center}
 \end{figure}

This chapter introduces the diffusion solver component of \truchas,
which is a new feature of this release.  A nonlinear solid-state
diffusion problem is used to illustrate its capabilities.

\clearpage

\section{Problem Description}%
\LSECH{ds:problem:description}
We consider the simple solid-state diffusion problem depicted in
Figure~\ref{ds:diag1}.  A solutal species with concentration $c$ diffuses
into a substrate material through a portion of the top surface.  On the
remainder of the top surface and on the left side symmetry plane no flux
of species is imposed.  Artificial boundaries on the right and bottom sides
are introduced to truncate the domain.  At $t=0$ we assume $c=0$ throughout
the domain, and impose $c=1$ on the inflow part of the top side for $t>0$.
The concentration field $c(x,t)$ satisfies the heat equation with a
concentration dependent diffusivity,
\[ \frac{\partial c}{\partial t} = \nabla\cdot D(c)\nabla c, \quad
   D(c) = 0.02 + c. \]

\begin{tabular}{ll}
Input file: & \texttt{UMProblems/Chapter\_ds/ds1.inp} \\
Mesh  file: & \texttt{UMProblems/Chapter\_ds/umds1.gen}
\end{tabular}

\begin{figure}[t]
  \begin{center}
    \includegraphics[height=1.5in]{figures/ds1-prob-geom.pdf}
  \end{center}
  \caption{Computational domain and boundary conditions for the nonlinear
    diffusion problem.}%
    \label{ds:diag1}
\end{figure}

\section{Setup}
\LSECH{ih:setup}

The complete input file for this problem is presented in \APPH{ds}.  The
discussion here focuses on those sections of the input file related to
the diffusion solver.


\subsection{Defining the mesh}%
\LSECH{ih:mesh}
The mesh, which can be seen in Figure~\ref{ds:plot1}, is an unstructured,
quasi-2D, hexahedral mesh that is one cell thick in the out-of-plane
$z$ direction.  It was created by Cubit and written in Exodus/Genesis
format to the file \texttt{umds1.gen}.  Note that the diffusion solver
requires that meshes be in this format.  Thus an internally generated mesh,
for example, can not be used with the diffusion solver.
{\small\begin{verbatim}
&MESH
  Mesh_File = 'umds1.gen'
  Mesh_File_Format = 'ExodusII'
/
\end{verbatim}}

\subsection{Specifying physics}%
\LSECH{ih:physics}
Here we enable the diffusion solver, though the specific type of diffusion
system to be solved will be specified later.  Since fluid flow is enabled
by default, we must also disable it.
{\small\begin{verbatim}
&PHYSICS
  Diffusion_Solver = .true.
  Fluid_Flow       = .false.
/
\end{verbatim}}
The diffusion solver is not currently co-operable with the other physics
solvers, so it is not possible, for example, to enable flow at the same
time and solve an advection-diffusion system.  This is a limitation that
will be eliminated in the next release of \truchas.


\subsection{Defining materials and bodies}%
\LSECH{ih:mat}
The computational domain consists of a single region to which we
associate a single material.  All the cells in the Cubit mesh belong to
element block 10, so we use a single \texttt{BODY} namelist referencing
that ID.  We also specify $0$ as the initial value of the concentration
field here.
{\small\begin{verbatim}
&BODY
  Surface_Name         = 'from mesh file'
  Mesh_Material_Number = 10
  Material_Number      = 1
  Temperature          = 0.0
  Concentration        = 0.0
/
\end{verbatim}}
The species diffusivity $D(c)$ in the substrate material is specified
using the \texttt{SPC\_Diffusivity\_*} variables in the \texttt{MATERIAL}
namelist for that material.  Here it is a polynomial function in the
concentration with the indicated powers and corresponding coefficients.
{\small\begin{verbatim}
&MATERIAL
  Material_Number  = 1
  Material_Name    = 'substrate'
  Material_Feature = 'background'
  Density          = 1.0, 
  SPC_Diffusivity_Relation  = 'concentration polynomial'
  SPC_Diffusivity_Constants = 0.02, 1.0
  SPC_Diffusivity_Exponents = 0, 1
/
\end{verbatim}}
We can see several idiosyncrasies of \truchas\ here: an initial
value for the (unused) temperature field must be assigned and every
material must be assigned a density even if it is not relevant to
the problem.


\subsection{Defining boundary conditions}%
\LSECH{ih:bc}
The boundary conditions for the diffusion solver's concentration
variable are specified in the \texttt{DS\_SPECIES\_BC} namelist instead
of the ususal \texttt{BC} namelist.  When this Cubit mesh was created,
the boundary cell faces were grouped into several suitable side sets, and
we reference the IDs of these sets when defining the boundary conditions.
Note that this is the \emph{only} way of identifying sets of boundary
faces in this namelist.
{\small\begin{verbatim}
&DS_SPECIES_BC
  Name         = 'inflow boundary'
  Face_Set_IDs = 5
  BC_Type      = 'dirichlet'
  BC_Value     = 1.0
/
&DS_SPECIES_BC
  Name         = 'noflow boundaries'
  Face_Set_IDs = 1, 4, 11, 12
  BC_Type      = 'hneumann'
/
&DS_SPECIES_BC
  Name         = 'artificial boundaries'
  Face_Set_IDs = 2, 3
  BC_Type      = 'dirichlet'
  BC_Value     = 0.0
/
\end{verbatim}}


\subsection{Numerics}%
\LSECH{ih:numerics}
{\small\begin{verbatim}
&DIFFUSION_SOLVER
  System_Type  = "species"
  Abs_Conc_Tol = 1.0e-4
  Rel_Conc_Tol = 1.0e-3
/
\end{verbatim}}
The diffusion solver is capable of solving different types of coupled
diffusion systems.  A simple scalar diffusion equation of the form
\( c_t = \nabla\cdot D(c)\nabla c \), as is our case, is selected by
giving the \texttt{"species"} keyword.  Currently this is the only
implemented option.

The diffusion solver uses a variable step-size BDF2 method to integrate
the coupled diffusion system.  The step sizes are chosen automatically
in order to maintain an estimate of the local truncation error of each
step within a user-defined range.  If $\delta\mathbf{c}$ is a concentration
field difference with reference concentration field $\mathbf{c}$, this
error is computed using the norm
\[ |||\delta\mathbf{c}|||\equiv\max_j\,|\delta c_j|/(\epsilon+\eta |c_j|). \]
The absolute tolerance $\epsilon$ is given by \texttt{Abs\_Conc\_Tol} and
the relative tolerance $\eta$ is given by \texttt{Rel\_Conc\_Tol}.

See the \RM\ for the other numerical parameters which can be specified
in the \texttt{DIFFUSION\_SOLVER} namelist.

Finally, the initial time step size, which pertains to all physics solvers,
is specified in the \texttt{NUMERICS} namelist.
{\small\begin{verbatim}
&NUMERICS
  Dt_Init = 1.0d-6
/
\end{verbatim}}
 

\section{Results}
Sample results from the problem are shown in Figure~\ref{ds:plot1}.

\begin{figure}[ht]
  \begin{center}
    \begin{tabular}{rl}
      \begin{tabular}{@{}c@{}c@{}}
        \includegraphics[width=2.5in]{figures/ds1-conc-1.png} &
        \includegraphics[width=2.5in]{figures/ds1-conc-2.png} \\
        \includegraphics[width=2.5in]{figures/ds1-conc-3.png} &
        \includegraphics[width=2.5in]{figures/ds1-conc-4.png}
      \end{tabular} &
      \begin{tabular}{@{}c@{}}
        \includegraphics[height=3in]{figures/ds1-conc-cbar.png}
      \end{tabular}
    \end{tabular}
  \end{center}
  \caption{Time sequence of the concentration profile.}%
  \label{ds:plot1}
\end{figure}
