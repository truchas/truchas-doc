\chapter{Flow and Filling}
\LCHAPH{flow}
\begin{figure}[h]
   \begin{center}
    \begin{tabular}{ll}
       \includegraphics[scale=0.5,keepaspectratio=true]{figures/fill_example_materials} &
       \hspace{2cm}
       \includegraphics[scale=0.5,keepaspectratio=true]{figures/fill_example_side_sets}
    \end{tabular}
    \end{center}
\end{figure}

\vspace{0.5in}

We now change gears and introduce flow, a feature that is independent
of heat transfer.  We demonstrate how to set up the filling of a
cylindrical mold.  Sections of the input file that are discussed
in this chapter are additions to the physics capabilities, \emph{viz.} assigning
material IDs to materials read from an imported mesh file, an inflow
boundary condition, and a variable time step.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{flow:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item   \hyperlink{sec:flow:physics}{Flow}
  \item   \hyperlink{sec:flow:init}{Assigning Material IDs to Mesh Materials}
  \item   \hyperlink{sec:flow:setup}{Scaling an externally generated mesh}
  \item   \hyperlink{sec:flow:bc}{Inflow Boundary Condition}
  \item   \hyperlink{sec:flow:numerics}{Variable Time Step}
\end{itemize}

\section{Problem Description}
\LSECH{flow:problem:description} 
In this chapter we demonstrate a problem where a cylindrical object is
filled with a fluid.  The cylinder is initially filled with void.
Fluid enters from an inlet through a boundary condition that sets a velocity
at which material flows into the mesh domain through a boundary, called
an inflow boundary condition.

One important point to note is that currently \truchas\ cannot stop a fill
boundary condition after either a certain volume of fluid has entered
the domain or after a given amount of time has elapsed.  One has to
stop the simulation, change the boundary condition in the input file,
and restart the simulation with new boundary conditions to achieve
this.  This will change in an upcoming release of the code.

\begin{tabular}{ll}
Units: & CGS\\
Input file:& UMProblems/Chapter\_flow/flow.inp \\
Mesh file: & UMProblems/\CHAPH{flow}/flow\protect\_mesh.txt \\
\end{tabular}

\section{Setup}
\LSECH{flow:setup}
The input file for this problem is presented in \APPH{flow}.  In this
chapter we discuss variables related to flow as well as boundary
conditions related to filling.  

\subsection{Defining a mesh}
\LSECH{flow:mesh}

\begin{verbatim}
&MESH
  mesh_file                = 'fill_example.exo'
  mesh_file_format         = 'exodousII'
  coordinate_scale_factor  =  1.
/
\end{verbatim}

The new line in this file is \verb|coordinate_scale_factor|
\verb|= 1| which directs \truchas\ to scale all coordinates in the input mesh
file \verb|flow_mesh.txt| by a factor typically used for unit converstion.  
See the \verb|MESH| of the \RM\ for a comprehensive listing of all fields that
can occur in this namelist.

\subsection{Specifying physics}
\LSECH{flow:physics}

Flow is the only type of \truchas\ physics that is on by default. In all
the preceding examples with heat conduction physics turned on, flow
physics needed to be explicitly set to \false to prevent it being on.
In this problem we explicitly set fluid flow on, whereas we need not
switch heat conduction off.

\begin{verbatim}
&PHYSICS
  fluid_flow  = .true.
  body_force  =  0.0, -981.0, 0.0
  inviscid    = .true.
/
\end{verbatim}

The \verb|body_force| invokes a force due to gravity.  We also turn
on inviscid flow capabilities. See the \verb|PHYSICS| chapter of the
\RM\ for further details on these variables.  

\subsection{Defining materials}
\LSECH{flow:mat}

\begin{verbatim}
&MATERIAL
  material_number   = 1
  material_name     = 'water'
  priority          = 1
  density           = 1.
/

\&MATERIAL
  material_number   = 2
  material_name     = 'void'
  priority          = 2
  density           = 0.
  material_feature  = 'background'
/

\&MATERIAL
  immobile          = .true.
  material_name     = 'mold'
  material_number   = 3
  priority          = 3
  density           = 1000
/ 
\end{verbatim}

The \verb|'immobile = .true.'| setting for material $3$ indicates that
for the purpose of flow calculations, this material does not move;
only materials $1$ and $2$ flow.  The \verb|'priority'| fields in the
materials namelists define the priorities with which we deal with
materials when computing flow.  See \CHAPH{HC} \SECH{cond:init} and
the \verb|MATERIAL| chapter in the \RM\ for details on these variables.

\subsection{Initializing the domain}
\LSECH{flow:init}

The initialization of the domain in this chapter is done differently
from the previous chapters.  Here we read in a mesh file
(see \SECH{flow:mesh}), and
assign material IDs in the mesh file to materials in the \truchas\.
In this case the materials as specified in the exodusII file are shown, 
with the red material corresponding to material 1 and the blue corresponding
to material 2.  This mechanism can only assign entire mesh blocks uniformly to single 
(possibly mixed) materials.

\begin{verbatim}
&BODY
  surface_name          = 'from mesh file'
  mesh_material_number  = 1
  material_number       = 3
  temperature           = 0.
/

&BODY
  surface_name          = 'from mesh file'
  mesh_material_number  = 2
  material_number       = 2
  temperature           = 0.
/
\end{verbatim}

A line \verb|surface_name =| \verb|'from mesh file'|
indicates that we have to initialize the body from a block in the mesh file.
One examines the mesh file and identifies the block number to which to assign
the current material.
On the line for \verb|mesh_material_number| this block ID is on the right hand side,
and presence of a \verb|material_number| assignment in the same namelist
completes the assignment of a material number to the mesh block.
See the \verb|BODY| chapter of the \RM\ for further options.

\subsection{Applying boundary conditions}
\LSECH{flow:bc}

An important difference exists in boundary conditions when flow
is run without heat conduction. Whereas heat conduction solutions
require some temperature boundary condition to be set on every external
surface of the computational domain---and there is no default condition---
flow can safely assume a default free-slip velocity boundary condition
on external surfaces. Therefore in this problem only the inflow
boundary condition on one surface needs to be explicitly set.
All other surfaces are implicitly free-slip by omission of BC
namelists to set them otherwise.  In the exodusII mesh file, four side
sets are specified corresponding to the top and bottom surfaces (side sets 1 and 2),
the outer boundary (side set 3) and the nozzle inlet (side set 4). 

\begin{verbatim}
&BC
  surface_name         = 'from mesh file'
  mesh_surface         = 4
  BC_variable          = 'velocity'
  BC_type              = 'Dirichlet'
  BC_value             = 0.0, -1000., 0.0
  inflow_material      = 1
  inflow_temperature   = 0.0
/
\end{verbatim}

The input is defined with a constant velocity boundary
condition with velocity of -1000 cm/s in the y direction.  The
material that enters has ID 1 and zero temperature.  See \CHAPH{HC}
\SECH{cond:bc} and the
\verb|BC| chapter of the \RM\ for all types of boundary conditions.


\subsection{Numerics}
\LSECH{flow:numerics}

\truchas\ flow physics has a considerable number of controls that must be
understood in order to use the code optimally.
In the \verb|NUMERICS| namelist we define a variable time step that
can grow or shrink automatically in response to changing flow conditions,
and we provide direction to the code on how to solve the flow problem.
The reader should peruse the material in the \PA\ to learn the variety
of flow methods installed in \truchas.  We discuss a few of the options below.

\begin{verbatim}
&NUMERICS
  dt_init                     = 1.e-4
  dt_grow                     = 1.05
  dt_min                      = 1.e-7
  dt_max                      = 1.
  discrete_ops_type           = 'ortho'
  projection_linear_solution  = 'projection-solver'
  Courant_number              = 0.4
  volume_track_interfaces     = .true.
  volume_track_Brents_method  = .true.
  volume_track_iter_tol       = 1.0e-12
  cutVOF                      = 1.0e-08
/
\end{verbatim}

The initial time step
is $10^{-4}$s, and this is allowed to grow at most by the ratio given
in \verb|dt_grow|.  The limits on the smallest and largest time
steps in the simulation are given by \verb|dt_min| and
\verb|dt_max| respectively.  For incompressible flow another type of solver,
projection, must be defined.  The Courant number, defined in the
\PA, is the basic flow constraint.
It must be less than 1, and the default is 0.5, so the choice here is
conservative.

Volume tracking is the method by which \truchas\ and many other
multimaterial simulation codes keep track of more than one material
in a computational cell.  Our method is known as a `volume of fluid'
or VOF method, and \verb|cutVOF| is a limit for a tiny fraction of
a cell in a material, below which it will be ``cut'' from the cell
and no longer tracked.
Technical details are in the \PA\ and an
explanation of all \verb|NUMERICS| namelist options is given in the
\RM.

\subsection{Linear solvers}

\begin{verbatim}
&LINEAR_SOLVER
  name                    = 'projection-solver'
  method                  = 'GMRES'
  preconditioning_method  = 'SSOR'
  preconditioning_steps   = 2
  relaxation_parameter    = 1.4
  convergence_criterion   = 1.e-8
  maximum_iterations      = 50
/
\end{verbatim}

This namelist specifies the solver for the projection solution and
is similar to previous examples.
See \CHAPH{HC} \SECH{cond:lsolver} and the
\verb|LINEAR_SOLVER| chapter of the \RM\ for a comprehensive listing
of all possible variables.

\section{Results}

Sample results from the program plotted using gmv are presented in
\FIGH{flow:results}.

\begin{figure}[h]
  \begin{center}
     \begin{tabular}{lll}	
     \includegraphics[width=4.0cm]{figures/fill_example_flow_pattern_t005} &
     \includegraphics[width=4.0cm]{figures/fill_example_flow_pattern_t02} &
     \includegraphics[width=4.0cm]{figures/fill_example_flow_pattern_t04}
     \end{tabular}
     \end{center}
     \caption{Sample results at 0.005, 0.02 and 0.04 s. 
     The figures are colored by the volume fraction of water
     with red representing the injected water, dark blue representing
     void, and grey respresenting the mold.  The arrows indicate the velocity of liquid.}
   \LFIGH{flow:results}
\end{figure}

--
