\chapter{Radiation}
\LCHAPH{RAD}
 \begin{figure}[h]
   \begin{center}
     \begin{tabular}{l}
        \includegraphics[height=5cm,width=6cm]{figures/radgmv000010.png} 
     \end{tabular}
   \end{center}
 \end{figure}

In this chapter we set up a problem to demonstrate the \truchas\ package
that computes radiative heat transfer using a viewfactor method. This capability
is important when treating heat transfer problems where bodies are
not touching (so that conduction cannot transfer heat) and are surrounded by
vacuum (so that convection cannot transfer heat).

\section{\truchas\ Capabilities Demonstrated}
\LSECH{rad:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item	Viewfactor radiative heat transfer
\end{itemize}

\section{Problem Description}
\LSECH{rad:problem:description}
The selected problem represents two concentric hollow spherical shells
separated by a vacuum(\FIGH{radprob}).  The two solid regions are denoted
by $R_1$, $R_2$ and the four surfaces separating solid from vacuum by
$S_j$ at radii $r_j$, $j=0,4$.
$R_1$ is bounded by $S_0$ and $S_1$ and $R_2$ by $S_2$ and $S_3$.
We impose an outward heat flux at the innermost surface $S_0$. The
temperature at the outermost surface $S_3$ is held constant.
Heat conducts through the inner sphere to its outer boundary $S_1$,
where it is radiated into the vacuum separating the inner and outer spheres.
Surface $S_2$ absorbs some of that heat and reflects the rest. 
The absorbed heat is conducted through the outer shell to the
temperature-clamped heat sink at $S_3$.

\begin{figure}[h]
  \begin{center}
    \includegraphics[height=4cm,width=15cm]{figures/radprob.pdf}
  \end{center}
  \caption{Concentric hollow spherical shells, $R_1$ and $R_2$, coupled
    through radiative heat transport within the intervening void.}
\LFIGH{radprob}
\end{figure}

The derivation uses two principles of physics: (1) conductive heat flux $q(r)$
in a solid is proportional to the negative of the temperature gradient, with
thermal conductivity as the linear constant; and (2) in a steady state
spherically symmetric configuration of bodies with spherically symmetric
sources and sinks of heat, the steady state heat flux at any radius varies
inversely as the square of radius, \emph{i.~e.} the quantity $r^2q(r)$ is
constant within $R_1$ and $R_2$. Thus for $r$ in $R_1$
\begin{equation}
  {r_0}^2 q_0 = -r^2 k_1 T'(r),
\end{equation}
where $k_1$ is the thermal conductivity in $R_1$, from whence it follows that
\begin{equation}
  T(r) = T_1 + \frac{{r_0}^2 q_0}{k_1}\Big(\frac1r - \frac1{r_1}\Big),
    \quad r\in[r_0,r_1].
\end{equation}
In particular,
\begin{equation}
  T_0 = T_1 + \frac{q_0}{k_1}\frac{r_0}{r_1}(r_1 - r_0).
\end{equation}
Thus the temperature in $R_1$ is completely determined once the surface
temperature $T_1$ is determined.
Similarly in $R_2$, ${r_2}^2 q_2 = -r^2 k_2 T'(r)$ from whence it follows that
\begin{equation}
  T(r) = T_3 + \frac{{r_2}^2 q_2}{k_2}\Big(\frac1r - \frac1{r_3}\Big),
    \quad r\in[r_2,r_3],
\end{equation}
where $T_3$ is the imposed temperature at $r_3$.  So in particular,
\begin{equation}
  T_2 = T_3 + \frac{q_2}{k_2}\frac{r_2}{r_3}(r_3 - r_2).
\end{equation}

Next consider radiative heat transfer in the void region.  Let $Q_j$
denote the net radiative flux leaving surface $S_j$, and $Q^i_j$
the radiative flux \emph{incident} on $S_j$, $j=1,2$.  Now $Q_j$ is the sum
of the emitted flux and the reflected portion of the incident flux; for
diffuse gray-bodies,
\begin{equation}
  Q_j = \epsilon_j\sigma{T_j}^4 + (1-\epsilon_j)Q^i_j, \quad j=1,2,
\end{equation}
where $0<\epsilon_j\le1$ is the emissivity of $S_j$, and $\sigma$ is the
Stefan-Boltzmann constant.  The incident flux $Q^i_j$ is defined by the
linear relation,
\begin{equation}
  \begin{bmatrix} Q^i_1 \\ Q^i_2 \end{bmatrix} = 
    F \begin{bmatrix} Q_1 \\ Q_2 \end{bmatrix}, \qquad
  F = \begin{bmatrix} f_{11} & f_{12} \\ f_{21} & f_{22} \end{bmatrix},
\end{equation}
where $f_{ij}$ is the view factor between surfaces $S_i$ and $S_j$.
For the present geometry, $f_{11}=0$ since no distinct points on $S_1$
are visible to one another.  Thus $f_{12} = 1 - f_{11} = 1$.  Since
viewfactors satisfy a reciprocity relation, namely for $S_1$ and $S-2$
$A_2 f_{21} = A_1 f_{12}$, where $A_j$ is the area of $S_j$,
$f_{21} = (r_1/r_2)^2$.  Finally $f_{22} = 1 - f_{21} = 1 - (r_1/r_2)^2$.
To summarize,
\begin{equation}
  F = \begin{bmatrix} 0 & 1 \\ (r_1/r_2)^2 & 1 - (r_1/r_2)^2 \end{bmatrix}.
\end{equation}

Next we match the conductive fluxes at $S_1$ and $S_2$ to the radiative
fluxes.
The flux $q_1$ is the difference between the emitted flux and the absorbed
portion of the incident flux
\begin{align}
  q_1 &= \epsilon_1 \sigma {T_1}^4 - \epsilon_1 Q^i_1, \notag \\
      &= Q_1 - Q^i_1.
\end{align}
Similarly,
\begin{equation}
  q_2 = Q^i_2 - Q_2.
\end{equation}
These relations lead to
\begin{equation}
  q_2 = \Big(\frac{r_1}{r_2}\Big)^2 q_1.
\end{equation}
That is, the second physical principle above is satisfied across the void gap
between the two concentric shells. Thus
\begin{equation}\label{q2}
  q_2 = \Big(\frac{r_0}{r_2}\Big)^2 q_0,
\end{equation}
giving the flux at $S_2$ in terms of the imposed heat flux at $S_0$.

From this we obtain the single independent equation
\begin{equation}
  Q_1 - Q_2 = \Big(\frac{r_0}{r_1}\Big)^2 q_0.
\end{equation}
Taking this equation together with equation 11.6 for $Q_j$ yields
a linear system for $Q_1$ and $Q_2$,
\begin{equation}
  \begin{bmatrix}
    1                          & -(1-\epsilon_1)                        \\
    -(1-\epsilon_2)(r_1/r_2)^2 & \epsilon_2 + (1-\epsilon_2)(r_1/r_2)^2 \\
    1                          & -1
  \end{bmatrix} \begin{bmatrix} Q_1 \\ Q_2 \end{bmatrix} =
  \begin{bmatrix}
    \epsilon_1\sigma{T_1}^4 \\
    \epsilon_2\sigma{T_2}^4 \\
    \displaystyle q_0 (r_0/r_1)^2
  \end{bmatrix}.
\end{equation}
By the Fredholm Alternative this sytem is solvable if and only
if the right hand side is orthogonal to the left nullspace of the coefficient
matrix.  It is easily verified that the nullspace is spanned by the vector
$\big(-\epsilon_2, \epsilon_1, \epsilon_2 + \epsilon_1(1-\epsilon_2)
(r_1/r_2)^2\big)$.  Thus solvability implies
\begin{equation}\label{t1}
  {T_1}^4 = {T_2}^4 + \frac{q_0}{\sigma} \Big(\frac{r_0}{r_1}\Big)^2
  \Big[\frac1{\epsilon_1} + \Big(\frac1{\epsilon_2}-1\Big)
  \Big(\frac{r_1}{r_2}\Big)^2\Big].
\end{equation}
This completes our set of relations between the temperatures at the four
surfaces.

To run this problem with \truchas, the bodies are initialized with some
constant temperature, and the temperature distribution then evolves toward
a steady state that depends on the imposed heat flux at $S_0$, the imposed
temperature at $S_3$, and the material parameters of the solids.
Our sample problem is presented in dimensionless units,
so that results can readily be compared with the analytic solution.

\begin{tabular}{ll}
Units: & dimensionless\\
Input file:& UMProblems/Chapter\_rad/rad.inp \\
Mesh file: & UMProblems/Chapter\_rad/2shell8.gen\\
Viewfactor file: & UMProblems/Chapter\_rad/2shell8-le.vf\\
\end{tabular}

\section{Setup}
\LSECH{rad:setup}
The input file for this problem is presented in its entirety in \APPH{rad}.
In this section we describe new features of the input needed for problems
involving radiative heat transfer using a viewfactor method.

\subsection{Specifying physics}
\LSECH{rad:physics}
In \truchas\ the radiative heat transfer package is called ``enclosure
radiation,'' to distinguish it from the plain ``radiation'' option among the
boundary conditions.
{\small\begin{verbatim}
&PHYSICS
  fluid_flow                  = .false.
  heat_conduction             = .true.
  Stefan_Boltzmann            = 1.0
/
\end{verbatim}}
We activate heat conduction physics only. Enclosure radiation physics, like
induction heating physics, has its own namelist to set parameters.
Note that we have reset the Stefan-Boltzmann constant from its default SI
value to unity, in keeping with dimensionless units.

\subsection{Defining materials}
\LSECH{rad:mat}

Also in keeping with dimensionless units, we define hypothetical materials
\verb|'foo'| and \verb|'bar'| with unit density and specific heat.
We give them different constant thermal conductivities to make things
interesting.  The user can play with these parameters to see how the
solution varies.

The third material must be included for a \verb|'background'| material.

{\small\begin{verbatim}
&MATERIAL
  material_number             = 1
  material_name               = 'foo'
  density                     = 1.0
  Cp_constants                = 1.0
  conductivity_constants      = 4.0
/

&MATERIAL
  material_number             = 2
  material_name               = 'bar'
  density                     = 1.0
  Cp_constants                = 1.0
  conductivity_constants      = 1.0
/

!! Dummy material to satisfy Truchas need for background material.
&MATERIAL
  material_number             = 3
  material_name               = 'void'
  material_feature            = 'background'
  density                     = 0.0
  Cp_constants                = 0.0
  conductivity_constants      = 0.0
  void_temperature            = 0.0
/
\end{verbatim}}

\subsection{Applying boundary conditions}
\LSECH{rad:bc}

Setting of temperature boundary conditions follows the definition of
the problem.  A Neumann condition specifies an imposed heat flux
(the value being that flux), a Dirichlet condition specifies an imposed
temperature (the value being that temperature), and a homogeneous Neumann
condition sets the flux to zero.

A new type of BC is specified for the surfaces on either side of the void between
the concentric shells.  The BC \verb|'enclosure_radiation'| specifies
viewfactor radiative transfer in and out of those surfaces. The
\verb|'BC_value'| settings have two values. The first is the index
of the enclosure to which the surface belongs---because a problam can
have multiple enclosures--- and the second value is the emissivity
($0<\epsilon\le1$) of the surface.  The presence of at least one
\verb|'BC_type'| enclosure radiation also alerts \truchas\ to look for
\verb|'RADIATION_ENCLOSURE'| namelists, one per enclosure.

{\small\begin{verbatim}
!! Imposed flux on inner surface of inner shell.
&BC
  surface_name                = 'from mesh file'
  mesh_surface                = 1
  BC_variable                 = 'temperature'
  BC_type                     = 'Neumann'
  BC_value                    = 16.0
/

!! Radiation from outer surface of inner shell.
&BC
  surface_name                = 'from mesh file'
  mesh_surface                = 2
  BC_variable                 = 'temperature'
  BC_type                     = 'enclosure_radiation'
  BC_value                    = 1, 0.8
/

!! Radiation from inner surface of outer shell. 
&BC
  surface_name                = 'from mesh file'
  mesh_surface                = 3
  BC_variable                 = 'temperature'
  BC_type                     = 'enclosure_radiation'
  BC_value                    = 1, 0.2
/

!! Imposed temperature on outer surface of outer shell.
&BC
  surface_name                = 'from mesh file'
  mesh_surface                = 4
  BC_variable                 = 'temperature'
  BC_type                     = 'Dirichlet'
  BC_value                    = 1.0
/

!! No-flux conditions on symmetry planes.
&BC
  surface_name                = 'from mesh file'
  mesh_surface                = 5
  BC_variable                 = 'temperature'
  BC_type                     = 'HNeumann'
/
\end{verbatim}}

\subsection{Specifying enclosure radiation: the \texttt{RADIATION\protect\_ENCLOSURE} namelist}
\LSECH{rad:rad}
Namelist instances allowed: multiple, one per enclosure

{\small\begin{verbatim}
&RADIATION_ENCLOSURE
  enclosureID                 = 1
  partial                     = .false.
  method                      = 'file'
  infile                      = '2shell8-le.vf'
  chaparral_symmetry          = 'x', 'y', 'z'
  linearsolver                = 'RAD LS'
/
\end{verbatim}}

In this namelist we specify the method for computing the viewfactor-based
radiative transfer between the surfaces in one enclosure.  We label
our enclosure number 1, declare it to be not partial (that is complete
with no openings to a surrounding environment), and specify the method
\verb|'file'|, meaning that the viewfactors between pairs of cell faces
have been precomputed and reside in the file specified by \verb|'infile'|.
Note that the viewfactors for the analytic solution derived above were
for entire surfaces, whereas to compute the problem with \truchas\ we
must have face-by-face viewfactors that depend on the way the domain
is meshed.

For our problem and the specific mesh provided, viewfactors were
computed using the code Chaparral. Please see the \truchas\
\emph{Installation Guide} for information on how to obtain Chaparral.
The \verb|'chaparral_symmetry'| line in our input file specifies that
in order to create the complete 3D geometry of cell faces that Chaparral
requires, the set of cell faces in our one-octant mesh must be reflected
about the $x$, $y$, and $z$ axes. Without these reflections (copies) of
the cell faces, Chaparral would not see that a face on the concave surface
$S_2$ can see
the majority of the other faces on $S_2$ as well as nearly half of the
faces on $S_1$. Chaparral would not compute the correct viewfactors.
If our mesh included half of the concentric spheres, only one reflection
would be necessary.

In the input file (\APPH{rad}) one will find two alternative lines that
could be put in the \verb|RADIATION| \verb|_ENCLOSURE| namelist, \emph{viz.}

{\small\begin{verbatim}
  method                      = 'chaparral'
  outfile                     = '2shell8.vf'
\end{verbatim}}

Because these lines are not between any valid pair of namelist delimiters,
they are ignored by the input file parser, as are comments.
If they were included in place of the \verb|'method'| and
\verb|'infile'| lines---and only if the \truchas\ code had been compiled
including the Chaparral package---then Chaparral would be invoked to
compute viewfactors before the heat conduction calculation was performed,
and in addition the resulting viewfactors would be written out to a file
with the specified name.  On a rerun of the problem, say with different
material parameter settings, that outfile could be used as an infile
with \verb|method| = \verb|'file'|.  However, if the mesh file were
modified, say to refine the mesh, viewfactors would need to be recomputed.

\subsection{Linear solver}
\LSECH{rad:lsolver}

The enclosure radiation package requires a linear solver to compute the
radiative heat balance in the enclosure using viewfactors.  The solver
specified as \verb|'RAD LS'| seems to work well for this problem. As
always if convergence of a solver is not being attained, alternative
settings should be tried.

{\small\begin{verbatim}
&LINEAR_SOLVER
  name                        = 'RAD LS'
  method                      = 'cg'
  preconditioning_method      = 'none'
  stopping_criterion          = '||r||/||r0||'
  convergence_criterion       = 1.0e-3
/
\end{verbatim}}

\subsection{Selecting output formats}
\LSECH{rad:output}

In the \verb|OUTPUTS| namelist is a new parameter,
\verb|enclosure_diagnostics|.  If this parameter is \true, \truchas\ will
produce GMV files that can be examined to check the reasonableness of
the computed viewfactors.  If needed surfaces have been inadvertently
omitted from a mesh file side set used to specify a radiating surface
in an enclosure, often these diagnostic files will reveal hot spots or
cold spots in the viewfactor set, or other anomalies.

\subsection{Numerics}
\LSECH{rad:numerics}

{\small\begin{verbatim}
&NUMERICS
  dt_constant                 = 4.0e-3
  discrete_ops_type           = 'ortho'
  HT_discrete_ops_type        = 'SO'
  energy_nonlinear_solution   = 'HT NLS'
  enclosure_flux_convergence  = 4.0e-5
/
\end{verbatim}}

One new line here is
\verb|HT_discrete_ops_type|.  This setting overrides the global setting
of \verb|discrete_ops_type| \emph{only} for heat transfer, namely
the computation of the face temperature gradients that are used to
compute cell-centered temperatures from face temperatures.  The choice
here is \verb|'SO'| or Support Operators. Please refer to the \PA\ for
details on various types of computations of discrete operators.  The user
can remove this line to compare results with \verb|'ortho'| computations.

For problems employing enclosure radiation, we can specify in this
namelist a convergence criterion for the flux computation.

\subsection{Requesting data probes: the \texttt{PROBE} namelist}
Namelist instances allowed: multiple, one for each data probe

{\small\begin{verbatim}
&PROBE
  probe_name    = 'r0'
  probe_coords  = 0.0, 0.0, 0.5
/

&PROBE
  probe_name    = 'r1'
  probe_coords  = 0.0, 0.0, 1.0
/

&PROBE
  probe_name    = 'r2'
  probe_coords  = 0.0, 0.0, 2.0
/

&PROBE
  probe_name    = 'r3'
  probe_coords  = 0.0, 0.0, 4.0
/
\end{verbatim}}

Data probes are points within the meshed domain in which the user desires
ancillary data as a function of time.  In this case we want to write out
to files the time history data at points at the four radii $r_0$ through
$r_3$, so that, for example, the temperatures at those points can be
compared to the temperatures expected from the analytic solution. We give
each data probe a name, so it can be distinguished from others, and we
specify the $xyz$ coordinates of the location desired.

A file will be written for each data probe.  Any plotting utility can be
employed to plot time histories for the variables.

\section{Results}
For this problem one must ensure that the viewfactor input file is in the
path, as well as the \truchas\ executable and the mesh file.

Temperature distributions are shown in \FIGH{radresults}.  Heat spreads
outward from the innermost surface $S_0$, reaching surface $S_1$ quickly.
There is then a delay, because radiative heat transfer across the void
gap to $S_2$ is slower than conduction.  After $S_2$ heats up, heat is
conducted through to $S_3$ and the distribution relaxes toward a steady
state.

Of course with a time evolution such as this computed with \truchas,
a true steady state can never be reached.  The approach to a steady state
can be seen more readily in \FIGH{radprobe}.  Expected values of the
temperatures at the four probe locations are given at the beginning of
the input file, calculated using the equations.
The user should verify those analytic values,
not forgetting to substitute actual material property
values from the \verb|MATERIAL| namelists.

Note that the time histories of the data probes do not exactly approach
the expected values.  In part this is due to not running the simulation
longer, but another effect is at work that the user needs to understand.
In \truchas\ temperatures are computed only at cell centers, whereas in
the mesh the probe locations are at or near nodes.  \truchas\ computes
cell temperatures using an averaging scheme, so the difference between
expected values and probed values is partly due to this difference in
location.

\vspace{2cm}
\begin{sidewaysfigure}[p]
  \begin{center}
     \begin{tabular}{lll}
      \includegraphics[height=3cm,width=5cm]{figures/radgmv000000.png} &
      \includegraphics[height=3cm,width=5cm]{figures/radgmv000005.png} &
      \includegraphics[height=3cm,width=5cm]{figures/radgmv000010.png}
      \end{tabular}
  \end{center}
  \caption{Distributions of $T$ are shown for times t = 0.0, 2.0 and 4.0 time units.
           The viewpoint is in the $xy$ plane, 45 degrees from the axes.}
\LFIGH{radresults}
\end{sidewaysfigure}

\vspace{2cm}
\begin{figure}[h]
  \begin{center}
     \begin{tabular}{l}
      \includegraphics[width=10cm]{figures/radProbe.png} 
      \end{tabular}
  \end{center}
  \caption{Data probe time histories, showing the approach to a steady
  state.}
\LFIGH{radprobe}
\end{figure}
