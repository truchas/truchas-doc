\chapter{Binary Alloy Phase Change}
\LCHAPH{bapc}
\vspace{1in}
\begin{figure}[h]
   \begin{center}
    \begin{tabular}{ll}
       \includegraphics[width=5cm]{figures/hc_bapcgmv000000.png} &
       \hspace{2cm}
       \includegraphics[width=5cm]{figures/hc_bapcgmv000010.png}
    \end{tabular}
  \end{center}
\end{figure}

\vspace{0.5in}

This chapter presents the solidification of a binary alloy following
the Scheil model for solidification.  New input features discussed in this
chapter are parameters for setting up a binary alloy phase
transition and partial initialization of a domain using the BODY
namelist.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{bapc:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item   \hyperlink{sec:bapc:pcparams}{Binary Alloy Phase Change}
  \item   \hyperlink{sec:bapc:init}{Partial Domain Initialization Using
    BODY Namelist}
  \item \hyperlink{sec:bapc:output}{Output of Thermodynamic and Fluid
    Information}
\end{itemize}

\section{Problem Description}
\LSECH{bapc:problem:description} This chapter demonstrates the setup of
a Binary Alloy Phase Change using \truchas.  The simulation domain is
presented in \FIGH{bapc:domain.fig}.  The graph of enthalpy
vs. temperature is presented in \FIGH{bapc:enthalpy.fig}, and the phase
diagram is presented in \FIGH{bapc:pd.fig}.  Initially the temperature
in the entire domain is 1000.  At time zero we drop the temperature
along the left and right edges to 100.  The drop in temperature causes
the liquid to solidify and a solidification front moves in from the
left edge of the problem towards the right.  We solve for the
progression of the solidification front, and for the temperature
profile between the two edges.

\begin{tabular}{ll}
Units: & SI\\
Input file:& UMProblems/Chapter\_hc\_bapc/hc\_bapc.inp\\
Mesh file: & None\\
\end{tabular}

\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_domain.pdf}
  \end{center}
\caption{Simulation domain for the current chapter.  The Mold areas, filled
with materials Mold1, and Mold2 are created using a BODY namelist.}
\LFIGH{bapc:domain.fig}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_enthalpy.pdf}
  \end{center}
\caption{Enthalpy Vs. temperature for the problem presented in this
chapter.}
\LFIGH{bapc:enthalpy.fig}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_pd.pdf}
  \end{center}
\caption{Phase diagram for the problem presented in this chapter.}
\LFIGH{bapc:pd.fig}
\end{figure}

\section{Setup}
\LSECH{bapc:setup}

The complete input file for this problem is presented in \APPH{hc:bapc}.
In this chapter we discuss the inputs related to binary alloy phase
change and partial domain initialization in detail.  

\subsection{Specifying physics}
\LSECH{bapc:physics}

\begin{verbatim}
&PHYSICS
  fluid_flow       = .false.
  phase_change     = .true.
  heat_conduction  = .true.
/
\end{verbatim}

The new line here, \verb|phase_change = .true.|,
directs the code to look for a binary alloy phase change block
(\SECH{bapc:pcparams}) and read it in.  There is only \emph{one type}
of phase change that can be defined at any given time.  In the case of
alloy phase changes, there is the additional restriction that
\emph{only one binary alloy phase change is allowed}.  This
restriction will be relaxed in a future release of the code.

See \CHAPH{HC} \SECH{cond:physics} and the \verb|PHYSICS| chapter of
the \RM\ for details.

\subsection{Defining materials}
\LSECH{bapc:mat}
Material definition follows previous examples. Four materials are defined,
\verb|'Liquid'|, \verb|'Solid'|, \verb|'Mold1'|, and \verb|Mold2|, which is made the
background material.

\subsection{Initializing a domain with box surfaces}
\LSECH{bapc:init}

In this chapter we present a new body type, a 'box'. The second body
namelist below converts the block of domain centered at (0.25, 0.5, 0.5) and
having extents (0.5, 1.0, 1.0) into material 1 (the liquid) with
solute species concentration 10\%.  The temperature of the box is set to 1000.
Although this box is aligned with the cells, in general bodies need
not be aligned with the internal mesh.  The third namelist converts another
section of the domain to material 3. Note that the \verb|'box'| construction
can initialize either the inside or the outside of the box.
See \CHAPH{HC} \SECH{cond:init}
and the \verb|BODY| chapter of the \RM\ for details.

\begin{verbatim}
&BODY
  surface_name     = 'background'
  material_number  = 4
  temperature      = 1000
/

&BODY
  surface_name     = 'box'
  length           = 0.5 , 1 , 1
  fill             = 'inside'
  translation_pt   = 0.25 , 0.5 , 0.5
  material_number  = 1
  concentration    = 10
  temperature      = 1000
/

&BODY
  surface_name     = 'box'
  length           = 0.5 , 1 , 1
  fill             = 'inside'
  translation_pt   = 0.75 , 0.5 , 0.5
  material_number  = 3
  temperature      = 1000
/
\end{verbatim}

\subsection{Selecting output formats}
\LSECH{bapc:output}

For this problem we select an additional type of output, the 'long output.'
This allows the user to examine results cell by cell without postprocessing.
The new keyword \verb|'long_output_dt| \verb|_multiplier'| tells the code
how often to produce these detailed outputs, in units of \verb|'output|
\verb|_dt'|, and the line with \verb|'long_edit_bounding_coords'| sets pairs
of coordinates between which the long output will be given.

\begin{verbatim}
&OUTPUTS
  output_t                    = 0 , 5000
  output_dt                   = 500
  XML_output_dt_multiplier    = 1
  short_output_dt_multiplier  = 1
  long_output_dt_multiplier   = 1
  long_edit_bounding_coords   = 0, 1.5, 0, 1, 0, 1
/
\end{verbatim}
See \CHAPH{outputs} and the \verb|OUTPUTS| chapter of the \RM\  for details.

\subsection{Phase Change Parameters}
\LSECH{bapc:pcparams}

Here we describe the binary alloy phase changes whose parameters are as displayed
in \FIGH{bapc:enthalpy.fig} and \FIGH{bapc:pd.fig}.  The Scheil model is employed for 
the phase change transformation. The latent heat is
1000, and the slope of the liquidus is -10.  The partition coefficient
is 0.5.  Note that the problem is overspecified because the liquidus
temperature is specified in this namelist, as well as the
concentration in the \texttt{BODY} namelist.  If these two numbers are not
consistent with each other and the phase diagram, the code will not
run. 

\begin{verbatim}
&PHASE_CHANGE_PROPERTIES
  phase_change_active              = .true.
  phase_change_type                = 'alloy'
  phase_change_model               = 'Scheil'
  hi_temp_phase_material_ID        = 1
  lo_temp_phase_material_ID        = 2
  latent_heat                      = 1000.0
  liquidus_slope                   = -10.0
  liquidus_temp                    = 900.0
  melting_temperature_solvent      = 1000.0
  partition_coefficient_constants  = 0.50
  eutectic_temperature             = 0.0
/
\end{verbatim}

%Additionally, if
%\hyperlink{sec:bapc:physics}{binary_alloy_phase_diagram} is not
%activated in the \hyperlink{sec:bapc:physics}{\texttt{PHYSICS}}
%namelist, the problem will abort.  
See the \verb|PHASE_CHANGE_PROPERTIES| chapter of the \RM\   for an
explanation of these settings and a comprehensive listing of all parameters.

\section{Results}

Sample results from the program plotted using gmv are presented in
\FIGH{bapc:results}. 

\begin{figure}[h]
  \begin{center}
    \begin{center}
    \begin{tabular}{ll}
       \includegraphics[width=5cm]{figures/hc_bapcgmv000001.png} &
       \hspace{1cm}
       \includegraphics[width=5cm]{figures/hc_bapcgmv000005.png}
   \end{tabular}
   \end{center}
  \end{center}
    \caption{Sample results from the calculation at time=500 and 2500s
    showing progression of solidification of the alloy.  The colors
    show the volume fraction of liquid where blue is 0 and red is 1.
    The gray areas highlight sections of the domain where the volume
    fraction of the solid is greater than 0.25, in essence showing the
    solidification front.}
    \LFIGH{bapc:results}
\end{figure}
 
--
