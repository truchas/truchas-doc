\chapter{Heat Conduction}
\LCHAPH{HC}
 \begin{figure}[h]
   \begin{center} 
     \begin{tabular}{ll}	
     \includegraphics[width=5cm]{figures/hcgmv000001.png} &
     \hspace{2cm}
     \includegraphics[width=5cm]{figures/hcgmv000010.png}
     \end{tabular}
   \end{center}
 \end{figure}

\vspace{0.5in}
To demonstrate the heat transfer capabilities of \truchas, 
we set up a simple heat conduction problem 
on an internally generated, quasi-two-dimensional block orthogonal mesh.
The input file is discussed in its entirety, and sample results
are presented.

A variation of this problem that demonstrates initial temperature
gradients and time-varying temperature boundary conditions is also
presented in section \SECH{cond:gradients}.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{cond:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item	Heat transfer
  \item	Internal block mesh generation
  \item	Constant temperature boundary condition
  \item	Insulating boundary condition
  \item	Nonlinear solver
  \item	Linear solver
  \item	Constant time step
\end{itemize}

Also in \SECH{cond:gradients}, the following additional capabilities
are demonstrated.
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item	Initializing a temperature gradient
  \item	Time-varyign temperature boundary condition
\end{itemize}

\section{Problem Description}
\LSECH{cond:problem:description}
We begin with a unit square
domain.  The top and bottom of the square are insulated, and the
initial temperature of the domain is zero.  At time zero we raise the
temperature along the right edge to 100 degrees (the unit is K, which
is equivalent to a degree Celsius, and the scale need not be absolute
temperature), while maintaining the
temperature of the left edge at zero.  We then solve for the
transient evolution of temperature between the two edges, as it
approaches a steady state in which there is a linear variation
in temperature from low to high.

\begin{tabular}{ll}
Units: & SI\\
Input file:& UMProblems/Chapter\_hc/hc.inp \\
Mesh file: & None\\
\end{tabular}

\section{Setup}
\LSECH{cond:setup}

The input file for this problem is presented in \APPH{cond}.
Here we discuss each section of the input file in detail.

\subsection{Defining a mesh; the \texttt{MESH} namelist}
\LSECH{cond:mesh}
Namelist instances allowed: single

\begin{verbatim}
&MESH
  ncell   = 10, 10, 1
  coord   = 3*-0.5, 3*0.5
/
\end{verbatim}

The \verb|MESH| namelist defines the simulation domain.  As we shall
see in the next chapter, we will usually specify the name of a mesh
file that \truchas\ can read, created by some mesh generation software
such as CUBIT.  If no mesh file is available, as is the case
here, \truchas\ can generate a block mesh internally, using the information
presented in a namelist for the number of cells in each of three
directions and the coordinate limits.

In this case we define a unit cube extending between
$-0.5$ and $0.5$ in each of the three directions, with 10 cells in the
$x$ and $y$ directions and a single cell (the default) in the $z$ direction,
effectively specifying a two-dimensional domain.  Note that since the
input file is a Fortran namelist, all Fortran I/O
conventions are valid.  Consequently, $3*-0.5$ is interpreted as
$-0.5, -0.5, -0.5$. Moreover, all lines in the input file are case-insensitive,
so the user can employ the case that makes the content clearest.

See the \verb|MESH| chapter of the \emph{Reference Manual} for further
details on using the \verb|MESH| namelist.

\subsection{Specifying physics; the \texttt{PHYSICS} namelist}
\LSECH{cond:physics}
Namelist instances allowed: single

\begin{verbatim}
&PHYSICS
  fluid_flow       = .false.
  heat_conduction  = .true.
/
\end{verbatim}

The \verb|PHYSICS| namelist specifies the types of physics we are
activating for this problem.
By default \truchas\ runs fluid flow, but for a pure heat conduction
problem we want to deactivate it, hence we set \verb|fluid_flow| to
\false\  To activate heat conduction, we set \verb|heat_conduction|
to \true\  For a list of different physics that can be activated refer to
the \verb|PHYSICS| chapter of the \emph{Reference Manual}.

\subsection{Defining materials; \texttt{MATERIAL} namelists}
\LSECH{cond:mat}
Namelist instances allowed: multiple, one for each phase of each material in
the problem.

\begin{verbatim}
&MATERIAL
  material_number         = 1
  material_name           = 'solid_Cu'
  material_feature        = 'background'
  density                 = 8960.
  cp_constants            = 385.
  conductivity_constants  = 400.
/
\end{verbatim}

Materials namelists define the properties of materials present in
the problem.  These include the $density$, specific heat $C_p$, and $thermal$
$conductivity$.  For a complete list of keywords see the
\verb|MATERIAL| chapter of the \emph{Reference Manual}. 
Each material is assigned a unique ID
by the keyword \verb|material_number|.  It is by this number
that this material will be referred to in other namelists such as
\verb|BC| and \verb|BODY|.  Here the name \verb|solid_Cu|
is used to identify the material in all outputs.

\subsection{Initializing the domain; \texttt{BODY} namelists}
\LSECH{cond:init}
Namelist instances allowed: multiple, as many as required to initialize the domain

\begin{verbatim}
&BODY
  material_number   = 1
  surface_name      = 'background'
  temperature       = 0.
/
\end{verbatim}

The \verb|BODY| namelist is used to initialize regions of the
simulation domain.  In this example we set the material with
\verb|material_number| = 1 as the 'background' material and initialize it
with temperature $T=0$. 
\truchas\ requires that one and only one material in the list
be designated as the 'background' material by being given the
\verb|material_feature| 'background.'  That material will be
assigned to any regions of the domain where no other
material is specified---everywhere, in this case.

For more advanced uses of the \verb|BODY| namelist take a look at
\CHAPH{bapc}, \SECH{bapc:init} and \CHAPH{flow}, \SECH{flow:init}.

\subsection{Applying boundary conditions; \texttt{BC} namelists}
\LSECH{cond:bc}
Namelist instances allowed: multiple, one for each boundary condition
that we must specify.

\truchas\ analyzes the problem geometry and detects all `external'
faces of bodies, \emph{i.e.} those that do not have
a neighboring cell on the other side. \emph{All external faces in a mesh
must be contained in a surface that has a temperature boundary condition
defined in a \texttt{BC}
namelist.} If at the end of initialization there are external faces
without a defined temperature boundary condition, \truchas\ will exit
with a fatal error. Pressure, velocity (\emph{e.g.} applied inflow),
and displacement boundary conditions need not be defined on any external
surfaces.

For our current problem we have three \verb|BC| namelists, one for
each of the three boundary conditions: constant
$T=100$ along the left edge, constant $T=0$ along the right edge, and
insulated along all other external cell boundaries.

\subsubsection{\texttt{BC} namelist 1: Constant T=100 along the left edge,
x=-0.5}
\LSECH{cond:bc1}

\begin{verbatim}
&BC
  surface_name     = 'conic'
  conic_relation   = '='
  conic_x          = 1.
  conic_constant   = 0.5
  conic_tolerance  = 1.e-6
  bc_variable      = 'temperature'
  bc_type          = 'dirichlet'
  bc_value         = 100.
/
\end{verbatim}

The setting \verb|surface_name='conic'| means that the boundary
lies within \texttt{conic\_tolerance} of a surface described
by the conic equation:

\begin{eqnarray*}
 \lefteqn{{\cal F} = }& &\mathtt{conic\_constant}  \\
 & + & \mathtt{conic\_xx}*x^2 + \mathtt{conic\_x}*x \\
 & + & \mathtt{conic\_yy}*y^2 + \mathtt{conic\_y}*y \\
 & + & \mathtt{conic\_zz}*z^2 + \mathtt{conic\_z}*z \\
 & + & \mathtt{conic\_xy}*x*y + 
       \mathtt{conic\_xz}*x*z + 
       \mathtt{conic\_yz}*y*z
\end{eqnarray*}

where $x$, $y$, and $z$ are the three coordinates of an external cell face
center, and the rest of the terms
are explained in the \verb|BC| chapter of the \emph{Reference Manual}. 
Any face center that has $||{\cal F} || < $ 
\verb|conic_tolerance| is considered part of
the boundary. Any prefactors not specified in the namelist are assumed
to be equal to 0.  In this case we specify that along the surface
defined by $1*x = 0.5 \pm 10^{-6}$ we want to set the value of the
temperature to 100.

\subsubsection{\texttt{BC} namelist 2: Constant T=0 along the right edge, x=0.5}
\LSECH{cond:bc2}

\begin{verbatim}
\&BC
  surface_name     =  'conic'
  conic_relation   =  '='
  conic_x          =  1.
  conic_constant   =  -0.5
  conic_tolerance  =  1.e-6
  bc_variable      =  'temperature'
  bc_type          =  'dirichlet'
  bc_value         =  0.
/
\end{verbatim}

This is similar to the first namelist instance, except that we set the
temperature to zero along $x=0.5 \pm 10^{-6}$.

\subsubsection{\texttt{BC} namelist 3: Insulate all other external boundaries}
\LSECH{cond:bc3}

\begin{verbatim}
&BC
  surface_name       =  'external material boundary'
  surface_materials  =  1
  bc_variable        =  'temperature'
  bc_type            =  'hneumann'
/
\end{verbatim}

This namelist specifies that for any external surfaces
(\verb|'external material boundary'|) where the surface is made of material 1
(\verb|'solid_Cu'|), insulate it. \verb|'hneumann'| indicates a homogeneous
Neumann (`zero flux') condition.  

\subsection{Selecting output formats; the \texttt{OUTPUTS} namelist}
Namelist instances allowed: single
\LSECH{cond:output}

\begin{verbatim}
&OUTPUTS
  output_t                  =  0., 3.e3
  output_dt                 =  300.
  xml_output_dt_multiplier  =  1
/
\end{verbatim}

The \verb|OUTPUTS| namelist controls what files are generated during a run.
This namelist says that we want
outputs from $t=0$ till $t=3000$ at intervals of 300
seconds. \emph{The upper limit of the output determines how long the
code runs}.  

The \verb|xml_output_dt_multiplier = 1| setting
indicates that we
want XML output at every \verb|output_dt| (= 300s in this
case).  This XML output is then interrogated by a postprocessor parser
to generate graphical or restart formats.  Note that
the output time step is different from the actual time step used in
the calculation.

See \CHAPH{outputs} and the \verb|OUTPUTS| chapter of the
\emph{Reference Manual} for details.

\subsection{Numerics; the \texttt{NUMERICS} namelist}
\LSECH{cond:numerics}
Namelist instances allowed: single

The \verb|NUMERICS| namelist is where we define details of the numerical
solution methods for each physics type in the problem.
This has two parts: the numerical controls themselves and the
pertinent solvers.  For a detailed description see the \verb|NUMERICS|
chapter of the \emph{Reference Manual}.

\begin{verbatim}
&NUMERICS
  dt_constant                =  100.
  energy_nonlinear_solution  =  'nk-energy'
/
\end{verbatim}                                                    

Here we set the time step to a constant 100 seconds and indicate
that we want to use the nonlinear solver with the label 'nk-energy'
for heat transfer.  Note that we can have multiple solvers defined
in \truchas\ input files and pick and choose between them.  The only
constraint is that any solver referenced in the \verb|NUMERICS|
namelist must exist in the input file (linear solvers in 
\verb|LINEAR_SOLVER| namelists, and nonlinear solvers in
\verb|NONLINEAR_SOLVER namelists|) and must be unique.
 
\subsection{Nonlinear solvers; \texttt{NONLINEAR\_SOLVER} namelists}
\LSECH{cond:nlsolver}
Namelist instances allowed: multiple, one for each solver referenced in
the \verb|NUMERICS| namelist.  Available nonlinear solvers are
described in the \verb|NONLINEAR_SOLVER| chapter of the
\emph{Reference Manual}. 

\begin{verbatim}
&NONLINEAR_SOLVER
  name                    =  'nk-energy'
  method                  =  'nk'
  linear_solver_name      =  'energy-linear'
  convergence_criterion   =  1.e-8
  use_damper              =  .true.
  perturbation_parameter  =  1.e-6
  damper_parameters       =  0.5, 2., 1., 1.
/ 
\end{verbatim}

This nonlinear solver namelist is labeled 'nk-energy'.  It uses the
Newton-Krylov (nk) method for solving the heat transfer equations.
The linear solver to be used to initialize this method is indicated as the one
with the label 'energy-linear'.  For a detailed description of the
rest of the parameters take a look at the \verb|NUMERICS| chapter of
the \emph{Reference Manual}.

\subsection{Linear solvers; \texttt{LINEAR\_SOLVER} namelists}
\LSECH{cond:lsolver}
Namelist instances allowed: multiple, one for each of the linear solvers
referenced in the input file.

In this file only one linear solver is referenced, so
only one linear solver namelist is needed.

\begin{verbatim}
&LINEAR_SOLVER
  name                    =  'energy-linear'
  method                  =  'gmres'
  preconditioning_method  =  'ssor'
  preconditioning_steps   =  2
  relaxation_parameter    =  1.4
  convergence_criterion   =  1.e-5
/                                                    
\end{verbatim}

Here we specify that the nonlinear solver should use the GMRES method
with a two stage SSOR preconditioner.  The convergence criterion is
set to 1.e-5.

\section{Running the problem; results}
The next step is to run the problem.  This is done by issuing the following command:

\begin{verbatim}
% truchas hc.inp
\end{verbatim}

Ensure that \verb|truchas|, the executable program, is in your path, and that
\verb|hc.inp| lies in the current working directory.

\begin{figure}[h]
  \begin{center}
%    \includegraphics[width=5in]{figures/cond_res.png}
     \begin{tabular}{llll}
      &
      \vspace{-4.0cm} 
      \includegraphics[width=4cm]{figures/hcgmv000000.png} &
      \includegraphics[width=4cm]{figures/hcgmv000002.png} &
      \includegraphics[width=4cm]{figures/hcgmv000004.png} \\
      \includegraphics[height=9cm]{figures/hccbgmv.png} &
      \includegraphics[width=4cm]{figures/hcgmv000006.png} &
      \includegraphics[width=4cm]{figures/hcgmv000008.png} &
      \includegraphics[width=4cm]{figures/hcgmv000010.png} \\
      \end{tabular}
      \end{center}
      \caption{Time progression of the temperature profile within the
      Cu block at times 0, 600 and 1200 s (top row) and 1800, 2400,
      and 3000 s (bottom).  The temperature scale
      is shown on the left, and the view is down the $z$-axis.  As 
      expected, the temperature of the block starts rising from right
      to left until it reaches a steady state profile at around 1800 s.}
  \LFIG{cond:results}
\end{figure}

While the code runs, it periodically outputs lines to the screen
updating the user about the status of the calculation.  If execution of the
\texttt{truchas} code needs to be halted for any reason, it can be stopped
gracefully---only while performing time step cycling, not while initializing---
by first obtaining the process IDs (PIDs) of running processes:

\begin{verbatim}
ps -x
\end{verbatim}

and then sending a URG signal:

\begin{verbatim}
kill -s URG nnnnn
\end{verbatim}

where `nnnnn' is the lowest process ID of the \verb|truchas|
processes, of which there will be one for each CPU in a parallel job.
Ignore the \verb|mpi| job that spawned the \verb|truchas| processes;
the signal must go the the zeroeth \verb|truchas| process.

After \truchas\ has
finished, the XML output from the program is parsed (see \SECH{output:postprocessor} for a
tutorial on the parser) to provide the \verb|GMV| files that are presented in
\FIG{cond:results}.  The temperature scale for the problem is
on the left hand side, with blue representing 0 deg
and red representing 100 deg.

\section{Temperature Variation; Initial Gradients and Time-varying
Temperature Boundary Conditions}
\LSECH{cond:gradients}

In this section, the problem discussed above is modified to use two
additional \truchas\ options:
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item	Initializing a temperature gradient
  \item	Time-varying temperature boundary condition
\end{itemize}

The modified problem sets up conditions that are equivalent to the
final state of the original problem, and then varies it through a
time-varying boundary condition.

\subsection{Modified Problem Description}
\vspace{0.5cm}

On the same unit square domain, the temperature is intialized with a
linear gradient in the X direction from 100 degrees at X=-0.5 to 0
degrees at X=0.5. This is the steady state of the original problem.
The initial boundary conditions are the same as before, but at X=0.5,
the boundary temperature is raised to 100 degrees over 1000 seconds.
We solve for the transient temperature evolution as it approaches a
uniform steady state temperature of 100 degrees everywhere.

\subsection{Modified Setup}
Three namelists are changed to created this modified setup:
\verb|BODY|, one \verb|BC|, and \verb|OUTPUTS|.

\begin{tabular}{ll}
Input file:& UMProblems/Chapter\_hc/hctg.inp \\
\end{tabular}

\subsubsection{Adding an Initial Temperature Gradient}
The \verb|BODY| namelist is modified to add the gradient.  The base
temperature is changed to 100 degrees and gradient parameters are
inserted to establish a gradient in the X direction that lowers the
temperature to zero degrees at X = 0.5.
\begin{verbatim}
&BODY
  material_number   = 1
  surface_name      = 'background'
  temperature       = 100.
  TG_Origin         = -0.5, 0.0, 0.0
  TG_Axis           =  1.0, 0.0, 0.0
  TG_Z_Constants    =  -1.0, 0.0, 0.0
/
\end{verbatim}
The temperature gradients that can be specified in \truchas\ are
specified with respect to a gradient origin \verb|TG_Origin| and a
gradient direction (axis) \verb|TG_Axis|.  Variation can be specified
both in the axis direction, referred to a the gradient dZ direction,
and radially from that axis, referred to as the gradient dR direction.

The functional form for calculating the temparature at a cell center
is given by \[\begin{array}{lllll}
 T(dZ,dR) & = & \multicolumn{3}{l}{T_{Origin}} \\
          & * & (1 & + & \mathtt{TG\_Z\_Constants}(1) * dZ^{\mathtt{TG\_Z\_Exponents}(1)}  \\
          &   &    & + & \mathtt{TG\_Z\_Constants}(2) * dZ^{\mathtt{TG\_Z\_Exponents}(2)}  \\
          &   &    & + & \mathtt{TG\_Z\_Constants}(3) * dZ^{\mathtt{TG\_Z\_Exponents}(3)}  \\
          &   &    & + & \mathtt{TG\_R\_Constants}(1) * dR^{\mathtt{TG\_R\_Exponents}(1)}  \\
          &   &    & + & \mathtt{TG\_R\_Constants}(2) * dR^{\mathtt{TG\_R\_Exponents}(2)}  \\
          &   &    & + & \mathtt{TG\_R\_Constants}(3) * dR^{\mathtt{TG\_R\_Exponents}(3)} )\\
 \end{array}\] where the temperature at the gradient origin, $T_{Origin}$, is the
\verb|BODY| namelist temperature.  The other parameters are other, 
optional \verb|BODY| namelist entries.  

In calculating the values at a cell center, the value of dZ is
calculated as the cell center's offset in the gradient axis direction
from the gradient origin.  The value of dR is calculated as the
perpendicular offset from the gradient axis to the cell center.

In this problem, we specify the gradient origin at (X,Y,Z)=(-0.5,0,0)
and the gradient axis in the X direction (1,0,0).  Defaulting the dZ
exponents and setting \verb|TG_Z_Constants| to (-1,0,0) gives linear
decrease in X, dropping to zero degrees at X=0.5 (100\% decrease in 1
unit length). No variation perpendicular to the axis (dR) is used.

While not used here, upper and lower bounds are also supported in dZ
and dR to limit the domain on which the gradient is applied..  See the
\verb|BODY| chapter of the \emph{Reference Manual} for defaults and
meanings of all the temperature gradient \verb|BODY| namelist options.

\subsubsection{Time Variation of Temperature Boundary Conditions}
The second \verb|BC| namelist is modified to vary the temperature on
that boundary (X=0.5) from zero to 100 degrees over 1000 seconds.
\begin{verbatim}
&BC
    surface_name                = 'conic'
    conic_relation              = '='
    conic_x                     = 1.
    conic_constant              = -0.5
    conic_tolerance             = 1.e-6
    bc_variable                 = 'temperature'
    bc_type                     = 'dirichlet'
    bc_value                    = 0., 1000.0, 100.0
/
\end{verbatim}
The only change is in the addition of two values to the
\verb|BC_VALUE| entry.  The two additional values represent a (time,
value) pair, specifying a new boundary value at the stated time. In
this case, we are specifying that the temperature at X=0.5 will be 100
degrees at 1000 seconds into the simulation.  \truchas\ linearly
interpolates in time, with the result of a linear increase in the
boundary temperature from zero to 100 over the first 1000 seconds of
the simulation.

\subsubsection{Extending the Simulation Time Interval}
The final modification is to the \verb|OUTPUTS| namelist to extend the
overall time interval.  This is done to allow additional time for
equilibration.
\pagebreak
\begin{verbatim}
&OUTPUTS
    output_t                    = 0., 4500.
    output_dt                   = 300.
/
\end{verbatim}

\subsection{Modified Problem Results}
This problem starts with a linear temperature gradient in the X
direction. The conditions change uniformly over the first 1000 seconds
of the simulation. After that time, the temperature equilibrates to a
uniform 100 degrees. 

\begin{figure}[h]
  \begin{center}
%   \includegraphics[width=5in]{figures/cond_res.png}
    \begin{tabular}{llll} 
      & 
      \vspace{-4.0cm}
      \includegraphics[width=4cm]{figures/hctggmv000000.png} &
      \includegraphics[width=4cm]{figures/hctggmv000001.png} &
      \includegraphics[width=4cm]{figures/hctggmv000002.png} \\
      \includegraphics[height=8.3cm]{figures/hccbgmv.png} &
      \includegraphics[width=4cm]{figures/hctggmv000003.png} &
      \includegraphics[width=4cm]{figures/hctggmv000004.png} &
      \includegraphics[width=4cm]{figures/hctggmv000005.png} \\
    \end{tabular} 
   \end{center} 
   \caption{Time progression of the temperature profile within the Cu
            block at times 0, 300 and 900 s (top row) and 1500, 2400,
            and 4500 s (bottom).  The temperature scale is shown on
            the left, and the view is down the $z$-axis.  As expected,
            the temperature of the block equilibrates towards a
            uniform temperature. The temperature is within one degree
            everywhere by 4500 s.}
    \LFIG{cond:modresults}
\end{figure}
% 
% \begin{enumerate}
% \item Change the output so that the interval is every 500 seconds instead
%   of 300 seconds.
% \item Change the geometry so that there are 10 cells in the z-direction. 
% \item Change the boundary conditions so that the temperature profile
%   appears from top to bottom instead of left to right.
% \end{enumerate}
