\chapter{Introduction}
\LCHAPH{Introduction}

In this Users Manual we describe and demonstrate the
capabilities of \truchas.  We begin with a simple conductive
heat transfer problem and progress to problems involving phase change,
fluid flow, radiative heat transfer, chemical reactions, solid deformation,
and electromagnetics. Further chapters explain how to run
\truchas\ in parallel on a cluster, how to obtain the
different kinds of output that \truchas\ can generate, and
some of the limitations, sensitivities, and uncertainties
a user may confront in executing \truchas. 

\section{Structure}
Each chapter covering a sample problem is divided into the following sections:
\newcounter{Lcount}
\begin{list}{\arabic{Lcount}}
  {\usecounter{Lcount}}
  \item Capabilities: a listing of the different capabilities
  demonstrated by that chapter.
  \item Problem Definition: a brief description of the problem.
  \item Setup: Dissection of relevant sections of the input file.
  \item Results: Presentation of results calculated by \truchas.
  \item Input: A description of the input file needed to run the problem.
        For each problem we describe only the new or different features.
        Full copies of the input files are available in the Appendix.
\end{list}

\section{Description of Problems}
The list of chapters and the physics they describe are presented in
table \TBL{capab}:

\begin{sidewaystable}[p]
  \LTBL{capab}
  \begin{tabular}{|c|l|c|c|c|c|c|c|c|c|c|c|c|c|c|}
    \hline
    Chapter     & \texttt{Description}      & \texttt{HT} &
    \texttt{HS} & \texttt{IP} & \texttt{BA} & \texttt{CH} & \texttt{FF} & \texttt{BF} &
    \texttt{SD} & \texttt{DB} & \texttt{EM} & \texttt{RT} & \texttt{3D} & \texttt{PE} \\  \hline   

    % link name ht hs ipc bapc ff o/p 
    \ref{chap:HC}       & \hyperlink{chap:HC}{Heat Conduction}                 & 
    $\star$ &        &        &        &        &        &        &        &        &        &        &        &        \\ \hline 

    \ref{chap:hc:hs:ipc}& \hyperlink{chap:hc:hs:ipc}{Phase Change, Pure}       & 
    $\star$ &$\star$ &$\star$ &        &        &        &        &        &        &        &        &$\star$ &        \\ \hline

    \ref{chap:bapc}     & \hyperlink{chap:bapc}{Phase Change, Alloy}           & 
    $\star$ &        &        &$\star$ &        &        &        &        &        &        &        &        &        \\ \hline

    \ref{chap:CHEM}     & \hyperlink{chap:CHEM}{Chemistry}                     &       
    $\star$ &        &        &        &$\star$ &        &        &        &        &        &        &        &        \\ \hline

    \ref{chap:flow}     & \hyperlink{chap:flow}{Flow and Filling}              & 
            &        &        &        &        &$\star$ &        &        &        &        &        &        &        \\ \hline

    \ref{chap:mipc}     & \hyperlink{chap:mipc}{Flow + Multiple Phase Change } & 
    $\star$ &        &$\star$ &        &        &$\star$ &$\star$ &        &        &        &        &        &        \\ \hline

    \ref{chap:VP}     & \hyperlink{chap:VP}{Elastic and Viscoplastic Deformation} & 
    $\star$ &        &        &        &        &        &        &$\star$ &        &        &        &$\star$ &        \\ \hline

    \ref{chap:DISP}     & \hyperlink{chap:DISP}{Internal Displacements} & 
            &        &        &        &        &        &        &$\star$ &$\star$ &        &        &$\star$ &        \\ \hline

    \ref{chap:IH} & \hyperlink{chap:IH}{Induction Heating} & 
    $\star$ &$\star$ &        &        &        &        &        &        &        &$\star$ &        &$\star$ &        \\ \hline

    \ref{chap:RAD} & \hyperlink{chap:RAD}{Enclosure Radiation} & 
    $\star$ &        &        &        &        &        &        &        &        &        &$\star$ &$\star$ &        \\ \hline

    \ref{chap:parallel} & \hyperlink{chap:parallel}{Heat Conduction in Parallel} & 
    $\star$ &        &        &        &        &        &        &        &        &        &        &$\star$ &$\star$ \\ \hline

  \end{tabular}
  \caption{Listing of \truchas\ capabilities demonstrated in each
    chapter's sample problem. The abbreviations are: HT, heat transfer;
    HS, volumetric heat source; IP,
    isothermal phase change; BA, binary alloy phase change; CH, chemsitry;
    FF, fluid flow; BF, buoyancy-driven flow; SD, solid deformation; DB, displacement
    boundary; EM, electromagnetics; RT, radiative transfer; 3D,
    three-dimensional domain; and PE, parallel execution.}
\end{sidewaystable}

A \truchas\ input file may contain all of the following namelists. For
some applications a reduced set is appropriate; see the following sections.

\begin{itemize}
  \item Defining a mesh: the MESH namelist
  \item Specifying physics: the PHYSICS namelist
  \item Defining material properties: MATERIAL namelists
  \item Initializing the domain: BODY namelists
  \item Applying boundary conditions: BC namelists
  \item Selecting output formats: the OUTPUTS namelist
  \item Defining numerical methods: the NUMERICS namelist
  \item Non-linear solvers: NONLINEAR\_SOLVER namelists
  \item Linear solvers: LINEAR\_SOLVER namelists
  \item Defining chemical reactions: CHEMICAL\_REACTION namelists
  \item Defining phase changes: PHASE\_CHANGE\_PROPERTIES namelists
  \item Defining electromagnetics : the ELECTROMAGNETICS namelist
\end{itemize}


\section{Units}
A few physical constants in \truchas\ are given default values that depend
on choosing an SI system of units, which includes the \emph{kelvin} (K) as
the unit of temperature.
These are

\begin{itemize}
  \item{the gas constant $R$ used in modeling chemical reactions.}
  \item{the Stefan-Boltzmann constant used in applying radiation boundary
    conditions  (see the \texttt{BC} chapter of the \emph{Reference Manual})}
  \item{the electromagnetic constants $\epsilon_0$ and $\mu_0$; 
    the default values assume an SI system (see the \texttt{ELECTROMAGNETICS}
    chapter of the \emph{Reference Manual}).};
\end{itemize}

In most uses of temperatures, for example in heat conduction and phase change,
only relative temperatures are important. However in radiative heat transfer
temperatures must be specified on the absolute Kelvin scale.

The user may change to any unit system if all constants needed are redefined
in that system---saving the above constants, the code is unitless.
Regardless of the unit system chosen, all values of input data for
properties, both material and physical, must be consistent.  Failure to ensure
this can lead \truchas\ to quantitatively unphysical results that are
challenging to understand and correct.

\section{General Information}
In reading this manual, you will find it useful to have the \truchas\
\emph{Reference Manual} handy, since we only discuss different namelists with
respect to the problems at hand.  At best this covers a small
fraction of the possible choices.

Before setting up a new problem, we recommend that the user identify the
\truchas\ capabilities needed and use the input files from
relevant chapters in this book as a guide in generating one's own
input.  After studying the \emph{Reference Manual}, if one still has
trouble getting a problem to run, one can send e-mail to
\texttt{telluride-support@lanl.gov}.


\emph{On to the sample problems ...}


