%-------------------------------------------------------------------------------
\chapter{Parallel Execution}
\LCHAPH{parallel}
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=3in]{figures/decomposition.png}
  \end{center}
\end{figure}
%-------------------------------------------------------------------------------

Parallel execution of \truchas\ is based on domain decomposition.  The
problem domain is decomposed into several subdomains, and the
computations in each subdomain are done independently in parallel.
The results from each subdomain computation are communicated to other
subdomains as needed by communication software.  The picture above
shows a mesh decomposed into several parts, ready for a parallel
computation.

\truchas\ uses the Message Passing Interface (MPI) as the underlying
parallel communication interface.  There are many MPI implementations,
all different.  Learning about and interfacing with your particular
MPI implementation will be the difficult part of running \truchas\ in
parallel.

Any of the problems in the User Manual can be run in parallel.  For
this example, we choose a simple problem, the heat conduction problem
from \CHAPH{HC}.  This problem uses a structured mesh, so
it can be used to demonstrate methods used in \truchas\ for domain
decomposition on structured and unstructured meshes.  The problem is
also made more than 1 cell thick in the Z direction to demonstrate
3-dimensional computations.

There are three major steps in running \truchas\ in parallel:

\begin{enumerate}
\item build a parallel executable
\item modify the input file
\item invoke the parallel executable
\end{enumerate}

%-------------------------------------------------------------------------------
\section{Building a Parallel Executable}
%-------------------------------------------------------------------------------

A different executable has to be built for parallel execution.  In a
perfect world, the new executable can be built with two commands:

\begin{verbatim}
    % make cleaner
    % make all-parallel-debug
\end{verbatim}

\texttt{-opt} can be substituted for \texttt{-debug}.  It is essential that the correct
MPI header files (mpi.h) and libraries are found during the build.
MPI locations vary with the operating system, from machine to machine,
often according to the whim of the system administrator.  This point
is particularly important if you have multiple MPI implementations
installed on your computer.  \truchas\ attempts to guess where MPI is
installed, using vendor defaults if possible.  The locations of the
MPI header files and libraries are specified in \texttt{src/options/libraries}.
If you have any trouble building the executable, look there to make
sure you have the correct paths for your MPI implementation.

%-------------------------------------------------------------------------------
\section{Modifying the Input File}
%-------------------------------------------------------------------------------

The input file for this problem is presented in \APPH{parallel-input}.
In this chapter we will only discuss the aspects of this file critical
to running the conduction problem in parallel.

A common technique for ``commenting'' sections of an input file is
demonstrated here.  Note that there is a namelist named
\texttt{PARALLEL\protect\_PARAMETERS} and one named
\texttt{xPARALLEL\protect\_PARAMETERS}.  \truchas\ looks for a namelist called
\texttt{PARALLEL\protect\_PARAMETERS} and ignores the namelist with the ``x''
in the name.  The \texttt{xPARALLEL\protect\_PARAMETERS} is
effectively a comment.

In the simplest case, no modifications to the input file are necessary
at all!  Running a parallel executable on \texttt{n} processors will
result in the problem being decomposed into \texttt{n} subdomains,
using Chaco.  Chaco is a mesh decomposition/graph partitioning package
from Sandia National Laboratory.  Chaco is distributed with \truchas.

Sometimes more control over the parallel parameters is desired.  For
these occasions, we have the \texttt{PARALLEL\_PARAMETERS}
namelist.  A \texttt{PARALLEL\_PARAMETERS} namelist can be
added to any input file.  Serial computations will ignore this
namelist; parallel computations will use it.

The simplest \texttt{PARALLEL\_PARAMETERS} is shown below:

\namelistinput{
\&PARALLEL\_PARAMETERS\\
\> partitioner \>= 'chaco'\\
/
}

Using \texttt{chaco} as the partitioner choice means \truchas\ will
always be able to decompose the mesh into subdomains.  This is the
default if a
\texttt{PARALLEL\_PARAMETERS} namelist is not supplied, and is
enough to get started.  Other choices will be described later.

%-------------------------------------------------------------------------------
\section{Invoking the Parallel Executable}
%-------------------------------------------------------------------------------

There are many ways to run a parallel executable.  Different MPI
implementations use different commands.  Large parallel machines
often have a queuing system you have to cope with.  In this example,
we assume you are running on a single machine with multiple CPUs.
MPI on clusters of computers connected via a network usually require a
machines file or an application schema to describe the machines
available in the cluster and how to map the processors to the
machines.  If you want to use clusters of computers, you'll need to
look at the documentation for the MPI that you're using.

The next step is to run the problem.  The input file resides at
{\small\begin{verbatim}
UMProblems/Chapter_pll_hc/pll_hc.inp
\end{verbatim}}

Most MPI implementations come
with a launcher program that starts the multiple processes and sets up
the communication channels.  This launcher is often called mpirun.
The man page for mpirun on Irix describes the situation succinctly:

\begin{quote}
However, several MPI implementations available today use a job
     launcher called mpirun, and because this command is not part of the MPI
     standard, each implementation's mpirun command differs in both syntax and
     functionality.
\end{quote}

If you have more than one MPI implementation installed on your
computer, it is essential that you use the launcher program that goes
with the MPI implementation that you built \truchas\ with.

On many machines the following command is sufficient to launch the
parallel computation:

\begin{verbatim}
   % mpirun -np 2 truchas parallel.inp
\end{verbatim}

The option ``-np 2'' sets the number of processes to 2.  It generally
isn't a good idea to specify more processes than you have available
processors.  ``-np 1'' is useful if you want to use a parallel
executable to run a serial computation (instead of building a serial
binary).

In the next sections, we'll discuss starting a parallel computation on
some common parallel computing configurations.

%-------------------------------------------------------------------------------
\subsection{Linux/LAM}
%-------------------------------------------------------------------------------

Redhat includes LAM MPI as part of their Linux distribution.  This
makes using LAM MPI with Redhat Linux very easy.  LAM is the default
MPI choice for \truchas\ on Linux.

LAM first requires the user to start the LAM daemons (using lamboot), which LAM then
uses for process creation and control.  After a parallel computing
session, it is the user's responsibility to stop the LAM daemons
(using lamhalt).  A
typical LAM session would consist of commands like the following:

\begin{verbatim}
   % lamboot lamhosts
   % mpirun -np 2 truchas parallel.inp
   % mpirun -np 2 truchas parallel.inp
   % lamhalt
\end{verbatim}

Any number of mpirun commands can be executed while the LAM daemons
are active.  Attempting to use mpirun without having started the
daemons will result in an informative error message.

%-------------------------------------------------------------------------------
\subsection{Linux/MPICH}
%-------------------------------------------------------------------------------

Many sites that run Linux install MPICH MPI (from Argonne National
Laboratory) on their parallel computers.  MPICH is often considered
the reference MPI implementation, and can be installed on many
different systems.  The following command is all that's needed to run
parallel programs using MPICH:

\begin{verbatim}
   % mpirun -np 2 truchas parallel.inp
\end{verbatim}

mpirun will need to connect to the local machine (using rsh or ssh) to
start the parallel processes.  A mechanism for making the connections
without requiring a password is usually desirable.  Doing this in a
secure manner with rsh is difficult.  When using ssh, the ssh agent
and RSA authentication can be used.  See the MPICH documentation and
the man pages for ssh and ssh-agent for details

%-------------------------------------------------------------------------------
\subsection{Tru64 and Compaq's MPI}
%-------------------------------------------------------------------------------

Compaq supplies an MPI implementation for Tru64.  No special machine
configuration is necessary.  The launcher is named dmpirun.  The
following command is all that's needed to run parallel programs on
Tru64:

\begin{verbatim}
   % dmpirun -np 2 truchas parallel.inp
\end{verbatim}

%-------------------------------------------------------------------------------
\subsection{Irix and SGI's MPI}
%-------------------------------------------------------------------------------

From the user's perspective, running in parallel on SGIs running Irix
is easy.  They have an mpirun command and don't need any special
magic.  The following command is all that's needed to run parallel
programs using SGI's MPI:

\begin{verbatim}
   % mpirun -np 2 truchas parallel.inp
\end{verbatim}

For this to work the system administrator needs to install and
configure the array services daemon.

%-------------------------------------------------------------------------------
\subsection{AIX and IBM's MPI}
%-------------------------------------------------------------------------------

AIX has a different scheme for running parallel executables.  They use
the Parallel Operating Environment, or POE.  Computational nodes on an
AIX cluster are divided into ``pools.''  The pool configuration can be
found with:

\begin{verbatim}
   % js
\end{verbatim}

On the AIX machines that \truchas\ has access to, pool 0 is used.

To launch a parallel executable on an AIX cluster, a command like the
following is used:

\begin{verbatim}
   % poe truchas parallel.inp -rmpool 0 -nodes 1 -procs 2
\end{verbatim}

This will allocate 1 machine out of pool 0, and use 2 processors on
that machine.  To use 16 machines with 4 processors on each (64
processors total), use a command like this:

\begin{verbatim}
   % poe truchas parallel.inp -rmpool 0 -nodes 16 -procs 4
\end{verbatim}

%-------------------------------------------------------------------------------
\section{Modifying the Input File, continued}
%-------------------------------------------------------------------------------

\truchas-generated structured meshes (\emph{i.~e.}, not from a mesh file)
can be decomposed using a simple block-structured scheme.  Note,
Chaco will also work in this situation, but the result will not likely
be blocks with flat faces and smooth edges.  An
example is the easiest way to understand the block decomposition method.  Assume we are trying
to solve a problem on an orthogonal mesh, with 12 cells in the X
and Y directions, and 4 cells in the Z direction.  This would be specified in the
\texttt{MESH} namelist by:

\newpage

\namelistinput{
\&MESH\\
\> ... \> \\
\> ncell \>= 12, 12, 4 \\
\> ... \> \\
/
}

Suppose we have a 4 processor machine and we want to decompose this
problem into 4 simple block subdomains, with each block being 6x6x4
cells.  We can specify a \texttt{processor\_array} in the
\texttt{PARALLEL\_PARAMETERS} namelist:

\namelistinput{
\&PARALLEL\_PARAMETERS\\
\> partitioner \>= 'cartesian'\\
\> processor\_array \>= 2, 2, 1 \\
/
}

Note that the \texttt{partitioner} is now specified as
\texttt{'cartesian'}.  The result of this will be to cut the
orthogonal mesh into two pieces in the X direction and two pieces in
the Y direction, as in \FIGH{cart4}.

\FIGH{chaco4} shows the 4 subdomain decomposition of the same
mesh produced by Chaco.  Note that while the subdomains are of
approximately equal size (as in the cartesian decomposition), the
boundaries between subdomains are not as straight.

\newpage

\begin{figure}[th]
  \begin{center}
    \includegraphics[angle=90,width=2in]{figures/decomposition-cartesian.png}
    \caption{The orthogonal mesh is cut into into two pieces in the 
     X direction and two pieces in the Y direction when the 'cartesian'
     partitioner is chosen.}
  \end{center}
  \LFIGH{cart4} 
\end{figure}

\begin{figure}[bh]
  \begin{center}
    \includegraphics[width=2in]{figures/decomposition-chaco.png}
  \end{center}   
    \caption{The orthogonal mesh is cut into four different subdomains when
     the 'chaco' partitioner is chosen.}
  \LFIGH{chaco4} 
\end{figure}
