\chapter{Continuing a Run from a Restart Dump}
\LCHAPH{restart}

\section{Overview}
\LSECH{restart:overview}

Often a simulation cannot be completed to the desired problem time
in a single run/job.  In this case \truchas\ allows the user to restart
the simulation from the results of a completed run.  The mechanism
for this is a restart file, which can be created by the
postprocessor from one of the binary dump files written during code
execution.  Steps to restart a run are:

\begin{enumerate}
  \item Run \truchas\ in an initial job, producing the binary dump files
  required by the Python postprocessor as described in
  \SECH{output:overview}, item 4.  If \verb|XML_output_dt_multiplier|
  is nonzero, the code will produce binary output files at the end of each
  cycle in which problem time reaches the given multiple of \verb|output_dt|.
  In any case the default is to write a final binary dump, provided the code
  exits gracefully.  If the code does not exit
  gracefully, and if at least one dump file was written during
  execution, any one of them can be used to create a restart file.
  \item Run the Python postprocessor as described in \SECH{output:restart}
  to read the desired binary dump, create a restart file,
  and write it out.  Standard and mapped restarts are produced in
  different ways.
  \item Copy the previous input file and edit the copy
  as will be described below.
  \item Make sure that the new edited input file and the restart file
  are in the current directory (or refer to them appropriately).
  \item Start another \truchas\ job, naming the restart file after
  the \verb|-r:| command line option.
\end{enumerate}

\section{Restart Considerations}
\LSECH{restart:consid}

A restart file---whether created as a standard or mapped restart---
contains headers and then a sequence of variable fields that are read
in by \truchas\ during the initialization of a restarted run.
One might think that these fields would be sufficient for \truchas\ to
initialize.  In fact a sequence of things happens when \truchas\ 
starts up and finds a \verb|-r:| option on the command line.  The most
important thing is that a complete input file containing material
specifications and boundary conditions, physics switches, solver
definitions, etc. must be present to restart the code.  Even a mesh
file must be present, because \truchas\ reads the number of cells and
nodes from that file, not from the restart file.  It also reads all 
side sets from the mesh file and then knows their IDs, so the BCs
must refer to the same side sets as the initial run.

The reason that \truchas\ does not presume to carry over the physics
switches (which types of physics are activated and deactivated)
from the restart file is that we have found that we often wish to
change the switches upon restarting.  For example at the end of
induction preheating of a casting assembly, current to the coils
might be turned off before the crucible is emptied to fill the mold.
We would then restart the code with EM off and flow on.

Material fields, particularly the last known volume fractions of
materials in all cells, are read from the restart file.  However,
all material properties are read from the input file and reinitialized.
Here again one might want to change them to track some physical effect.
In the restart run's input file the materials namelists must be in the
original order, the same as in the initial run, because materials are
numbered internally in the order in which they appear in the input file.

Similarly boundary conditions for the restarted run must be set exactly
as if it were an initial run.  An example of why one must do
this is if flow had been initiated with an inflow BC as in the above
flow sample problem (\CHAPH{flow}).  At some time inflow might stop,
but one wishes to continue tracking flow in a mold or other container.
In the restart run, the BC for the inflow surface would be changed to
another type.

Sequences of calculations connected by restarts must use a consistent 
enthalpy-temperature relationship, because cell enthalpy is transferred
by the restart file and used to recompute and re-initialize temperatures.
This usually requires that phase-change models not be changed between
such calculations.  For certain problems this is not suitable.  We are
considering adding an option in the next release to allow making temperature
the primary field variable upon restart; enthalpies would then be recomputed
and re-initialized instead.

\section{Options for Restarts: the \texttt{RESTART} namelist}
\LSECH{restart:options}
Namelist instances allowed: single

{\small\begin{verbatim}
!! Example only -- not a paradigm!
&RESTART
  ignore_dt               = .true.
  ignore_Joule_heat       = .false.
  ignore_solid_mechanics  = .false.
  temperature_is_primary  = .false.
/
\end{verbatim}}

The first of these options is commonly used.  It allows the user to
choose for the first cycle of the restarted run a different time step
from the final time step of the previous run.  Particularly if
other parameters have been changed, it may be prudent to restart with
a smaller time step and let it grow back on subsequent cycles.

The last three options direct \truchas\ to ignore certain field
variables inherited from the previous run through the restart file.
For each of them the default value is \false.
\begin{itemize}
  \item If \verb|ignore_Joule_heat| is \true, and Electromagnetics (EM)
  is activated, this option will force a new EM solve to recompute
  the Joule heating rate distribution.  If it is defaulted or set to
  \false, an EM run (heat-up) can be continued with the inherited
  Joule heating rate distribution.
  \item If \verb|ignore_solid_mechanics| is \true, and solid mechanics
  physics is activated, the stresses and strains will be recomputed,
  ignoring those in the restart file.
  \item If \verb|temperature_is_primary| is \true, the initial value of the
  enthalpy field will be calculated from the restart file temperature values.
  The default action is to calculate the initial value of the temperature
  field from the restart file enthalpy values.  This option can be useful if
  some material properties or phase changes have been changed in the input
  file for the restart run.  It also allows a better approximation to reality
  when restarting on a newly mapped mesh (\SECH{output:restart}).
\end{itemize}

Another namelist whose parameters are commonly changed upon restart
is the \verb|OUTPUTS| namelist.  As phenomena change with the
evolution of the system, one might desire more or less frequent
outputs, or even start a different type of output.

