\chapter{Induction Heating}
\LCHAPH{IH}
 \begin{figure}[h]
   \begin{center}
     \includegraphics[height=3in]{figures/ih-title.png}
   \end{center}
 \end{figure}

In this chapter we present a simple induction heating problem to introduce
the electromagnetic (EM) modeling capabilities of \truchas.  Inputs
related to setting up an EM simulation are discussed in detail.

\clearpage

\section{\truchas\ Capabilities Demonstrated}
\LSECH{ih_capabilities}

\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item   \textbf{\hyperlink{sec:ih:physics}{Electromagnetics}}
\end{itemize}

\section{Problem Description}
\LSECH{ih:problem:description}

We consider the simple induction heating problem depicted in \FIG{ih:diag}.
A graphite sphere is positioned at the center of a 3-turn induction coil.
An imposed sinusoidal current in the coil produces an alternating magnetic
field that penetrates the graphite, inducing azimuthally-directed,
alternating electric currents within the graphite.  These currents generate
a spatially varying heat source through resistive heating (Joule heat).  The
heat is thermally conducted throughout the graphite and is radiated from the
surface of the sphere.

The problem is axially symmetric, and we exploit this symmetry by solving
only in the positive octant, applying appropriate boundary conditions on
the introduced symmetry planes.  The computational domain is the quarter
cylinder $\{(x,y,z)\,|\,x,y\ge0,\,0\le z\le0.045,\,\sqrt{x^2+y^2}\le0.045\}$,
which encloses the 6 cm diameter sphere and a bit of the surrounding free space
within the coil.  The coil itself lies outside the domain; its influence is
captured by specifying the tangential component of the magnetic field on the
exterior boundary.  We simulate the heating of the sphere for a short time,
starting from a uniform initial temperature of 300 K.

\begin{tabular}{ll}
Units: & SI\\
Input file: & UMProblems/Chapter\_ih/ih.inp \\
Mesh files: & UMProblems/Chapter\_ih/ih-hex.gen, \\
            & UMProblems/Chapter\_ih/ih-tet.gen
\end{tabular}

\begin{figure}[t]
  \begin{center}
    \includegraphics[height=2.5in]{figures/ih-prob-geom.png}
  \end{center}
  \caption{Geometry of the induction heating problem.  A graphite sphere
    is positioned at the center of a 3-turn induction coil.  EM is modeled
    on the pictured cylindrical region that encloses the sphere and some of
    the surrounding free space.  Symmetry allows computation to be confined
    to the highlighted octant.}%
    \LFIG{ih:diag}
\end{figure}

\section{Setup}
\LSECH{ih:setup}

The complete input file for this problem is presented in \APPH{ih}.  The
discussion here focuses on those sections of the input file related to
electromagnetics.


\subsection{Defining the meshes}
\LSECH{ih:mesh}

Alone among \truchas's physics packages, the EM solver requires a tetrahedral mesh.
Although it is possible to use such a mesh for all the physics in \truchas,
this is not recommended because many of the algorithms perform poorly with this
type of mesh.  Hence simulations that include electromagnetics are normally done
by defining a pair of meshes: (1) a secondary, or alternative, tetrahedral mesh
used for the EM calculations; and (2) the usual (hexahedral-cell) mesh used for
the rest of the physics.  Quantities are interpolated from one mesh to the other
as needed.
{\small\begin{verbatim}
&MESH
  Mesh_File        = 'ih-hex.gen'
  Mesh_File_Format = 'ExodusII'
/
&ALTMESH
  Altmesh_File     = 'ih-tet.gen'
/
\end{verbatim}}
The \texttt{ALTMESH} namelist describes the tetrahedral mesh used by the EM
solver.  Here the mesh is read from the Genesis/Exodus-format file
\texttt{ih-tet.gen}.  For this problem we used the external meshing package
CUBIT to generate both meshes, which are shown in \FIG{ih:meshes}.  Notice
that the free space region is only included in the tet mesh, and that the
two meshes do not exactly conform to one another along the curved surface of
the sphere, but this is not necessary: the interpolation procedure is able
to handle reasonably general pairs of meshes.  The hex-to-tet interpolation
(used to map material parameters) is constant-preserving, and the tet-to-hex
interpolation (used to map the computed Joule heat) is conservative.

There are a few requirements that a hex-tet mesh pair needs to satisfy.
Both mesh files must be Genesis/Exodus format, and each element block (also
referred to as a mesh material) in the tet mesh that describes a conductive
region must be `contained' within the region described by the hex-mesh
element block of the same ID, allowing that some tets may extend slightly
outside the region.
See Appendix~I in \PA\ and the \texttt{ALTMESH} chapter of \RM\ for
further details.

\begin{figure}[t]
  \begin{center}
    \begin{tabular}{ll}
      \includegraphics[height=2.5in]{figures/ih-tet-mesh.png} &
      \includegraphics[height=2.5in]{figures/ih-hex-mesh.png}
    \end{tabular}
  \end{center}
  \caption{Tetrahedral mesh used by the EM solver (left), and the hexahedral
    mesh used for heat conduction/radiation (right), which omits the free
    space region.}%
  \LFIG{ih:meshes}
\end{figure}

\subsection{Specifying physics}
\LSECH{ih:physics}

{\small\begin{verbatim}
&PHYSICS
  fluid_flow       = .false.
  heat_conduction  = .true.
  electromagnetics = .true.
/
\end{verbatim}}
Enabling \texttt{electromagnetics} instructs \truchas\ to perform an auxiliary
EM simulation to calculate the Joule heating distribution for use as a
volumetric heat source in heat conduction.  It is assumed that the time scale
of the alternating EM fields is much shorter than the time scale of the rest
of the physics.  In this case it suffices to solve Maxwell's equations to a
periodic steady state while temporarily freezing the other physics, and then
average the rapid temporal variation in the Joule heating distribution over
a cycle.  In effect, Maxwell's equations are solved over an \emph{inner time}
distinct from that of the rest of the physics.  In this problem the Joule
heating distribution is constant in time, so it is computed just once at the
outset of the simulation.   In general, however, it is recomputed whenever
indicated by changes in the EM material parameters (due to their temperature
dependence) or by changes in the magnetic source field.

\subsection{Defining materials}
\LSECH{ih:mat}

{\small\begin{verbatim}
&MATERIAL 
  material_number           = 1            
  material_name             = 'graphite'   
  material_feature          = 'background' 
  density                   = 1750.0       
  Cp_constants              = 895.0        
  conductivity_constants    = 7.80         
  EM_conductivity_constants = 5.6e4        
  EM_permittivity_constants = 1.0          
  EM_permeability_constants = 1.0          
/
\end{verbatim}}
Here we have specified the values for electric conductivity, relative
electric permittivity, and relative magnetic permeability for graphite.
These are the only material parameters used by the EM solver.  The default
values are 0 for conductivity and 1 for relative permittivity and permeability,
so in this case we could have omitted the latter two from the namelist.



\subsection{Applying boundary conditions}
\LSECH{ih:bc}

For this example, the cell faces on the boundary of the domain
were grouped into two side sets, labeled~1 and~2 within the mesh file.
Faces on the symmetry planes were placed in side set~1, and we specify a
homogeneous Neumann condition for temperature to that part of the boundary.
The remaining boundary faces---those on the surface of the sphere---were
placed in side set~2, and we specify a thermal radiation boundary condition
there, with a surface emissivity of~0.9 and an ambient temperature of~300K.

{\small\begin{verbatim}
&BC 
  surface_name = 'from mesh file'  
  mesh_surface = 1                 
  BC_variable  = 'temperature'     
  BC_type      = 'HNeumann'        
/ 
&BC 
  surface_name = 'from mesh file'  
  mesh_surface = 2                 
  BC_variable  = 'temperature'     
  BC_type      = 'radiation'       
  BC_value     = 0.9, 300.0        
/
\end{verbatim}}

Notice that we have not specified boundary conditions for the EM fields.
These are handled differently; see \SECH{ih:em}.


\subsection{Numerics}
\LSECH{ih:numerics}

Here we specify how to perform the heat conduction solve. Parameters for
the electromagnetic solve are set in the following namelist.

\tRMSect{NUMERICS}{NUMERICS}.
{\small\begin{verbatim}
&NUMERICS 
  dt_constant               = 0.5 
  energy_nonlinear_solution = 'HT NLS' 
  HT_discrete_ops_type      = 'SO' 
/
\end{verbatim}}
 
\subsection{Electromagnetic Solver}
\LSECH{ih:em}
Instances allowed: single \texttt{ELECTROMAGNETICS}, and multiple
\texttt{INDUCTION\_COIL}, one for each coil (if any).
{\small\begin{verbatim}
&ELECTROMAGNETICS 
  EM_Domain_Type        = 'quarter_cylinder' 
  Source_Frequency      = 2000.0 
  Steps_Per_Cycle       = 20 
  Maximum_Source_Cycles = 5 
  SS_Stopping_Tolerance = 0.01 
  Maximum_CG_Iterations = 100 
  CG_Stopping_Tolerance = 1.0e-8 
  Num_Etasq             = 1.0e-6 
/
&INDUCTION_COIL 
  Center  = 3*0.0 
  Radius  = 0.05 
  Length  = 0.04 
  NTurns  = 3 
  Current = 5000.0 
/
\end{verbatim}}
Here we specify all the variables that control the Joule heat calculation.
Several aspects of this specification differ from other types of physics.

First, the boundary conditions for EM are defined internal to the EM package.
In this release there is no
facility for specifying general boundary conditions for EM simulations.
Consequently the computational domain is restricted to a few special cases.
\texttt{quarter\_cylinder} declares that the domain discretized by the
alternative mesh (see \SECH{ih:mesh}) is bounded by the form
$\{(x,y,z)\,|\,x,y\ge0,\,z_1\le z\le z_2,\,x^2+y^2\le r^2\}$.
This ensures that the proper symmetry conditions are applied on the
boundaries $x,y=0$, and that a Dirichlet source field condition is applied
to the tangential component of the magnetic field on the remaining boundaries.
For this problem, the source field used to set boundary values is that
due to a coil whose physical characteristics are specified in the 
\texttt{INDUCTION\_COIL} namelist.  The coil carries a sinusoidally-varying
current with frequency 2000 Hz and peak amplitude of 5000 A.
See the \texttt{ELECTROMAGNETICS} and \texttt{INDUCTION\_COIL} chapters of
the \RM\ for the other possible types of domains and source fields.

Second, parameters for the EM solver are defined in the \texttt{ELECTROMAGNETICS}
namelist.  Recall that the Joule
heat is computed by evolving Maxwell's equations to a periodic steady state
and then averaging the Joule heat over a cycle.  Here we use 20 time steps
per cycle of the source field.  We continue to evolve until the relative
difference in the averaged Joule heat in successive cycles is less than $0.01$,
or we have completed the maximum number of cycles specified by
\texttt{maximum\_source\_cycles}.  The EM solver uses its own special
preconditioned conjugate gradient (CG) solver.  Here we limit the number
of CG iterations in a linear solve to $100$ and stop iterating as soon as
the initial residual has been reduced by $10^{-8}$.  Finally, in this physical
regime the coefficient $\eta^2$ of the normalized displacement current term is
exceedingly small, and we have chosen to replace it with the larger artificial
value specified by \texttt{num\_etasq} in order to improve the numerical
robustness of the problem without noticably effecting the results.
See the \texttt{ELECTROMAGNETICS} chapter of the \RM\ for a comprehensive
list of all possible EM solver variables.

\section{Results}
Sample results from the problem are shown in \FIGA{ih:plot1}{ih:plot2}.

\begin{figure}[p]
  \begin{center}
    \includegraphics[width=3in]{figures/ih-joule.png}
  \end{center}
  \caption{Joule heating distribution in the graphite.  Data shown on the
    hex mesh after interpolation from the tetrahedral EM mesh where it was
    computed.}%
    \LFIG{ih:plot1}
\end{figure}

\begin{figure}[p]
  \begin{center}
    \begin{tabular}{llll}
      \includegraphics[width=1.5in]{figures/ih-temp-1.png} &
      \includegraphics[width=1.5in]{figures/ih-temp-2.png} &
      \includegraphics[width=1.5in]{figures/ih-temp-3.png} &
      \includegraphics[height=2in]{figures/ih-temp-cbar.png}
    \end{tabular}
  \end{center}
  \caption{Time sequence of the temperature profile showing the heating
    of the graphite starting from a uniform 300K at $t=0$.}%
  \LFIG{ih:plot2}
\end{figure}

\FIG{ih:plot1} shows the result of the EM solve. We see that
Joule heat is concentrated near the equator of the sphere, because the strongest
induced current loops are there, and the fields produced by those current loops
are in the opposite sense to the induction coil's imposed field, tending to
cancel that field in the inner parts of the sphere. Since there are then small
or no currents in the inner parts, there is no induced Joule heating there.

30 seconds of heat-up for the sphere under the oscillating imposed coil field
is shown in \FIG{ih:plot2}. Joule heat appears as a volumetric source near
the equator and conducts throughout the graphite volume.


