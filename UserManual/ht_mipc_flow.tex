\chapter{Multiple Phase Changes + Buoyancy Driven Flow}
\LCHAPH{mipc}

\vspace{0.5in}
\begin{figure}[h]
    \includegraphics[width=5in]{figures/mipc_leadin.pdf}
\end{figure}

\vspace{1.5in}
This chapter puts it all together with a problem that combines heat
transfer, multiple isothermal phase changes and buoyancy driven flow.
The new features introduced in this chapter are definitions of
multiple phase changes and temperature dependent properties to induce
buoyancy driven flow.

\newpage
\section{\truchas Capabilities Demonstrated}
\LSECH{mipc:capabilities}
\begin{itemize}
\setlength{\itemsep}{-6pt}
\item   Heat Transfer
\item   Flow
\item   \textbf{\hyperlink{sec:mipc:mat}{Temperature Dependent Density}}
\item   \textbf{\hyperlink{sec:mipc:pcparams}{Multiple Isothermal Phase Changes}}
\item   Constant Temperature Boundary Condition
\item   Insulating Boundary Condition
\item   Non-Linear Solver
\item   Linear Solver
\item   Variable Time Step
\end{itemize}

\section{Problem Description}
\LSECH{mipc:problem:description} 
This chapter demonstrates multiple isothermal phase changes along with
buoyancy driven flow. The graph of Enthalpy Vs. Temperature is
presented in \FIGH{mipc:enthalpy.fig}.  Initially the temperature in
the entire domain is 1010, and it is liquid.  At time zero we drop the
temperature along the right edge of the domain to 700 while at the
same time raising the temperature along the left edge to 1200.  At
steady state, the left side of the domain will be liquid, solid 1
will be in the middle, and the right side of the domain will be solid
2.  Due to the buoyancy driven flow, the solidification front will not
be straight, but will have a lip at the bottom.  

\begin{tabular}{ll}
Units:& MKS\\
Input file:& UMProblems/Chapter6/hc\_mipc\_flow.inp \\
Mesh file: & None\\
\end{tabular}
\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/mipc_enthalpy.pdf}
  \end{center}
\LFIGH{mipc:enthalpy.fig}
\caption{
Enthalpy Vs. Temperature for the problem presented in this
chapter.
}
\end{figure}

\section{Setup}
\LSECH{mipc:setup}

The input file for this problem is presented in \APP{mipc:flow}.
In this chapter we discuss the variables related to setting up
multiple phase changes and temperature dependent density in detail.

\subsection{Defining a mesh}
\LSECH{mipc:mesh}
Instances allowed: single
\namelistinput{
\&MESH  \\
 \> Ncell                          \> = 12, 8, 1 \\
 \> Coord                          \> = 0 , 0 , 0, 1.5 , 1 , 1  \\
/
}
See \CHAPH{HC} \SECH{cond:mesh} and \tRMSect{MESH}{MESH} for details.

\subsection{Specifying physics}
\LSECH{mipc:physics}
Instances allowed: single
\namelistinput{
\&PHYSICS \\
\>    fluid\_flow                        \> .true. \\
\>    pure\_material\_phase\_change      \> = .true.  \\
\>    heat\_conduction                   \> = .true.  \\
\>    body\_force                        \> = 0.0, -9.81, 0.0 \\
\>    inviscid                           \> = .true. \\
\>    isImmobile(2)                      \> = .true. \\
\>    isImmobile(3)                      \> = .true. \\
/
}
By default, the Boussinesq approximation is activated.
See \CHAPH{hc:hs:ipc} \SECH{hc:hs:ipc:physics} and
\CHAPH{flow} \SECH{flow:physics} for details on the rest of the
variables.  See the \tRMSect{PHYSICS}{PHYSICS} for further details on
these variables and a comprehensive listing of all possible variable
allowed in this namelist.  Although this should really be a viscous
problem to get the buoyancy driven flow right, we run it here as an
inviscid problem for demonstration purposes.

\subsection{Defining materials}
\LSECH{mipc:mat}
Instances allowed: multiple, one for each phase of each material in
the problem.
\namelistinput{
\&MATERIAL  \\
\>    Material\_Name                 \> ='liquid'  \\
\>    Material\_Number               \> = 1  \\
\>    Material\_Feature              \> = 'background'  \\
\>    Priority                      \> = 1  \\
\>    Density                       \> = 5000  \\
\> \textbf{density\_change\_relation}       \> \textbf{= 'temperature\_polynomial'} \\
\> \textbf{density\_change\_coefficients\_T} \> \textbf{= -1.e-5} \\
\> \textbf{density\_change\_exponents\_T}    \> \textbf{= 1.} \\
\> \textbf{reference\_temperature}         \> \textbf{= 1000.} \\
\>    Conductivity\_Constants        \> = 250  \\
\>    Cp\_Constants                  \> = 10  \\
/ \\
 \\
\&MATERIAL  \\
\>    Material\_Name                 \> = 'solid1'  \\
\>    Material\_Number               \> = 2  \\
\>    Priority                      \> = 2  \\
\>    Density                       \> = 5000  \\
\>    Conductivity\_Constants        \> = 150  \\
\>    Cp\_Constants                  \> = 10 \\
/ \\
 \\
\&MATERIAL  \\
\>    Material\_Name                 \> = 'solid2'  \\
\>    Material\_Number               \> = 3  \\
\>    Conductivity\_Constants        \> = 1000  \\
\>    Density                       \> = 5000  \\
\>    Cp\_Constants                  \> = 10 \\
\>    Priority                      \> = 3  \\
/
}
The new feature introduced here is the temperature dependent density
for material 1, the liquid.  This temperature dependence is given as a
polynomial with the following form:
\[\rho = \rho_0*(1 + \alpha*(T - T_{ref})^1)\]
Which, using the parameters for the liquid, gives us:
\[\rho = 5000*(1+  - 10^{-5}*(T - 1000))\]
See \CHAPH{HC} \SECH{cond:mat} and \tRMSect{MATERIAL}{MATERIAL} for
further details on these variables.

\subsection{Initializing the domain}
\LSECH{mipc:init}
Instances allowed: multiple, as many as required to initialize the domain
\namelistinput{
\&BODY  \\
 \> Surface\_Name                   \> = 'background'  \\
 \> Fill                           \> = 'nodefault'  \\
 \> Material\_Number                \> = 1 \\
 \> Temperature                    \> = 1010  \\
/
}
See \CHAPH{HC} \SECH{cond:init} and the \tRMSect{BODY}{BODY} for details.

\subsection{Applying boundary conditions}
\LSECH{mipc:bc}
Instances allowed: multiple, one for each boundary condition we wish to specify.
\namelistinput{
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_X                        \> = 1  \\
 \> Conic\_Constant                 \> = 0  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                  \> = '='  \\
 \> BC\_Type                        \> = 'dirichlet'  \\
 \> BC\_Variable                    \> = 'temperature'  \\
 \> BC\_Value                       \> = 1200  \\
/  \\
 \\
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_X                        \> = 1  \\
 \> Conic\_Constant                 \> = -1.5  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                 \> = '='  \\
 \> BC\_Type                        \> = 'dirichlet'  \\
 \> BC\_Variable                    \> = 'temperature'  \\
 \> BC\_Value                       \> = 700  \\
/  \\
 \\
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_Y                        \> = 1  \\
 \> Conic\_Constant                 \> = 0  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                 \> = '='  \\
 \> BC\_Type \> = 'hneumann'  \\
 \> BC\_Variable \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_Y                        \> = 1  \\
 \> Conic\_Constant                 \> = -1  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                 \> = '='  \\
 \> BC\_Type                        \> = 'hneumann'  \\
 \> BC\_Variable                    \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_Z                        \> = 1  \\
 \> Conic\_Constant                 \> = 0  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                 \> = '='  \\
 \> BC\_Type                        \> = 'hneumann'  \\
 \> BC\_Variable                    \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
 \> Surface\_Name                   \> =  'conic'  \\
 \> Conic\_Z                        \> = 1  \\
 \> Conic\_Constant                 \> = -1  \\
 \> Conic\_Tolerance                \> = 1e-06  \\
 \> Conic\_Relation                 \> = '='  \\
 \> BC\_Type                        \> = 'hneumann'  \\
 \> BC\_Variable                    \> = 'temperature'  \\
/
}
See \CHAPH{HC} \SECH{cond:bc} and \tRMSect{BC}{BC} for details.

\subsection{Selecting output formats}
\LSECH{mipc:output}
Instances allowed: single
\namelistinput{
\&OUTPUTS  \\
 \> Output\_T                       \> = 0, 400 \\
 \> Output\_Dt                      \> = 25 \\
 \> xml\_output\_dt\_multiplier       \> = 1 \\
 \> Short\_Output\_Dt\_Multiplier     \> = 1 \\
 \> Long\_Output\_Dt\_Multiplier      \> = 1 \\
 \> Long\_Edit\_Bounding\_Coords      \> = 0, 1.5, 0, 1, 0, 1  \\
/
}
See \CHAPH{outputs} and the \tRMSect{OUTPUTS}{OUTPUTS} for
a comprehensive listing of all possible variables. 

\subsection{Numerics}
instances allowed: single
\namelistinput{
\&NUMERICS  \\
 \> dt\_init                        \> = 1.e-3 \\
 \> dt\_grow                        \> = 1.05 \\
 \> dt\_min                         \> = 1.e-7 \\
 \> dt\_max                         \> = 2.e0 \\
 \> discrete\_ops\_type              \> = 'ortho' \\
 \> energy\_nonlinear\_solution      \> = 'nk-energy'  \\
 \> projection\_linear\_solution     \> = 'projection-solver' \\
 \> courant\_number                 \> = 0.4 \\
 \> volume\_track\_interfaces        \> = .true. \\
 \> volume\_track\_brents\_method     \> = .true. \\
 \> volume\_track\_iter\_tol          \> = 1.0e-12 \\
 \> cutvof                         \> = 1.0e-08 \\
/
}
See \CHAPH{flow} \SECH{flow:numerics} and the \tRMSect{NUMERICS}{NUMERICS} for a
comprehensive listing of all possible variables. 

\subsection{Non-Linear Solvers}
Instances allowed: multiple, one for each of the solvers referenced in
the \texttt{NUMERICS} namelist.
\namelistinput{
\&NONLINEAR\_SOLVER  \\
 \> name                           \> = 'nk-energy'  \\
 \> method                         \> = 'nk'  \\
 \> linear\_solver\_name             \> = 'nk-linear'  \\
 \> convergence\_criterion          \> = 1.0e-03  \\
 \> maximum\_iterations             \> = 300  \\
 \> output\_mode                    \> = 'none'  \\
 \> use\_damper                     \> = .true.  \\
 \> Damper\_Parameters              \> =   0.8, 1.5, 1.0, 1.0  \\
 \> perturbation\_parameter         \> = 1.0e-06  \\
/
}
See \CHAPH{HC} \SECH{cond:nlsolver}  and the
\tRMSect{NONLINEAR\_SOLVER}{NONLINEAR\_SOLVER} for a comprehensive
listing of all possible variables.   

\subsection{Linear Solvers}
Instances allowed: multiple, one for each of the linear solvers
referenced in the input file.
\namelistinput{
\&LINEAR\_SOLVER  \\
 \> name                           \> = 'nk-linear'  \\
 \> method                         \> = 'fgmres'  \\
 \> convergence\_criterion          \> = 1.0e-6  \\
 \> preconditioning\_method         \> = 'ssor'  \\
 \> relaxation\_parameter           \> = 0.9  \\
 \> preconditioning\_steps          \> = 3  \\
 \> maximum\_iterations             \> = 300  \\
/ \\
\\
\&LINEAR\_SOLVER\\
 \> name                           \> = 'projection-solver'\\
 \> method                         \> = 'gmres'\\
 \> preconditioning\_method         \> = 'ssor'\\
 \> preconditioning\_steps          \> = 2\\
 \> relaxation\_parameter           \> = 1.4\\
 \> convergence\_criterion          \> = 1.e-7\\
 \> maximum\_iterations             \> = 2500 \\
/
}
See \CHAPH{HC} \SECH{cond:lsolver} and the
\tRMSect{LINEAR\_SOLVER}{LINEAR\_SOLVER} for a comprehensive listing
of all possible variables.

\subsection{Phase Change Parameters}
\LSECH{mipc:pcparams}
Instances allowed: single for binary phase changes, and multiple for
isothermal phase changes
\namelistinput{
\&PHASE\_CHANGE\_PROPERTIES  \\
 \> Phase\_Change\_Type              \> = 'solid\_liquid\_isothermal'  \\
 \> Hi\_Temp\_Phase\_Material\_Id      \> = 1  \\
 \> Lo\_Temp\_Phase\_Material\_Id      \> = 2  \\
 \> Latent\_Heat                    \> = 1000.0 \\
 \> Melting\_temperature            \> = 1000.0 \\
/ \\
\&PHASE\_CHANGE\_PROPERTIES  \\
 \> Phase\_Change\_Type              \> = 'solid\_liquid\_isothermal'  \\
 \> Hi\_Temp\_Phase\_Material\_Id      \> = 2  \\
 \> Lo\_Temp\_Phase\_Material\_Id      \> = 3  \\
 \> Latent\_Heat                    \> = 1000.0  \\
 \> Melting\_temperature            \> = 850.0 \\
/
}

The only difference between this and the input in \CHAPH{hc:hs:ipc},
\SECH{hc:hs:ipc:pcparams} is that we have two phase changes defined.
The fields within the namelists are identical in both cases.  The
\FIGH{mipc:enthalpy.fig} shows the graph of Enthalpy
Vs. Temperature for these two phase changes.  See \CHAPH{hc:hs:ipc},
\SECH{hc:hs:ipc:pcparams} and the \tRMSect{PHASE\_CHANGE\_PROPERTIES}
{PHASE\_CHANGE\_PROPERTIES} for a comprehensive listing of all
possible variables.

\section{Results}
The next step is to run the problem.  This is done by issuing the following command:

\begin{verbatim}
% truchas ht_mipc.inp
\end{verbatim}

Ensure that \texttt{truchas}, the executable program, is in your path, and that
\texttt{ht\_mipc.inp} lies in the current working directory.

\begin{figure}[h]
  \begin{center}
    \includegraphics[height=5in]{figures/mipc_res.pdf}
    \caption{Sample results from the calculation at time=25 and 400s
    showing progression of solidification.  The colors
    show the temperature, the arrows indicate the flow direction in
    cells that contain liquid, and the gray areas highlight 
    sections of the domain where the volume
    fraction of solid1 is greater than 0.25, in essence showing the
    two solidification fronts.  
        }
    \LFIGH{mipc:results}
  \end{center}
\end{figure}

Sample results from the program plotted using gmv are presented in
\FIGH{mipc:results}. The upper figure is a transient state at 25s while
the lower figure is the steady state solution with liquid on the left,
solid1 in the middle (in gray), and solid2 on the right.  
 
--
