#!/bin/sh

# PARSER needs to be set to the path of the python parser in the environment.

# Generate the gmv graphics files.
python $PARSER << EOF >/dev/null
load ds1_output/ds1.TBrook.xml
write
-
-
-
-1
-
ds1
"concentration"
quit
EOF

# Concentration plots
options="-w 0 0 600 400 -a ds1-conc-attr -s temp.rgb"
textopt="-font Helvetica -pointsize 28 -stroke white -fill white"
gmvbatch $options -i ds1.gmv.000001
convert temp.rgb $textopt -draw "text 400,370 ' t = 0.05'" ds1-conc-1.png
gmvbatch $options -i ds1.gmv.000005
convert temp.rgb $textopt -draw "text 400,370 ' t = 0.25'" ds1-conc-2.png
gmvbatch $options -i ds1.gmv.000015
convert temp.rgb $textopt -draw "text 400,370 ' t = 0.75'" ds1-conc-3.png
gmvbatch $options -i ds1.gmv.000028
convert temp.rgb $textopt -draw "text 400,370 ' t = 1.40'" ds1-conc-4.png

# Color bar for the concentration plots
gmvbatch -w 0 0 600 600 -a ds1-cbar-attr -s temp.rgb -i ds1.gmv.000028
convert temp.rgb -crop 65x470+6+120 ds1-conc-cbar.png

# Title page plot
gmvbatch -w 0 0 900 600 -a ds1-conc-attr -i ds1.gmv.000028 -s temp.rgb
convert temp.rgb ds-title.png

# Remove the intermediate files
rm ds1.gmv.* temp.rgb

