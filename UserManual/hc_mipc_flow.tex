\chapter{Multiple Phase Changes + Buoyancy-Driven Flow}
\LCHAPH{mipc}

\begin{figure}[h]
   \begin{center}
   \begin{tabular}{ll}
       \includegraphics[width=6cm]{figures/hc_mipc_flowgmv000000.png} &
       \hspace{2cm}
       \includegraphics[width=6cm]{figures/hc_mipc_flowgmv000004.png}
    \end{tabular}
    \end{center}
\end{figure}

This chapter combines the heat transfer, phase change and flow capabilities 
in a problem that computes buoyancy-driven flow.
New features introduced in this chapter are definitions of
multiple phase changes and temperature-dependent properties that induce
buoyancy-driven flow.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{mipc:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item   \hyperlink{sec:mipc:mat}{Temperature-Dependent Density}
  \item   \hyperlink{sec:mipc:pcparams}{Multiple Isothermal Phase Changes}
\end{itemize}

\section{Problem Description}
\LSECH{mipc:problem:description} 
This chapter demonstrates multiple isothermal phase changes along with
buoyancy-driven flow. The graph of enthalpy vs. temperature is
presented in \FIGH{mipc:enthalpy.fig}.  Initially the temperature in
the entire domain is 1010, and it is liquid.  At time zero we drop the
temperature along the right edge of the domain to 700 while at the
same time raising the temperature along the left edge to 1200.  At
steady state, the left side of the domain will be liquid, solid 1
will be in the middle, and the right side of the domain will be solid
2.  Due to flow driven by buoyancy, the solidification front will not
be straight, but will have a lip at the bottom.  

\begin{tabular}{ll}
Units:& SI\\
Input file:& UMProblems/Chapter\_hc\_mipc\_flow/hc\_mipc\_flow.inp \\
Mesh file: & None\\
\end{tabular}

\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/mipc_enthalpy.pdf}
  \end{center}
\caption{Enthalpy Vs. Temperature for the problem presented in this
chapter.}
\LFIGH{mipc:enthalpy.fig}
\end{figure}

\section{Setup}
\LSECH{mipc:setup}

The input file for this problem is presented in \APPH{mipc:flow}.
In this chapter we discuss the variables related to setting up
multiple phase changes and temperature-dependent density in detail.


\subsection{Specifying physics}
\LSECH{mipc:physics}

In this problem for the first time we have fluid flow,
heat conduction, and phase change physics active at the same time.
This is the most complex situation we have treated.

\begin{verbatim}
&PHYSICS
  fluid_flow       = .true.
  phase_change     = .true.
  heat_conduction  = .true.
  body_force       =  0.0, -9.81, 0.0
  inviscid         = .true.
/
\end{verbatim}

By default, the Boussinesq approximation is active, as explained in the \PA.
See \CHAPH{hc:hs:ipc} \SECH{hc:hs:ipc:physics} and
\CHAPH{flow} \SECH{flow:physics} for details on the rest of the
variables.  Although this should really be a viscous
problem to get buoyancy-driven flow correct, we run it here as an
inviscid problem for demonstration purposes, because the viscous problem
runs much more slowly.

\subsection{Defining materials}
\LSECH{mipc:mat}

A new feature introduced here is the temperature-dependent density
for material 1, the liquid.  This temperature dependence is given as a
polynomial with the following form:
\[\rho = \rho_0*(1 + \alpha*(T - T_{ref})^n).\]
Using the parameters for the liquid $n = $ 
\texttt{density\protect\_change\protect\_exponents\protect\_T = 1.}, gives us:
\[\rho = 5000*(1+  - 10^{-5}*(T - 1000)).\]

\begin{verbatim}
&MATERIAL
  material_name                  ='liquid'
  material_number                = 1
  material_feature               = 'background'
  priority                       = 1
  density                        = 5000
  density_change_relation        = 'temperature_polynomial'
  density_change_coefficients_T  = -1.e-5
  density_change_exponents_T     = 1.
  reference_temperature          = 1000.
  conductivity_constants         = 250
  Cp_constants                   = 10
/

&MATERIAL
  immobile                 = .true.
  material_name            = 'solid1'
  material_number          = 2
  priority                 = 2
  density                  = 5000
  conductivity_constants   = 150
  Cp_constants             = 10
/

&MATERIAL
  immobile                 = .true.
  material_name            = 'solid2'
  material_number          = 3
  conductivity_constants   = 1000
  density                  = 5000
  Cp_constants             = 10
  priority                 = 3
/
\end{verbatim}

See the \verb|MATERIAL| chapter of the \RM\ for
further details on temperature functions for material properties.

\subsection{Initializing the domain}
\LSECH{mipc:init}

Here we are again using a block mesh, internally generated, and we
initialize it with a fill of \verb|'nodefault'|, because the entire
domain starts filled with our material.

\begin{verbatim}
&BODY
  surface_Name     = 'background'
  fill             = 'nodefault'
  material_Number  = 1
  temperature      = 1010
/
\end{verbatim}

See the \verb|BODY| chapter of the \RM\ for details.

\subsection{Applying boundary conditions}
\LSECH{mipc:bc}

In this problem we apply only temperature boundary conditions,
yet flow will be induced. In \APPH{mipc:flow} it will be seen
that we apply a constant temperature BC on the two faces of our
box domain that are perpendicular to the $x$ axis, and zero flux
BCs on the other four faces. This will induce convective
circulation around the box as well as phase changes.


\subsection{Numerics}

For this problem we must specify solvers for both the energy and
projection solutions. Other items in this namelist are similar
to previous problems.

\begin{verbatim}
&NUMERICS
  dt_init                     = 1.e-3
  dt_grow                     = 1.05
  dt_min                      = 1.e-7
  dt_max                      = 2.e0
  discrete_ops_type           = 'ortho'
  energy_nonlinear_solution   = 'NK-energy'
  projection_linear_solution  = 'projection-solver'
  Courant_number              = 0.4
  volume_track_interfaces     = .true.
  volume_track_Brents_method  = .true.
  volume_track_iter_tol       = 1.0e-12
  cutVOF                      = 1.0e-08
/
\end{verbatim}

See \CHAPH{flow} \SECH{flow:numerics} and the \verb|NUMERICS| chapter
of the \RM\ for a comprehensive listing of all possible variables. 


\subsection{Phase change parameters}
\LSECH{mipc:pcparams}

The only difference between this and the input in \CHAPH{hc:hs:ipc},
\SECH{hc:hs:ipc:pcparams} is that we need to define two phase changes.
The fields within the namelists are identical in both cases.
\FIGH{mipc:enthalpy.fig} shows the graph of enthalpy
vs. temperature for these two phase changes. 

\begin{verbatim}
&PHASE_CHANGE_PROPERTIES
  phase_change_active        = .true.
  phase_change_type          = 'isothermal'
  phase_change_model         = 'none'
  hi_temp_phase_material_ID  = 1
  lo_temp_phase_material_ID  = 2
  latent_heat                = 1000.0
  melting_temperature        = 1000.0
/

&PHASE_CHANGE_PROPERTIES
  phase_change_active        = .true.
  phase_change_type          = 'isothermal'
  phase_change_model         = 'none'
  hi_temp_phase_material_ID  = 2
  lo_temp_phase_material_ID  = 3
  latent_heat                = 1000.0
  melting_temperature        = 850.0
/
\end{verbatim}


\section{Results}

Sample results from the program plotted using GMV are presented in
\FIGH{mipc:results}. The upper figure is a transient state at 25 s, while
the lower figure is the steady state solution with liquid on the left,
solid1 in the middle (in gray), and solid2 on the right.  

\begin{figure}[bh]
  \begin{center}
   \vspace{1cm}
   \begin{tabular}{ll}
    &
    \vspace{-6.5cm}
    \includegraphics[width=6cm]{figures/hc_mipc_flowgmv000001.png} \\
    \includegraphics[height=13cm]{figures/hc_mipc_flowcbgmv.png}    &
    \includegraphics[width=6cm]{figures/hc_mipc_flowgmv000004.png} 
    \end{tabular}
    \end{center}
    \caption{Sample results from the calculation at time=100 and 400 s
    showing the progression of solidification.  The colors
    show the temperature, the arrows indicate the flow direction in
    cells that contain liquid, and the gray areas highlight 
    sections of the domain where the volume
    fraction of solid1 is greater than 0.25, in essence showing the
    two solidification fronts.}
    \LFIGH{mipc:results}
\end{figure}

 
--
