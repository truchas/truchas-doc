\chapter{Chemistry}
\LCHAPH{CHEM}
 \begin{figure}[h]
   \begin{center}
     \includegraphics[width=3in]{figures/chemgmv000005.png}
   \end{center}
 \end{figure}

In this chapter we discuss the capability of \truchas\ to model chemical
reactions along with other physics, in this case heat conduction.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{chem:capabilities}
\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item	Chemical reaction
\end{itemize}

\section{Problem Description}
\LSECH{chem:problem:description}
We add chemistry to the heat conduction problem.  The left half of
the domain is filled with a mold material, and reactants make up
the right half of the domain.  The reactants are initially at
550$^{\circ}$C while the mold material is at 400$^{\circ}$C.  At time zero, we set
the temperature of the left edge of the domain to 100$^{\circ}$C and follow the
progress of the chemical reaction giving us products from the
reactants as a function of time.

\begin{tabular}{ll}
Units: & SI \\
Input file:& UMProblems/Chapter\_chem/chem.inp \\
Mesh file: & None\\
\end{tabular}

\section{Setup}
\LSECH{chem:setup}

The entire input file for this problem is presented in \APPH{chem}.
We will focus on the aspects of this file that
define a chemical reaction as a pure material phase transition.  This
chapter is primarily concerned with defining parameters in the
\texttt{CHEMICAL\_REACTION} namelist.

\subsection{Defining a chemical reaction}
\LSECH{chem:defining:a:chemical:reaction}

The \texttt{CHEMICAL\_REACTION} namelist identifies the different
chemical reactions that are present during the simulation. Currently
only one model is available for chemical reactions, but this list will
soon increase. The current model describes a self-catalytic reaction
through the following equation: 

\begin{eqnarray}
\LEQ{crode}
\PARTIALOFWRT{}{t}C(t) &=& (k_1 + k_2*{C(t)}^m){(C_{max} - C(t))}^n \nonumber\\
k_1 &=& k^o_1*e^{-E^a_1/RT}                                         \nonumber\\
k_2 &=& k^o_2*e^{-E^a_2/RT}                                         
\end{eqnarray}

where $C$ is concentration of the product, $t$ is time, $k_1^o$ and $k_2^o$
the reaction constants for the two stages and are both a function of
temperature, $T$.  The order of the reaction is given by $n+m$.  $R$
is the gas constant.  It is further assumed that the heat of
reaction, $H_{tot}$ is evenly distributed as the reaction progresses.
i.e. 
\begin{eqnarray}
\PARTIALOFWRT{H}{t} = {H_{tot}}\PARTIALOFWRT{C}{t}
\end{eqnarray}

NOTE: Because $R$ is defined in SI units, the temperature must be the
absolute temperature ($K$), the activation energies, $E^a_1$ and $E^a_2$,
should be in consistent units ($J$), and the the time and rates $k_1$,
$k_2$, should be consistent. 

\begin{verbatim}
&CHEMICAL_REACTION
  cr_m               = 1, 
  cr_n               = 1, 
  cr_reactants_ID    = 2, 
  cr_products_ID     = 1, 
  cr_Cmax            = 0.99, 
  cr_ko1             = 1.0e5, 
  cr_ko2             = 0.0
  cr_Ea1             = 7.0e4, 
  cr_Ea2             = 0.0, 
  cr_Htot            = 2000
/
\end{verbatim}

The different variables in the
\texttt{CHEMICAL\_REACTIONS} namelist are named as  in equation
\EQ{crode}.  For this particular set of parameters, the equation
becomes:

\begin{eqnarray}
\LEQ{crodewithnumbers}
\PARTIALOFWRT{}{t}C(t) &=& (k_1 + k_2*{C(t)}^1){(0.99 - C(t))}^1 \nonumber\\
k_1 &=& 10^5*e^{-7.0*10^4/{8.312 T}}                                         \nonumber\\
k_2 &=& 0.0
\end{eqnarray}

where C is the concentration of material 1, $t$ is time, and $T$ is
temperature.

For additional details see the \texttt{CHEMICAL\_REACTIONS} chapter
of the \RM.

\subsection{Specifying physics}
\LSECH{chem:physics}

This namelist is the same as for a phase change problem.

%The new line in here, \textbf{binary\_alloy\_phase\_change = .true.}
%directs the code to look for a binary alloy phase change block
%(\SECH{chem:pcparams}) and read it in.  There is only \emph{one type}
%of phase change that can be defined at any given time.  In the case of
%alloy phase changes, there is the additional restriction that
%\emph{only one binary alloy phase change is allowed}.  This
%restriction will be relaxed in a future release of the code.


\subsection{Defining materials}
\LSECH{chem:mat}

Setting up materials that react is similar to setting up a material
with more than one phase.

\begin{verbatim}
&MATERIAL 
  material_name            = 'Product'
  material_number          = 1
  conductivity_constants   = 0.31
  density                  = 30.
  Cp_constants             = 100.
/

&MATERIAL 
  material_name            = 'Reactant'
  material_number          = 2
  material_feature         = 'background'
  conductivity_constants   = 0.31
  density                  = 30.
  Cp_constants             = 100.
/

&MATERIAL 

  material_name            = 'Mold'
  material_number          = 3
  conductivity_constants   = 0.1
  density                  = 30.
  Cp_constants             = 10.
/
\end{verbatim}


\subsection{Initializing the domain}
\LSECH{chem:init}

For this problem we again use a box to initialize part of the domain.

\begin{verbatim}
&BODY
  material_number   = 3, 
  surface_name      = 'box', 
  fill              = 'inside', 
  temperature       = 400., 
  translation_pt      =0.25, 0.50, 0.50, 
  length            = 0.50, 1.0, 1.0
/

&BODY 
  surface_name      = 'background', 
  fill              = 'nodefault', 
  material_number   = 2, 
  temperature       = 550.
/
\end{verbatim}

\subsection{Linear Solvers}
\LSECH{chem:lsolver}

In this file only one linear solver, 'SSOR-GMRES', is referenced, so
only one linear solver namelist exists. For this problem it is found
that there is benefit in choosing a stopping criterion for the solver
different from the default, \verb/'||r||'/.

\begin{verbatim}
&LINEAR_SOLVER 
  name                     = 'SSOR-GMRES', 
  method                   = 'GMRES', 
  convergence_criterion    = 1.e-4
  preconditioning_method   = 'SSOR', 
  relaxation_parameter     = 0.9, 
  preconditioning_steps    = 3, 
  stopping_criterion       = '$||r||/||r0||$'
/
\end{verbatim}

See \CHAPH{HC} \SECH{cond:lsolver} and the
\texttt{LINEAR\_SOLVER} chapter of the \RM\ for a comprehensive listing
of all parameter settings, as well as recommendations  for choice of
stopping criteria for different solver applications in \truchas.

\section{Results}

Results of running \truchas\ with this input are shown in 
\FIG{chemresults}. Plotted is the variation in concentration of
products as a function of time.  Note that the reaction does not
progress as fast near the mold as it does in the rest of the domain.
This is because the mold is held at a lower temperature than the
reactant materials and in addition is in contact with a heat sink at
100$^{\circ}$C along its left edge.

\vspace{2cm}
\begin{figure}[h]
\begin{center}
\begin{tabular}{llll}
&
\vspace{-4.0cm}
\includegraphics[width=4cm]{figures/chemgmv000000.png} &
\includegraphics[width=4cm]{figures/chemgmv000001.png} &
\includegraphics[width=4cm]{figures/chemgmv000002.png} \\
\includegraphics[height=8cm]{figures/chemcbgmv.png} &
\includegraphics[width=4cm]{figures/chemgmv000003.png} &
\includegraphics[width=4cm]{figures/chemgmv000004.png} &
\includegraphics[width=4cm]{figures/chemgmv000005.png} \\
\end{tabular}
\end{center}
\caption{Time progression of concentration of Material
ID 1.  The concentration scale is shown on the left.  As is
expected, the progression of the reaction for reactants in
contact with the mold (along the left edge) is slower than
that in the rest of the domain.}
\LFIG{chemresults}
\end{figure}




