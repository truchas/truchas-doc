\chapter{Binary Alloy Phase Change}
\LCHAPH{bapc}
\vspace{1in}
\begin{figure}[h]
    \includegraphics[angle=90,width=5in]{figures/bapc_leadin.pdf}
\end{figure}

\vspace{0.5in}

This chapter presents the solidification of a binary alloy following
the Scheil model for solidification.  The inputs discussed in this
problem are the parameters for setting up a binary alloy phase
transition and the partial initialization of a domain using the BODY
namelist.

\newpage
\section{\truchas Capabilities Demonstrated}
\LSECH{bapc:capabilities}
\begin{itemize}
\setlength{\itemsep}{-6pt}
\item   Heat Transfer
\item   \textbf{\hyperlink{sec:bapc:pcparams}{Binary Alloy Phase Change}}
\item   Constant Temperature Boundary Condition
\item   Insulating Boundary Condition
\item   \textbf{\hyperlink{sec:bapc:init}{Partial Domain Initialization Using BODY Namelist}}
\item   Non-Linear Solver
\item   Linear Solver
\item   Constant Time Step
\item \textbf{\hyperlink{sec:bapc:output}{Output of Thermodynamic and Fluid Information (long and
              short edits)}}
\end{itemize}

\section{Problem Description}
\LSECH{bapc:problem:description} This chapter demonstrates the setup of
a Binary Alloy Phase Change using \truchas.  The simulation domain is
presented in \FIGH{bapc:domain.fig}.  The graph of Enthalpy
Vs. Temperature is presented in \FIGH{bapc:enthalpy.fig} and the phase
diagram is presented in \FIGH{bapc:pd.fig}.  Initially the temperature
in the entire domain is 1000.  At time zero we drop the temperature
along the left and right edges to 100.  The drop in temperature causes
the liquid to solidify and a solidification front moves in from the
left edge of the problem towards the right.  We solve for the
progression of the solidification front, and for the temperature
profile between the two edges.

\begin{tabular}{ll}
Units: & MKS\\
Input file:& UMProblems/Chapter4/hc\_bapc.inp\\
Mesh file: & None\\
\end{tabular}

\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_domain.pdf}
  \end{center}
\LFIGH{bapc:domain.fig}
\caption{
Simulation domain for the current chapter.  The Mold areas, filled
with materials Mold1, and Mold2 are created using a BODY namelist.
}
\end{figure}
\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_enthalpy.pdf}
  \end{center}
\LFIGH{bapc:enthalpy.fig}
\caption{
Enthalpy Vs. Temperature for the problem presented in this
chapter.
}
\end{figure}
\begin{figure}
  \begin{center}
    \includegraphics[height=3in]{figures/bapc_pd.pdf}
  \end{center}
\LFIGH{bapc:pd.fig}
\caption{
Phase diagram for the problem presented in this chapter.
}
\end{figure}

\section{Setup}

The input file for this problem is presented in \APP{hc_bapc}.  In
this chapter we discuss the inputs related to binary alloy phase
change and partial domain initialization in detail.  

\LSECH{bapc:setup}
\subsection{Defining a mesh}
\LSECH{bapc:mesh}
Instances allowed: single
\namelistinput{
\&MESH\\ 
\> Ncell                             \> = 24 , 16 , 2 \\
\> Coord                             \> = 0 , 0 , 0 , 1.5 , 1.0, 1.0\\
/
}
See \CHAPH{HC} \SECH{cond:mesh} and \tRMSect{MESH}{MESH} for details.

\subsection{Specifying physics}
\LSECH{bapc:physics}
Instances allowed: single
\namelistinput{
\&PHYSICS \\
\> fluid\_flow                  \> = .false. \\
\> \textbf{binary\_alloy\_phase\_change}  \> \textbf{= .true. }\\
\>    heat\_conduction            \> = .true. \\
/
}
The new line in here, \textbf{binary\_alloy\_phase\_change = .true.}
directs the code to look for a binary alloy phase change block
(\SECH{bapc:pcparams}) and read it in.  There is only \emph{one type}
of phase change that can be defined at any given time.  In the case of
alloy phase changes, there is the additional restriction that
\emph{only one binary alloy phase change is allowed}.  This
restriction will be relaxed in a future release of the code.

See \CHAPH{HC} \SECH{cond:physics} and \tRMSect{PHYSICS}{PHYSICS} for
details.

\subsection{Defining materials}
\LSECH{bapc:mat}
Instances allowed: multiple, one for each phase of each material in
the problem.
\namelistinput{
\&MATERIAL  \\
\> Material\_Name                     \> = 'liquid'  \\
\> Material\_Number                   \> = 1  \\
\> Conductivity\_Constants            \> = 15  \\
\> Density                           \> = 5000  \\
\> Cp\_Constants                      \> = 1000  \\
\> Priority                          \> = 1  \\
/ \\
\&MATERIAL  \\
\> Material\_Name                     \> = 'solid'  \\
\> Material\_Number                   \> = 2  \\
\> Conductivity\_Constants            \> = 15  \\
\> Density                           \> = 5000  \\
\> Cp\_Constants                      \> = 1000  \\
\> Priority                          \> = 2  \\
/ \\
\&MATERIAL  \\
\> Material\_Name                     \> = 'mold1'  \\
\> Material\_Number                   \> = 3  \\
\> Conductivity\_Constants            \> = 20  \\
\> Density                           \> = 10000  \\
\> Cp\_Constants                      \> = 2000  \\
\> Priority                          \> = 3  \\
/ \\
\&MATERIAL  \\
\> Material\_Name                     \> = 'mold2'  \\
\> Material\_Number                   \> = 4  \\
\> Material\_Feature                  \> = 'background'  \\
\> Conductivity\_Constants            \> = 25  \\
\> Density                           \> = 5000  \\
\> Cp\_Constants                      \> = 1000  \\
\> Priority                          \> = 4  \\
/
}
See \CHAPH{HC} \SECH{cond:mat} and \tRMSect{MATERIAL}{MATERIAL} for details.

\subsection{Initializing the domain}
\LSECH{bapc:init}
Instances allowed: multiple, as many as required to initialize the domain
\namelistinput{
\&BODY\\ 
\> Surface\_Name                     \>= 'background' \\
\>    Material\_Number                \> = 4 \\
\>    Temperature                    \> = 1000 \\
/\\
\&BODY\\
 \> \textbf{Surface\_Name}                    \>= 'box' \\
 \> \textbf{Length}                          \> \textbf{= 0.5 , 1 , 1 } \\
 \> \textbf{Fill}                             \> \textbf{= 'inside' } \\
 \> \textbf{Translation\_Pt}                   \> \textbf{= 0.25 , 0.5 , 0.5 } \\
 \> \textbf{Material\_Number}                  \> \textbf{= 1 } \\
 \> \textbf{Concentration}                    \> \textbf{= 10 } \\
 \> \textbf{Temperature}                      \> \textbf{= 1000 } \\
/\\
\&BODY \\
  \> Surface\_Name                     \> = 'box' \\
  \> Length                           \> = 0.5 , 1 , 1 \\
  \> Fill                             \> = 'inside' \\
  \> Translation\_Pt                   \> = 0.75 , 0.5 , 0.5 \\
  \> Material\_Number                  \> = 3 \\
  \> Temperature                      \> = 1000 \\
/
}
In this chapter we present a new body called a 'box'. The second body
namelist converts the block of domain centered at (0.25, 0.5, 0.5) and
having extents (0.5, 1.0, 1.0) into material 1 (the liquid) with
concentration 10\%.  The temperature of the box is set to 1000.
Although this box is aligned with the cells, in general bodies need
not be aligned with the internal mesh.  See \CHAPH{HC} \SECH{cond:init}
and the \tRMSect{BODY}{BODY} for details.

\subsection{Applying boundary conditions}
\LSECH{bapc:bc}
Instances allowed: multiple, one for each boundary condition we wish to specify.
\namelistinput{
\&BC  \\
\> Surface\_Name                      \> =  'conic'  \\
\> Conic\_X                           \> = 1  \\
\> Conic\_Constant                    \> = 0  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'dirichlet'  \\
\> BC\_Variable                       \> = 'temperature'  \\
\> BC\_Value                          \> = 100  \\
/  \\
\\
\&BC  \\
\> Surface\_Name                      \> =  'conic'  \\
\> Conic\_X                           \> = 1  \\
\> Conic\_Constant                    \> = -1.5  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'dirichlet'  \\
\> BC\_Variable                       \> = 'temperature'  \\
\> BC\_Value                          \> = 100  \\
/  \\
 \\
\&BC  \\
\> Surface\_Name                      \> = 'conic'  \\
\> Conic\_Y                           \> = 1  \\
\> Conic\_Constant                    \> = 0  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'hneumann'  \\
\> BC\_Variable                       \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
\> Surface\_Name                      \> =  'conic'  \\
\> Conic\_Y                           \> = 1  \\
\> Conic\_Constant                    \> = -1  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'hneumann'  \\
\> BC\_Variable                       \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
\> Surface\_Name                      \> =  'conic'  \\
\> Conic\_Z                           \> = 1  \\
\> Conic\_Constant                    \> = 0  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'hneumann'  \\
\> BC\_Variable                       \> = 'temperature'  \\
/  \\
 \\
\&BC  \\
\> Surface\_Name                      \> =  'conic'  \\
\> Conic\_Z                           \> = 1  \\
\> Conic\_Constant                    \> = -1  \\
\> Conic\_Tolerance                   \> = 1e-06  \\
\> Conic\_Relation                    \> = '='  \\
\> BC\_Type                           \> = 'hneumann'  \\
\> BC\_Variable                       \> = 'temperature'  \\
/ 
}
See \CHAPH{HC} \SECH{cond:bc} and \tRMSect{BC}{BC} for details.

\subsection{Selecting output formats}
\LSECH{bapc:output}
Instances allowed: single
\namelistinput{
\&OUTPUTS  \\
\> Output\_T                          \> = 0 , 5000  \\
\> Output\_Dt                         \> = 500  \\
\> gra\_output\_dt\_multiplier          \> = 1 \\
\> graphics\_format                   \> = 'gmv' \\
\> Short\_Output\_Dt\_Multiplier        \> = 1 \\
\> Long\_Output\_Dt\_Multiplier         \> = 1 \\
\> Long\_Edit\_Bounding\_Coords         \> = 0, 1.5, 0, 1, 0, 1  \\
/
}
See \CHAPH{outputs} and the \tRMSect{OUTPUTS}{OUTPUTS} for details.

\subsection{Numerics}
\LSECH{bapc:numerics}
instances allowed: single
\namelistinput{
\&NUMERICS \\
\> dt\_constant                       \> = 10 \\
\> energy\_nonlinear\_solution         \> = 'nk'\\ 
/
}
See \CHAPH{HC} \SECH{cond:numerics} and the \tRMSect{NUMERICS}{NUMERICS} for a
comprehensive listing of all possible variables. 

\subsection{Non-Linear Solvers}
\LSECH{bapc:nlsolver}
Instances allowed: multiple, one for each of the solvers referenced in
the \texttt{NUMERICS} namelist.
\namelistinput{
\&NONLINEAR\_SOLVER  \\
\> name                              \> = 'nk'  \\
\> method                            \> = 'nk'  \\
\> linear\_solver\_name                \> = 'nk-linear'  \\
\> convergence\_criterion             \> = 1.0e-01  \\
\> maximum\_iterations                \> = 50  \\
\> output\_mode                       \> = 'none'  \\
\> use\_damper                        \> = .true.  \\
\> Damper\_Parameters                 \> = 0.9, 1.1, 1.0, 1.0 \\
\> perturbation\_parameter            \> = 1.0e-06  \\
/
}
See \CHAPH{HC} \SECH{cond:nlsolver}  and the
\tRMSect{NONLINEAR\_SOLVER}{NONLINEAR\_SOLVER} for a comprehensive
listing of all possible variables.   

\subsection{Linear Solvers}
\LSECH{bapc:lsolver}
Instances allowed: multiple, one for each of the linear solvers
referenced in the input file.

In this file only one linear solver, 'energy-linear' is referenced, so
only one linear solver namelist exists.
\namelistinput{
\&LINEAR\_SOLVER  \\
\> name                              \> = 'nk-linear'  \\
\> method                            \> = 'fgmres'  \\
\> convergence\_criterion             \> = 1.0e-6  \\
\> preconditioning\_method            \> = 'ssor'  \\
\> relaxation\_parameter              \> = 0.9  \\
\> preconditioning\_steps             \> = 3  \\
/
}
See \CHAPH{HC} \SECH{cond:lsolver} and the
\tRMSect{LINEAR\_SOLVER}{LINEAR\_SOLVER} for a comprehensive listing
of all possible variables.

\subsection{Phase Change Parameters}
\LSECH{bapc:pcparams}
Instances allowed: single for binary phase changes, and multiple for
isothermal phase changes
\namelistinput{
\&PHASE\_CHANGE\_PROPERTIES  \\
\> \textbf{Phase\_Change\_Type}               \>  \textbf{= 'solid\_liquid\_alloy'}  \\
\> \textbf{local\_scale\_model}               \> \textbf{= 'scheil'}  \\
\> \textbf{Hi\_Temp\_Phase\_Material\_Id}     \> \textbf{= 1}  \\
\> \textbf{Lo\_Temp\_Phase\_Material\_Id}     \> \textbf{= 2}  \\
\> \textbf{Latent\_Heat}                      \> \textbf{= 1000.0} \\
\> \textbf{Liquidus\_Slope}                   \> \textbf{= -10.0} \\
\> \textbf{Liquidus\_temp}                    \> \textbf{= 900.0} \\
\> \textbf{Melting\_Temperature\_Solvent}     \> \textbf{= 1000.0} \\
\> \textbf{Partition\_Coefficient\_Constants} \> \textbf{= 0.50} \\
/
}

Here we describe the phase changes whose parameters are as displayed
in \FIGH{bapc:enthalpy.fig} and \FIGH{bapc:pd.fig}.  The latent heat is
1000, and the slope of the liquidus is -10.  The partition coefficient
is 0.5.  Note that the problem is overspecified because the liquidus
temperature is specified in this namelist, as well as the
concentration in the body namelist.  If these two numbers are not
consistent with each other and the phase diagram, the code will not
run. Additionally, if
\hyperlink{sec:bapc:physics}{binary\_alloy\_phase\_diagram} is not
activated in the \hyperlink{sec:bapc:physics}{\texttt{PHYSICS}}
namelist, the problem will abort.  See the
\tRMSect{PHASE\_CHANGE\_PROPERTIES} {PHASE\_CHANGE\_PROPERTIES} for a
comprehensive listing of all possible variables.

\section{Results}
The next step is to run the problem.  This is done by issuing the following command:
\begin{verbatim}
% truchas HT_Alloy.inp
\end{verbatim}
Ensure that \texttt{truchas}, the executable program, is in your path, and that
\texttt{HT\_Alloy.inp} lies in the current working directory.

\begin{figure}[h]
  \begin{center}
    \includegraphics[angle=90,width=5in]{figures/bapc_res.pdf}
    \caption{Sample results from the calculation at time=500 and 2500s
    showing progression of solidification of the alloy.  The colors
    show the volume fraction of liquid where blue is 0 and red is 1.
    The gray areas highlight sections of the domain where the volume
    fraction of the solid is greater than 0.25, in essence showing the
    solidification front.  
        }
    \LFIGH{bapc:results}
  \end{center}
\end{figure}
Sample results from the program plotted using gmv are presented in
\FIGH{bapc:results}. 
 
--
