\chapter{Internal Interface Displacements}
\LCHAPH{DISP}
\begin{figure}[h]
   \begin{center}
     \begin{tabular}{l}
        \includegraphics[width=2in]{figures/dispdrgmv000001.png} 
     \end{tabular}
   \end{center}
\end{figure}

In this chapter we set up a simple problem to demonstrate the contact
boundary condition for internal interfaces.  The problem is
a linear thermo-elastic solution of displacements, stresses, and
strains for a solid aluminum ring in a graphite mold.  The use of gap elements in
order to model internal interfaces that can open gaps or accommodate
interfacial sliding is  demonstrated.

\section{\truchas\ Capabilities Demonstrated}
\LSECH{disp-bc_capabilities}

\begin{itemize}
  \setlength{\itemsep}{-6pt}
  \item \hyperlink{sec:disp:mesh}{Gap element interfaces}
  \item \hyperlink{sec:disp:bc}{Contact boundary conditions}
\end{itemize}

\section{Problem Description}
\LSECH{disp:problem:description}

The problem consists of a ring of aluminum alloy initially in contact
with a graphite mold.  The mold consists of a cylindrical disk with a
cavity in the shape of a ring.  The mesh represents a 60 degree
segment of the mold, and the boundary conditions are chosen to give a
solution that is axisymmetric.  Both materials have a stress reference
temperature set to 700$^o$C, and the initial temperature of both
bodies is set to 600$^o$C.  All materials are solid, and there is
no fluid flow or heat transfer in the problem.

The simulation only solves for the initial displacement, stress and
strain fields caused by the difference between the initial temperature
and the reference temperature and the difference in thermal expansion
coefficients.  All of the interfaces between the ring and graphite
mold use either the \texttt{'contact'} or \texttt{'normal-constraint'}
boundary condition.  The contact BC prevents penetration of the
materials on either side of the interface but allows separation and
sliding displacement along the interface.  The normal constraint
condition allows sliding but does not allow either separation or
penetration.  Both sides of the interface can also be completely
unconstrained by using the \texttt{'free-interface'} condition.  The
\texttt{'free-interface'} boundary condition does not prohibit
interpenetration of the two bodies on either side of the interface.

\begin{tabular}{ll}
Units:     & SI\\
Input file:& UMProblems/Chapter\_disp/disp.inp \\
Mesh file: & UMProblems/Chapter\_disp/disp.g   \\
\end{tabular}

\section{Setup}
\LSECH{disp:setup}

The complete input file for this problem is presented in \APPH{disp}.
Here we discuss previously unseen sections of the input file in detail.

\subsection{Defining a mesh}
\LSECH{disp:mesh}

\begin{verbatim}
&MESH
  mesh_file         = 'disp.g'
  mesh_file_format  = 'exodusII'
\end{verbatim}

In addition to the familiar mesh blocks and several side sets,
The mesh file for this problem also contains
element blocks whose members are ``gap elements''.

Gap elements are mesh cells (initially) containing no volume that are
used to model interfaces between two solid bodies.
The \verb|'contact'| BC type is only used with gap elements, as are
\verb|'free-interface'| and \verb|'normal-constraint'| BCs. 
Gap elements provide connectivity between pairs of nodes that are at
the same position but connected to elements on either side of the
interface.  Gap elements are only supported in EXODUS meshes, and are
specified by block number in the \texttt{MESH} namelist.

Gap elements can be of zero or finite thickness.  The easiest method
of producing meshes with zero thickness gap elements is to use the
CUBIT mesh generator and a mesh processor provided with the \truchas\
distribution.  The following description assumes the the user knows
how to generate a mesh with CUBIT and create an EXODUS mesh file that
can be read by \truchas.

\subsubsection{Mesh Generation}

When creating the mesh using CUBIT, the internal surfaces that require
gap elements must be assigned to side~sets.  These surfaces must
correspond to geometric surfaces in the CUBIT model, which will put
element faces on that surface.  For example, if the surface number is
2 and the user wishes to label the side~set as number 3 the line command
would be:

\begin{verbatim}
> sideset 3 surface 2
\end{verbatim}

The side~set identifier specified in the CUBIT command is needed for the
\truchas\ \texttt{BC} namelist input variable
\verb|mesh_surface|.

There are some requirements or limitations in how the side sets are
defined.

\begin{itemize}

\item The behavior of the interface constraints at corners or edges
  depends on how the side~sets are defined.  If a corner is formed by
  multiple side sets, \truchas\ will add a separate constraint for
  each side set.  This is probably what the user wants.
  If there is a sharp corner within a single side~set, then an
  average normal at the corner will be calculated by \truchas\, and
  the displacements at the corner may produce poor results.  This is a
  \truchas\ feature that will probably not change.

\item Currently the maximum number of side sets that can intersect at a
  node is 3.  This allows the user to construct a corner that is
  constrained on three sides, and should be adequate in most cases.
  This is a limitation of the \truchas\ code and will probably not
  change soon.

\item This version of the mesh preprocessor has not been tested for tet
  meshes.  In any case tetrahedral elements are not recommended for
  solid mechanics.
\end{itemize} 

\subsubsection{Mesh Preprocessing} 

The mesh processor for creating gap elements takes a binary EXODUS
mesh file as input and generates another mesh file with additional
nodes and gap elements.  The mesh processor is obtained by compiling
the ``addgaps'' program by typing ``make'' in \\
\verb|truchas/tools/mesh_processors/addgaps|.  The command
line syntax for the addgaps program is \\
\verb|addgaps input_file output_file sideset_id1 [sideset_id2 ...]|

You will be prompted for a new element block ID number for each side
set.  This will create the mesh file 'output\_file', with new
element blocks for each side~set.  New element block numbers for
each sideset will be written to a file called \texttt{addgaps.log}.
See \\
\verb|truchas/tools/mesh_processors/addgaps/addgaps.1|
for more information.

It should also be possible to create gap elements manually in the mesh
generator with a finite thickness.  The element block specified as
having gap elements must be a layer of cells one cell thick with cells
on both sides of the layer.  If the gap element block is not in this
configuration the run will fail.

\subsubsection{\truchas\ Input}

If solid mechanics interface boundary conditions are to be specified,
the \texttt{MESH} namelist must have the \verb|gap_element_blocks| set
to a list of element blocks that are to be treated as gap elements.
Also, the \texttt{BC} namelist must have the \verb|surface_name| set
to \texttt{'from mesh file'} and the \verb|mesh_surface| set to the
side~set ID in the mesh file.  A separate BC namelist is required for
each side~set.

If the simulation includes fluid flow, a \texttt{BODY}
namelist must be defined for each gap element block that assigns
a material with the attribute \texttt{immobile} set to \true.
This prevents \truchas\ from attempting to compute flow in gap
elements.  The gap element
material properties are not actually used in any of the physics
calculations.  However to avoid problems with the heat transfer
calculation, the density, conductivity and heat capacity of gap
element materials should be set to zero.  The elastic constants for
the gap material should also not be changed from the defaults of zero.

\begin{figure}[h]
   \begin{center}
     \includegraphics[height=3in]{figures/ring_mold_ss.png}
   \end{center}
   \caption{Side set definitions for this example.}%
   \LFIGH{disp:side-sets.fig}
\end{figure}

The side set definitions for this problem are shown in
\FIGH{disp:side-sets.fig}.  The uses of these side sets are listed
in the following table.

\begin{tabular}{c|c|c} \hline
Side Set & Boundary Condition & Gap Element Block \\ \hline
1 & \texttt{'normal-displacement'} & N/A \\
2 & \texttt{'normal-constraint'}   & 4 \\
3 & \texttt{'contact'}   & 5 \\
4 & \texttt{'contact'}   & 6 \\
5 & \texttt{'z-traction'}   & N/A \\ \hline
\end{tabular}

\subsection{Specifying physics}
\LSECH{disp:physics}

\begin{verbatim}
&PHYSICS
  fluid_flow       = .false.
  heat_conduction  = .false.
  solid_mechanics  = .true.
\end{verbatim}

This problem uses only solid mechanics.

\subsection{Defining materials}
\LSECH{disp:mat}

In this problem the graphite and aluminum materials can both be
defined as linear elastic.  After their namelists an example
of how to define a material for gap elements is given.

\begin{verbatim}
Define the plug material properties:

&MATERIAL
  material_number               = 1
  priority                      = 1
  material_name                 ='graphite'
  density                       = 1.7e+03
  Cp_constants                  = 1.925e+03
  conductivity_constants        = 1.95e+02
  Lame1_constants               = 3.4e+9
  Lame2_constants               = 2.76e+9
  CTE_constants                 = 7.0e-06
  stress_reference_temperature  = 7.00e+02
  material_feature              = 'background'
  viscoplastic_model            = 'elastic_only'
  immobile                      = .true.
/

Define the ring material properties:

&MATERIAL
  material_number               = 2
  priority                      = 2
  material_name                 = '5754 aluminum'
  density                       = 2.70e+03
  Cp_constants                  = 9.00e+02
  conductivity_constants        = 2.40e+02
  Lame1_constants               = 5.20e+10
  Lame2_constants               = 2.60e+10
  CTE_constants                 = 2.20e-05
  stress_reference_temperature  = 7.00e+02
  viscoplastic_model            = 'elastic_only'
  immobile                      = .true.
/

Define a suitable gap material:

&MATERIAL
  material_number         =  3
  priority                =  3
  material_name           = 'gap'
  density                 =  0.0
  Cp_constants            =  0.0
  conductivity_constants  =  0.0
  Lame1_constants         =  0.0
  Lame2_constants         =  0.0
  CTE_constants           =  0.0
  immobile                = .true.
/
\end{verbatim}

\subsection{Initializing the domain}
\LSECH{disp:init}

In this problem the initial temperatures of the bodies are different
from the reference temperatures specified for each material in the
\texttt{MATERIAL} namelists.  As in the previous viscoplastic
problem, an initial calculation of the displacement field and linear
elastic stresses is computed before the first time step.

The first two bodies make up the graphite mold, and the third body is
the aluminum ring.  The last three bodies are gap element blocks
created from three side sets as described above.

\subsection{Applying boundary conditions}
\LSECH{disp:bc}

The first two BC namelists set symmetry conditions for the bottom of
the mold ($z=0$) and the plane $x=0$.  Setting displacements to
zero mimics a problem in which there is a full cylinder of infinite
vertical extent.

\begin{verbatim}
&BC
  surface_name     = 'conic'
  conic_relation   = '='
  conic_z          =  1.0e+00
  conic_constant   =  0.0
  conic_tolerance  =  1.0e-06
  BC_variable      = 'displacement'
  BC_type          = 'z-displacement'
  BC_value         =  0.0
/

&BC
  surface_name     = 'conic'
  conic_relation   = '='
  conic_x          =  1.0e+00
  conic_constant   =  0.0
  conic_tolerance  =  1.0e-06
  BC_variable      = 'displacement'
  BC_type          = 'x-displacement'
  BC_value         =  0.0
/
\end{verbatim}

The third BC namelist sets the normal displacement to zero for the
plane 60$^o$ away from the $x=0$ plane.  The surface is specified
using side set number 1, which in this case is easier than using
a conic.

\begin{verbatim}
&BC
  surface_name  = 'from mesh file'
  mesh_surface  =  1
  BC_variable   = 'displacement'
  BC_type       = 'normal-displacement'
  BC_value      =  0.0
/
\end{verbatim}

The fourth BC namelist refers to side set 2 at the interface between
the ring and the inner diameter of the mold cavity.  The
\texttt{'normal-constraint'} condition is used for this interface.
Since the ring will shrink onto the plug, we assume that the ring and
plug will not separate.  Since the problem is axisymmetric, it is
expected that displacements tangential to the interface in the $x-y$
plane will be zero.

\begin{verbatim}
&BC
  surface_name  = 'from mesh file'
  mesh_surface  =  2
  bc_variable   = 'displacement'
  bc_type       = 'normal-constraint'
  bc_value      =  0.0
/
\end{verbatim}

The next BC namelist uses side set 3 at the interface between the ring
and outer diameter of the mold cavity.  The \texttt{'contact'}
constraint prevents the ring and mold from penetrating each other, but
allows relative separation or displacement.

\begin{verbatim}
&BC
  surface_name  = 'from mesh file'
  mesh_surface  =  3
  bc_variable   = 'displacement'
  bc_type       = 'contact'
  bc_value      =  0.0
/
\end{verbatim}

The fifth BC namelist applies a \texttt{'contact'} condition on
the interface between the bottom of the ring and the horizontal
surface of the mold.

\begin{verbatim}
&BC
  surface_name  = 'from mesh file'
  mesh_surface  =  4
  bc_variable   = 'displacement'
  bc_type       = 'contact'
  bc_value      =  0.0
/
\end{verbatim}

The sixth BC namelist applies a very small pressure (1.0 Pa downward)
on the top of the ring.  The contact boundary conditions do not
constrain the ring from moving in the positive $z$ direction, and this
BC ensures that there is a unique solution.

\begin{verbatim}
&BC
  surface_name  = 'from mesh file'
  mesh_surface  =  5
  bc_variable   = 'displacement'
  bc_type       = 'z-traction'
  bc_value      =  -1.0
/
\end{verbatim}

\subsection{Selecting output formats}
\LSECH{disp:output}

The output time is set to run only one time step.  Since this is an
elastic solution with no heat transfer, the solution is not
time-dependent.  At this time there is no way to stop a \truchas\
simulation after the initial elastic solution, so one nonlinear
solution will be computed.

\subsection{Numerics}
\LSECH{disp:numerics}

Because the solution is not time-dependent, time step parameters
are not important.  The three contact parameters are specified here,
but the vales are all equal to the defaults.  The
parameter \verb|contact_penalty| should probably not be changed
for most problems, but it may be necessary to adjust the
\verb|contact_distance| or \verb|contact_norm_trac| parameters to obtain
reliable convergence.

\begin{verbatim}
&NUMERICS
  dt_constant                      = 1.0e-1
  displacement_nonlinear_solution  = 'displacement nonlin'
  contact_penalty                  = 1.0e3
  contact_norm_trac                = 1.0e4,
  contact_distance                 = 1.0e-7
/
\end{verbatim}

\subsection{Nonlinear Solvers}

In this release the same nonlinear solver is used for both the initial
elastic solution and the time-dependent solution.  

\begin{verbatim}
&NONLINEAR_SOLVER 
  name                   = 'displacement nonlin' 
  method                 = 'AIN' 
  linear_solver_name     = 'displacement precon' 
  convergence_criterion  = 1e-3
  AIN_max_vectors        = 30 
  AIN_vector_tolerance   = 0.001 
  maximum_iterations     = 500
/
\end{verbatim}

\subsection{Linear Solvers}

One linear solver is needed for the nonlinear AIN solver.

\begin{verbatim}
&LINEAR_SOLVER
  name                    = 'displacement precon',
  method                  = 'none',
  preconditioning_steps   = 4,
  relaxation_parameter    = 1.0,
  preconditioning_method  = 'TM_SSOR',
/
\end{verbatim}

\section{Results}

Some output showing interface variables is shown in
\FIGH{dispinterface}.  The gap opening displacement and traction
normal to the interface are available for plotting.  The variables are
identified by the element block number.

\begin{figure}[bh]
  \begin{center} \begin{tabular}{lr}
  \includegraphics[width=7.5cm]{figures/dispgap5gmv000001.png} &
  \includegraphics[width=7.5cm]{figures/dispntr4gmv000001.png}
  \end{tabular} \end{center} \caption{Plots of the interface gap for
  side set 3 (block 5) and normal traction for side set 2 (block 4).}
\LFIGH{dispinterface}
\end{figure}

Some stress results are plotted in \FIGH{dispinterface}.  A gmv file
with stresses and strains transformed to cylindrical coordinates can
be generated using the python script
{\small\begin{verbatim}
tools/scripts/solid_mech_cyl_coords.py
\end{verbatim}}
The $xx$ component of stress in Cartesian coordinates and $\theta
\theta$ component in cylindrical coordinates are plotted in the
figure.  The mesh geometry and symmetry boundary conditions produce an
axisymmetric solution.
\vspace{1cm}
\begin{figure}[bh]
  \begin{center} \begin{tabular}{lr}
  \includegraphics[width=7.5cm]{figures/dispsxxgmv000001.png} &
  \includegraphics[width=7.5cm]{figures/dispsttgmv000001.png}
  \end{tabular} \end{center} \caption{Plots of $\sigma_{xx}$ and
  $\sigma_{\theta \theta}$}
\LFIGH{disinterface}
\end{figure}


