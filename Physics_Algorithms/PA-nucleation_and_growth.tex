\chapter{Nucleation and Growth}
\LAPP{nucleation_and_growth}

\section{Rappaz-Th\'{e}voz Model with One-Way Coupling}

We have implemented the method of Th\'{e}voz et. al.
for modeling microstructure formation during
solidification~\cite{thevoz_desbiolles_rappaz} using one-way coupling.  
In this method, computational cells
that are actively undergoing solidification ($0<f_s<1$) are subjected
to $N$ explicit ``microscopic'' timesteps per implicit ``macroscopic''
timestep ($\delta t=\Delta t / N$).  In this case, the macroscopic
heat flow calculation provides the cell an enthalpy $H$ and change in
enthalpy $\Delta H$ for the cell, and the microscopic time evolution
will evolve the nuclei number density $n$ and average grain
radius $R$ in the cell.  From these quantities, the solid fraction
$f_s$ can be updated.  Since 
\begin{equation}
\delta H=c_p\delta T-L\delta f_s,\label{eq:dH}
\end{equation}
we then can obtain the change in temperature $\delta T$ for the 
micro-timestep.  Performing this $N$ times, we obtain the change in $T$ for
the macro-timestep. (Note:  we avoid calling this change ``$\Delta
T$'', since the symbol $\Delta T$ is already reserved to denote the 
undercooling $T_l-T$ where $T_l$ is the liquidus temperature.)

  Thus as opposed to the usual assumed monotonic relationship between
enthalphy and temperature, it is now possible to model {\em
recalescence} where a sudden increase in $f_s$ actually leads to an
increase in temperature, even while heat is being removed from the
cell.  At this point, the release of latent heat due to solidification
exceeds the heat being removed from the cell, the temperature rises,
and nucleation of new grains is halted.  

\subsection{Nucleation Model}
As in~\cite{thevoz_desbiolles_rappaz}, it is assumed that nuclei number density at a
given undercooling is given by the
integral of a Gaussian nucleation site density distribution from zero undercooling
to the current undercooling.  (However, nuclei number density increase
is permanently halted at recalescence.)  The Gaussian distribution is
characterized by its standard deviation $\Delta T_\sigma$, its mean
(located at undercooling $\Delta T_N$) and its integral $n_{\rm
max}$.  These three parameters are determined experimentally for each
melt.

\subsection{Growth Model}
The solid fraction in the Rappaz-Th\'{e}voz model is given by
$$
f_s(t)=n(t)\cdot\frac{4}{3}\pi R^3(t)\cdot f_i(t),
$$
where $f_i$ is the internal solid fraction which corresponds to the
fraction of solid within the expanding spherical envelope of the
grains (which have radius $R(t)$).  We write
\begin{equation}
\delta f_s=n(t)\cdot\left(4\pi R^2\delta R\cdot f_i+\frac{4}{3}\pi
R^3\cdot \delta f_i\right),\label{eq:f_s}
\end{equation}
if one neglects the change $\delta n$.  (One can see the absurdity of
including a $\delta n$ term as given by the product rule for
derivatives, since such a term would imply the instantaneous creation
of grains {\it at radius} $R$ instead of radius $0$.  However,
omission of the $\delta n$ term does not change the fact that
the model is still a simplification because it assumes a {\it single}
radius $R$ describes the distribution of grains in the nucleation and
growth phases.)

Assuming~(\ref{eq:f_s}), to compute $\delta f_s$,
we need to know $\delta R$ and $\delta f_i$.  First, $\delta R$ is naturally
obtained by knowing the velocity $v$ of the dendrite tip.  This
velocity is obtained from the model of Lipton
et. al.~\cite{lipton_glicksman_kurz} which relates $v$ to the known
undercooling of the grain.  Second, the solute diffusion model of
Rappaz and Th\'{e}voz~\cite{rappaz_thevoz} relates $f_i$ to the the
Peclet number ${\rm Pe}\equiv\frac{Rv}{2D}$
of the grain and the supersaturation at the dendrite tip.  The
supersaturation is 
\[
\Omega\equiv\frac{c^\ast-c_0}{c^\ast(1-k)},
\]
which can be related to the undercooling by the phase diagram.  (Here
$c^\ast$ is the concentration at the dendrite tips, $c_0$ is the alloy
concentration, and $k$ is the partition coefficient.)  Since
we know $R$, $v$, and $D$ (solute diffusion coefficient), we can
compute Pe.
Thus we have enough information to obtain $\delta f_i$.  Knowing $\delta
f_i$ and $\delta R$ gives us $\delta f_s$ by~(\ref{eq:f_s}).  Knowing
$\delta f_s$ and $\delta H$ gives us $\delta T$ by~(\ref{eq:dH}).

\subsection{One-Way Coupling Assumption}

Ideally the change $\delta T$ computed by our micro model should be
fed back to the macro model.  However, since the macro model is 
doing an implicit time step, it will be likely that this feedback
from the micro model would be requested many times per macro time step.
This is in practice prohibitively expensive.  Instead, we try to only
compute the maximum undercooling and grain size at recalescence.
So we are interested in running the micro model for only a short
period of time between when the temperature has dropped below the 
liquidus temperature and when recalescence takes place.  We then
make the assumption that during this critical period of time, the 
macro change of enthalpy is determined by thermal gradients set up
by temperature differences on a macroscopic scale and not by thermal
gradients resulting from local release of latent heat.  In this case,
it is justified in running a standard macro solidification model
and using the resulting enthalpy changes to drive the micro model.
The micro model then computes temperature changes, but these temperature
changes are not fed back into the macro model.  

In effect, from the onset of solidification to recalescence, there
are two different temperatures:  $T_{\rm macro}$ and $T$.  
$T_{\rm macro}$ is not used by the micro model, except that when
$T_{\rm macro}$ cools so that $T_{\rm macro}=T_l$, the micro model
is starting with $T=T_l$ as well.  The micro temperature $T$ then
is allowed to diverge from $T_{\rm macro}$.  $T$ is used in the 
nucleation and growth model until recalescence, at which time the 
model has predicted the maximum undercooling $\Delta T=T_l-T$ and 
the corresponding grain size.  At this point, the micro model is 
not used any more.  The \truchas\ output only contains the 
macro temperature $T_{\rm macro}$; the micro temperature is not explicitly
output.  The only fields in the \truchas\ output relating to the micro
model are maximum undercooling {\tt Max\_Underc} and grain radius 
{\tt Grain\_R} which again do not affect
the macro code in any way.

The effect of the one-way coupling assumption was investigated
in~\cite{starobin04} by comparing to a fully-coupled model.  
There it was found that one-way coupling causes undercoolings to be 
predicted accurately to within one degree celsius all along the length
of a one-dimensional casting.  However, the error observed was a 
systematic overestimation of undercooling, resulting in a systematic
overestimation of nuclei density, and consequently
systematic underestimation of grain size.

\subsection{Test Problem}

In the \truchas\ test suite, the problem 
{\tt grain\_growth\_rnt\_mold.inp} tests this
model.  The parameters of interest appear in the {\tt PHASE\_CHANGE\_PROPERTIES} namelist:
\begin{description}
\item[\tt phase\_change\_model:] Equal to {\tt grain\_growth\_rnt} so that
this model is called.
\item[\tt deltempnucl:] This is $\Delta T_N$, the undercooling where the 
Gaussian nucleation site density distribution peaks.
\item[\tt deltempsigma:] $\Delta T_\sigma$, the width of the distribution.
\item[\tt ndmaxnucl:] $n_{\rm max}$, integral of distribution.
\item[\tt a2\_coeff, a3\_coeff:] $a_2,a_3$, coefficients in cubic polynomial
$v(\Delta T)=a_2(\Delta T)^2+a_3(\Delta T)^3$ that gives tip velocity as
a function of undercooling.  This polynomial is a fit to the $v(\Delta T)$
function given by the Lipton model.
\item[\tt forward\_diffusion\_coefficient:] $D$, solute diffusion coefficient.
\item[\tt full\_coupling\_flag:] Set to zero, because full coupling for the
Rappaz-Th\'{e}voz model is not supported.
\end{description}
