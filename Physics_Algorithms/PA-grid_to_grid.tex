\chapter{Grid to Grid Mapping}

The following chapter presents a \truchas\ capability to map quantities between
tetrahedral and hexahedral meshes. This capability is currently
utilized only when the electromagnetics component 
(see \CHAPH{electromagnetics}) is active, but the algorithms 
presented here are independent of the physical application and thus could
easily be applied in other situations where mapping of quantities between
grids is necessary.

\section{Physics} 

In this section, we provide the physical context for which the grid-to-grid
mapping is currently applied. 

\subsection{Assumptions}

Given a tetrahedral cell-based function $f^T$ (which in this context 
represents the heat per unit volume per unit time that is produced by
electromagnetic Joule heating) and a cell-based function $f^H$ on the main hexahedral
\truchas\ mesh (which in this context represents electric permittivity $\epsilon$,
magnetic permeability $\mu$, and electric conductivity
$\sigma$), the \truchas\ grid-to-grid capability 
\begin{itemize}
\item performs the mapping $f^T\rightarrow f^H$ conservatively
\item performs the mapping $f^H\rightarrow
f^T$ in a constant preserving fashion
\end{itemize}

For the grid-to-grid mapping to work effectively,  
\begin{itemize}
\item both meshes must be face-connected,
\item both meshes must be obtained from mesh files in exodus (genesis, cubit) format,
\item each exodus element block in the tet mesh must be
``contained'' within the region described by the hex mesh element 
block of the same ID, allowing that some tets may extend slightly
outside the region. 
\end{itemize}

\subsection{Interaction With Physics Components}

At this time, the grid-to-grid mapping capability is only necessary
when the electromagnetics component is used with a secondary
(``alternative'') tetrahedral mesh.  In this case, the grid-to-grid
capability facilitates the interaction between the electromagnetics
component and the heat transfer component in both directions.
In one direction, the electromagnetic Joule heating, calculated in the electromagnetics 
component on a tetrahedral mesh, is transferred to the \truchas\ hexahedral mesh to be used as 
a source term in \EQ{HT_conserv}.  In the other direction, 
material properties such as electric permittivity $\epsilon$,
magnetic permeability $\mu$, and electric conductivity $\sigma$ on the hexehadral mesh are transferred
to the tetrahedral mesh for use by the electromagnetics solver. 
Since heat should be conserved in our simulation, we map the Joule heat
source conservatively from the tet mesh to the hex mesh.  
In contrast, for the hex-to-tet mapping, it is important to make sure that constants are
preserved, so that if $f^H\equiv k$ (constant), then mapping $f^H\rightarrow
f^T$ produces $f^T\equiv k$.  This is because unnatural
non uniformities in the electromagnetic quantities would cause spurious
reflections inside of vacuum and uniform media where $\epsilon, \mu$ are constant
and reflection should not occur.

\section{Theory}
\LSECH{gtgtheory}

Our task is to map a cell-based function defined on unstructured
tetrahedra to a cell-based function defined on unstructured hexahedra
conservatively (i.e. such that the integral of the function over the
hex mesh is equal to the integral over the tet mesh).  Let $f^H({\bf x})$ be
the function defined by the $n_H$ hexahedral cell values and $f^T({\bf x})$ be our
function defined by the $n_T$ tet cell values.  
Here ${\bf x}\in\Omega_H=\bigcup^{n_H}_{i=1} H_i$, the computational domain defined by
the union of the hexahedra.  This domain is assumed to be
face-connected.  I.e., we assume that between any two hexahedra
$H_a,H_b$, there is a path
\begin{displaymath}
H_a=H_{i_1}, H_{i_2}, H_{i_3}, \ldots, H_{i_m}=H_b
\end{displaymath}
such that $H_{i_k}$ and $H_{i_{k+1}}$ share a quadrilateral face for
all $1\leq k\leq m-1$.  Similarly, we assume the function $f^T({\bf x})$
is defined on a domain $\Omega_T=\bigcup_{j=1}^{n_T} T_j$ which is the union of
face-connected tetrahedra.  For purposes of our mapping algorithm, we
assume $\Omega_T\subseteq\Omega_H$.  This condition will be slightly
relaxed later.

Let $f^H_i$ be the cell values of $f^H$ on the hex mesh, and $f^T_j$
be the cell values of $f^T$ on the tet mesh.  Let $\chi_{\rule{0pt}{2pt}\atop H_i}$ be the 
characteristic function for hexahedron $H_i$ which is defined by
\begin{displaymath}
\chi_{\rule{0pt}{2pt}\atop H_i}({\bf x})=\left\{ 
\begin{array}{ll} 1, & {\bf x}\in H_i\\
0, & {\bf x}\notin H_i  
\end{array}
\right. .
\end{displaymath}
Similarly, let $\chi_{\rule{0pt}{2pt}\atop T_j}$ be the characteristic function for
tetrahedron $T_j$.  In this notation, $f^H({\bf x})=\sum_i
f^H_i\chi_{\rule{0pt}{2pt}\atop H_i}({\bf x})$ and $f^T({\bf x})=\sum_j
f^T_j\chi_{\rule{0pt}{2pt}\atop T_j}({\bf x})$.  We thus desire that
\begin{equation}
\int_{{I\!\!R}^3}f^H({\bf x})\;dV=\int_{{I\!\!R}^3}f^T({\bf x})\;dV.\label{eq:conservation}
\end{equation}
Obviously if $f^H({\bf x})=f^T({\bf x})\ \forall{\bf x}$, this
would be satisfied.  Of course, this is not possible in general, but
since we have $n_H$ undetermined cell values $f^H_i$ to work with, we
can force $f^H=f^T$ in the ``weak sense'' by requiring
\begin{equation}
\int_{{I\!\!R}^3}f^H\chi_{\rule{0pt}{2pt}\atop H_i}\; dV = \int_{{I\!\!R}^3}f^T\chi_{\rule{0pt}{2pt}\atop H_i}\;
dV, \qquad 1\leq i\leq n_H.\label{eq:weak}
\end{equation}
These $n_H$ equations will fix the $n_H$ unknown coefficients $f^H_i$.
Indeed from $(\ref{eq:weak})$ we have
\begin{displaymath}
\int\sum_k f^H_k\chi_{\rule{0pt}{2pt}\atop H_k}\chi_{\rule{0pt}{2pt}\atop H_i}\; dV =\int\sum_j
f^T_j\chi_{\rule{0pt}{2pt}\atop T_j}\chi_{\rule{0pt}{2pt}\atop H_i}\; dV.
\end{displaymath}
So
\begin{displaymath}
f^H_i|H_i|=\sum_j f^T_j |T_j\cap H_i|,
\end{displaymath}
where $|H_i|$ denotes the volume of $H_i$ and $|T_j\cap H_i|$ denotes
the volume of the intersection of tet $T_j$ with hex $H_i$.  This
implies that we should set
\begin{displaymath}
f^H_i={1\over|H_i|}\sum_j f^T_j |T_j\cap H_i|.
\end{displaymath}
Let $V_{ij}=|H_i\cap T_j|$.  Then $\sum_i V_{ij}=|T_j|$, {\it provided}
$\Omega_T\subseteq\Omega_H$.  (Otherwise $\sum_i V_{ij}\leq |T_j|$.)
Now with this choice for $f_i^H$, 
\begin{eqnarray*}
\int f^H\;dV&=&\sum_i|H_i| f^H_i\\
&=&\sum_i|H_i|\left({1\over|H_i|}\sum_j f^T_j V_{ij}\right)\\
&=&\sum_j f^T_j\sum_i V_{ij}\\
&=&\sum_j f^T_j|T_j|\quad(\hbox{assuming}\ |T_j|=\sum_i
V_{ij})\\
&=&\int f^T\; dV,
\end{eqnarray*}
as required for conservation.

This derivation hinges on the property $|T_j|=\sum_i V_{ij}$.  This
means we can approximately compute the volume $V_{ij}=|H_i\cap T_j|$,
getting a result $\widetilde{V_{ij}}\approx V_{ij}$, and as long as we
have the property
\begin{displaymath}
\sum_i\widetilde{V_{ij}}=|T_j|,
\end{displaymath}
we will get (\ref{eq:conservation}) holding if we set
\begin{equation}
f^H_i={1\over|H_i|}\sum_j f^T_j \widetilde{V_{ij}}.\label{eq:hexval}
\end{equation}

Our strategy is to approximate the values $V_{ij}$ with values
$V'_{ij}$ (using an algorithm to find the approximate volume of
$H_i\cap T_j$) and then compute the corrected quantities
\begin{displaymath}
\widetilde{V_{ij}}={|T_j|\over\sum_k V'_{kj}}V'_{ij}.
\end{displaymath}
With this correction we have that 
\begin{equation}
\sum_i\widetilde{V_{ij}}=|T_j|\quad\forall j,
\LEQ{corrected}
\end{equation}
and thus defining $f^H_i$ by (\ref{eq:hexval}),
we have constructed, using only approximate intersections, a cell-based
function defined over tetrahedra that (a) exactly satisfies
(\ref{eq:conservation}) and (b) approximately satisfies the weak
condition (\ref{eq:weak}). An efficient algorithm to map cell-based 
functions between tets and
hexes thus requires efficient evaluation of the $\widetilde{V_{ij}}$.
We will discuss two algorithms to perform this calculation in 
\SECH{gtgalgorithms}. In addition, in implementing these 
algorithms, we will show that the 
the second requirement, the mapping $f^H\rightarrow
f^T$ is constant preserving is satisfied.


\section{Algorithms}
\LSECH{gtgalgorithms}

An efficient algorithm to map cell-based functions between tets and
hexes requires efficient evaluation of the $\widetilde{V_{ij}}$ term
given in \EQ{corrected}.
This task is naturally broken up into two parts:
\begin{description}
\item[Find intersections:] Find out for which $i,j$ we have
$\widetilde{V_{ij}}\neq 0$.  (Here we assume the approximations
$\widetilde{V_{ij}}$ will only be nonzero if the exact volumes $V_{ij}$
are nonzero.)  $\widetilde{V_{ij}}$ is a sparse matrix:  it has far
fewer than $n_H\cdot n_T$ nonzero entries.  Looping over all $n_H\cdot
n_T$ entries would be fatally inefficient and is unnecessary.  Our
approach has complexity ${\cal O}(N\log N)$ where $N=\max(n_H,n_T)$.
\item[Compute Intersections:] For nonzero $\widetilde{V_{ij}}$, compute
the intersection volumes to within an acceptable tolerance of the
ideal volume $V_{ij}$.  Currently, we compute the $\widetilde{V_{ij}}$
using a point-sampling scheme that works adequately, but which may be
improved in a future release.
\end{description}

\subsection{Finding Intersections}

The idea of our algorithm is to use the assumption of
face-connectedness of the hexahedral and tetrahedral meshes.  This
allows us to traverse the hex and tet meshes simultaneously, finding
the nonzero $\widetilde{V_{ij}}$ values along the way.  We first start
by putting $T_1$ (i.e. tetrahedron number one) onto a stack. Now we
proceed by
popping off the stack the tetrahedron on top of the stack (call it
$T_j$) and we say that we
have ``visited'' this tet; we then place back on the stack all unvisited
tetrahedral face-neighbors of $T_j$.  We
deal with $T_j$ by finding all $H_i$ such that $H_i\cap
T_j\neq\emptyset$ and computing the corresponding approximate
intersection volumes $\widetilde{V_{ij}}$.  Initially, for $T_1$, we
start with $H_1$ and walk along the hex-grid using the hex-grid
connectivity until we find an $H_i$ such that $T_1\cap
H_i\neq\emptyset$.  Then we do some more walking in the hex mesh in
the neighborhood of $H_i$ until we have discovered all the $H_i$ such
that $T_1\cap H_i\neq\emptyset$. 
We continue by popping another tet $T_j$ off the tet stack.  We then
walk towards this new tet on the hex mesh starting not from $H_1$ but
from the last hex $H_i$ that had a nonzero intersection with the
previous tet.

We thus are effectively performing a coordinated walk on the hex and
tet meshes, and this should have a complexity ${\cal O}(N)$, where
$N=\max(n_H,n_T)$.  After all the tets have been visited, we reorder
the volume contributions $\widetilde{V_{ij}}$ so that they can be put in
row-packed sparse matrix form.  This reordering process takes ${\cal
O}(N\log N)$ operations.  

Algorithm 1 gives the overall algorithm for finding nonzero
intersections, sorting the volumes, and normalizing these
contributions.  The output of this algorithm consists of two sparse
matrices $(HT)_{ij}$ and $(TH)_{ji}$.  $(HT)_{ij}$ represents the
conservative tet-to-hex mapping.  If we multiply a tet cell-based
function $f^T_j$ by $(HT)_{ij}$, we have 
\begin{eqnarray*}
\sum_j(HT)_{ij}f^T_j&=&\sum_j{1\over
|H_i|}\widetilde{V_{ij}}f^T_j\\
&=&f^H_i,
\end{eqnarray*}
as required by (\ref{eq:hexval}).

\begin{figure}
\begin{tabbing}

% setup the tabbing for the body of the algorithm
\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\kill

{\bf Algorithm 1: Outer Algorithm [Walking on Tet Mesh]}\\
  \\
  Do $j\ =\ 1,n_T$\\
  \>${\tt onstack}(T_j)\gets{\tt .false.}$\\
  $H\gets H_1$\\
  Put $T_1$ on stack $S_T$\\
  ${\tt onstack}(T_1)\gets{\tt .true.}$\\
  Do while stack $S_T$ nonempty\\
  \>Pop $T_j$ off stack $S_T$\\
  \>Put the face neighbors $T_k$ of $T_j$ with ${\tt
  onstack}(T_k)={\tt .false.}$ onto $S_T$\\
  \>and assign ${\tt onstack}(T_k)\gets{\tt .true.}$ for these tets\\
  \>Pick a point ${\bf x}_k\in T_j$\\
  \>Call {\tt walk\_hex\_pt}$(H,{\bf x}_k)$ [Walk on hex mesh, until
  ${\bf x}_k\in H$]\\
  \>Compute $V'_{ij}\approx|H_i\cap T_j|$ for $H_i=H$ and other $H_i$
  in the neighborhood of $H$ that intersect $T_j$\\
  \>$\widetilde{V_{ij}}\gets{|T_j|\over\sum_k V'_{kj}}V'_{ij}$\\
  Sort entries $\widetilde{V_{ij}}$ using heapsort on the $(i,j)$
  multi-index.\\
  (I.e., $\widetilde{V_{i_1,j_1}}$ appears before
  $\widetilde{V_{i_2,j_2}}$ iff $i_1<i_2$ or ($i_1=i_2$ and
  $j_1<j_2$))\\
  Do $i\ =\ 1,n_H$\\
  \>For each $j$ such that there is a $\widetilde{V_{ij}}$ entry\\
  \>\>$(HT)_{ij}\gets{1\over|H_i|}\widetilde{V_{ij}}$\\
  Write $(HT)_{ij}$ out to file as row-packed sparse matrix\\

  Sort entries $\widetilde{V_{ij}}$ using heapsort on the $(j,i)$
  multi-index.\\
  (I.e., $\widetilde{V_{i_1,j_1}}$ appears before
  $\widetilde{V_{i_2,j_2}}$ iff $j_1<j_2$ or ($j_1=j_2$ and
  $i_1<i_2$))\\
  Do $j\ =\ 1,n_T$\\
  \>For each $i$ such that there is a $\widetilde{V_{ij}}$ entry\\
  \>\>$(TH)_{ji}\gets{1\over|T_j|}\widetilde{V_{ij}}$\\
  Write $(TH)_{ji}$ out to file as row-packed sparse matrix\\

\end{tabbing}
\end{figure}

It is important to note that the output $(TH)_{ji}$ represents a ``constant-preserving'' hex-to-tet
mapping, hence satisfying our original requirements.  If we multiply a cell-based hex function $f^H_i$ by
$(TH)_{ji}$, we have
\begin{eqnarray*}
\sum_i(TH)_{ji}f^H_i&=&\sum_i{1\over|T_j|}\widetilde{V_{ij}}f^H_i\\
&\equiv& f^T_j
\end{eqnarray*}
Although the function $f^T(x)$ defined by the mapping doesn't have the
conservation property\break
 $\int f^T({\bf x})\;dV=\int f^H({\bf x})\;dV$,
we have that if $f^H({\bf x})=k$ (constant), then
\begin{eqnarray*}
\sum_i(TH)_{ji}f^H_i&=&\sum_i{1\over|T_j|}\widetilde{V_{ij}}k\\
&=&k
\end{eqnarray*}
by virtue of (\ref{eq:corrected}).

\begin{figure}
\begin{tabbing}

% setup the tabbing for the body of the algorithm
\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\hspace{0.40in}\=\kill

{\bf Algorithm 2: Walk\_hex\_pt [Walking on hex mesh to point $x$]}\\
\\
  {\tt Walk\_hex\_pt} (H,{\bf x})\\
  \\
  Initially place $H$ on hex stack $S_H$\\
  Do while $S_H$ nonempty\\
  \>Pop $H$ off stack\\
  \>${\tt viable}\gets{\tt .true.}$\\
  \>$H_{\rm top}\gets 0$\\
  \>Do $i=1,6$\\
  \>\>If (${\tt viable}$) then\\
  \>\>\>Let $H_{\rm opp}$ be hex adjacent to $H$ across face $i$\\
  \>\>\>(If no such hex ($i$ on boundary) assign $H_{\rm opp}\gets
  0$.)\\
  \>\>\>If ($H_{\rm opp}\neq 0$) then\\
  \>\>\>\>If ${\bf x}$ on ``wrong'' side of face $i$ (so that ${\bf x}$ is not inside $H$) then\\
  \>\>\>\>\>${\tt viable}\gets{\tt .false.}$\\
  \>\>\>\>\>If $H_{\rm opp}$ was never put on $S_H$ then\\
  \>\>\>\>\>\>$H_{\rm top}\gets H_{\rm opp}$ [$H_{\rm opp}$ will be
  neighbor of $H$ put on \\
  \>\>\>\>\>\>$S_H$ {\it last} so it will be the next hex considered.]\\
  \>\>\>\>Else\\
  \>\>\>\>\>If $H_{\rm opp}$ was never put on $S_H$\\
  \>\>\>\>\>\>Put $H_{\rm opp}$ on $S_H$\\
  \>\>\>Else\\
  \>\>\>\>If ${\bf x}$ on wrong side of face $i$\\
  \>\>\>\>\>${\tt viable}\gets{\tt .false}$\\
  \>\>Else\\
  \>\>\>Let $H_{\rm opp}$ be hex adjacent to $H$ across face $i$\\
  \>\>\>(If no such hex ($i$ on boundary) assign $H_{\rm opp}\gets
  0$.)\\
  \>\>\>If ($H_{\rm opp}\neq 0$) then\\
  \>\>\>\>If $H_{\rm opp}$ was never put on $S_H$ then\\
  \>\>\>\>\>Put $H_{\rm opp}$ on $S_H$\\
  \>If {\tt viable} then\\
  \>\>return [success: ${\bf x}\in H$]\\
  \>Else\\
  \>\>If $H_{\rm top}\neq 0$ then\\
  \>\>\>Put $H_{\rm top}$ on $S_H$\\
  $H\gets 0$ [failure]\\
  return
\end{tabbing}
\end{figure}

The subroutine \texttt{walk\protect\_hex\protect\_pt} (Algorithm 2) accomplishes the task
of ``walking'' from a starting hex $H$ along a path of face-connected
hexes and ending at a new hex $H$ that contains the point ${\bf x}$.
The idea of this algorithm is that given a current hex $H$, the point
is tested against the six faces of $H$.  (Note: although the faces are bilinear, for
each face we
actually test ${\bf x}$ against the plane that passes through the 
midpoints of the 4 linear edges bounding the face.  It is easily 
shown that the 4 midpoints are coplanar.)  If ${\bf x}$ is on the wrong
side of face $i$, then $H$ doesn't contain ${\bf x}$ and we must move
to a hex neighbor of $H$.  We usually move towards ${\bf x}$ by
choosing the neighboring hex across face $i$.  Since nonconvex domains
may necessitate somewhat circuitous walking paths, we prove the
following lemma.

\vfil\eject

\begin{description}
\item[Lemma.] In Algorithm 2, if ${\bf x}$ is in some hex, and we are
given a valid starting hex, Algorithm 2 will successfully find the hex
containing ${\bf x}$.\\
{\bf Proof:} Since we assume the hex grid is face-connected, there is
a face-connected path
\begin{equation}
H=H_0,H_1,H_2,\ldots,H_n\ni{\bf x}\label{eq:path}
\end{equation}
from the starting hex $H$ to $H_n$ which should be the new $H$ returned by
the algorithm.  Algorithm 2 uses a stack $S_H$ and initially $H_0$ is placed
on the stack.  Every time a hex $H$ is popped off the stack, it is
checked for the property ${\bf x}\in H$, and if true, we are done.
Otherwise, all the neighbors of $H$ that have never been on the stack
before are put on the stack.  Since a hex can only be placed at most
once on the stack, the only way the algorithm can fail is that
eventually the stack is empty and $H_n$ has not been found.  In this
case, there is a finite set of hexes that were visited before the 
algorithm failed.  This set is
\begin{displaymath}
{\cal H}_{\rm visited}=\{H\;|\;H\hbox{\ was popped off $S_H$}\}.
\end{displaymath}
Let $H_i$ be the hex in the sequence in (\ref{eq:path}) that belongs
to ${\cal H}_{\rm visited}$ and has maximal index in the sequence in
(\ref{eq:path}).  When $H_i$ was popped off the stack, $H_{i+1}$ was
either placed on the stack because $H_i$ and $H_{i+1}$ are
face-neighbors or it wasn't because it had previously been placed on
the stack.  Either way, this contradicts the assumption that
$H_i\in{\cal H}_{\rm visited}$ has maximal index.  Q.E.D.
\end{description}

It is similarly proven that in Algorithm 1, the traversal of
$\Omega_T$ using the stack $S_T$ successfully visits all tetrahedra once
and terminates.

\subsection{Practical Geometry Considerations}

In practice we only approximately satisfy
$\Omega_T\subseteq\Omega_H$.  For example, if we alternately
discretize some sphere $S$ using tetrahedra and hexahedra, we will not
usually have $\Omega_T=\Omega_H$ and certainly not $\Omega_T=S$ or
$\Omega_H=S$.  This is assuming the tetrahedra have planar facets and
the hexahedra have bilinear facets.  If the boundary nodes of the hex
and tet grids reside on $\partial S$, then ${\rm vol}(S)>{\rm
vol}(\Omega_T)$ and ${\rm vol}(S)>{\rm
vol}(\Omega_H)$.  In fact, even with a large number of elements,
although
\begin{displaymath}
{\rm vol}(S)\approx{\rm vol}(\Omega_T)\approx{\rm vol}(\Omega_H),
\end{displaymath}
probably none of these volumes will be exactly equal.  (In this case,
the function $f^T({\bf x})\equiv 1$ cannot be mapped to $f^H({\bf
x})\equiv 1$ and still satisfy conservation!)  In this scenario, parts
of the tet mesh might slightly poke out of $\Omega_H$.  Since when
Algorithm 1 calls Algorithm 2, it is called with some ${\bf
x}\in\Omega_T$, it is possible that ${\bf x}\notin\Omega_H$ but is
``close to'' it.  We solve this problem by relaxing the test in
Algorithm 2 that asks if ${\bf x}$ is on the ``wrong'' side of face
$i$ in hex $H$.  Instead, now if face $i$ is a boundary face, then we
consider ${\bf x}$ to be on the wrong side of the face if it is more
than some $\epsilon>0$ outside of face $i$.  The user can supply this
$\epsilon$ in the variable {\tt Boundary\_Tolerance}.  If this
variable is not defined, we use the following heuristic.  We set
$\epsilon$ to be equal to the inscribed radius of the tetrahedron $T$
in which ${\bf x}$ resides.  So far, we have not seen this heuristic
fail with our ensemble of test meshes.  

What can go wrong?  If $\epsilon$ is set too small we could have for a
particular tet $T$ that Algorithm 2 will fail to locate a point
${\bf x}\in T$ within any hex, resulting in a warning message.
This is usually not fatal for the overall algorithm, since we will
attempt to locate {\it several} points in $T$ (see Section~\ref{sec:compute_int})
and we can salvage things if even a single point ${\bf x}\in T$ is
located within any hex.  A fatal error occurs if every ${\bf x}\in T$
we try cannot be located with any hex of $\Omega_H$.  If instead, we
set $\epsilon$ too large, we could end up accepting the wrong
$H\in\Omega_H$ and end up ``transporting'' the ``mass'' (i.e., the
integral of $f^T({\bf x})$) a large distance $\epsilon$ from ${\bf x}$
to $H$.

\subsection{Element Blocks}

Currently, the tet and hex meshes must be read in from files in Exodus
format.  In Exodus format, there is the concept of ``Element blocks''
which are effectively a decomposition of the mesh into regions.  Thus,
instead of there being a single $\Omega_T$, there is a decomposition
\begin{displaymath}
\Omega_T=\bigcup_{i=1}^P\Omega^{e_i}_T
\end{displaymath}
and a decomposition
\begin{displaymath}
\Omega_H=\bigcup_{i=1}^Q\Omega^{f_i}_H.
\end{displaymath}
The simplest case is $e_i=i$ and $f_i=i$, but the element block
numbers need not be sequential.  In the general case, we assume
that $\{e_i\;|\;1\leq i\leq P\}\subseteq\{f_i\;|\;1\leq i\leq Q\}$ and
there is for each $1\leq i\leq P$ a correspondence between
$\Omega^{e_i}_T$ and $\Omega^{e_i}_H$.  We assume that
$\Omega^{e_i}_T\subseteq\Omega^{e_i}_H$, or at least to within a
tolerance $\epsilon$ as in the previous discussion.  In this case, our
tet-to-hex algorithm assures conservation within each region,
\begin{displaymath}
\int_{\Omega^{e_i}_H}f^H\;dV=\int_{\Omega^{e_i}_T}f^T\;dV.
\end{displaymath}
Our hex-to-tet algorithm assures preservation of independent constants
in each region:
\begin{displaymath}
f^H\big|_{\Omega^{e_i}_H}\equiv k_i\Rightarrow
f^T\big|_{\Omega^{e_i}_T}\equiv k_i .
\end{displaymath}
The boundaries between adjacent regions
$\Omega^{e_i}_H,\Omega^{e_j}_H$ are treated in a similar fashion as in
the previous discussion about boundary tolerances.  Hence, if a point
${\bf x}\in\Omega^{e_i}_T$ is not in $\Omega^{e_i}_H$, but is within a
distance $\epsilon$ (either given by {\tt Boundary\_Tolerance} or the
default heuristic inscribed radius estimate), then ${\bf x}$ will be
determined to be ``inside'' $\Omega^{e_i}_H$.

Nevertheless, if ${\bf x}\in\Omega^{e_i}_T$ and we are searching for
some $H\in\Omega^{e_i}_H$ with ${\bf x}\in H$, we {\it can} traverse
across regions $\Omega^{e_j}_H$ with $j\neq i$.  Because of this, it
is not necessary for the individual $\Omega^{e_i}_H$ to be
face-connected.  We still only require the whole domains
$\Omega_T,\Omega_H$ to each be face-connected.

\subsection{Computing Intersections}
\label{sec:compute_int}

For a particular tet $T_j$, when we have located a hex $H$ such that
$H\cap T_j\neq\emptyset$, there will be a set of intersection
volumes $V_{ij}=|H_i\cap T_j|$ which will be nonzero, with $H_i=H$ or
$H_i$ being in a neighborhood of $H$.  In this section, we describe
how approximations $V'_{ij}$ to the $V_{ij}$ are computed.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/tetrahedron.pdf}
\caption{Node 
numbering scheme for tetrahedron vertex points, edge
 midpoints, centroid, and face centroids. Using these nodes, the tet
can be split into 4 hexahedra.  We show the edges of ``subhex 1'' in blue.}

\LFIG{fig:tetrahedron}
\end{figure}

In \FIG{fig:tetrahedron}, the tet $T_j$ has four vertices $V_1,V_2,V_3,V_4$.  We add edge
midpoint vertices $V_5,V_6,V_7,$ $V_8,V_9,V_{10}$, and cell centroid
vertex $V_{11}$.  We then add $V_{12}$
at the centroid of the face with vertices $V_1,V_2,V_3$, we add $V_{13}$ at
the centroid of the face with vertices $V_1,V_2,V_4$, we add $V_{14}$ at the
centroid of the face with vertices $V_2,V_3,V_4$, and we add $V_{15}$
at the centroid of the face with vertices $V_3,V_1,V_4$.  With these
definitions, we can split $T_j$ into 4 hexahedra:
\begin{eqnarray*}
\hbox{Subhex\ 1:}\quad V_1\quad V_5\quad V_{12}\quad V_7\quad V_8\quad
V_{13}\quad V_{11}\quad V_{15}\\
\hbox{Subhex\ 2:}\quad V_2\quad V_6\quad V_{12}\quad V_5\quad V_9\quad
V_{14}\quad V_{11}\quad V_{13}\\
\hbox{Subhex\ 3:}\quad V_3\quad V_7\quad V_{12}\quad V_6\quad V_{10}\quad
V_{15}\quad V_{11}\quad V_{14}\\
\hbox{Subhex\ 4:}\quad V_4\quad V_8\quad V_{15}\quad V_{10}\quad V_9\quad
V_{13}\quad V_{11}\quad V_{14}
\end{eqnarray*}

For a subhex $S^q$ of $T_j$\ ($1\leq q\leq 4$), define a trilinear
mapping
\begin{displaymath}
{\bf X}^q(u,v,w):\ [0,1]^3\mapsto S^q.
\end{displaymath}
For any integer $M\geq 1$, we can split $S^q$ into $M^3$ subcubes
\begin{displaymath}
H^q_{klm}={\bf
X}^q\left(\left[\frac{k-1}{M},\frac{k}{M}\right]\times\left[\frac{l-1}{M},\frac{l}{M}\right]\times\left[\frac{m-1}{M},\frac{m}{M}\right]\right),\quad
1\leq k,l,m\leq M.
\end{displaymath}
We place a sample point at the center of each subcube. I.e.,
\begin{equation}
{\bf x}^q_{klm}={\bf
X}^q\left(\frac{k-1/2}{M},\frac{l-1/2}{M},\frac{m-1/2}{M}\right),\quad
1\leq k,l,m\leq M,\quad 1\leq q\leq 4.\label{eq:sample}
\end{equation}
Now suppose we determine which of the $4M^3$ sample points from $T_j$
lie within $H_i$.  We then have the intersection volume estimate
\begin{equation}
|H_i\cap T_j|\approx\sum_{1\leq q\leq 4\atop{1\leq k,l,m\leq M\atop {\bf
 x}^q_{klm}\in H_i}}\frac{1}{M^3}J({\bf
 x}^q_{klm}),\label{eq:samplesum}
\end{equation}
where $J({\bf x}^q_{klm})$ is the Jacobian of the mapping ${\bf X}^q$
evaluated at ${\bf x}^q_{klm}$.  Previously, the following portion of
Algorithm 1 was left somewhat vague:

\begin{tabbing}
\AlgoBody
  \>Pick a point ${\bf x}_k\in T_j$\\
  \>Call {\tt walk\_hex\_pt}$(H,{\bf x}_k)$ [Walk on hex mesh, until
  ${\bf x}_k\in H$]\\
  \>Compute $V'_{ij}\approx|H_i\cap T_j|$ for $H_i=H$ and other $H_i$
  in the neighborhood of $H$ that intersect $T_j$
\end{tabbing}

We can now replace this with the following concrete algorithm:

\begin{tabbing}
\AlgoBody
  \>Do $q=1,4$\\
  \>\>Do $k=1,M$\\
  \>\>\>Do $l=1,M$\\
  \>\>\>\>Do $m=1,M$\\
  \>\>\>\>\>Call {\tt walk\_hex\_pt}$(H,{\bf x}^q_{klm})$\\
  \>\>\>\>\>$V'_{ij}\gets V'_{ij}+\frac{1}{M^3}J({\bf x}^q_{klm})$
  [using $i$ such that $H_i=H$]
\end{tabbing}

Since the approximate volumes are eventually normalized to force exact
conservation, the Jacobians $J({\bf x}^q_{klm})$ need not be evaluated
with utmost accuracy.  Consider that if $k,l,m$ are odd that 
\begin{eqnarray}
& &
\hskip-0.5in
\textstyle
J({\bf
x}^q(\frac{k}{M},\frac{l}{M},\frac{m}{M}))\approx\frac{M^3}{8}\left[
\left({\bf X}^q\left(\frac{k+1}{M},\frac{l}{M},\frac{m}{M}\right)-{\bf
X}^q\left(\frac{k-1}{M},\frac{l}{M},\frac{m}{M}\right)\right)
\times\right.\nonumber\\
& & \hskip-0.5in\ \ \textstyle\left.\left({\bf X}^q\left(\frac{k}{M},\frac{l+1}{M},\frac{m}{M})-{\bf
X}^q(\frac{k}{M},\frac{l-1}{M},\frac{m}{M}\right)\right)
\cdot
\left({\bf X}^q\left(\frac{k}{M},\frac{l}{M},\frac{m+1}{M})-{\bf
X}^q(\frac{k}{M},\frac{l}{M},\frac{m-1}{M}\right)\right)
\right]\label{eq:approxjac}
\end{eqnarray}

By (\ref{eq:sample}), ${\bf X}^q(\frac{k}{M},\frac{l}{M},\frac{m}{M})$
is located to leading order at the center of the hex formed by the
points
\begin{equation}
{\bf x}^q_{k,l,m},{\bf x}^q_{k+1,l,m},{\bf x}^q_{k,l+1,m},\cdots,{\bf x}^q_{k+1,l+1,m+1}.\label{eq:8pts}
\end{equation}
We use the single estimate (\ref{eq:approxjac}) to give the Jacobian
at the eight points in (\ref{eq:8pts}).  The Jacobian is evaluated
with odd $i,j,k$ indices, and so at
only $\frac{M^3}{8}$ points per subhex.  To make this ``Jacobian
grid'' fit nicely inside the $M^3$ grid of points ${\bf x}^q_{klm}$,
we use only even values of $M$.

\subsection{Noise Level of Mapping}

The mapping $(HT)_{ij}$ maps tet cell-based functions conservatively
to hex cell-based functions.  The mapping $(TH)_{ji}$ maps constant
hex cell-based functions to constant tet cell-based functions (with
the same constant).  We now ask how well $(HT)_{ij}$ maps constants
and how well $(TH)_{ij}$ conserves mapped quantities.

Let $S_i\equiv\sum_j(HT)_{ij}={1\over|H_i|}\sum_j\widetilde{V_{ij}}$
be the row sums of the tet-to-hex mapping produced by our
algorithm. 

Suppose now that $f^T({\bf x})=k$ (constant), with $k\neq 0$.  Then
the {\it relative noise level of}\ $f^H({\bf x})$ (produced by
$(HT)_{ij}$) has
\begin{eqnarray*}
\mbox{\it relative noise level}&=&\max_{\bf x}{|f^H({\bf x})-k|\over|k|}\\
&=&\max_i{|\sum_j(HT)_{ij}k-k|\over|k|}\\
&=&\max_i|S_i-1|
\end{eqnarray*}

Now suppose $f^H({\bf x})>0$ on $\Omega_H$.  The {\it relative
conservation error} of $f^T({\bf x})$ (produced by $(TH)_{ji}$) has
\begin{eqnarray*}
\mbox{\it relative conservation error}&=&
{|\int f^T\; dV - \int f^H\;dV|\over|\int f^H\;dV|}\\
&=&{|\sum_j|T_j|(\sum_i{1\over|T_j|}\widetilde{V_{ij}}f^H_i)-\sum_i|H_i|f^H_i|\over
\sum_i|H_i|f^H_i}\\
&=&{|\sum_i\sum_j\widetilde{V_ij}f^H_i-\sum_i|H_i|f^H_i|\over\sum_i|H_i|f^H_i|}\\
&\leq&{\sum_i|H_i|f^H_i
\left|{1\over |H_i|}\sum_j\widetilde{V_{ij}}-1\right|
\over\sum_i|H_i|f^H_i}\\
&\leq&{\sum_i|H_i|f^H_i (\max_k|S_k-1|)\over\sum_i|H_i|f^H_i}\\
&=&\max_k|S_k-1|
\end{eqnarray*}

Thus $\max_i|S_i-1|$ is both a bound on the relative noise level produced
by the mapping $(HT)_{ij}$ and the relative conservation error
produced by the mapping $(TH)_{ji}$.

If $H_i\subseteq\Omega_T$ it is true that as $M$ is increased, our
volume calculations will become more accurate, so that 
\begin{displaymath}
\widetilde{V_{ij}}\rightarrow |H_i\cap T_j|
\end{displaymath}
and
\begin{displaymath}
S_i=\sum_j (HT)_{ij}={1\over|H_i|}\sum_j
\widetilde{V_{ij}}\rightarrow{1\over|H_i|}\sum_j|H_i\cap T_j|=1.
\end{displaymath}
Thus it would seem reasonable to increase $M$ until
$\max_i|S_i-1|$ becomes less than the user-specified {\tt
Transfer\_Noise\_Tolerance} which is to be a bound on the relative
noise level of $f^H({\bf x})$, as defined previously.
However, it is not true that necessarily $H_i\subseteq\Omega_T$, and
in fact we can only say that 
\begin{displaymath}
S_i\rightarrow{1\over|H_i|}|H_i\cap\Omega_T|\leq 1.
\end{displaymath}
So for example if $\Omega_T$ is a strict subset of $\Omega_H$ and
there is a hex $H_i$ that doesn't intersect $\Omega_T$, then any
function $f^T$ mapped by $(TH)_{ji}$ will suffer a 100\% relative
conservation error at $H_i$, since $\int_{H_i}f^H\;dV$ will be
completely ``lost''.  So it makes more sense to halt refinement of
tets (i.e., halt increasing $M$ for tets) when all the row sums
$S_i$ have settled down.
Thus, in practice, we generate a sequence of row sums
$S^1_i,S^2_i,\ldots$ by redoing our algorithm with $M$ equal to a sequence of increasing values
$M_1,M_2,\ldots$, and we halt at iteration $n$ when for each $i$ we
have
\begin{equation}
|S^n_i-S^{n-1}_i|<{\tt Transfer\_Noise\_Tolerance}.\label{eq:conv_crit}
\end{equation}
To make things more efficient, we actually only refine those
tetrahedra $T_j$ with larger values of $M$ that are involved in those
row sums $S_i$ that have not converged according to the criterion
given by (\ref{eq:conv_crit}).

In practice, we have found that using the default value of 0.1 for 
{\tt Transfer\_Noise\_Tolerance} is entirely adequate for our ensemble
of test meshes.




