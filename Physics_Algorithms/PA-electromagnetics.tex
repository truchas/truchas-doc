{% local definitions go here

\newcommand{\Bf}{\vec{B}} \newcommand{\Hf}{\vec{H}}
\newcommand{\Df}{\vec{D}} \newcommand{\Ef}{\vec{E}}
\newcommand{\Jf}{\vec{J}}
\newcommand{\Bh}{\vec{B}_h} \newcommand{\Eh}{\vec{E}_h}

\newcommand{\be}{\mathbf{e}}\newcommand{\bb}{\mathbf{b}}
\newcommand{\bg}{\mathbf{g}}
\newcommand{\wk}[1]{\vec{w}^{(#1)}}

\newcommand{\W}[1]{\ensuremath{\mathcal{W}^{#1}}}
\newcommand{\Th}{\ensuremath{\mathcal{T}_h}}

\newcommand{\e}{\varepsilon}
\newcommand{\eps}{\epsilon}
\newcommand{\x}{\mathbf{x}}
\newcommand{\hdt}{\tfrac{\Delta t}{2}}

\newcommand{\grad}{\nabla}
\newcommand{\curl}{\nabla\times}
\renewcommand{\div}{\nabla\cdot}
\newcommand{\pd}[2]{\frac{\partial{#1}}{\partial{#2}}}

\chapter{Electromagnetics}
\LCHAPH{electromagnetics}

This chapter presents the electromagnetic (EM) modeling capabilities in
\truchas, and discusses the solution procedure used for EM field equations
in detail.  Although the core EM solver is reasonably general, its current
application within \truchas\ is narrowly focused on treating induction
heating problems---problems where a workpiece is surrounded by an induction
coil that generates a low-frequency magnetic field.  This focus has guided
the special manner EM is coupled to the other physics in \truchas.  While
there are a number of limitations in this initial release, the current
induction heating capability is still quite useful, and will be improved in
future releases.  It is also expected that the EM modeling capabilities will
be expanded in the future to include other phenomena, such as magnetic
stirring of fluids due to Lorentz forces.

\section{Physics}

\subsection{Assumptions}

The EM implementation is based on a direct solution of Maxwell's equations
in the time domain.  The material parameters---permittivity, permeability,
and electrical conductivity---are assumed to be isotropic and linear in the
fields.  Imposed currents within the computational domain are not presently
treated.

Other assumptions are:

\textbf{Low frequency driving field:}  The frequency of the magnetic driving
field is assumed to be low in the sense that its wavelength is large compared
to the diameter of the computational domain.  This is due to the way in which
the driving fields are constructed.  The displacement current term in
Maxwell's equations, which is normally dropped in the low-frequency regime, is
retained in this implementation.

\textbf{Separation of time scales:}  The time scale associated with the EM
fields, which is inversely proportional to the driving field frequency, is
assumed to be much shorter than the time scale associated with the other
physics, particularly heat conduction.  It is this assumption which allows the
special coupling used in this implementation.  So while we require a
low-frequency driving field, its frequency must not be too low.

\textbf{External induction coil:} Because imposed currents are not treated in
the present implementation, it is not possible to directly model induction
coils within the computational domain.  Instead, the influence of the coil is
captured through boundary conditions on the magnetic field.  What is assumed
is that there is sufficient free-space separation between the coil and
workpiece, so that the boundary of the computational domain may be placed at
an intermediate point far enough away from the workpiece so that the reaction
fields induced in the workpiece are not unduly affected by the boundary, yet
still excluding the coil itself.

\textbf{Tetrahedral mesh:}  The current numerical procedure used to solve the
EM field equations requires a tetrahedral mesh.  Thus \truchas\ now operates
with a pair of meshes: a secondary tet mesh used for the EM calculations and
the primary mesh used elsewhere in \truchas.  Quantities are interpolated
from one mesh to the other as needed.

\textbf{Fixed domain type:}  The computational domain is required to be one of
a few specific types.   This is due to the current inability to associate
boundary conditions to specific portions of the boundary of the mesh used for
the EM calculation.  This limitation will be removed in a future release. 
This issue is discussed in more detail later in this section.

\subsection{Equations}

The evolution of general time-varying EM fields is governed by Maxwell's
equations, which in SI units can be written as
\begin{align}
  \pd{\Bf}{t} &= - \curl\Ef, && \text{(Faraday's law)}, \label{em:faraday} \\
  \pd{\Df}{t} + \Jf &= \curl\Hf, && \text{(Ampere-Maxwell law)},
      \label{em:ampere} \\
  \div\Df &= \rho, && \text{(Gauss' electric law)}, \label{em:gauss}\\
  \div\Bf &= 0, && \text{(Gauss' magnetic law)}. \label{em:gauss-mag}
\end{align}
Here $\Ef$ is the electric field intensity, $\Hf$ is the magnetic field
intensity, $\Bf$ is the magnetic flux density, $\Df$ is the electric flux
density, $\Jf$ is the electric current density, and $\rho$ is the electric
charge density.  In addition we have a continuity equation governing
conservation of charge,
\begin{equation} \label{em:cont}
  \pd{\rho}{t} + \div\Jf = 0.
\end{equation}

The fields are connected by the constitutive relations
\begin{align}
  \Df &= \eps\Ef, \\
  \Bf &= \mu\Hf, \\
  \Jf &= \sigma\Ef + \Jf_{\text{src}},
\end{align}
where the parameters $\eps$, $\mu$, and $\sigma$ denote, respectively, the
permittivity, permeability, and conductivity of the medium.  Currently we
consider only isotropic materials, where these parameters are scalars.  The
field $\Jf_{\text{src}}$ denotes an imposed current density.  However, in this
release we assume $\Jf_{\text{src}}\equiv0$; this will be relaxed in a future
version.

Finally, the Joule heat, $q$, which couples the electromagnetics to heat
conduction, is simply computed as
\begin{equation}
  q = \sigma \|\Ef\|^2.
\end{equation}
This is a power density.

\subsection{Boundary Conditions}

Internally to \truchas, either the tangential component of the electric field
or the tangential component of the magnetic field may be specified on the
boundary.  Specifically, suppose the boundary is partitioned into two
disjoint parts, $\Gamma_1$ and $\Gamma_2$, either one possibly empty.  Then
the boundary conditions are
\begin{align}
  \hat{n}\times\Ef &= \hat{n}\times\Ef_b, \quad\text{on $\Gamma_1$},
      &&\text{(Type 1)} \label{em:type1} \\
  \hat{n}\times\Hf &= \hat{n}\times\Hf_b, \quad\text{on $\Gamma_2$},
      &&\text{(Type 2)} \label{em:type2} 
\end{align}
where $\hat{n}$ denotes the outward normal to the boundary, and $\Ef_b(\x,t)$
and $\Hf_b(\x,t)$ are given boundary data.

Currently, however, there is no means for the user to associate boundary
conditions to specific portions of the boundary of the secondary tetrahedral
mesh used for the EM calculation.  As a result the domain $\Omega$ is limited
to three special types that are typical of basic induction heating problems:
\begin{itemize}
\item Full cylinder, $\Omega=\{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;z_1 \le z
      \le z_2 \}$
\item Half cylinder, $\Omega = \{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;x \ge 0,
      \;z_1 \le z \le z_2 \}$
\item Quarter cylinder, $\Omega = \{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;x,y
      \ge 0, \;z_1 \le z \le z_2 \}$
\end{itemize}
The half and quarter cylinder domains are assumed to be associated with a full
cylinder problem that possesses half and quarter symmetry, respectively.  If
present, the symmetry boundaries $\partial\Omega\cap\{x=0\}$ and
$\partial\Omega\cap\{y=0\}$ are assigned to $\Gamma_1$ with $\Ef_b=\vec{0}$. 
This is consistent with the symmetry that allows a globally azimuthal electric
field.  The remaining portion of the boundary is assigned to $\Gamma_2$, and
several pre-defined choices for $\Hf_b$ may be selected.

\subsubsection{Magnetic driving fields.} The current choices for $\Hf_b$ all
correspond to the magnetic field produced by a cylindrical coil, of some
configuration, that carries a sinusoidally varying current.  They may be
expressed in the form
\begin{equation}
  \Hf_b(\x,t) = I \sin(2\pi f t) \vec{h}(\x),
\end{equation}
where $I$ is the peak current per unit length, $f$ is the linear frequency,
and $\vec{h}(\x)$ is a vector field that depends on the geometric
configuration of the coil.

\subsection{Interaction With Other Physics}

The EM field solution is independent of all other physics, except 
temperature, and that only if the EM material parameters are temperature
dependent. Heat conduction is coupled to EM through the
Joule heat which serves as a volumetric heat source in the enthalpy equation. The coupling between the
two physics is greatly simplified, however, by the fundamental assumption that
the time scale associated with the EM fields (inversely proportional to the
frequency of the magnetic driving field) is much shorter than the time scale
associated with heat conduction.  In this case, the EM field persists in a
periodic steady-state equilibrium that continually adjusts to the slowly
evolving temperature field.  To find this steady state, it suffices to solve
Maxwell's equation to the periodic steady state, starting from arbitrary
initial conditions (zero fields, for example), while temporarily freezing all
other physics.  Finally, the rapid temporal fluctuations in the derived Joule
heat is removed by averaging over a cycle, yielding the time-averaged heat
source used in heat conduction.

\subsection{Material Properties}

The material parameters relevant to EM are the permittivity $\eps$,
permeability $\mu$, and electrical conductivity $\sigma$.  The first two are
expressed in terms of their free-space values, $\eps=\eps_0\eps_r$ and
$\mu=\mu_0\mu_r$, where $\eps_r$ and $\mu_r$ are the relative permittivity and
permeability, respectively.  The parameters $\eps_r$, $\mu_r$, and $\sigma$
are specified in the material input and may be temperature dependent.  The
free-space parameters $\eps_0$ and $\mu_0$ are pre-assigned SI-unit values,
but these may be overridden if necessary.

\section{Algorithms}
\label{em:Algorithms}

\subsection{The Whitney Complex}

Let \Th\ be a discretization of the domain $\Omega$ into a
face-conforming tetrahedral mesh.  Let $\mathcal{N}$, $\mathcal{E}$,
$\mathcal{F}$, and $\mathcal{K}$ denote the sets of nodes, oriented edges and
faces, and tetrahedral cells in the mesh.  Each edge and face appears just
once with a fixed but arbitrary orientation.

We define the Whitney family of finite element spaces \W0, \W1, \W2, and \W3
that are associated with the nodes, edges, faces, and cells of \Th.  Let
$\phi_n(\x)$ denote the familiar continuous, piecewise-linear, `hat' function
that equals 1 at node $n$, and 0 at all other nodes.  Then for each oriented
edge $\e=[m,n]$ define the vector function
\begin{equation}
  \wk1_\e(\x) = \phi_m\grad\phi_n - \phi_n\grad\phi_m,
\end{equation}
and for each oriented face $f = [l,m,n]$ define the vector function
\begin{equation}
  \wk2_f(\x) = 2(\phi_l\grad\phi_m\times\grad\phi_n +
      \phi_m\grad\phi_n\times\grad\phi_l + \phi_n\grad\phi_l\times\grad\phi_m).
\end{equation}
These functions have the following properties (see \cite{BossavitCEM}):
\begin{itemize}
\item $\wk1_\e$ is tangentially continuous, and $\wk2_f$ is normally
      continuous across each face.
\item The tangential component of $\wk1_\e$ is constant on each edge, its
      circulation along edge $\e$ equals 1, and equals 0 for all other edges.
\item The normal component of $\wk2_f$ is constant on each face, its flux
      across face $f$ equals 1, and equals 0 for all other faces.
\item The sets $\{\wk1_\e\}_{\e\in\mathcal{E}}$ and
      $\{\wk2_f\}_{f\in\mathcal{F}}$ are linearly independent.
\end{itemize}
We then let $\W1 = \text{span}\{\wk1_\e\}$ and $\W2 = \text{span}\{\wk2_f\}$.
The degrees of freedom (DOF) for \W1 are the circulations of a vector field
along the oriented edges, and the DOF for for \W2 are the fluxes of a vector
field across the oriented faces.  For completeness we also let $\W0 =
\text{span}\{\phi_n\}$, whose DOF are simply the nodal values of a scalar
field, and let \W3 denote the space of piecewise constant functions with
respect to \Th, whose DOF are the total masses of a scalar field on the cells.
(We could just as well used the constant cell values as the DOF---it's just a
matter of a change in basis.)

The functions in \W0 are continuous, hence $\grad u$ is defined for all
$u\in\W0$; that is, \W0 is grad-conforming, $\W0\subset H(\Omega,\grad)$.
Similarly, it follows from the above properties that \W1 is curl-conforming,
$\W1\subset H(\Omega,\curl)$, and \W2 is div-conforming, $\W2\subset
H(\Omega,\div)$.  Moreover, it can be shown (non-trivial) that
$\grad(\W0)\subset\W1$, $\curl(\W1)\subset\W2$, and $\div(\W2)\subset\W3$.
These key properties of the Whitney complex are summarized in the following
diagram:
\begin{equation}
  \W0 \xrightarrow{\quad\displaystyle\grad\quad}
  \W1 \xrightarrow{\quad\displaystyle\curl\quad}
  \W2 \xrightarrow{\quad\displaystyle\div\quad} \W3
\end{equation}
In particular it follows that $\curl(\grad u)\equiv 0$ for all $u$ in the
finite dimensional space \W0, and $\div(\curl\vec{v})\equiv0$ for all
$\vec{v}$ in \W1.

\subsection{Spatial Discretization}

Not all of the equations (\ref{em:faraday})--(\ref{em:cont}) are independent.
Equation (\ref{em:gauss-mag}) follows from (\ref{em:faraday}) provided that
$\div\Bf=0$ at the initial time.  Equation (\ref{em:cont}) follows from
(\ref{em:ampere}) and (\ref{em:gauss}).  Moreover, since the charge density
is of no direct interest, we ignore (\ref{em:gauss}), other than to require
that $\div\Df=\rho$ at the initial time.  These conditions on the initial
fields may be trivially satisfied by considering a charge-free initial state
that is free of fields, as we do here.  What remains then are the first order
curl equations (\ref{em:faraday}) and (\ref{em:ampere}), which after
eliminating $\Df$, $\Hf$, and $\Jf$, become
\begin{align}
  \pd{\Bf}{t} &= -\curl\Ef, \label{em:Beq}\\
  \eps\pd{\Ef}{t} + \sigma\Ef &= \curl\tfrac{1}{\mu}\Bf. \label{em:Eeq}
\end{align}
Let $\Bh\in\W2$ and $\Eh\in\W1$ be finite element approximants to $\Bf$ and
$\Ef$, respectively.  Because of the inclusion $\curl(\W1)\subset\W2$,
Faraday's law (\ref{em:Beq}) may be discretized directly,
\begin{equation}\label{em:dB}
  \pd{\Bh}{t} = -\curl\Eh.
\end{equation}
Ampere's law (\ref{em:Eeq}), however, must be interpreted weakly.  The
weak-form equation is
\begin{equation}\label{em:dE}
  \int_\Omega \bigl(\eps\pd{\Eh}{t}+\sigma\Eh\bigr)\cdot\vec{w} =
  \int_\Omega \tfrac{1}{\mu}\Bh\cdot\curl\vec{w} +
  \int_{\Gamma_2} \vec{w}\cdot\hat{n}\times\Hf_b, \quad
  \text{for all $\vec{w}\in\W1$}.
\end{equation}
The type 2 boundary condition (\ref{em:type2}) is imposed naturally through
the boundary integral.  The type 1 boundary condition (\ref{em:type1}) on the
tangential component of $\Ef$ is also easily imposed on $\Eh$ whose DOF are
the circulation of the electric field along the edges.

The discrete system (\ref{em:dB}) and (\ref{em:dE}) can be recast in matrix
form.  Suppose the edges and faces have been enumerated,
$\mathcal{E}=\{\e_j\}_{j=1}^{N_\e}$ and $\mathcal{F}=\{f_j\}_{j=1}^{N_f}$,
and write $\Eh$ and $\Bh$ in terms of their respective bases,
\begin{align}
  \Eh(\x,t) &= \sum_{j=1}^{N_\e} e_j(t) \;\wk1_{\e_j}(\x), \\
  \Bh(\x,t) &= \sum_{j=1}^{N_f}  b_j(t) \;\wk2_{f_j}(\x).
\end{align}
Set $\be(t) = (e_1, \ldots, e_{N_\e})$ and $\bb(t) = (b_1, \ldots, b_{N_f})$.
Then (\ref{em:dB}) and (\ref{em:dE}) can be written
\begin{gather}
  \dot\bb = - C\be, \label{em:db2} \\
  M_1(\eps)\dot\be + M_1(\sigma)\be = C^T M_2(\mu^{-1})\bb + \mathbf{g}(t),
     \label{em:de2}
\end{gather}
where $C$ is the matrix representation of the curl operator as a map from \W1
to \W2, $\mathbf{g}(t)$ stems from the boundary integral, and $M_1(\eps)$,
$M_1(\sigma)$, and $M_2(\mu^{-1})$ are mass matrices defined by
\begin{equation}
  M_k(\omega) = \Biggl(\!\!\Biggl( \int_\Omega \wk{k}_i \cdot \wk{k}_j \,
    \omega(\x) d\x \Biggr)\!\!\Biggr)
\end{equation}

\subsection{Time Discretization}

We discretize the time derivatives in (\ref{em:db2}) and (\ref{em:de2}) using
the trapezoid rule.  Using superscripts to denote the time level, and a time
step of $\Delta t$ we obtain
\begin{gather}
  \bb^{n+1} - \bb^n = -\hdt C (\be^{n+1} + \be^n), \label{em:db3}\\
  \begin{split}
    &M_1(\eps)(\be^{n+1} - \be^n) + \hdt M_1(\sigma)(\be^{n+1} + \be^n) = \\
    &\qquad\qquad\hdt C^T M_2(\mu^{-1})(\bb^{n+1} + \bb^n) + \hdt(\bg^{n+1} + \bg^n)
  \end{split}\label{em:de3}
\end{gather}
As an implicit method, it allows us to take time steps whose size is of the
order of the temporal variation of the magnetic driving field, which is
tremendously larger than the CFL stability condition imposed on an explicit
method.  Moreover, the trapezoid rule has the very desirable property that the
equation of global energy conservation is exactly discretized.

To solve this system we first eliminate $\bb^{n+1}$ from (\ref{em:de3}) using
(\ref{em:db3}), to obtain
\begin{multline} \label{em:de4}
  \Bigl[ M_1(\eps) + \hdt M_1(\sigma) +
    \bigl(\hdt\bigr)^2 C^T M_2(\mu^{-1})C \Bigr] \be^{n+1} = \\\hfil
  \Bigl[ M_1(\eps) - \hdt M_1(\sigma) -
    \bigl(\hdt\bigr)^2 C^T M_2(\mu^{-1})C \Bigr] \be^n + \\
    \Delta t C^T M_2(\mu^{-1})\bb^n + \hdt(\bg^{n+1} + \bg^n)
\end{multline}
The solution $\be^{n+1}$ of this equation is then substituted into
(\ref{em:db3}) to obtain $\bb^{n+1}$.

\subsection{Linear Solution}

The coefficient matrix of (\ref{em:de4}) is symmetric, positive-definite, and
we use the conjugate gradient method to solve the system.  It is, however, extremely
poorly conditioned when taking the relatively huge time steps we require. 
This is due to the fact that the term $C^T M_2(\mu^{-1})C$ , which dominates
in the free-space region where $\sigma=0$, has a large nullspace.  As a
result, the convergence rate is very poor using symmetric Gauss-Seidel (GS) as
a preconditioner.  To remedy this we have adapted a relaxation scheme proposed
by Hiptmair \cite{Hiptmair98} for use as a preconditioner.  In the following algorithm,
$G$ denotes the matrix representation of the gradient operator as a map from
the finite dimensional spaces \W0 to \W1.

\textbf{Hiptmair Preconditioning for $A\be=\mathbf{f}$:}
\begin{enumerate}
\item Symmetric GS step for $A\be=\mathbf{f}$ (with 0 initial guess)
\item Transfer residual to nodes: $\mathbf{r}' \leftarrow G^T(\mathbf{f}-A\be)$
\item Symmetric GS step on $A'\be'=\mathbf{r}'$, $A'\equiv G^TAG$
\item Correct edge based solution: $\be \leftarrow \be + G\be'$
\item Final symmetric GS step on $A\be=\mathbf{f}$ to symmetrize.
\end{enumerate}

}
