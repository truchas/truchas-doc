
\chapter{Displacement, Sliding Interface and Contact Constraints}
\LAPP{contact}

\section{Notation}
%\begin{tabbing}
%XXXX \= \kill
\begin{tabular}{l c  p{6in}}
$\bf{u}$ & - & the displacement vector for the entire domain \\
$\vec{u}_j$ & - & the displacement vector (in ndim dimensions) for node $j$\\
$\vec{f}_j$ & - & the portion of the force vector at node $j$ that is a 
                 function of the displacement vector $\bf{u}$ \\
$\vec{r}_j$ & - & the portion of the force vector at node $j$ that is a 
                 function of source terms such as thermal strain \\
$s$ & - & the relative displacement of the nodes across a gap interface 
         $\hat{n} \cdot (\vec{u}_k - \vec{u}_j)$\\
$\Lambda$ & - & a contact function for a gap interface that depends on $s$ .  
                $\Lambda = 1$ for nodes in contact but not in tension, and 
                $\Lambda = 0$ for nodes not in contact.
\end{tabular}

\section{One normal displacement}

\begin{figure}[h]
\centering
\includegraphics[scale=1.0]{figures/surf-node.pdf}
\end{figure}

For specified displacement magnitude $d$ along surface normal $\hat{n}$
\begin{equation}
([I] - [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r_j}) - c[\hat{n}\hat{n}^T](\vec{u}_j - d \hat{n}) = 0
\end{equation}
or
\begin{equation}
([I] - [\hat{n}\hat{n}^T]) \vec{f}_j - c[\hat{n}\hat{n}^T]\vec{u}_j =
- ([I] - [\hat{n}\hat{n}^T]) \vec{r_j} - c[\hat{n}\hat{n}^T] d \hat{n} 
\label{eq:1nd}
\end{equation}

\subsection{Preconditioning matrix}

The displacement constraints are added to the preconditioning matrix by replacing the coefficients 
for $f_j$ with the coefficients of the left hand side of equation \ref{eq:1nd}.

\section{Two normal displacements} 

\begin{figure}[h]
\centering
\includegraphics[scale=1.0]{figures/edge-node.pdf}
\end{figure}

Where $d_1$ and $d_2$ are specified displacements in directions 
$\hat{n}_1$ and $\hat{n}_2$, and a vector along the edge between the 
two surface is $\hat{t} \propto \hat{n}_1 \times \hat{n}_2$,
\begin{equation}
[\hat{t}\hat{t}^T] (\vec{f}_j + \vec{r_j}) - c([I] - [\hat{t}\hat{t}^T])(\vec{u}_j - \vec{a}) = 0
\end{equation}
or
\begin{equation}
[\hat{t}\hat{t}^T] \vec{f}_j - c([I] - [\hat{t}\hat{t}^T])\vec{u}_j = 
- [\hat{t}\hat{t}^T] \vec{r_j} - c([I] - [\hat{t}\hat{t}^T])\vec{a}
\label{eq:2nd}
\end{equation}
where the vector $a$ is constructed as
\begin{equation}
\vec{a} = b_1 \hat{n}_1 + b_2 \hat{n}_2
\end{equation}
where 
\begin{equation}
\left[ \begin{array}{c} b_1 \\ b_2 \end{array} \right] = 
\frac{1}{1-\cos^2 \theta} \left[ \begin{array}{cc} 1 & -\cos \theta \\
 -\cos \theta & 1 \end{array} \right] \left[ \begin{array}{c} d_1 \\ 
d_2 \end{array} \right]
 \end{equation}
and $\cos \theta = \hat{n}_1 \cdot \hat{n}_2$

\subsection{Preconditioning matrix}

The displacement constraints are added to the preconditioning matrix by replacing the coefficients 
for $f_j$ with the coefficients of the left hand side of equation \ref{eq:2nd}.


\section{Three normal displacements}

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{figures/3-surf.pdf}
\end{figure}

Solve for unique $\vec{a}$ once during initialization, 

\begin{equation}
  \left[ \begin{array}{c} \hat{n}_1 \\ \hat{n}_2 \\ \hat{n}_3 \end{array} \right] [\vec{a}] = 
  \left[ \begin{array}{c} d_1 \\ d_2 \\ d_3 \end{array} \right]
\end{equation}

and use the trivial equation

\begin{equation}
  c \vec{u}_j = c \vec{a}
  \label{eq:3nd}
\end{equation}

\subsection{Preconditioning matrix}

The displacement constraints are added to the preconditioning matrix by replacing the coefficients 
for $f_j$ with the coefficients of the left hand side of equation \ref{eq:3nd}.  In this case only 
the diagonal coefficients are non-zero.

\section{One normal constraint}

\begin{figure}[h]
\centering
\includegraphics[scale=1.0]{figures/gap-node.pdf}
\end{figure}

For node $j$ with contact function $\Lambda$ :
\begin{equation}
([I] - \Lambda [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r_j}) + \Lambda [\hat{n}\hat{n}^T](\vec{f}_j + \vec{r_j} + 
\vec{f}_k + \vec{r_k}) + \Lambda c [\hat{n}\hat{n}^T](\vec{u}_k - \vec{u}_j) = 0
\end{equation}
or
\begin{equation}
\vec{f}_j + \Lambda [\hat{n}\hat{n}^T](\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = - \vec{r_j} 
\label{eq:1nc}
\end{equation}
%where $s = \hat{n} \cdot (\vec{u}_k - \vec{u}_j)$, and $\Lambda$ is a
%function that is (approximately) zero for (($s > 0$) .and. ($\hat{n} \cdot (\vec{f}_k + \vec{r}_k) < 0$))
% and one for $s < 0$

\subsection{Preconditioning matrix}

In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.

\begin{equation}
\vec{f}_j - \Lambda [\hat{n}\hat{n}^T](\vec{f}_j - c(\vec{u}_k - \vec{u}_j)) = 
- ([I] - \Lambda [\hat{n}\hat{n}^T]) \vec{r_j} 
\label{eq:1nc_pre}
\end{equation}


\section{Two normal constraints} 

There are two normal vectors $\hat{n}$ and $\hat{m}$, one for each interface 
and one unit tangent vector $\hat{t} \propto \hat{n} \times \hat{m}$.

\begin{figure}[h]
%\centering
\includegraphics[scale=0.3]{figures/2-gap.pdf}
\includegraphics[scale=0.3]{figures/2-gap-y.pdf}
\includegraphics[scale=0.3]{figures/2-gap-t.pdf}
\end{figure}

For node $j$ there are multiple possibilities:

\subsection{No Contact}
\begin{equation}
\vec{f}_j = - \vec{r_j}
\end{equation}

\subsection{Contact with only one surface}

As before:

gap interface 1

\begin{equation}
([I] - \Lambda [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r_j}) + \Lambda [\hat{n}\hat{n}^T](\vec{f}_j + \vec{r_j} + 
\vec{f}_k +  \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = 0  
\label{eq:2nc_1a}
\end{equation}

which is equivalent to

\begin{equation}
\vec{f}_j + \Lambda [\hat{n}\hat{n}^T](\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = - \vec{r_j} 
\label{eq:2nc_1}
\end{equation}

or gap interface 2

\begin{equation}
([I] - \Lambda [\hat{m}\hat{m}^T]) (\vec{f}_j + \vec{r_j}) + \Lambda [\hat{m}\hat{m}^T](\vec{f}_j + \vec{r_j} + 
\vec{f}_k +  \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = 0  
\label{eq:2nc_2a}
\end{equation}

which is equivalent to

\begin{equation}
\vec{f}_j + \Lambda [\hat{m}\hat{m}^T](\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = - \vec{r_j} 
\label{eq:2nc_2}
\end{equation}

\subsection{Contact with two surfaces but only one node}

Interpolating between no contact and contact with both surfaces,

\begin{equation}
([I] - \Lambda_1 \Lambda_2 ([I] - [\hat{t}\hat{t}^T])) (\vec{f}_j + \vec{r}_j) + 
\Lambda_1 \Lambda_2 ([I] - [\hat{t}\hat{t}^T])(\vec{f}_j + 
\vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = 0
\label{eq:2nc_3a}
\end{equation}

or

\begin{equation}
\vec{f}_j + \Lambda_1 \Lambda_2 ([I] - [\hat{t}\hat{t}^T])(\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = - \vec{r_j} 
\label{eq:2nc_3}
\end{equation}


Equations \ref{eq:2nc_1a}, \ref{eq:2nc_2a} and \ref{eq:2nc_3a} can be combined to interpolate linearly
between the various cases,
\begin{multline}
([I] - \Lambda_1 [\hat{n}\hat{n}^T] - \Lambda_2 [\hat{m}\hat{m}^T] - 
\Lambda_1 \Lambda_2 ([I] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T] - [\hat{t}\hat{t}^T])) (\vec{f}_j + \vec{r}_j) + \\
(\Lambda_1 [\hat{n}\hat{n}^T] + \Lambda_2 [\hat{m}\hat{m}^T] +
\Lambda_1 \Lambda_2 ([I] - [\hat{t}\hat{t}^T] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T])
(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = 0
\label{eq:2nc_4a}
\end{multline}

or

\begin{equation}
\vec{f}_j + (\Lambda_1 [\hat{n}\hat{n}^T] + \Lambda_2 [\hat{m}\hat{m}^T] + 
\Lambda_1 \Lambda_2 ([I] - [\hat{t}\hat{t}^T] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T]))
(\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) = - \vec{r_j} 
\label{eq:2nc_4}
\end{equation}

Note that if $\hat{n} = \hat{m}$ and there is only one gap node then we use 
equation \ref{eq:1nc}.  If the angle between $\hat{n}$ and $\hat{m}$ is small,
we may want to average the normals and also use equation \ref{eq:1nc}.
 
\subsection{Contact with two surfaces but two different nodes}

At an intersection of three different gap surfaces along an internal edge:

If $\Lambda_{kj}$ is the contact function between nodes $j$ and $k$,

\begin{multline}
([I] - \Lambda_{kj} [\hat{n}\hat{n}^T] - \Lambda_{lj} [\hat{m}\hat{m}^T] - 
\Lambda_{kj} \Lambda_{lj} ([I] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T] 
- [\hat{t}\hat{t}^T])) (\vec{f}_j + \vec{r}_j) + \\
\Lambda_{kj} [\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
\Lambda_{lj} [\hat{m}\hat{m}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j)) + \\
\Lambda_{kj} \Lambda_{lj} (([I] - [\hat{t}\hat{t}^T]) ( \vec{f}_j + \vec{r}_j + \vec{f}_k + 
\vec{r}_k + \vec{f}_l + \vec{r}_l + c(\vec{u}_k - \vec{u}_j) + c(\vec{u}_l - \vec{u}_j)) - \\
[\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) -
[\hat{m}\hat{m}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j))) = 0
\label{eq:2nc_5a}
\end{multline}

If 
$ C_{kj} = \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)$

\begin{equation}
\vec{f}_j + 
\Lambda_{kj} [\hat{n}\hat{n}^T] C_{kj} +
\Lambda_{lj} [\hat{m}\hat{m}^T] C_{lj} + 
\Lambda_{kj} \Lambda_{lj} (([I] - [\hat{t}\hat{t}^T]) (C_{kj} + C_{lj}) -
[\hat{n}\hat{n}^T] C_{kj} - 
[\hat{m}\hat{m}^T] C_{lj})
 = - \vec{r_j} 
\label{eq:2nc_5}
\end{equation}

\subsubsection{Two surfaces, two nodes, but only one normal}

If $\hat{n} = \hat{m}$, then $\hat{t}$ is indeterminate, and we have the sum of 
two single constraints (equation \ref{eq:1nc}).  This will be the case for a ``T'' 
intersection of three gap interfaces.  (It is not currently possible to specify a ``T''
intersection of only two interfaces, but the equations would be the same.)  In this case
the last term of equation \ref{eq:2nc_4} is eliminated:
%modified to give:
%\begin{multline}
%([I] - \Lambda_{kj} [\hat{n}\hat{n}^T] - \Lambda_{lj} [\hat{n}\hat{n}^T] + 
%\Lambda_{kj} \Lambda_{lj} [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + \\
%\Lambda_{kj} [\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
%\Lambda_{lj} [\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j)) = 0
%\label{eq:2nc_6a}
%\end{multline}
\begin{multline}
([I] - \Lambda_{kj} [\hat{n}\hat{n}^T] - \Lambda_{lj} [\hat{n}\hat{n}^T] + 
\Lambda_{kj} \Lambda_{lj} [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + \\
\Lambda_{kj} [\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
\Lambda_{lj} [\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j)) + \\
\Lambda_{kj} \Lambda_{lj} ([\hat{n}\hat{n}^T] ( \vec{f}_j + \vec{r}_j + \vec{f}_k + 
\vec{r}_k + \vec{f}_l + \vec{r}_l + c(\vec{u}_k - \vec{u}_j) + c(\vec{u}_l - \vec{u}_j)) - \\
[\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) -
[\hat{n}\hat{n}^T] (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j))) = 0
\label{eq:2nc_6a}
\end{multline}

or

%\begin{multline}
%([I] - (\Lambda_{kj} + \Lambda_{lj} - \Lambda_{kj} \Lambda_{lj}) [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + \\
%[\hat{n}\hat{n}^T] (\Lambda_{kj} (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
%\Lambda_{lj} (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j))) = 0
%\label{eq:2nc_6b}
%\end{multline}
\begin{multline}
([I] - (\Lambda_{kj} + \Lambda_{lj} - \Lambda_{kj} \Lambda_{lj}) [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + \\
[\hat{n}\hat{n}^T] (\Lambda_{kj} (\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
\Lambda_{lj} (\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j)) + \\
\Lambda_{kj} \Lambda_{lj} (( \vec{f}_j + \vec{r}_j + \vec{f}_k + 
\vec{r}_k + \vec{f}_l + \vec{r}_l + c(\vec{u}_k - \vec{u}_j) + c(\vec{u}_l - \vec{u}_j)) - \\
(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) -
(\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j))) = 0
\label{eq:2nc_6b}
\end{multline}

or (finally)

\begin{multline}
\vec{f}_j + 
[\hat{n}\hat{n}^T] (\Lambda_{kj} (\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) +
\Lambda_{lj} (\vec{f}_l + \vec{r}_l + c(\vec{u}_l - \vec{u}_j))) = - \vec{r}_j
\label{eq:2nc_6}
\end{multline}
%
%\begin{equation}
%\vec{f}_j + 
%\Lambda_{kj} (1 - \frac{\Lambda_{lj}}{2})[\hat{n}\hat{n}^T] C_{kj} +
%\Lambda_{lj} (1 - \frac{\Lambda_{kj}}{2})[\hat{m}\hat{m}^T] C_{lj}
% = - \vec{r_j} 
%\label{eq:2nc_6}
%\end{equation}
%
\subsection{Preconditioning matrix}

In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.

\section{Three normal constraints} 

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{figures/3-gap.pdf}
\end{figure}

For node $j$:
\begin{itemize}
\item Three gap unit normals $\hat{n}$ $\hat{m}$ $\hat{p}$
\item Three tangent unit vectors:
\begin{itemize}
  \item[] $\hat{t}_1 = \frac{\hat{n} \times \hat{m}}{\| \hat{n} \times \hat{m} \| }$
  \item[] $\hat{t}_2 = \frac{\hat{m} \times \hat{p}}{\| \hat{m} \times \hat{p} \| }$
  \item[] $\hat{t}_3 = \frac{\hat{p} \times \hat{n}}{\| \hat{p} \times \hat{n} \| }$
\end{itemize}
\end{itemize}

Restricted to the case where there is only one node across all three gap surfaces:

\begin{multline}
([I] - \Lambda_1 [\hat{n}\hat{n}^T] - \Lambda_2 [\hat{m}\hat{m}^T] - \Lambda_3 [\hat{p}\hat{p}^T] +
\Lambda_1 \Lambda_2 ([\hat{t}_1 \hat{t}_1^T] + [\hat{n}\hat{n}^T] + [\hat{m}\hat{m}^T] - [I]) + \\
\Lambda_2 \Lambda_3 ([\hat{t}_2 \hat{t}_2^T] + [\hat{m}\hat{m}^T] + [\hat{p}\hat{p}^T] - [I]) +
\Lambda_3 \Lambda_1 ([\hat{t}_3 \hat{t}_3^T] + [\hat{p}\hat{p}^T] + [\hat{n}\hat{n}^T] - [I]) + \\
\Lambda_1 \Lambda_2 \Lambda_3 (2[I] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T] - [\hat{p}\hat{p}^T] -
[\hat{t}_1 \hat{t}_1^T] - [\hat{t}_2 \hat{t}_2^T] - [\hat{t}_3 \hat{t}_3^T]))(\vec{f}_j + \vec{r}_j) + \\
(\Lambda_1 [\hat{n}\hat{n}^T] + \Lambda_2 [\hat{m}\hat{m}^T] + \Lambda_3 [\hat{p}\hat{p}^T] +
\Lambda_1 \Lambda_2 ([I] - [\hat{t}_1 \hat{t}_1^T] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T]) + \\
\Lambda_2 \Lambda_3 ([I] - [\hat{t}_2 \hat{t}_2^T] - [\hat{m}\hat{m}^T] - [\hat{p}\hat{p}^T]) +
\Lambda_3 \Lambda_1 ([I] - [\hat{t}_3 \hat{t}_3^T] - [\hat{p}\hat{p}^T] - [\hat{n}\hat{n}^T]) + \\
\Lambda_1 \Lambda_2 \Lambda_3 (-2[I] + [\hat{n}\hat{n}^T] + [\hat{m}\hat{m}^T] + [\hat{p}\hat{p}^T] +
[\hat{t}_1 \hat{t}_1^T] + [\hat{t}_2 \hat{t}_2^T] + [\hat{t}_3 \hat{t}_3^T]))(\vec{f}_j + \vec{r}_j +
\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) \\
 = 0 
\label{eq:3nc_a}
\end{multline}

or

\begin{multline}
\vec{f}_j + (
\Lambda_1 [\hat{n}\hat{n}^T] +\Lambda_2 [\hat{m}\hat{m}^T] + \Lambda_3 [\hat{p}\hat{p}^T] + 
\Lambda_1 \Lambda_2 ([I] - [\hat{t}_1 \hat{t}_1^T] - [\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T])+ \\
\Lambda_2 \Lambda_3 ([I] - [\hat{t}_2 \hat{t}_2^T] - [\hat{m}\hat{m}^T] - [\hat{p}\hat{p}^T])+ 
\Lambda_3 \Lambda_1 ([I] - [\hat{t}_3 \hat{t}_3^T] - [\hat{p}\hat{p}^T] - [\hat{n}\hat{n}^T])- \\
\Lambda_1 \Lambda_2 \Lambda_3 (2[I] - [\hat{t}_1 \hat{t}_1^T] - [\hat{t}_2 \hat{t}_2^T] - [\hat{t}_3 \hat{t}_3^T] - 
[\hat{n}\hat{n}^T] - [\hat{m}\hat{m}^T] - [\hat{p}\hat{p}^T])) (\vec{f}_k + \vec{r}_k + c(\vec{u}_k - \vec{u}_j)) \\
 = - \vec{r_j} 
\label{eq:3nc_b}
\end{multline}

\subsection{Preconditioning matrix}

In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.

\section{One normal constraint and one normal displacement} 
Intersection between a gap interface and a surface with a displacement constraint:

\begin{figure}[h]
\centering
\includegraphics[scale=1.0]{figures/gap-surf.pdf}
\end{figure}

For node $j$:
\begin{itemize}
\item Surface unit normal $\hat{n}$
\item Gap unit normal $\hat{n}_g$
\item Tangent unit vector $\hat{t} = \frac{\hat{n} \times \hat{n}_g}{\| \hat{n} \times \hat{n}_g \| }$
\item Vector to complete the orthogonal set $\hat{v} = \hat{t} \times \hat{n}$
\item $\cos \theta = \hat{v} \cdot \hat{n}_g$
\end{itemize}

\begin{multline}
(([I] - [\hat{n}\hat{n}^T]) - \Lambda ([I] - [\hat{n}\hat{n}^T] - 
[\hat{t}\hat{t}^T])) (\vec{f}_j + \vec{r}_j) + \\
\Lambda [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k) 
+ \Lambda c \cos^2 \theta [\hat{v}\hat{v}^T](\vec{u}_k  - \vec{u}_j) - 
c[\hat{n}\hat{n}^T](\vec{u}_j- d \hat{n}) = 0
\label{eq:1nc_1nd_a}
\end{multline}

%\begin{multline}
%(([I] - [\hat{n}\hat{n}^T]) - \Lambda ([I] - [\hat{n}\hat{n}^T] - [\hat{t}\hat{t}^T])) \vec{f}_j + \\
%\Lambda [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k) 
%+ \Lambda c \cos^2 \theta [\hat{v}\hat{v}^T](\vec{u}_k  - \vec{u}_j) - 
%c[\hat{n}\hat{n}^T]\vec{u}_j = \\
%-([I] - [\hat{n}\hat{n}^T]) \vec{r}_j - c[\hat{n}\hat{n}^T] d \hat{n}
%\end{multline}
%
%\begin{equation}
%[\hat{t}\hat{t}^T] (\vec{f}_j + \vec{r}_j) + [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k) 
%       + c \cos^2 \theta [\hat{v}\hat{v}^T](\vec{u}_k 
%       - \vec{u}_j) - c[\hat{n}\hat{n}^T](\vec{u}_j- d \hat{n}) = 0
%\end{equation}

or (using $[\hat{n}\hat{n}^T] + [\hat{v}\hat{v}^T] + [\hat{t}\hat{t}^T] = [I]$):
%\begin{equation}
%([I] - [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + \Lambda [\hat{v}\hat{v}^T](\vec{f}_k + \vec{r}_k 
%       + c \cos^2 \theta (\vec{u}_k - \vec{u}_j)) 
%       - c[\hat{n}\hat{n}^T](\vec{u}_j- d \hat{n}) = 0
%\end{equation}
\begin{equation}
([I] - [\hat{n}\hat{n}^T]) \vec{f}_j + \Lambda [\hat{v}\hat{v}^T](\vec{f}_k + \vec{r}_k 
       + c \cos^2 \theta (\vec{u}_k - \vec{u}_j)) 
       - c[\hat{n}\hat{n}^T]\vec{u}_j = -([I] - [\hat{n}\hat{n}^T]) \vec{r}_j - c[\hat{n}\hat{n}^T] d \hat{n}
\label{eq:1nc_1nd}
\end{equation}

\subsection{Preconditioning matrix}
In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.  However, the normal
displacement constraint is included in the same way as for a normal
displacement without the interface constraints.

\section{Two displacements, one normal constraint}
Intersection between a gap interface and a surface with a displacement constraint:

\begin{figure}[h]
\centering
\includegraphics[scale=1.0]{figures/gap-edge.pdf}
\end{figure}


\begin{itemize}
\item Surface unit normals $\hat{n}_1$ and $\hat{n}_2$
\item Gap unit normal $\hat{n}_g$
\item Tangent unit vector $\hat{t} = \frac{\hat{n}_1 \times \hat{n}_2}{\| \hat{n}_1 \times \hat{n}_2 \| }$
\item $\cos \theta = \hat{t} \cdot \hat{n}_g$
\item $d_1$ and $d_2$ are specified displacements in directions $\hat{n}_1$ and $\hat{n}_2$
\end{itemize}

\begin{equation}
\vec{a} = b_1 \hat{n}_1 + b_2 \hat{n}_2
\end{equation}
where 
\begin{equation}
\left[ \begin{array}{c} b_1 \\ b_2 \end{array} \right] = 
\frac{1}{1-\cos^2 \theta} \left[ \begin{array}{cc} 1 & -\cos \theta \\
 -\cos \theta & 1 \end{array} \right] \left[ \begin{array}{c} d_1 \\ d_2 \end{array} \right]
\end{equation}

For node $j$:
\begin{equation}
(1 - \Lambda)[\hat{t}\hat{t}^T](\vec{f}_j + \vec{r}_j) +  \Lambda [\hat{t}\hat{t}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
c \cos^2 \theta (\vec{u}_k - \vec{u}_j)) - c([I] - [\hat{t}\hat{t}^T])(\vec{u}_j- \vec{a}) = 0
\label{eq:2nc_1nd_a}
\end{equation}
or
\begin{equation}
[\hat{t}\hat{t}^T] \vec{f}_j + 
\Lambda [\hat{t}\hat{t}^T](\vec{f}_k + \vec{r}_k + 
c \cos^2 \theta (\vec{u}_k - \vec{u}_j)) - c([I] - [\hat{t}\hat{t}^T])\vec{u}_j = 
- [\hat{t}\hat{t}^T] \vec{r}_j - c([I] - [\hat{t}\hat{t}^T])\vec{a}
\label{eq:2nc_1nd}
\end{equation}

\subsection{Preconditioning matrix}
In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.  However, the normal
displacement constraint is included in the same way as for a two normal
displacements without the interface constraint.

\section{One displacement, two normal constraints}

\begin{itemize}
\item Surface normal $\hat{n}$
\item $d$ is the magnitude of the specified displacement.
\item gap normal vectors $\hat{m}$ and $\hat{p}$, one for each interface
\item two unit tangent vectors: $\hat{t}_1 = \frac{\hat{n} \times \hat{m}}{\| \hat{n} \times \hat{m} \| }$ and $\hat{t}_2 = \frac{\hat{n} \times \hat{p}}{\| \hat{n} \times \hat{p} \| }$
\item Vectors $\hat{v}$ and $\hat{w}$ are defined to make two orthogonal sets with 
$\hat{n}$: $\hat{v} = \hat{t}_1 \times \hat{n}$ and $\hat{w} = \hat{t}_2 \times \hat{n}$
\item $\cos{\theta_1} = \hat{v} \cdot \hat{m}$ and $\cos{\theta_2} = \hat{w} \cdot \hat{p}$
\end{itemize}

For node $j$ there are multiple possibilities:

\subsection{No Contact}
\begin{equation}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j - c [\hat{n}\hat{n}^T] \vec{u}_j = 
- ([I] - [\hat{n}\hat{n}^T])\vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n} 
\end{equation}

\subsection{Contact with only one surface}

As before:

gap interface 1

\begin{multline}
([I] - [\hat{n}\hat{n}^T] -\Lambda_1 ([I] - [\hat{n}\hat{n}^T] -[\hat{t_1}\hat{t_1}^T])) (\vec{f}_j + \vec{r}_j) + 
\Lambda_1 [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
\cos^2{\theta_1} c (\vec{u}_k - \vec{u}_j))  \\
-  c [\hat{n}\hat{n}^T] (\vec{u}_j - d \hat{n}) = 0 
\label{eq:d2nc_1a}
\end{multline}

Using $([I] - [\hat{n}\hat{n}^T] -[\hat{t_1}\hat{t_1}^T]) = [\hat{v}\hat{v}^T]$ we obtain:

\begin{equation}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j + [\hat{v}\hat{v}^T](\vec{f}_k + \vec{r}_k + 
\cos^2{\theta_1} c (\vec{u}_k - \vec{u}_j))  - 
c [\hat{n}\hat{n}^T] \vec{u}_j = 
- ([I] - [\hat{n}\hat{n}^T])\vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n} 
\label{eq:d2nc_1}
\end{equation}

or gap interface 2

\begin{equation}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j + [\hat{w}\hat{w}^T](\vec{f}_k + \vec{r}_k + 
\cos^2{\theta_2} c(\vec{u}_k - \vec{u}_j))  
c [\hat{n}\hat{n}^T] \vec{u}_j = 
- ([I] - [\hat{n}\hat{n}^T])\vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n} 
\label{eq:d2nc_2}
\end{equation}

\subsection{Contact with two surfaces but only one node}

%\begin{multline}
%(1 - \Lambda_1 \Lambda_2) ([I] - [\hat{n}\hat{n}^T]) (\vec{f}_j + \vec{r}_j) + 
%\Lambda_1 \Lambda_2 ([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
%(\cos^2{\theta_1} + \cos^2{\theta_2}) c (\vec{u}_k - \vec{u}_j))  \\
%-  c [\hat{n}\hat{n}^T] (\vec{u}_j - d \hat{n}) = 0 
%\label{eq:d2nc_3a}
%\end{multline}

%\begin{equation}
%([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
%(\cos^2{\theta_1} + \cos^2{\theta_2}) c(\vec{u}_k - \vec{u}_j)) 
%- c [\hat{n}\hat{n}^T] (\vec{u}_j - d \hat{n})= 0
%\end{equation}

If $\Lambda_1 = \Lambda_2 = 1$

\begin{equation}
([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r_j} + \vec{f}_k + \vec{r}_k + 
(\cos^2{\theta_1} + \cos^2{\theta_2}) c(\vec{u}_k - \vec{u}_j)) 
- c [\hat{n}\hat{n}^T] \vec{u}_j = - c [\hat{n}\hat{n}^T] d \hat{n} 
\label{eq:d2nc_3}
\end{equation}


Equations \ref{eq:d2nc_1}, \ref{eq:d2nc_2} and \ref{eq:d2nc_3} can be combined with contact functions 
for each interface $\Lambda_1$ and $\Lambda_2$,

\begin{multline}
([I] - [\hat{n}\hat{n}^T] - \Lambda_1 ([I] - [\hat{n}\hat{n}^T] - [\hat{t_1}\hat{t_1}^T]) -
\Lambda_2 ([I] - [\hat{n}\hat{n}^T] - [\hat{t_2}\hat{t_2}^T]) +
\Lambda_1 \Lambda_2 ([I] - [\hat{n}\hat{n}^T] - [\hat{t_1}\hat{t_1}^T] - [\hat{t_2}\hat{t_2}^T])) (\vec{f}_j + \vec{r_j}) + \\
\Lambda_1 [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) +
\Lambda_2 [\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_2}(\vec{u}_k - \vec{u}_j)) + \\
\Lambda_1 \Lambda_2 (([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
c (\cos^2{\theta_1} + \cos^2{\theta_2})(\vec{u}_k - \vec{u}_j)) - 
[\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) - \\
[\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_2}(\vec{u}_k - \vec{u}_j))) - 
[\hat{n}\hat{n}^T] c (\vec{u}_j - d \hat{n}) = 0
\label{eq:d2nc_4a}
\end{multline}
using the identities $[\hat{n}\hat{n}^T] + [\hat{t_1}\hat{t_1}^T] + [\hat{v}\hat{v}^T] = [I]$ and
$[\hat{n}\hat{n}^T] + [\hat{t_2}\hat{t_2}^T] + [\hat{w}\hat{w}^T] = [I]$,
\begin{multline}
([I] - [\hat{n}\hat{n}^T] - \Lambda_1 [\hat{v}\hat{v}^T] - \Lambda_2 [\hat{w}\hat{w}^T] +
\Lambda_1 \Lambda_2 ([\hat{v}\hat{v}^T] + [\hat{w}\hat{w}^T] - ([I] - [\hat{n}\hat{n}^T]))) (\vec{f}_j + \vec{r_j}) + \\
\Lambda_1 [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) +
\Lambda_2 [\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_2}(\vec{u}_k - \vec{u}_j)) + \\
\Lambda_1 \Lambda_2 (([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + 
c (\cos^2{\theta_1} + \cos^2{\theta_2})(\vec{u}_k - \vec{u}_j)) - 
[\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) - \\
[\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_2}(\vec{u}_k - \vec{u}_j))) - 
[\hat{n}\hat{n}^T] c (\vec{u}_j - d \hat{n}) = 0
\label{eq:d2nc_4b}
\end{multline}

or

\begin{multline}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j +
(\Lambda_1 [\hat{v}\hat{v}^T] + \Lambda_2 [\hat{w}\hat{w}^T] + 
\Lambda_1 \Lambda_2 ([I] - [\hat{n}\hat{n}^T] - [\hat{v}\hat{v}^T] - [\hat{w}\hat{w}^T])) 
(\vec{f}_k + \vec{r}_k) + \\(\Lambda_1 [\hat{v}\hat{v}^T] \cos^2{\theta_1} + \Lambda_2 [\hat{w}\hat{w}^T] \cos^2{\theta_2} +
\Lambda_1 \Lambda_2 (([I] - [\hat{n}\hat{n}^T])(\cos^2{\theta_1} + \cos^2{\theta_2}) - \\
[\hat{v}\hat{v}^T] \cos^2{\theta_1} - [\hat{w}\hat{w}^T] \cos^2{\theta_2})) (c(\vec{u}_k - \vec{u}_j)) -
c [\hat{n}\hat{n}^T] \vec{u}_j  \\
= - ([I] - [\hat{n}\hat{n}^T]) \vec{r}_j - c [\hat{n}\hat{n}^T] d \hat{n}
\label{eq:d2nc_4}
\end{multline}

Note that if $\hat{m} = \hat{p}$ and there is only one gap node then we use 
equation \ref{eq:1nc_1nd}.  If the angle between $\hat{m}$ and $\hat{p}$ is small,
we may want to average the normals and also use equation \ref{eq:1nc_1nd}.
 

\subsection{Contact with two surfaces but two different nodes}

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{figures/surf-2-gap.pdf}
\end{figure}

At an intersection of three different gap surfaces along an internal edge and a normal constraint
at the free surface (after equation \ref{eq:d2nc_4b}):
\begin{multline}
([I] - [\hat{n}\hat{n}^T] - \Lambda_1 [\hat{v}\hat{v}^T] - \Lambda_2 [\hat{w}\hat{w}^T] +
\Lambda_1 \Lambda_2 ([\hat{v}\hat{v}^T] + [\hat{w}\hat{w}^T] - ([I] - [\hat{n}\hat{n}^T]))) (\vec{f}_j + \vec{r_j}) + \\
\Lambda_1 [\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) +
\Lambda_2 [\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c \cos^2{\theta_2}(\vec{u}_l - \vec{u}_j)) + \\
\Lambda_1 \Lambda_2 (([I] - [\hat{n}\hat{n}^T])(\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + \vec{f}_l + \vec{r}_l +
c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j) + c\cos^2{\theta_2}(\vec{u}_l - \vec{u}_j)) - 
[\hat{v}\hat{v}^T](\vec{f}_j + \vec{r}_j + \vec{f}_k + \vec{r}_k + c \cos^2{\theta_1}(\vec{u}_k - \vec{u}_j)) - \\
[\hat{w}\hat{w}^T](\vec{f}_j + \vec{r}_j + \vec{f}_l + \vec{r}_l + c \cos^2{\theta_2}(\vec{u}_l - \vec{u}_j))) - 
[\hat{n}\hat{n}^T] c (\vec{u}_j - d \hat{n}) = 0
\label{eq:d2nc_5b}
\end{multline}



If $ C_{kj} = \vec{f}_k + \vec{r}_k + \cos^2{\theta_1} c(\vec{u}_k - \vec{u}_j)$, 
$ C_{lj} = \vec{f}_l + \vec{r}_l + \cos^2{\theta_2} c(\vec{u}_l - \vec{u}_j)$,
and $\Lambda_{kj}$ and $\Lambda_{lj}$ are the contact functions between nodes $j$ and $k$, 
and $l$ and $j$ respectively:

\begin{multline}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j + 
\Lambda_{kj} [\hat{v}\hat{v}^T] C_{kj} +
\Lambda_{lj} [\hat{w}\hat{w}^T] C_{lj} + \\
\Lambda_{kj} \Lambda_{lj} (([I] - [\hat{n}\hat{n}^T]) (C_{kj} + C_{lj}) -
[\hat{v}\hat{v}^T] C_{kj} - 
[\hat{w}\hat{w}^T] C_{lj}) -
c [\hat{n}\hat{n}^T] \vec{u}_j \\
= - ([I] - [\hat{n}\hat{n}^T]) \vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n}
\label{eq:d2nc_5}
\end{multline}

\subsubsection{Two surfaces, two nodes, but only one normal}

If $\hat{m} = \hat{p}$, then sliding along the two surfaces should be
allowed and we have the sum of two single constraints (equation
\ref{eq:1nc_1nd}).  This will be the case for a ``T'' intersection of
three gap interfaces.  (It is not currently possible to specify a
``T'' intersection of only two interfaces, but the equations would be
the same.)  In this case the last term of equation \ref{eq:d2nc_5} is
not used:

\begin{equation}
([I] - [\hat{n}\hat{n}^T])\vec{f}_j + 
\Lambda_{kj} [\hat{v}\hat{v}^T] C_{kj} +
\Lambda_{lj} [\hat{w}\hat{w}^T] C_{lj} - \\
c [\hat{n}\hat{n}^T] \vec{u}_j \\
= - ([I] - [\hat{n}\hat{n}^T]) \vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n}
\label{eq:d2nc_6}
\end{equation}

%\begin{multline}
%([I] - [\hat{n}\hat{n}^T])\vec{f}_j + 
%\Lambda_{kj} [\hat{v}\hat{v}^T] C_{kj} +
%\Lambda_{lj} [\hat{w}\hat{w}^T] C_{lj} - \\
%c [\hat{n}\hat{n}^T] \vec{u}_j \\
%= - ([I] - [\hat{n}\hat{n}^T]) \vec{r_j} - c [\hat{n}\hat{n}^T] d \hat{n}
%\end{multline}

\subsection{Preconditioning matrix}
In the current version of the code normal constraints and contact are
not incorporated in the preconditioning matrix.  However, the normal
displacement constraint is included in the same way as for a normal
displacement without the interface constraints.


