\chapter{Discrete Operators}
\LAPP{discrete_ops}

Discrete operators in \truchas are defined as those
functions that discretely approximate continuous vector calculus
operators. The four operators currently approximated are the gradient
($\GRAD{}$), curl ($\CURL{}$), divergence ($\DIV{}$), and average
($\AVG{}$) operators.  The operators act on either scalar ($\phi$) or
vector ($\mathbf{v}$) input data and return as output either scalar or
vector data, as summarized in the table below.

\begin{table}[ht]
\caption{\truchas discrete operators.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|} \hline
Operator & Input & Output & Input Type & Output Type \\ \hline\hline
gradient   & $\phi$       & $\GRAD{\phi}$ & scalar & vector \\ \hline
curl       & $\mathbf{v}$ & $\CURL{v}$    & vector & vector \\ \hline
divergence & $\mathbf{v}$ & $\DIV{v}$     & vector & scalar \\ \hline
average    &  $\phi$      & $\AVG{\phi}$  & scalar & scalar \\ \hline
\end{tabular}
\end{center}
\LTBL{disops}
\end{table}

\section{Summary}

The vector and scalar data input to the discrete operators itemized in
\TBL{disops} can be located at either cell centroids or cell
nodes. Similarly, the scalar or vector discrete operator output can
be located at cell centroids, cell face centroids, or cell nodes,
as summarized in \TBL{disops_loc} below.

\begin{table}[ht]
\caption{Discrete operator input/output data location.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|} \hline
Operator & Input & Output & Input Type & Output Type \\ \hline\hline
face gradient   & $\phi_c$       & $\GRAD_{\hspace{-3pt}f}{\phi_c}$ & scalar  
   & vector \\ \hline
cell gradient   & $\phi_c$       & $\GRAD_{\hspace{-3pt}c}{\phi_c}$ & scalar 
   & vector \\ \hline
cell curl       & $\mathbf{v}_c$ & $\GRAD_{\hspace{-3pt}c}\times\mathbf{v}_c$& vector 
   & vector \\ \hline
cell divergence & $\mathbf{v}_c$ & $\GRAD_{\hspace{-3pt}c}\cdot\mathbf{v}_c$& vector 
   & scalar \\ \hline
cell divergence & $\mathbf{v}_n$ & $\GRAD_{\hspace{-3pt}c}\cdot\mathbf{v}_n$& vector 
   & scalar \\ \hline
face average    &  $\phi_c$      & $\langle\phi_c\rangle_{\hspace{-2pt}f}$& scalar 
   & scalar \\ \hline
node average    &  $\phi_c$      & $\langle\phi_c\rangle_{\hspace{-2pt}n}$ & scalar 
   & scalar \\ \hline
\end{tabular}
\end{center}
\LTBL{disops_loc}
\end{table}

\noindent
The location of data is indicated by the subscripts $c$, $f$, and $n$,
for cell centroid, face centroid, and cell node, respectively.

Each discrete operator in \TBL{disops_loc} above will be
derived from an expanded data set if its location is on or near a
boundary.  If the discrete operator location coincides exactly with a
cell face centroid on a boundary, then it is \textit{on a boundary}.
If the discrete operator location is \textit{near a boundary}, then
then at least one of the cells in the domain of dependence will have
at least one face or node on a boundary.  A cell in the domain of
dependence is an immediate neighbor whose data effects the value of
the discrete operator. For these cases, additional boundary condition
(BC) data is used to determine the discrete operator, as itemized in
the table below.

\begin{table}[ht]
\caption{Conditions for including additional BC data in determining discrete operators.}
\begin{center}
\begin{tabular}{|c|c|c|} \hline
         & Additional BC Data & Source of BC Data: \\
Operator & is Included if $\ldots$ & All boundary faces $\ldots$ \\ \hline\hline
face gradient   & face owns $\geq 1$ boundary node & sharing $\geq 1$ face node \\ \hline
cell gradient   & cell owns $\geq 1$ boundary node & of cell and neighbors \\ \hline
cell curl       & cell owns $\geq 1$ boundary node & of cell and neighbors \\ \hline
cell divergence & cell owns $\geq 1$ boundary node & of cell and neighbors \\ \hline
face average    & face owns $\geq 1$ boundary node & sharing $\geq 1$ face node \\ \hline
node average    & node on a boundary & sharing boundary node \\ \hline
\end{tabular}
\end{center}
\LTBL{disops_bc}
\end{table}

\noindent
As is evident in \TBL{disops_bc} above, BC data is included
in the computation of discrete operators if the cell or face of
concern is on or near a boundary. In this case, BC data comes from the
boundary faces of the reference cell (or face) as well as those
boundary faces of immediate neighbor cells. As an example, the
influence of BC data on a cell and face gradient is depicted in the
schematic below.

\subsection{Algorithm Overview}
\LSEC{algover}

Given discrete scalar data $\phi$ residing at cell centroids,
approximations for first-order derivatives (e.g., $\GRAD\phi$) on 2-D
and 3-D fully-unstructured meshes must be made.  The method is based
on the work of Barth~\cite{barth92}, who has devised innovative least
squares algorithms for the linear and quadratic reconstruction of
discrete data on unstructured meshes. Second (and higher) order
accuracy has been demonstrated on highly irregular (e.g., random
triangular) meshes.  In this approach, Taylor series expansions
$\phi^{\rm TS}_n$ are formed from each reference cell $i$ at centroid
$\mathbf{x}_i$ to each immediate cell neighbor $n$ at centroid
$\mathbf{x}_n$:
\begin{equation}
\phi^{\rm TS}_n(\mathbf{x}_i) = \phi_i(\mathbf{x}_i) +
          \left(\mathbf{x}_n - \mathbf{x}_i\right)\cdot
          \GRAD_i{\phi_i}(\mathbf{x}_i) +
          \ldots
\LEQ{lts}
\end{equation}
where \textit{immediate neighbor} cells are defined to be those cells
sharing at least one vertex with the reference cell $i$.  The
Taylor-Series expansion of $\phi$ in \EQ{lts} above is
termed a \textit{linear reconstruction} if only the first derivative
(gradient) terms are retained in the expansion. This is the assumption
currently made in \truchas.  The sum $(\phi^{\rm TS}_n -
\phi_i)^2$ over all $n$ immediate neighbors is then minimized in the
least squares ($L_2$ norm) sense:
\begin{equation}
\min\sum_n{(\phi^{\rm TS}_n - \phi_i)^2} \Longrightarrow
\sum_n{(\phi^{\rm TS}_n - \phi_i)
\frac{\partial(\phi^{\rm TS}_n - \phi_i)}{\partial{\GRAD_i{\phi_i}}}}
 = 0
\LEQ{lslr}
\end{equation}

The above minimization yields $N=\mathtt{ndim*nneighbors}$ equations
for the unknown components of $\GRAD_i{\phi_i}$. Here
\texttt{nneighbors} is the total number of immediate neighbors, which
includes the number of interior (\texttt{int\_neighbors}) and boundary
(\texttt{bc\_neighbors}) neighbors, and \texttt{ndim} is the
dimensionality of the system, i.e., the number of unknowns for
$\GRAD_i{\phi_i}$.  Each neighbor $n$ therefore yields \texttt{ndim}
separate, linearly-independent equations for $\GRAD_i{\phi_i}$.  If
$N<\mathtt{ndim}$ the system is undetermined, if $N=\mathtt{ndim}$,
the system is solvable, and if $N>\mathtt{ndim}$, the system is
overdetermined. In general the system is overdetermined, hence a
minimizing solution must be sought according to \EQ{lslr} above.

One way in which least squares solutions to \EQ{lslr}
can be found by solving a linear system known as the normal
equations~\cite{}:
\begin{equation}
\left(A^{\textrm{T}}WA\right)\mathbf{x} = A^{\textrm{T}}W\mathbf{b},
\LEQ{normaleq}
\end{equation}
where $A$,
\begin{equation}
A = \left(
\begin{array}{ccc}
\left(x_k - x_i\right) &
\left(y_k - y_i\right) &
\left(z_k - z_i\right) \\
\vdots & \vdots & \vdots \\ 
\left(x_n - x_i\right) &
\left(y_n - y_i\right) &
\left(z_n - z_i\right) 
\end{array}
\right),
\end{equation}
is a dense $\mathtt{ndim}\times N$ matrix,
and $W$,
\begin{equation}
W = \left(
\begin{array}{ccc}
w_k & \ldots & 0 \\
\vdots & \ddots & \vdots \\
0 & \ldots & w_n
\end{array}
\right),
\end{equation}
is a diagonal $N\times N$ matrix.  The diagonal entries in $W$ are
individual weights $w_k = w^d_k*w^g_k$, expressed in general form as
the product of a geometric weight $w^g_k$ and a data-dependent weight
$w^d_k$.  The geometric weight $w^g_k$ is
$1/\ABS{\mathbf{x}_k-\mathbf{x}_i}^t$ (we take $t=2$) and the
data-dependent weight $w^d_k$, unity by default, is the optional
\verb=Weight= argument passed into all discrete operator procedures,
hence its value is determined at execution time.  The vector
$\mathbf{b}$ (length $N$) is given by
\begin{equation}
{\bf b} = \left(
\begin{array}{cc}
\phi_k - \phi_i \\
\vdots \\
\phi_n - \phi_i
\end{array}
\right),
\end{equation}
and the solution vector $\mathbf{x}$ (length \texttt{ndim}) is
\begin{equation}
{\bf x} = \left(
\begin{array}{c}
\GRAD_{xi}\phi_i \\
\GRAD_{yi}\phi_i \\
\GRAD_{zi}\phi_i
\end{array}\right).
\end{equation}

After carrying out the matrix-vector and matrix-matrix multiplications
in \EQ{normaleq}, the linear system
\begin{equation}
A^{\prime}\mathbf{x} = \mathbf{b}^{\prime},
\LEQ{solvable_eq}
\end{equation}
is obtained, where $A^{\prime}$ is a
$\mathtt{ndim}\times\mathtt{ndim}$ matrix,
\begin{equation}
A^{\prime} = \left(
\begin{array}{ccc}
\sum_{n}w_n\delta x_{ni}\delta x_{ni} &
\sum_{n}w_n\delta y_{ni}\delta x_{ni} &
\sum_{n}w_n\delta z_{ni}\delta x_{ni} \\
\sum_{n}w_n\delta x_{ni}\delta y_{ni} &
\sum_{n}w_n\delta y_{ni}\delta y_{ni} &
\sum_{n}w_n\delta z_{ni}\delta y_{ni} \\
\sum_{n}w_n\delta x_{ni}\delta z_{ni} &
\sum_{n}w_n\delta y_{ni}\delta z_{ni} &
\sum_{n}w_n\delta z_{ni}\delta z_{ni} \\
\end{array}
\right),
\end{equation}
where $\delta x_{ni} = x_n-x_i$, $\delta y_{ni} = y_n-y_i$, and
$\delta z_{ni} = z_n-z_i$. The vector $\mathbf{b}^{\prime}$ (length
\texttt{ndim}) is given by
\begin{equation}
\mathbf{b}^{\prime} =
\left(
\begin{array}{c}
\sum_{n}w_n\delta x_{ni}\delta\phi_{ni} \\
\sum_{n}w_n\delta y_{ni}\delta\phi_{ni} \\
\sum_{n}w_n\delta z_{ni}\delta\phi_{ni} \\
\end{array}
\right)\, ,
\end{equation}
where $\delta\phi_{ni}=\phi_n-\phi_i$.  Each component of the
$A^{\prime}$ and $\mathbf{b}^{\prime}$ is therefore derived by summing
over all immediate interior and boundary neighbors
(\texttt{nneighbors}) of the reference cell $i$. The resulting
$\mathtt{ndim}\times\mathtt{ndim}$ linear system is easily solved with
conventional direct methods such as LU decomposition.

If the reference value of $\phi$, e.g., $\phi_i$ in \EQ{lts}, is not known, then
\EQ{lslr} is derived by
performing partial derivatives with respect to $\phi_i$ in addition to
the components of $\GRAD\phi$. This results in $\mathtt{ndim}+1$
unknowns, hence $\phi_i$ will also be part of the solution.  This is
the case for face gradients, where $\phi_i$ is $\phi_f$, the unknown
value of $\phi$ at face $f$. Face values of $\phi$ are not known
because discrete values of $\phi$ reside only at cell centroids.
\begin{equation}
A^{\prime} = \left(
\begin{array}{cccc}
\sum_{n}w_n\delta x_{ni}\delta x_{ni} &
\sum_{n}w_n\delta y_{ni}\delta x_{ni} &
\sum_{n}w_n\delta z_{ni}\delta x_{ni} &
\sum_{n}w_n\delta x_{ni} \\
\sum_{n}w_n\delta x_{ni}\delta y_{ni} &
\sum_{n}w_n\delta y_{ni}\delta y_{ni} &
\sum_{n}w_n\delta z_{ni}\delta y_{ni} &
\sum_{n}w_n\delta y_{ni} \\
\sum_{n}w_n\delta x_{ni}\delta z_{ni} &
\sum_{n}w_n\delta y_{ni}\delta z_{ni} &
\sum_{n}w_n\delta z_{ni}\delta z_{ni} &
\sum_{n}w_n\delta z_{ni} \\
\sum_{n}w_n\delta x_{ni} &
\sum_{n}w_n\delta y_{ni} &
\sum_{n}w_n\delta z_{ni} &
\sum_{n}w_n \\
\end{array}
\right),
\end{equation}
\begin{equation}
\mathbf{b}^{\prime} =
\left(
\begin{array}{c}
\sum_{n}w_n\delta x_{ni}\phi_n \\
\sum_{n}w_n\delta y_{ni}\phi_n \\
\sum_{n}w_n\delta z_{ni}\phi_n \\
\sum_{n}w_n\phi_n \\
\end{array}
\right)\, ,
\end{equation}
\begin{equation}
{\bf x} = \left(
\begin{array}{c}
\GRAD_{xi}\phi_i \\
\GRAD_{yi}\phi_i \\
\GRAD_{zi}\phi_i \\
\phi_i \\
\end{array}\right).
\end{equation}

Least squares reconstruction methods are quite powerful and attractive
for a number of reasons. First, they are not married to any particular
mesh topology or dimensionality, hence are easily amenable to any
unstructured mesh in one, two, or three dimensions.  All that is
required is a set of discrete data points described by their data
values and their physical location.  Second, there are no constraints
(other than conservation of the mean, as described by Gooch~\cite{gooch1})
on what has to be minimized or how that minimization is to be
performed. For example, $L_1$ or $L_\infty$ norms might also be
minimized rather than the $L_2$ norm as above. Third, the discrete
data points can be arbitrarily weighted and/or constrained in the
minimization process, which is apparent in \EQ{normaleq} via inclusion
of the geometric weights $w_k$. Data-dependent weights can also be
included. These might arise as a result of constraints such as
monotonicity, validity, etc.  With data-dependent weight, however, the
resulting overdetermined system of equations frequently has to be
solved with a method other than the normal equations.  As a final
note, the accuracy of this method is easily increased by including
additional terms in the Taylor series expansion
$\phi^{\mathrm{TS}}_i$.
