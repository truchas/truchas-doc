\chapter{Linear Solution Methods}
\LAPP{LinSolve}

An incompressible flow algorithm constructed with the fractional-step
projection method discussed in \CHAP{flow} requires solutions to linear
systems of equations.  A solution to a pressure projection equation,
\EQ{projection} where $P$ is the pressure, are
required. The equation is basically elliptic in
nature, and can be expressed in matrix notation as
\begin{equation}
\mathbf{A}{\bf x} = {\bf b}\, ,
\LEQ{Ax}
\end{equation}
where $\mathbf{A}$ is a matrix resulting from the discretization,
${\bf x}$ is the solution vector, and ${\bf b}$ is a vector source
term. Since for our equations the matrix $\mathbf{A}$ arises from
finite volume discretizations of the Laplacian, we expect $\mathbf{A}$
to be sparse, positive definite (${\bf x}^{\rm T}\mathbf{A}{\bf x} > 0$),
and in general symmetric, hence our solution methods should take
advantage of this structure.  The total computational effort of our
fractional-step scheme will be dominated by the effort required to
find solutions to \EQ{Ax}, therefore designing an efficient and
scalable method for solving these systems of linear equations is of
paramount importance.

Which method for the solution of \EQ{Ax} is recommended? There
are several metrics one must take into account when considering a
solution method for linear systems of equations: robustness, or
ability to converge; efficiency, or convergence rate (if the method is
iterative); scalability of the computational effort (relative to the
number of unknowns $N$) required to find a solution; and complexity of
implementation. Ideally, we desire a method that \emph{always}
converges (provided our equations are well-posed), that exhibits
computational effort scaling linearly with $N$, and that
requires \emph{grid-independent iterations to convergence}. This last
requirement may be restated as requiring our method to converge to a
solution for a given physical domain in an iteration count that does
not change with the number of grid points used to partition the
domain.  Of the possible solution methods briefly mentioned here,
including direct and stationary iterative methods, Krylov subspace
methods, multigrid methods, and hybrid methods, only the multigrid and
hybrid methods have shown promise in meeting all of our requirements.

\section{Direct and Stationary Iterative Methods}

Since $\mathbf{A}$ for our equations is not dense, but rather sparse
and usually diagonally-dominant, direct solution methods such as
Gaussian elimination and Cholesky or LU factorization are not
recommended.  Because of the non-constant coefficients of the
operators, FFT or cyclic reduction methods are also not effective.
These methods require computational effort that scale like $N^3$,
which can improve to $N^2$ if the solution method takes advantage of
the sparsity of $\mathbf{A}$, but this scaling is not the linear
scaling we desire. The primary attractiveness of direct solvers is
their robustness, or ability to find a solution for any nonsingular
$\mathbf{A}$, e.g., especially when $\mathbf{A}$ has a high condition
number (ratio of maximum to minimum eigenvalues) arising from small
mesh spacings or high density ratio flows. Stationary iterative
methods, such as Jacobi, Gauss-Seidel, and symmetric successive
over-relaxation (SSOR)~\cite{varga}, are attractive because of their
ease of implementation, but they exhibit poor scaling, requiring
computational effort that scales like $N^2$, and they can frequently
exhibit a lack of robustness (inability or slowness to converge).
While these stationary iterative methods are not recommended for
finding solutions to \EQ{Ax}, they do remain quite useful as
preconditioners in Krylov subspace methods or as smoothers in the
multigrid method.  Further information about direct and stationary
iterative methods can be found in a host of classic textbooks.  I have
found references~\cite{strang76,strang86,gvl} to be particularly
useful.

\section{Krylov Subspace Methods}

Krylov subspace methods~\cite{gvl,saad} are iterative methods in which
solutions to \EQ{Ax} are extracted from a subspace by imposing
constraints on the residual vector ${\bf b} - \mathbf{A}{\bf x}$,
typically that it be orthogonal to $m$ linearly independent vectors in
the subspace.  The most popular and widely used Krylov subspace
methods are the conjugate gradient (CG) algorithm if our positive
definite matrix $\mathbf{A}$ is symmetric or generalized minimal
residual (GMRES) algorithm if $\mathbf{A}$ is not symmetric. These
methods are in general robust, with the CG method theoretically
guaranteed to converge in $N$ iterations, and GMRES usually able to
converge if $\mathbf{A}$ does not have an excessive condition number
and is diagonally-dominant. Krylov subspace methods are also
relatively easy to implement.

The problem, however, is that Krylov subspace methods can be slow to
converge unless the linear system given by \EQ{Ax} is first
``preconditioned'' with a preconditioning matrix $\mathbf{M}$ that
either multiplies the system from the left side (left preconditioning),
\begin{equation}
(\mathbf{M}^{-1}\mathbf{A}){\bf x} = \mathbf{M}^{-1}{\bf b}\, ,
\end{equation}
or from the right side (right preconditioning),
\begin{equation}
(\mathbf{A}\mathbf{M}^{-1}){\bf y} = {\bf b}; \quad {\bf y} = \mathbf{M}{\bf x}\, .
\end{equation}
The net effect of preconditioning is a linear operator (in parentheses
above) that is closer to the identity matrix, which accelerates the
convergence of the Krylov subspace method.  The new linear operator,
$\mathbf{A}\mathbf{M}^{-1}$ or $\mathbf{M}^{-1}\mathbf{A}$, will have
a smaller condition number and eigenvalues that are more clustered
than with $\mathbf{A}$ alone.  Choosing the preconditioning matrix
$\mathbf{M}$ is often \emph{not} easy, as it must be an approximation
to $\mathbf{A}$ that is easily inverted. Unfortunately,
preconditioning the Krylov subspace method is almost always necessary,
as without it these methods converge too slowly. With preconditioning,
one must also solve an additional preconditioning equation, given by
$\mathbf{M}{\bf z} = {\bf r}$, where ${\bf z}$ is a Krylov vector and
${\bf r}$ is a residual. Fortunately, approximate solutions for ${\bf
z}$ are usually good enough, hence using simple iterative methods like
SSOR or Jacobi to find ${\bf z}$ is often adequate.

If a good preconditioning matrix $\mathbf{M}$ can be chosen,
preconditioned Krylov subspace methods can be quite powerful and
efficient linear solution methods. They have been perhaps the most
popular choice for the past two decades, primarily because of their
robustness and ease of implementation.  The problem, however, is that
preconditioned Krylov subspace methods do not exhibit the scaling we
desire, requiring computational work that scales like $N^{5/4}$ (at
best) and iterations to convergence somewhere between $N^{1/4}$ and
$N^{1/2}$. Can this scaling problem of Krylov subspace methods be overcome?
Perhaps, if one is willing to focus efforts on the preconditioner, as
discussed in \SEC{hybrid} below.

Additional information on Krylov space methods can be found
in~\cite{templates}, where detailed algorithm templates sufficient for
implementation are provided. See also reference~\cite{shewchuk} for an
insightful introductory overview and~\cite{sw95,meister} for performance
comparisons on linear systems arising from the NS equations.

\section{Multigrid Methods}

As stated previously, we seek a linear solution algorithm that
requires computational work scaling like $N$, and we furthermore
require that the method can find solutions in an iteration count that
does not change with $N$. These requirements are usually met for
linear elliptic solutions with a multigrid (MG) method~\cite{brandt},
hence the scalability of the MG method is a powerful attraction. The
basic premise of the MG method is the identification and suppression
of long wavelength (low frequency) error modes in the residual via
solutions of an equivalent linear system on a series of grids coarser
than the base (finest) grid.  This is in contrast to traditional
iterative (Jacobi, SSOR, Gauss-Seidel) and Krylov subspace methods,
which quickly eliminate only those high frequency error modes
indicative of coupled nearest neighbor cells. Low frequency error
modes, however, tend to persist without elimination until enough
iterations have taken place for the long wavelength modes to be
``seen''. MG methods, on the other hand, immediately ``see'' these
long wavelength modes on the coarser grids, which, once identified,
can be suppressed after transfer back to the series of finer grids.

On each grid in the MG method, iterative approximate solutions to the
linear system are obtained; rigorous solutions are not obtained on any
one grid, but rather obtained on the base (finest) grid after many
fine-to-coarse-to-fine (V) cycles.  Space does not permit a detailed
discussion of this powerful technique, but overviews can be found in
the excellent monograph of Briggs~\cite{briggs} and the introductory
textbook by Wesseling~\cite{wess}.  The reader is also encouraged to
consult references~\cite{shyy3,rudman98} for examples of the MG method
applied to linear systems of equations arising specifically from
incompressible interfacial flows.

One of the most important and difficult tasks in formulating an MG
algorithm is the approximation of the intergrid transfer functions,
namely the restriction (fine-to-coarse) and prolongation
(coarse-to-fine) operators. Performance of the MG method, measured as
scalability and convergence robustness, depends crucially upon the
choice of these operators.  One approach is using the intergrid
transfer functions to define variational or Galerkin coarse grid
operators.  This task can be complicated and expensive, however,
especially if the restriction and prolongation operators are anything
but piecewise constant (e.g., stencil growth can occur). This fact has
motivated others~\cite{rider94,icase} to adopt the simpler approaches
like those suggested in~\cite{llm}.

Experience has shown that the MG method at times lacks robustness,
having a propensity to fail and/or exhibit slow convergence on the
types of linear systems arising in incompressible interfacial
flows. Here ``tough'' systems are those resulting from flows
possessing large, abrupt, and localized changes in density and/or
surface tension along an interface that is topologically complex.
This lack of robustness can usually be traced to restriction and
prolongation operators that are misrepresenting important interfacial
physics because of inaccurate or inappropriate interpolation and/or
smoothing functions.  Robustness and convergence can be enhanced in
many cases with more intelligent restriction and prolongation
operators that do not incorrectly smooth across interfaces.
Formulation and implementation of such operators in the presence of
arbitrarily complex interface topologies, however, can be very
expensive and cumbersome.

\section{Hybrid Methods}
\LSEC{hybrid}

Until the MG method can be made more robust and easily implemented,
and Krylov subspace methods more scalable, more and more researchers
are devising unified, \emph{hybrid} methods aimed at combining the
strengths of both methods while eliminating their weaknesses. Two
basic types of hybrid methods have appeared in the literature to date.
In the first approach, MG is the principal solution method, but a
Krylov subspace method is used for solutions on one or more of the
(coarser) grids rather than a simple iterative method. This approach
helps to alleviate the MG robustness problem by relying on a robust
Krylov method. In the second approach, a preconditioned Krylov
subspace method is retained as the principal solution algorithm, but
an MG-like (\emph{multilevel}) method is used to obtain solutions to
the preconditioning equation. Both approaches have merit and have
exhibited improved performance, but it is not clear at this time which
hybrid method exhibits the desired scalable performance without loss
of robustness. One issue has become clear, however: scalable
performance absolutely requires a multilevel algorithm, i.e., ideas
inherent in the MG method \emph{must} pervade.

The idea of using a symmetric MG algorithm to precondition a standard
Krylov subspace (CG) method was first proposed and demonstrated by
Kettler~\cite{kettler}.  This idea, however, did not gain acceptance
and popularity until the recent work of Tatebe~\cite{cm93}. Its use
has since exploded, having shown utility in modeling incompressible
flows~\cite{icase,pabmr}, semiconductor performance~\cite{meza}, and
groundwater flow~\cite{ashby}.  It possesses the robustness lacking in
many MG algorithms, able to find solutions on the most challenging of
interfacial flow problems.  A hybrid ``MGCG'' method, while usually
scaling like a MG algorithm, occasionally exhibits CG-like scaling,
hence additional research is needed to understand which aspects of the
algorithm hinder consistent MG-like scaling.  For most of the flow
problems tested, the hybrid MGCG method requires less computational
work than the MG method to seek a solution. This same basic approach
was recently shown to be effective for the parallel solution of linear
systems of equations on 3-D unstructured meshes~\cite{lfkkt}.  In this
case, a combined additive/multiplicative Schwarz~\cite{cai,saad} technique
was ideal for providing a means by which multilevel solutions to the
preconditioning equation on parallel architectures could be obtained.

\section{Approximating the Preconditioning Matrix}

\section{Inverting the Preconditioning Matrix}

We develop a parallel, two-level, solver for 3 dimensional (3-D),
unstructured grid, nonsymmetric, elliptic problems. The solver is a
preconditioned GMRES method with cell centered finite volume spatial
discretization. The preconditioner can be viewed as a two-level
Schwarz method or a two-grid multigrid V-cycle with aggressive
coarsening. Our coarse grid correction employs simple summation and
injection for inter-grid transfer operators and a variational method
to construct the coarse grid operator.  These choices have resulted in
a minimum of software complexity.

\subsection{Introduction}

As the field of computational physics matures the algorithmic
challenges grow more complex.  Among theses challenges are the need to
solve large 3-D elliptic problems efficiently, the use of unstructured
grids for modeling complex geometries, and the ability to effectively
utilize modern parallel computing platforms. In this paper we detail
an algorithm for the iterative solution of large 3-D elliptic
problems, on unstructured grids, and on modern parallel computing
platforms. Our initial motivation for the development of this
algorithm has been simulation of casting processes using 3-D
unstructured finite volume methods and the development of the
\truchas simulation code~\cite{telluride,ppsc97tell}. However, the method is
quite general and should find use in a variety of applications such as
ground water flow.  Also, while we
apply this method to stationary unstructured finite volume grids, it
could be applied to simulation problems using adaptive mesh
refinement.

Our model equations of interest are,
\begin{equation} \frac{\partial \phi}{\partial t} +
\nabla \cdot (-D({\mathbf r}) \nabla \phi ) = s_{1}({\mathbf r}),
\end{equation}
and
\begin{equation} 
\nabla \cdot (D({\mathbf r}) \nabla \phi ) = s_{2}({\mathbf r}).
\end{equation}

In the simulation of casting the first equation arises from heat
conduction with phase change and the second equation is a pressure
Poisson equation for variable density incompressible flow. The two
modern iterative approaches to the solution of linear systems arising
from the discretization of these equations are preconditioned Krylov
methods~\cite{saad} and multigrid methods~\cite{wess}.  Paramount to
the development of an efficient elliptic solver is the notion of
algorithmic scalability.  An optimal algorithm is one who's required
iteration count is not a function of the grid refinement, and
multigrid methods are the most widely understood methods which posses
this character.  Developing parallel multigrid methods for
unstructured grids is more challenging than developing a serial
multigrid method for structured grids.  Especially if one is required
to use a sophiticated diffusion operater on the unstructured
grid~\cite{morel}. A notable success has been the work of
Mavriplis~\cite{MavripilisVenkatakrishnan96}. Developing a parallel
preconditioned Krylov solver for unstructured grids is rather straight
forward.  The path of least resistance appears to be using a
domain-based preconditioner~\cite{ddbook}, the simplest being block
Jacobi (one-level additive Schwarz). This approach can provide one
with good parallel efficiency but it does not possess the property of
algorithmic scalability.  That is, as the grid is refined, and the
number of blocks (or processors) is increased, the number of required
Krylov iterations will increase. The two approaches for overcoming
this scaling, as put forth in \cite{ddbook}, are the addition of
a coarse grid correction scheme and overlap between the blocks.  In
order to minimize the lack of algorithmic scalability we add a simple
coarse grid correction scheme to a straight forward block Jacobi
preconditioner.  In constructing our solver we have attempted to
strike a balance between: 1) code complexity and development time,
2) algorithmic scalability, and 3) parallel efficiency.

By ``simple coarse grid correction'' we mean 3 specific things. The
coarsening is predefined by the parallel decomposition of our 3-D
grid. This is another way of saying that every processor represents a
coarse grid finite volume.  Our inter-grid transfer operators are
low-complexity, piece-wise constant interpolation.  Our coarse grid
linear operator is formed in an algebraic manner from our fine grid
matrix and our inter-grid transfer operators, i.e. a variational
method. As a result of these choices no physics operator is
discretized on a coarse grid, and in fact no coarse grid is actually
formed.

An additional simplification for unstructured grid problems, 
afforded us through the use of an outer
Krylov iteration, is the fact that we never form the matrix resulting from our
true discrete operator.  The effect of the true discrete operator is
only realized in the matrix-vector multiply of the Krylov method. The only
matrix which is formed (for preconditioning purposes) is one which
results from a lower-order, more approximate, discrete operator.
The true discrete operator is a least squares based method~\cite{gooch1,kr97}
which can in general produce a nonsymmetric matrix.  We use the General Minimal
Residual (GMRES) algorithm~\cite{gmres} for our Krylov method.

We clearly acknowledge that our method could be described
as either a two-grid multigrid V-cycle preconditioner with block Jacobi
as the fine grid smoother and a variational
coarse space, or as a two-level Schwarz preconditioner which is
additive on the fine level, multiplicative between levels and has
minimal overlap.  
This dual distinction has also been acknowledged in \cite{ddbook}.
We are less concerned with the exactness of our
chosen vernacular and more concerned with the clear presentation 
of the algorithm and its performance.  The number of possible algorithms
in the numerical analysis literature which may be viewed as
closely related to ours are to numerous to mention. In point of fact, the main
algorithmic character of our preconditioner can be traced back to
the early work of Poussin~\cite{poussin}, Settari and Aziz~\cite{AC1}, 
and Nicolaides~\cite{nicolaides}.

Finally, we wish to emphasize  that this two-level preconditioner can
be viewed as a means through which to parallelize one's favorite
non-parallel iterative preconditioner such as SSOR, ILU, or
algebraic multigrid. In Section 2 we describe our fine grid solver,
and Section 3 discusses the coarse grid correction in
the preconditioner.  Algorithmic 
performance is given in Section 4, and conclusion is Section 5.

\subsection{Fine Grid Solver}

In this section we outline the details of the fine grid solver which
is a preconditioned Krylov method.  The preconditioning matrix is
constructed using a simplified, approximate, spatial discrete
operator.  A block Jacobi method is used to iteratively invert this
fine grid preconditioner.  We use left preconditioning with in the
Krylov software package JTpack90~\cite{ppsc97jtpack} and and utilize the
parallel communication library PGSLib~\cite{ppsc97pgslib}.

\subsubsection{Preconditioned Krylov Methods}

Probably the best known Krylov method is the conjugate gradient (CG)
method made popular in the computational physics community by
Kershaw~\cite{kershaw}.  CG is applicable only to symmetric matrices.
Let us compare CG to GMRES~\cite{gmres}, which can be applied to
nonsymmetric matrices.  CG enjoys a short vector recurrence
relationship which allows one to construct an orthogonal set of search
directions without storing all of the search directions.  Thus in CG,
work scales linearly, and required storage is constant, as the number
of iterations increases.  In GMRES, work scales quadratically, and
required storage scales linearly, as a function of iteration
count. This is because GMRES must store all of the search directions
in order to maintain an orthogonal set.  An often employed ``fix" is
to store only k Krylov vectors, GMRES(k).  If linear convergence is
not achieved after k iterations a new, temporary, linear solution is
constructed from the existing k vectors and GMRES is restarted, with
this temporary solution as the initial guess.  Restarting can
significantly effect the convergence rate of GMRES.  As a result, when
using GMRES as we are here, there is great motivation to keep the
required number of GMRES iterations low. We feel this translates into
a clear need for effective multilevel preconditioning.  We also
acknowledge that Krylov methods exist which can be applied to
nonsymmetric systems, and which possess storage requirements which are
independent of Krylov iteration~\cite{tfqmr,bicgstab}.

It will be our convention to use lower case bold faced letters to
represent vectors and upper case bold faced letters to represent
matrices.  The general linear system arising from the discretization
of our model equations is \( \mathbf{A } \mathbf{\phi} = \mathbf{s}\).
The left preconditioned form of is,
\begin{equation}
\mathbf{ P}^{-1} \mathbf{A} \phi = \mathbf{ P}^{-1} s ,
\label{eq.prematrix}
\end{equation}
where
\( \mathbf{P } \) represents symbolically  the preconditioning
matrix and \( \mathbf{P }^{-1} \) represents
its inverse.
In practice, this inverse is only
approximately realized through some standard iterative process,
and the symbol \( {\tilde {\mathbf P }}^{-1} \) may be more appropriate.
Each GMRES iteration requires a preconditioned  matrix-vector multiply,
\begin{equation} {\mathbf y} =  ({\tilde {\mathbf P}}^{-1} \mathbf{A} ) \mathbf{v} ,
\end{equation}
where \( \mathbf{v} \) is the known, \(n^{th}\), search direction and
\( \mathbf{w}\) represents the first step in forming the \(n+1^{st}\)
search direction.
The multiply requires two steps. \\
Step 1, matrix vector multiply:
\begin{equation} \mathbf{w} =  \mathbf{ A v} , 
\end{equation}
Step 2, Preconditioning (parallel bottle neck):
\begin{equation}  \mathbf{ y} = {\tilde {\mathbf P}}^{-1} \mathbf{w} , 
\end{equation}
(i.e iteratively solve \( {\mathbf P} \mathbf{ y} =  \mathbf{w} \) ).

There are two important questions to ask at this point.
What approximation of \( {\mathbf A} \) to use to form 
\( {\mathbf P} \), and what iterative method to use to 
approximate \( {\mathbf P}^{-1} \). As stated previously our
spatial operator on the 3-D unstructured grid is based on
a least-squares method~\cite{gooch1,kr97}.  To evaluate 
\(\mathbf{w} =  \mathbf{ A v} \) with out evaluating \(\mathbf{ A}\)
is straight forward, but to actually form the elements of \( {\mathbf A} \)
is more complicated.  We chose to form \(\mathbf{ P}\) from
a simple 7-point method for the \(\nabla \cdot (- D({\mathbf r}) \nabla  )\)
operator which assumes a locally orthogonal grid. 
This not only reduces complexity
but also insures a preconditioning matrix which is diagonally dominant.
Next we define our approach for approximating \( {\mathbf P}^{-1} \).

\subsubsection{ Block Jacobi: Basic Domain Decomposition}

The goal of domain-based preconditioners is to decompose the global 
inversion into a sum of local
inversions which can be done on processor, in parallel~\cite{ddbook}.  
The local, on processor, ``subdomain'' inversions
\( ({\mathbf P}^{sub}_{i})^{-1}\) can
be direct solves, ILU, SSOR, multigrid or something else.
For a 4 subdomain (4 processor) problem we have;

\begin{equation} {\mathbf P} = {\mathbf P}^{sub}_{1} + {\mathbf P}^{sub}_{2}
+ {\mathbf P}^{sub}_{3} + {\mathbf P}^{sub}_{4}.
\end{equation}
Block Jacobi (single pass) assumes;

\begin{equation} {\mathbf P}^{-1} \approx ({\mathbf P}^{sub}_{1})^{-1} 
+ ({\mathbf P}^{sub}_{2})^{-1}
+ ({\mathbf P}^{sub}_{3})^{-1} + ({\mathbf P}^{sub}_{4})^{-1}.
\end{equation}
In~\cite{ddbook} This would be referred to as an additive Schwarz method.
This approximation degrades with the number of
subdomains (i.e processors) and thus the effectiveness of the preconditioner
degrades.


For further insight consider block Jacobi with 4 subdomains in
fig. (\ref{fig.4sub}). Here the fine grid spatial scale is defined as
\(h\), and the subdomain spatial scale is defined as \(H\), with
\(H \gg h\).

\begin{figure}[h]
\begin{center}
 \setlength{\unitlength}{1.0in}
 \begin{picture}(4,4)

\thicklines

 \multiput(0.0,0.0)(0,2.0){3}{\line(1,0){4.0}}


 \multiput(0.0,0.0)(2.0,0){3}{\line(0,1){4.0}}

 \put(0.3,3.7){(1)}
 \put(2.3,3.7){(2)}
 \put(0.3,1.7){(3)}
 \put(2.3,1.7){(4)}

 \put(0.6,1.5){fine grid, \(h\)}

 \put(2.8,.3){\vector(-1,0){.75}}
 \put(3.2,.3){\vector(1,0){.75}}
 \put(2.95,.25){\(H\)}

 \multiput(.4,0.6)(0,0.2){4}{\line(1,0){1}}
 \multiput(0.6,.4)(0.2,0){4}{\line(0,1){1}}
 \end{picture}
\end{center}

\caption{4 subdomain example}
\label{fig.4sub}
\end{figure}
In our parallel implementation the global matrix \({\mathbf P} \) is not stored.
The unknowns in each subdomain problem have a unique ordering within 
that subdomain.  
This is equivalent
to thinking of a global system
\( {\mathbf P} \mathbf{ y} =  \mathbf{w} \), reordered within subdomains,
to look like:

\[  \left[ \begin{array}{cccc}
 {\mathbf D}_{11} & {\mathbf U}_{12} & {\mathbf U}_{13} & 
{\mathbf U}_{14} \\
 {\mathbf L}_{21} & {\mathbf D}_{22} & {\mathbf U}_{23} & 
{\mathbf U}_{24} \\
 {\mathbf L}_{31} & {\mathbf L}_{32} & {\mathbf D}_{33} & 
{\mathbf U}_{34} \\
 {\mathbf L}_{41} & {\mathbf L}_{42} & {\mathbf L}_{43} & {\mathbf D}_{44} \\
\end{array} \right] \left[ \begin{array}{c}
{\mathbf y}_{1} \\ {\mathbf y}_{2} \\ {\mathbf y}_{3} \\ 
{\mathbf y}_{4} \end{array} \right]
= \left[ \begin{array}{c}
{\mathbf w}_{1} \\ {\mathbf w}_{2} \\ {\mathbf w}_{3} \\ 
{\mathbf w}_{4} \end{array} \right] .  \]
This is a domain-based reordering and results from blocking our
physical geometry (finite volumes) into subdomains.
Here the block matrix \({\mathbf U}_{12} \) represents the coupling 
of subdomain 1 to subdomain 2, the block matrix
\( {\mathbf U}_{13} \) represents the coupling of subdomain 1 to subdomain 3, 
and
so on.  The \(n^{th}\) block Jacobi iteration, assuming a direct solve
on the subdomains,  can then be represented by;

\noindent
\begin{eqnarray}
{\mathbf y}_{1}^{n} &  = &{\mathbf D}_{11}^{-1}[ {\mathbf w}_{1} - 
{\mathbf U}_{12} {\mathbf y}_{2}^{n-1} - {\mathbf U}_{13} {\mathbf y}_{3}^{n-1}
- {\mathbf U}_{14} {\mathbf y}_{4}^{n-1}] \nonumber \\ 
{\mathbf y}_{2}^{n} &  = &{\mathbf D}_{22}^{-1}[ {\mathbf w}_{2} - 
{\mathbf L}_{21} {\mathbf y}_{1}^{n-1} - {\mathbf U}_{23} {\mathbf y}_{3}^{n-1}
- {\mathbf U}_{24} {\mathbf y}_{4}^{n-1}] \nonumber \\ 
{\mathbf y}_{3}^{n} &  = &{\mathbf D}_{33}^{-1}[ {\mathbf w}_{3} - 
{\mathbf L}_{31} {\mathbf y}_{1}^{n-1} - {\mathbf L}_{32} {\mathbf y}_{2}^{n-1}
- {\mathbf U}_{34} {\mathbf y}_{4}^{n-1}] \nonumber \\ 
{\mathbf y}_{4}^{n} &  = &{\mathbf D}_{44}^{-1}[ {\mathbf w}_{4} - 
{\mathbf L}_{41} {\mathbf y}_{1}^{n-1} - {\mathbf L}_{42} {\mathbf y}_{2}^{n-1}
- {\mathbf L}_{43} {\mathbf y}_{3}^{n-1}]  
\end{eqnarray}
As can be seen this is highly parallel.
For \( {\mathbf y}^{0} = 0 \), and one pass, this is equivalent to 
"block" diagonal
scaling, and the \({\mathbf U}\)'s and \({\mathbf L}\)'s need not be formed. 
Again, an approximate inverse may be more practical on the subdomains (i.e.
replacing \( {\mathbf D}_{11}^{-1} \) with 
\( {\tilde {\mathbf  D}}_{11}^{-1} \) ) and 
\( {\tilde {\mathbf D}}_{11}^{-1} \) can be ILU, SSOR, multigrid, 
or something else.

Since it is our intention to use this block Jacobi method as
a fine grid smoother \({\mathbf y}^{0} \) will not always be zero, and
we need to form the \({\mathbf U}\)'s and \({\mathbf L}\)'s. 
In our structured grid,
4 subdomain, example forming the \({\mathbf U}\)'s and \({\mathbf L}\)'s 
can be thought of as
using a 1 cell(finite volume) overlap to form Dirichlet boundary conditions.  In
our parallel implementation, \({\mathbf D}_{11}, {\mathbf U}_{12}, 
{\mathbf U}_{13}, {\mathbf U}_{14} \)
and \( {\mathbf y}_{1}^{n} \) reside on processor 1. Copies of
the elements of
\({\mathbf y}_{2}^{n-1}, {\mathbf y}_{3}^{n-1}, {\mathbf y}_{4}^{n-1}\) 
which are required to form
\( {\mathbf y}_{1}^{n} \) are also stored on processor 1.  
This forces some level
of inter-processor communication for multiple passes of block Jacobi.
Algorithm performance penalties for removing levels of communication
will be considered in the future.
In the terminology of Schwarz methods~\cite{ddbook} this is the extent of
our overlap. While it is known that increased overlap improves
algorithmic scalability, increased overlap is a 
challenge for unstructured grids.
Thus our fine grid solver is a GMRES method with a block Jacobi preconditioner.
The blocks are defined by the part of our geometry which resides on a 
processor.  The local "on processor" solves within the block Jacobi can
be direct solves, ILU, SOR, or multigrid. Next we describe our coarse grid
correction which is added to the block Jacobi preconditioner.

\subsection{Coarse Grid Correction Scheme}

As stated, our block Jacobi preconditioner, will become less
effective with increasing numbers of subdomains / processors, resulting
in an undesirable increase in the number of GMRES iterations.  Since
it is our goal to run on \(O (10^{3}) \) processors we must add something to
our algorithm to remedy this scaling.  
It is well understood that our preconditioner lacks  
a multilevel component
which would enhance global communication.  
In order to correct this we have opted for a single
coarse grid correction scheme where each subdomain represents a
``coarse grid finite volume'' and thus our coarsening is
predefined by our domain decomposition.  It will be demonstrated that this
simple addition positively impacts the performance of our solver
by improving algorithmic scaling without significantly degrading parallel
performance.


Consider the following two-level preconditioner which could be referred to
as an additive Schwarz method, multiplicative between levels , or
as a 
 two-grid V-cycle with a block Jacobi smoother.
We desire the iterative solution to \( \mathbf{P y = w} \) with 
\(f \equiv \) fine,
and \(c \equiv \) coarse.  The act of restriction transfers fine grid
data to the coarse grid and the act of prolongation transfers coarse
grid data to the fine grid.

\begin{enumerate}

\item Relax  \({\mathbf y}_{f}^{0}\) to \({\mathbf y}_{f}^{1} \) by
approximately solving \( ({\mathbf P}_{f})^{-1}({\mathbf w} -
{\mathbf P}_{f} {\mathbf y}_{f}^{0}) \)

(Additive Schwarz / block Jacobi is the smoother)

\item Evaluate linear residual \({\mathbf res}_{f} ={\mathbf w} -  
{\mathbf P}_{f} {\mathbf y}_{f}^{1} \), and restrict to coarse grid, \\ 
\({\mathbf res}_{c} = {\cal R} \ast {\mathbf res}_{f} \) 

\item  Solve coarse grid problem, \({\mathbf P}_{c} {\mathbf \delta y}_{c} 
= {\mathbf res}_{c} \), for coarse grid correction \\
 \( {\mathbf \delta y}_{c} \).

\item Prolongate coarse grid correction and update fine grid solution
vector, \\
\({\mathbf y}_{f}^{2} = {\mathbf y}_{f}^{1} + {\cal P} \ast
{\mathbf \delta y}_{c} \)


\item Relax  \({\mathbf y}_{f}^{2}\) to \({\mathbf y}_{f}^{3} \) by
approximately solving \( ({\mathbf P}_{f})^{-1}({\mathbf w} -
{\mathbf P}_{f} {\mathbf y}_{f}^{2}) \)

(Additive Schwarz / block Jacobi is the smoother)

\end{enumerate}
First let us highlight the required inter-processor communication
in this preconditioner assuming one pass of block Jacobi at steps
1 and 5. Step 2 requires communication since in terms of our 4 subdomain
example \({\mathbf res}_{f,1} \) is a function of \({\mathbf y}^{1}_{f,2}
\; , \; {\mathbf y}^{1}_{f,3} \), and \({\mathbf y}^{1}_{f,4}\). Step 3 requires
communication in order to assimilate or coarse grid operator onto
1 processor where it is solved.  Step 4 requires communication
to distribute the coarse grid correction to all processors.
Step 5 requires communication since our block Jacobi has a
nonzero initial guess, \({\mathbf y}_{f}^{2}\).  This complete level
of communication should provide increased robustness and improve
algorithmic scaling. Removing
levels of communication, and evaluating performance gains or
losses, is part of our future plans.

Next we  define the inter-grid transfer operators,
Restriction and Prolongation ( 
\( \cal{R}\) and \( \cal{P} \) ) and the coarse grid operator
\( {\mathbf P}_{c} \).
The unstructured grid has motivated the simplest choices
for \( \cal{R}\) and \( \cal{P} \), summation for \( \cal{R}\)
and injection for \( \cal{P} \) (piece-wise constant).
For \(i \equiv \) grid cell index, and \(I \equiv \) sub-domain index
we have,

\[{\mathbf res}_{c} = {\cal R} \ast {\mathbf res}_{f} \; \; \Rightarrow
\; \;  {\mathbf res}_{I,c} = \sum_{i \in I} {\mathbf res}_{i,f} \]

and

\[{\mathbf y}_{f}^{2} = {\mathbf y}_{f}^{1} + {\cal P} \ast \delta 
{\mathbf y}_{c} \; \; \Rightarrow \; \; 
i \in I, \; \; {\mathbf y}_{f,i}^{2} = {\mathbf y}_{f,i}^{1} +  
{ \mathbf \delta y}_{c,I} \]

Forming the coarse grid operator by re-discetizing the problem on a
coarse grid can be challenging with an unstructured grid.  It may also
involving bringing "physics" down to coarse grid which we view as
undesirable.  We form our coarse grid operator in "Black Box"
fashion~\cite{dendy1} using our restriction and prolongation
operators,

\begin{equation} {\mathbf P}_{c} = {\cal R} \ast {\mathbf P}_{f} \ast {\cal P}. 
\label{eq.var}
\end{equation}
The coarse grid operator is constructed algebraically from fine grid
operator and we will provide a simple algorithm for this below.  This
is often referred to as a variational approach and our differs from
that of~\cite{dendy1} due to the simplicity of our inter-grid transfer
operators.

A few comments are in order regarding the simplicity of our inter-grid
transfer operators. It is known, within the multigrid community, that
to develop an optimal method for a second order PDE either restriction
or prolongation must be linear interpolation~\cite{wess}.  We do not
satisfy this.  However, recent results with multigrid as a
preconditioner indicate that the penalty to be paid for this
simplicity may not be as significant as when multigrid is a stand
alone solver~\cite{rk98,jcpmgp}.  It should also be noted that if our
fine grid operator comes from a conservative finite volume
discretization, and we use our simple inter-grid transfer operators to
form a coarse grid operator, then our coarse space problem will also
be a conservative balance of face fluxes and volume sources. This is
known to be a useful property from black box
multigrid~\cite{dendy1,moulton}.

As ``experimental evidence'' that this simple coarse grid correction
should produce a positive effect we cite the recent work of Jenssen
and Weinerfelt~\cite{aiaa-cgc} applied to the fully coupled
Navier-Stokes equations.  Their two-level method is used without a
Krylov accelerator, using direct solves on the subdomains, and no post
smoothing after the coarse grid correction. They employ summation and
injection with finite volumes ( identical inter-grid transfer
operators to ours), and generate their coarse grid operator with the
variational approach.

Finally, we describe the algorithm for constructing the coarse grid operator.
We define the total number of finite volumes as \(N_{fv}\) and the
total number of subdomains as \(N_{sd}\). \({\mathbf P}_{f} \) is an
\(N_{fv} \times N_{fv}\) matrix and \({\mathbf P}_{c} \) is an 
\(N_{sd} \times N_{sd}\) matrix.
\({\cal P} \) is an
\(N_{fv} \times N_{sd}\) matrix and \({\cal R} \) is an 
\(N_{sd} \times N_{fv}\) matrix.  Due to the simplicity of
\({\cal P} \) and \({\cal R} \) they are not formed.
We define an element of \({\mathbf P}_{f} \) as \( P_{f} (i,j) \) and
an element of \({\mathbf P}_{c} \) as \( P_{c} (I,J) \).
Given these definitions, and Eq. (\ref{eq.var}), each element of the 
coarse grid matrix is constructed as

\begin{equation}
P_{c} (I,J) = \sum_{i \in I} \sum_{j \in J} ( P_{f} (i,j) ).
\end{equation}
Each row of \({\mathbf P}_{c} \) can be constructed on it's own 
processor from the part of \({\mathbf P}_{f} \) which lives on
that processor.  For a clarity, we return to the 4 subdomain
example.  In this example \( P_{c} (1,1) \) is the sum of
all elements in \({\mathbf D}_{1,1}\), \( P_{c} (1,2) \) is the sum of
all elements in \({\mathbf U}_{1,2}\), \( P_{c} (1,3) \) is the sum of
all elements in \({\mathbf U}_{1,3}\), and \( P_{c} (1,4) \) is the sum of
all elements in \({\mathbf U}_{1,4}\).
We are not required to 
have an equal number of finite volumes within each subdomain. 

\subsection{Future Work}

\begin{itemize}

\item Multiple coarse grid volumes per subdomain

\item damped block Jacobi smoother

\item reduced and/or altered communication in V-cycle

\item more processors

\end{itemize}
