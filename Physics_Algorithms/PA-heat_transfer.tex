\chapter{Heat Transfer and Phase Changes}
\LCHAPH{htpc}

The following chapter presents the heat transfer and phase change algorithms incorporated into \truchas. The heat transfer algorithm solves the enthalpy advection equation with sources implicitly. The phase change algorithm assumes binary temperature-concentration phase diagrams but can employ multiple phase transformation types. The heat transfer and phase change section is responsible for calculating the enthalpy, solutal concentration and solid volume fractions given fluid velocities and chemical, electromagnetic and user specified heat sources.

%Authors: M. Stan, A. Starobin and B. Mihaila
%Based on the work of: D. B. Kothe, D. Knoll, B. Lally, R. Ferrell, R. Anand, K. Lam, M. Stan, S. Swaminarayan, and A. Starobin.
%
%

\section{Physics}

\subsection{Assumptions}
We list the primary assumptions of the heat transfer and phase change modeling below.
\begin{itemize}
\item Local thermodynamic equilibrium, e.g., $T_1 = T_2 = T_m = T$ for
  all materials $m$ in a given cell.
\item No solid movement
\item Scalar, isotropic heat diffusion
\item Use of binary temperature-concentration phase diagrams whose equilibrium lines are approximated by straight lines.
\item The specific enthalpy (per volume, or per mass) does not depend on pressure and only depends on temperature and concentration
\end{itemize}

\subsection{Material Properties}

The concept of a ``material'' is extended to include ``phases.'' Solid Cu and liquid Cu are therefore different ``materials.'' Each cell may contain $nmat$ materials at thermodynamic equilibrium.

The density of each material is assumed constant and the average density of the cell is given by:
\begin{equation}
   \rho _{cell}  = \sum\limits_m^{nmat} {\rho_m f_m }
\end{equation}
where $f$ are the volume fractions of all materials in the cell and:
\begin{equation}
   \sum\limits_{m = 1}^{nmat} {f_m  = 1}
\end{equation}

In the current implementation, materials that are part of a series of phase transformations must all have the same density.  The specific heat capacity at constant pressure (per unit mass) of each material is represented as a polynomial in temperature $T$ and composition $c$:
\begin{equation}
C_P (T) =
\sum\limits_{i = 0}^{imax} {C_i T^{^{e_i}} } +
\sum\limits_{j = 0}^{jmax} {D_{j} c^{e_j} }
\end{equation}
where the indexes $i$ and $j$, $C$ and $D$ are real coefficients and
$e_i, e_j \neq -1$ are real exponents.
%..bm $e_i, e_j$ are non-negative real exponents.
%This representation has a limited validity and will be changed in a future version of the code.

A typical form of the temperature depended heat capacity, consistent with the commercial databases, is:
\begin{equation}
\LEQ{heatcap}
C_P (T) = A + BT + CT^2  + DT^{ - 2}
\end{equation}

The heat capacity is related to the specific enthalpy (per unit mass) by:
\begin{equation}
\LEQ{Cp}
C_P (T) = \left( {\frac{{\partial h}}{{\partial T}}} \right)_P
\end{equation}
That allows for the integration of the heat capacity to obtain the specific enthalpy:
\begin{equation}
h (T) = h_{\rm ref}  + \int\limits_{T_{\rm ref} }^{T } {C_P (\tau )d\tau }
\end{equation}
where the superscript 'ref' is used to describe the reference temperature and enthalpy. The reference states are internally chosen as the lowest temperature of stability for a certain material and the corresponding enthalpy. As an example, for a liquid, the melting temperature is used as reference (see \FIG{enthalpy} ). In the case of binary alloys, the reference is the temperature corresponding to the liquidus line, at the specified concentration. The most stable solid phase is referenced to zero, both in temperature and enthalpy. Although the latent heat is temperature dependent, the parameter used as input is the latent heat $L^L$ at the liquidus temperature. For an isothermal transformation, $T^S$ = $T^L$ and $L^S$ = $L^L$. In the mushy zone \EQ{Cp} defines the ``effective'' heat capacity.

\begin{figure}[ht]
\centering \includegraphics[scale=0.7]{figures/enthalpy.png}
\caption {Typical specific enthalpy of an alloy as function of
  temperature. The enthalpy in the mushy zone depends on the phase
  transformation model.}
\LFIG{enthalpy}
\end{figure}

The integration of the typical form of the heat capacity \EQ{heatcap} leads to:
\begin{equation}
    h(T) =
    h_{\rm ref}
    + A(T - T_{\rm ref} )
    + \frac{B}{2} (T^2 - T_{\rm ref}^2 )
    + \frac{C}{3} (T^3 - T_{\rm ref}^3 )
    - D \Bigl ( \frac{1}{T} - \frac{1}{T_{\rm ref}} \Bigr )
\end{equation}

The total enthalpy $H$ of a cell of volume $V_{cell}$ is given by:
\begin{equation}
H_{cell} (T) = V_{cell} \sum\limits_{m = 1}^{nmat} {\rho _m f_m(T) h_m (T)}
\end{equation}

The cell properties are referenced to zero, for both temperature and enthalpy, to avoid the ambiguity created by various references in multi-material cells. For constant heat capacity, various specific (per unit mass) properties of the ``fully homogenized'' material in a cell are internally calculated and used for checking thermodynamic consistency:
\begin{equation}
h_{cell} (T) = \frac{{\sum\limits_m^{} {h_m (T)\rho _m f_m } }}{{\rho _{cell} }}
\end{equation}
\begin{equation}
C_{Pcell}  = \frac{{\sum\limits_m^{} {C_{Pm} \rho _m f_m } }}{{\rho _{cell} }}
\end{equation}
\begin{equation}
h_{cell}^{\rm ref}  = \frac{{\sum\limits_m^{} {\left( {h_m^{\rm ref}
          - C_{Pm} T_m^{\rm ref} } \right)\rho _m f_m } }}{{\rho
    _{cell} }}
\end{equation}


\subsection{Phase Diagrams}

Only binary temperature-concentration phase diagrams are allowed for now. Multi-component systems will be included in future versions of the code. The binary diagrams can feature complete solubility or eutectic points. \FIG{solub-poz} shows a typical diagram of a generic system with complete solubility both in the liquid (L) and solid solution (SS). The melting temperatures of the two components are $T_A^{M}$ and $T_B^{M}$. The equilibrium lines are approximated by straight lines. At a given temperature $T_1$, after thermodynamic equilibrium was achieved, the concentrations in the two phase are given by $x^L$ and $x^S$. For a given concentration $x_0$, the corresponding temperatures are $T^L$ and $T^S$.

Given the straight line approximation, the slopes of the liquidus and solidus lines are:
\begin{equation}
S^L  = \frac{T^L - T_A^M} {x_0}
\end{equation}
\begin{equation}
S^S  = \frac{T^S - T_A^M} {x_0}
\end{equation}
Both positive and negative slopes are allowed (see
\FIG{solub-neg}). The partition coefficient is defined as:
\begin{equation}
\LEQ{part}
K  = \frac{x^S} {x^L} = \frac{S^L} {S^S}
\end{equation}

The eutectic points  ( \FIG{eutectic}) are define by composition $x^E$ and temperature $T^E$. At the eutectic temperature the transformation is isothermal. A limitation of the current implementation is that for $x_0$ between the solubility limits, the concentration is fixed below the temperature $T^E$, and the decomposition $SS_1 + SS_2$ is not captured.

%A similar limitation is present in the treatment of peritectic systems ( \FIG{peritectic}).


\begin{figure}[t!]
\centering \includegraphics[scale=0.7]{figures/solub-poz.png}
\caption {Binary system with complete solubility and positive liquidus slope.}
\LFIG{solub-poz}

\vspace{1cm}

\centering \includegraphics[scale=0.7]{figures/solub-neg.png}
\caption {Binary system with complete solubility and negative liquidus slope}
\LFIG{solub-neg}
\end{figure}

\begin{figure}[h!]
\centering \includegraphics[scale=0.7]{figures/eutectic.png}
\caption {Binary system with eutectic}
\LFIG{eutectic}
\end{figure}

%\vspace{1cm}

%\centering \includegraphics[scale=0.7]{figures/peritectic.png}
%\caption {Binary system with peritectic}
%\LFIG{peritectic}
%\end{figure}

\subsection{Phase Changes}

Multiple, consecutive phase transformations are allowed in Truchas. Besides the isothermal transformation, the ``non-isothermal'' type is defined by the specific enthalpy of an alloy as a function of temperature (see \FIG{enthalpy}). This is useful to describe multi-component alloys for which the phase diagram is unknown. Several phase transformation models are implemented to describe binary alloys. The models provide the volume fractions at a given temperature and are described in more detail below.

\subsubsection{Lever rule}

In this model the diffusivity of the solute is assumed to be infinite in both the liquid and the solid. The system equilibrates instantaneously and the volume fraction of the solid at temperature
$T$ is:
\begin{equation}
   f^S  =
   \left(
      1 -
      \frac{x_0}{x^L}
%..bm         \frac{T^L-T_A^M}{T-T_A^M}
   \right)
   \frac{1}{1-K}
\end{equation}
In the present straight-line approximation, we have
\begin{equation}
   \frac{x_0}{x^L}
   =
      \frac{T^L - T_A^M}{T - T_A^M}
   \>.
\end{equation}

\subsubsection{Scheil}

The Scheil model preserves the infinite diffusivity in the liquid but assumes that the solute does not diffuse at all in the solid. The volume fraction of the solid is given by:
\begin{equation}
   f^S =
   1 -
   \left(
      \frac{x^L}{x_0}
%..bm         \frac{T-T_A^M}{T^L-T_A^M}
   \right)^{\frac{1}{K-1}}
\end{equation}

For the eutectic diagram shown in  \FIG{eutectic}, the dashed line represents the average concentration in the solid during a solidification process. However, at any fixed temperature, the concentration of the new formed solid is given by the equilibrium solidus line (blue).

\subsubsection{Clyne and Kurz}

This model allows for a finite value of the diffusivity of the solute in the solid phase \cite{clyne_kurz81}. The diffusivity in the liquid is still infinite. The model features a control parameter, relating the diffusivity of the solute in the solid phase $D_B^S$, the solidification time $\Delta t$, and the dendrite arm spacing $\lambda $:
\begin{equation}
\alpha = \frac {D_B^S \, \Delta t} {(\lambda/2)^2}
\end{equation}
The parameter $\alpha$ can take any positive value and is mapped onto the [0, 0.5] interval by the function:
\begin{equation}
   F( \alpha ) = 
   \alpha \left( 1-e^{- \frac {1} { \alpha } } \right)
   - 0.5 \ e^{- \frac {1} {2 \alpha } }
   \>.
\end{equation}
Finally, the volume fraction of the solid is calculated as:
\begin{equation}
   f^S =
   \frac{1}{u}
   \left [
      1-
      \left(
         \frac{x^L}{x_0}
%..bm         \frac{T-T_A^M}{T^L-T_A^M}
      \right)^{\displaystyle \frac{u}{K-1}}
   \right ] \>,
\end{equation}
with
\begin{equation}
   u = 1-2 K F( \alpha )
   \>.
\end{equation}
Here, $K$ is the partition coefficient (\EQ{part}). We note that the lever rule corresponds to $F(\alpha)=0.5$, whereas the Scheil equation corresponds to $F(\alpha)=0$.


\subsubsection{Volume Change During Phase Change}

In case of phase changes that involve a volume change we compute the volume change due to phase change by conserving the mass of the cell. Let us say that there is a cell which has a mass $M_1$ defined by materials of volumes ${V_1}^i$ and densities $\rho^i_1$.  Let us say that the enthalpy of this cell changes resulting in a phase change.  This changes the volumes of the materials to ${V_2}^i$ and the densities to $\rho^i_2$ resulting in a mass change to $M_2$.  If we constrain the mass of the cell after phase change to be equal to the mass of the cell before phase change, this sets up the following equations:
\begin{eqnarray}
\label{massBalance}
&&M_1 = M_2 = M \nonumber \\
&\Rightarrow &\left(\rho^H_1 f^H_1 +  \rho^L_1 f^L_1 \right)\left({V_1}^H + {V_1}^L\right)
      = \left(\rho^H_2 f^H_2 +  \rho^L_2 f^L_2 \right)\left({V_2}^H + {V_2}^L\right)
      \nonumber \\
&\Rightarrow &\left({V_2}^H + {V_2}^L\right) = \left({V_1}^H + {V_1}^L\right)
      \left[\frac{\rho^H_1 f^H_1 +  \rho^L_1 f^L_1 }{\rho^H_2 f^H_2 +  \rho^L_2 f^L_2 }\right]
\end{eqnarray}
where the superscript H refers to the high temperature phase and the superscript L refers to the low temperature phase involved in the phase change, the subscript 1 refers to values before the phase change took place and the subscript 2 refers to the final values after the phase change, and $f^H + f^L = 1$ for both states 1, and 2.

Using Equation \ref{massBalance} we can now calculate the change in total volume of the cell as:
\begin{eqnarray*}
&&V_{cell}^1 = \sum_{i}{V_1}^i = \sum_{i \ne \{H,L\}}{V_1}^i +
                                   \left({V_1}^H + {V_1}^L\right)\nonumber \\
&&V_{cell}^2 = \sum_{i}{V_2}^i = \sum_{i \ne \{H,L\}}{V_2}^i +
                                   \left({V_2}^H + {V_2}^L\right)\nonumber \\
&\Rightarrow& V_{cell}^2 =
    V_{cell}^1 
    + 
    \left[ \frac{\rho^H_1 f^H_1 + \rho^L_1 f^L_1 }
                {\rho^H_2 f^H_2 + \rho^L_2 f^L_2 }
           -1
    \right]
    \left({V_1}^H + {V_1}^L\right)\nonumber \\
\end{eqnarray*}
This gives us a change in volume of the cell as:
\begin {equation}
\label{volumeChange}
{\Delta}V_{cell} = \left[\frac{\rho^H_1 f^H_1 + \rho^L_1 f^L_1 }
                              {\rho^H_2 f^H_2 + \rho^L_2 f^L_2 }
                        -1 \right]
                   \left({V_1}^H + {V_1}^L\right)
\end {equation}
and a change in density of the cell as:
\begin {equation}
\label{volumeChange}
{\Delta}{\rho}_{cell} = M \left(\frac{1}{V_{cell}^2} - \frac{1}{V_{cell}^1}\right)
\end {equation}
Note that if the densities of other materials in the cell were temperature dependent, then that would be reflected in this equation as well.

%\begin{figure}[ht]
%\centering \includegraphics[scale=1]{figures/solidification.png}
%\caption {Phase Diagram}
%\LFIG{int-gen}
%\end{figure}%

%\begin{equation}
%\varepsilon _l \rho _l \frac{{\partial C_l }}{{\partial t}} +
%\varepsilon _l \rho _l u_l \cdot \nabla C_l = \left( {C_l - C_{si} }
%\right)\frac{\partial }{{\partial t}}\left( {\varepsilon _s \rho _s }
%\right) + \frac{{S_v \rho _s D_s }}{{l_s }}\left( {C_s - C_{si} }
%\right)
%\end{equation}
%
%\begin{equation}
%\varepsilon _s \rho _s \frac{{\partial C_s }}{{\partial t}} = \left(
%{C_{si} - C_s } \right)\frac{\partial }{{\partial t}}\left(
%{\varepsilon _s \rho _s } \right) + \frac{{S_v \rho _s D_s }}{{l_s
%}}\left( {C_{si} - C_s } \right)
%\end{equation}
%
%\begin{equation}
%T = T_m  + m_l C_{li} {\rm{ ;  }}C_{si}  = kC_{li}
%\end{equation}


%\subsubsection{Chemical Reaction Heat Source}

\subsection{Boundary and Initial Conditions}

\subsubsection{Radiative Boundary Conditions}
Two types of radiative boundary conditions are implemented in
\truchas:
\begin{itemize}
\item Radiation to an ambient temperature
\item Viewfactor based enclosure radiation model
\end{itemize}

\textbf{Radiation to an ambient Temperature}\\
This boundary condition replaces the flux on boundary surfaces using
the Stefan-Boltzmann law and a time varying ambient temperature.  The
flux leaving any surface calculated thus can be written as:
\begin{equation}
q = - \epsilon \sigma (T_f^4 - T_o^4)
\end{equation}
where $T_f$ is the (absolute) temperature at the boundary face,
$\epsilon$ is the emissivity, $T_o$ is the (absolute) ambient
temperature, and $\sigma$ is the Stefan\_Boltzmann constant.  Since
{\truchas \space} uses cell based temperatures, the temperature at the
face is calculated by equating the flux leaving the surface due to
radiation to the conductive flux from the center of the cell. This is
achieved by solving the following equation:
\begin{equation}
k (T_f - T_c)  - \epsilon \sigma (T_f^4 - T_o^4) = 0
\end{equation}
where $k$ is the conductivity of the cell, $T_c$ is the temperature at
the cell temperature, and all other symbols are as defined before.
The above equation is solved for every face that participates in the
boundary condition at every iteration of the solver.

\textbf{Viewfactor based enclosure radiation model}\\
\truchas \space also supports a more sophisticated radiation model for
enclosures within the simulation domain where radiation leaving one
face might arrive at another face within the system.  In this case we
use a viewfactor based enclosure radiation model.  This model works by
first calculating the fraction of energy leaving any given surface
within the enclosure and arriving at any other surface therein, also
called the viewfactors.  We use the Chaparral package from Sandia
National Laboratory to calculate the viewfactors, and additional
details on the calculation of viewfactors can be found therein.

Once we have the viewfactors, we calculate the flux $q_i$ at an
external boundary face $i$ using the temperatures on all other faces
$j$ and the ambient temperature as\:
\begin{equation}
q_i = - {\epsilon}_i  \sigma T_i^4 - \left( 1 - \rho _i \right) \sum \limits_j
{F_{ij} q_j}
\end{equation}
where $\epsilon_i$ is the emissivity of face $i$, $\sigma$ is the
Stefan-Boltzmann constant, $T_i$ is the absolute temperature at the
boundary face, $F_{ij}$ is the view factor of face $j$ as seen from
face $i$, and the sum on $j$ is over all faces in the system and
includes a special term for the contribution from the ambient
environmant.

This sets up a linear system of equations that is solved for the
fluxes, $q_i$.  As with the simple radiation model, the face
temperatures can be found by nesting this solve within a second loop
that adjusts $T_f$ based on the flux from each face $i$ to the center
of the corresponding cell.

Since this calculation is done on every iteration of the solver for
every timestep, it has the capability to slow down the heat
transfersolution.  To speed the calculation, we allow the user the
option of choosing from the following two approximations:
\begin{itemize}
\item Use cell temperatures for $T_f$
\item Use $q$ based on previous timestep cell temperatures
\end{itemize}

The first of these approximations, using cell temperatures instead of
face temperatures, eliminates the need for the nested solve for face
temparatures, while the second approximation allows us to calculate
the radiative fluxes once every time step and impose a flux boundary
condition on the corresponding faces.  Both these approximations are
independent of each other and can be used based on the timestep, size
of cells in the system, and the overall accuracy of the solution
desired.

%%\subsubsection{Convective Boundary Conditions}
%%\subsubsection{Applied Temperature Boundary Conditions}
%%\subsubsection{Gap Temperature Boundary Conditions}

\subsection{Conservation Law}

The heat transfer algorithm solves for the mixture specific volume enthalpy $h$ and
mixture density $\rho$ via the following conservation law,

\begin{equation}
\LEQ{HT_conserv}
\frac{\partial (\rho h)}{{\partial t}}  + \nabla \cdot
\left( {\rho ^L h^L f^L \mathbf {u}} \right) - \nabla \cdot \left( \kappa {\nabla
 T} \right) = p
\end{equation}

where $f^L$ and $h^L$ are the liquid volume fractions
and the liquid specific volume enthalpy respectively (note that as solid is not mobile, only
the liquid component of the enthalpy is advected). Body sources are represented by $p$.
In the current version of the code these
body sources are due to eddy current induced Joule heating, chemical exothermal reactions
and to a user specified Gaussian source/sink.

\subsection{Boundary Conditions}

The boundary conditions implemented
into the code are imposed on the temperature field and include Dirichlet and Neumann. It is
also possible to specify a heat transfer coefficient, $htc$ which will set the magnitude of thermal
flux between two materials. The flux is set to be proportional to $htc$ and to the temperature
difference on opposite sides of the material interface: $flux = htc (T_+ - T_-)$. Convective
cooling at a surface is implemented as a special case of the heat transfer coefficient boundary condition
with $T_-$ set to the ambient temperature outside the cooled part.

\subsection{Interaction With Other Physics}

The interactions of the heat transfer and phase change solution are primarily with the
flow solution and to a lesser extent, the chemistry and electromagnetic solutions.
Given the fluid velocity, this section calculates the mixture specific volume
enthalpy due to flow advection, chemistry and electromagnetic heat sources.
It also calculates new temperatures, solutal concentrations and solid fractions
which feedback into the flow solution as changes in material properties.

%\section{Algorithms}
%\include{PA+HTPC}

%\def\partial{\d}
%\def\partial{\eps}
%\def\lket{\bigg \{ }
%\def\rket{\bigg \} }

%\section{Heat Transfer}
\section{Heat Transfer Algorithm}
        The heat transfer algorithm in Truchas is based on a finite
volume discretization
of the enthalpy formulation of the energy conservation.
The finite volume approach ensures that the
numerical scheme is locally and globally conservative, while the enthalpy
formulation allows treatment in
a straightforward and unified manner many possible
phase change mechanisms. The phase boundary in the enthalpy method is not tracked, but
is captured via the continous solid/liquid fraction fields. A finite
difference variant of the algorithm is described in a paper by Knoll, Kothe and Lally \cite{kkl}.

To solve \EQ{HT_conserv} an implicit treatment is
employed for the diffusion and temporal terms, a semi-implicit treatment for the advection term and
an explicit treatment for the source terms. This treatment is employed because the
stability restrictions on the diffusion term are more
severe than those on the advection or source terms. The sources currently
allowed in the code are Joule heating, due to eddy currents flowing through the melt induced
by an exterior induction coil (see \CHAPH{electromagnetics}), the power source of chemical nature
(see \CHAPH{chemistry}), and a Gaussian source/sink as specified in the \texttt{HEAT SOURCES}
\texttt{NAMELIST} in the \texttt{Reference Manual}.

\subsection{The Discrete Equations and the Non-linear Residual}

The non-linear residual given below in \EQ{HT_residual} is used in this algorithm to both
evaluate the next iterate of the Newton iteration and to evaluate the Jacobian $\cdot$
vector products during the linear solve with
conjugate gradient-like Krylov iteration (see the chapter on Non-linear solution methods).
The non-linear residual vector is {\it ncells} long, where {\it ncells}
is the number of cells in the simulation. In every cell, upon convergence
of the current non-linear iteration, the residual $F(^kH_{n+1})$ should fall below a specified threshold.
Here, $k$ is the non-linear iteration index, $n+1$ is the index of the time level
at which the solution is sought, and $H$ is the total enthalpy in the cell of interest.
During the iteration, the residual is non-zero and is written down explicitly with the use
of local conservation of energy (\EQ{HT_conserv}).
The terms in \EQ{HT_residual} are all in finite-volume form just as they appear in the code.
The terms in curly brackets indicate a discrete approximation of the enclosed operator.
%The sources currently
%allowed in the code are Joule heating, due to eddy currents flowing through the melt induced
%by an exterior induction coil (see EM chapter), the power source of chemical nature
%(see Chemistry chapter), and a Gaussian source/sink as specified in the ${\rm HEAT\_SOURCE}$ namelist.
%All the source terms are treated explicitly.
\begin{eqnarray}
%\LEQ{HT_residual}
\bigg \{ \frac{\partial H}{\partial t} \bigg \}_{n+1} +
\bigg \{ \sum_{F=1}^{6} {\rm DiffFlux_{F}} \bigg \}_{n+1} +
\bigg \{ \sum_{F=1}^{6} {\rm AdvFlux_{F}} \bigg \}_{n} - \nonumber \\
P_{n, Joule} - P_{n, Chem} - P_{n, Other}  =
\LEQ{HT_residual}
F(^kH_{n+1})
%\LEQ{HT_Hstar}
%H_{*} = \bigg \{ \sum_{F=1}^{6} {\rm AdvFlux_{k}} \bigg \}_{n} + P_{n, Joule} + P_{n, Chem} + P_{n, Other}
\end{eqnarray}

The advection term (sum) has a semi-implicit form.  A single term of
the sum is written in more detail in \EQ{HT_advflx}. The face velocity
${\mathbf u}_{F, n+1}$ is computed from converged cell-centered
velocity values at the current time step ($t = t_{n+1}$), while the
specific enthalpy is taken at $t = t_n$.  Here, $F$ is a cell face
index which ranges from 1 to 6 on a hexahedral mesh.  Non-liquid
materials are assumed to be immobile in the code and thus do not
contribute to enthalpy advection. In \EQ{HT_Pstar}, the explicit and
semi-implicit advection terms are grouped into a quantity $P_*$ which
is computed once at the beginning of the time step.  $P_*$ is further
used to set the first non-linear iterate of the Newton iteration at
the new time level $n+1$ (\EQ{HT_Newtoninit}).
\begin{eqnarray}
\LEQ{HT_advflx}
 \bigg \{ {\rm AdvFlux_{F}} \bigg \}_n  = {\mathbf u}_{F, n+1} {\hat{\mathbf n}_F} A_F h_{liq, n}, \\
\nonumber \\
\LEQ{HT_Pstar}
P_{*} = \bigg \{ \sum_{F=1}^{6} {\rm AdvFlux_{k}} \bigg \}_{n} + P_{n, Joule} + P_{n, Chem} + P_{n, Other} \\
\LEQ{HT_Newtoninit}
^1H_{n+1} = P_* \delta t + H_n
 \end{eqnarray}

The two remaining terms on the left hand side of \EQ{HT_residual} are
the flux term (sum) and the time derivative of $H$. The latter is
given in \EQ{HT_dHdt} and results in a second order time derivative
approximation ($o(\delta t^2)$) at the half-time level ($t = t_n + 0.5
\delta t$).  For a locally orthogonal mesh, a single term in the total
diffusive flux sum is written in \EQ{HT_difflx}.  Here, $R_F$ is the
vector connecting cell centers, $A_F$ is the face area, $k_F$ is face
conductivity and ${\hat{\mathbf n}_F}$ is unit face normal.  For a
non-orthogonal mesh, the flux operator must be different.   Truchas
provides two types of flux operators to use for non-orthogonal meshes
for heat transfer.  See \APP{discrete_ops} and \APP{support_ops} for
details.  By default, the discretization for orthogonal meshes is
fully implicit.  This corresponds to $\theta = 1 $ in \EQ{HT_theta1} and
\EQ{HT_theta2}.  However, $\theta$ is a user defined quantity and is
specified as $Conduction\_implicitness$ variable in the NUMERICS
namelist.  The stability restrictions on the diffusion term are more
severe than those on the advection or source terms. The source terms
currently do not depend explicitly on the enthalpy, or the temperature
field. Therefore a fully implicit discretization is a sensible choice
for the diffusion term.
%normally treated implicitely. These
%are given in more detail in \EQ{HT_dHdt} and \EQ{HT_difflx}.
%A tuning parameter $\theta$ in \EQ{HT_dHdt} appears as
%$Conduction\_implicitness$ variable in the NUMERICS namelist and allows
%to set the degree of implicitness for the  $\big \{{\Large \frac{\partial H}{\partial t} }\big \}$
% term of the residual.
%The default value is one which gives a fully impicit discrete approximation.
% Further, the stability restrictions on the diffusion term are more severe than those on the advection,
%or source terms which currently do not depend explicitly on the enthalpy, or the temperature field.
%Therefore a fully implicit discretization is a sensible choice for the diffusion term. For a locally
%orthogonal mesh a single term in the total diffusive flux sum is written in \EQ{HT_difflx}.
%Here, $R_F$ is the vector connecting cell centers, $A_F$ is the face area, $k_F$ is face conductivity
%and ${\hat{\mathbf n}_F}$ is unit face normal.
\begin{equation}
\LEQ{HT_dHdt}
 \bigg \{ \frac{\partial H}{\partial t} \bigg \}_{n+1} = \frac{^kH_{n+1} - H_n}{\delta t}
 \end{equation}
\begin{equation}
\LEQ{HT_difflx}
 \bigg \{ {\rm DiffFlux_{F}} \bigg \}_{n+1}
   =  -{\mathbf R}_F {\hat{\mathbf n}_F} A_F k_F
     \frac{T(^kh_{n+1}, \hspace{0.05in} h_{n}, \hspace{0.05in} \theta) -
     T(^kh_{n+1, F},\hspace{0.05in} h_{n, F}, \hspace{0.05in} \theta)}{R_F^2}
\end{equation}

For pure materials or alloys undergoing non-isothermal
transformations, the cell-centered temperatures are evaluated by
inverting a  $h(T)$ thermodynamic relationship. For temperature
dependent properties and in the phase change region of a binary alloy
such an inversion also requires taking into account the volume
fractions given by the solidification model and the concentrations
given by the phase diagram. This is achieved via a local cell-by-cell
Newton-Raphson iteration (see the PHYSICS NAMELIST section for the
$h(T)$ specification used in the code).
\begin{eqnarray}
\LEQ{HT_theta1}
  T(^kh_{n+1}, \hspace{0.05in} h_{n}, \hspace{0.05in} \theta)  =
  \theta  T(^kh_{n+1})  + (1 - \theta) T(h_{n})    \\
  \nonumber \\
\LEQ{HT_theta2}
  T(^kh_{n+1, F},\hspace{0.05in} h_{n, F}, \hspace{0.05in} \theta)  =
  \theta  T(^kh_{n+1, F}) + (1 - \theta) T(h_{n, F})
 \end{eqnarray}

%    214           dX(n)  = Cell(j)%Centroid(n) - X_e(n,f,j)
%    215           Distance = Distance + dX(n)**2
%    220           do n = 1,ndim
%    221             dX_tmp = dX(n)/(Distance + alittle)
%    222             dX_scaled(n,f,j) = dX_tmp
%    223           end do
%    136                 dPhi = Phi(j) - Phi_e(f,j)
%    137                 do n = 1,ndim
%    138                    Grad(n,f,j) = dPhi * dX_scaled(n,f,j)
%    139                 end do


\subsection{Preconditioninig}

The linear solution in Truchas requires preconditioning of the locally linearized
non-linear system of equations. The Jacobian based linearization, and
the details of the conjugate gradient and general minimum residual
(GMRES) algorithms are described in detail in the NON-LINEAR SOLUTION
chapter and \APP{LinSolve}.  A preconditionioning strategy adopted
elsewhere in the code uses an approximation to Jacobian for improving
the condition of the local (in time) linear system. The preconditioner
is formed once at the beginning of each time step and the matrix
formed in the code initially coincides with the Jacobian on orthogonal
grids (the true Jacobian changes with every non-linear
iteration). Therefore, the preconditioner can be written as follows:
\begin{eqnarray}
\LEQ{HT_precond}
       {\mathbf P}_{n+1}
      % \frac{\partial\hspace{0.05in} ^1{\mathbf F({\mathbf H}_{n+1})}}{\partial {\mathbf H}_{n+1}}
  \equiv
  \frac{\partial{\mathbf F({\mathbf H})}}{\partial {\mathbf H}} \bigg |_{^1{\mathbf H}_{n+1}}, \quad \quad
       P_{ij} = \frac{\partial F(H_i)}{\partial H_j}
\end{eqnarray}

Here, $i$ and $j$ are cell indexes. The preconditioner is sparse.
On a hexahedral mesh and with a seven point diffusive flux operator
the preconditioner has at most seven non-zero elements in every
row. An off-diagonal and a diagonal elements of the preconditioner are
formed in \EQ{HT_Prec_ij}  and \EQ{HT_Prec_ii}.  In the current state
of the code the form of preconditioner is the same for an orthogonal
and a non-orthogonal mesh.
\begin{equation}
\LEQ{HT_Prec_ii}
i = j, \quad P_{ii} = \delta t^{-1} -  \theta \frac{\partial T_i}{\partial h_i} \frac{1}{Vol_i}
\sum_{F=1}^6 \frac{{\mathbf R}_{iF}{\hat{\mathbf n}_{iF}} A_{iF} k_{iF}} {R_{iF}^2}
\end{equation}
\begin{equation}
\LEQ{HT_Prec_ij}
i \neq j, \quad P_{ij} =  \theta \frac{\partial T_j}{\partial h_j} \frac{1}{Vol_j}
\frac{{\mathbf R}_{ij} {\hat{\mathbf n}_{ij}} A_{ij} k_{ij}}{R_{ij}^2}
\end{equation}

Where ${\rm Vol}_i, {\rm Vol}_j$ are cell volumes and the
$\frac{1}{C_p}$ factor is evaluated using the appropriate $h(T)$
relationship in every cell, taking into account possible
multi-material (multi-phase) composition. In cells undergoing phase
change, the $C_p$ is replaced by the effective heat capacity
$^{{\rm eff}}C_p$, which incorporates the incremental release of
latent heat with an incremental change in the solid/liquid
fraction. For example for a cell composed of solid with solid fraction
$f^S$ and liquid of liquid fraction $1 - f^S$ the effective heat
capacity would read:
\begin{equation}
     ^{eff}C_p = C_{p, liq} + \frac{d f_{sol}}{d T} (h_{sol}(T) - h_{liq}(T)) +
     f_{sol} (C_{p,sol} - C_{p, liq})
\end{equation}

The solid fraction increments with temperature are formed analytically
using one of the currently available phase change models such as
isothermal and non-isothermal transformations and binary alloy
specific Scheil, Lever and Clyne and Kurz models.  For an isothermal
transformation the efffective heat capacity is set at a large finite
value, while its inverse is essntially zero resulting in near zero
off-diagonal terms and a diagonal term $P_{ii} = \delta t^{-1}$ for
the cells undergoing phase transformation.

%\subsection{Matrix vector product via the non linear residual}
%Reference Nonlinear Solution Methods (J. Turner did this one)

%\section{BCs}
%\section{Physics}
%\subsection{Assumptions}
%\subsection{Equations}

\subsection{Heat Sources/Sinks}

\subsubsection{External Heat Source}

All heat sources defined with this namelist are ``volumetric heat sources,'' i.e. the amount of energy deposited in a cell over a given time step has the units of energy/volume. The integral of the heat source over the domain divided by the incremental time step gives the total power defined by the heat source constant. (Note that , in the case of the moving Gaussian below, if the heat source is defined partially outside of the domain this will not be the case.) The two cases below are mutually exclusive, such that only one or the other can be specified in the input file, and not both.

\begin{itemize}
\item {\bf Moving Gaussian:}\\
This type of source is distributed throughout some portion of the domain weighted by a Gaussian distribution, i.e. the change in energy, $\Delta E$,  in a cell due to the heat source over a time step $\Delta t$ is given by,
\begin{equation}
\Delta E =
\frac{A}{\pi^3 r_1 r_2 r_3} \exp{\Biggl(
- \biggl(\frac{x_1}{r_1}\biggr)^2-\biggl(\frac{x_2}{r_2}\biggr)^2-\biggl(\frac{x_3}{r_3}\biggr)^2} \Biggr )
\Delta t \hspace{0.02in} V \hspace{0.04in} ,
\end{equation}

where $(r_1,r_2,r_3)$ represents the radii of the heat source, $(x_1,x_2,x_3)$ is the vector from the cell center to the centroid of the heat source, $V$ is the cell volume and $A$ is the heat source constant.  The total amount of energy, $\Delta E_{\hspace{0.005in}\Omega}$  over a time step, $\Delta t$, is determined by the equation
\begin{equation}
\Delta E_{\hspace{0.005in}\Omega} = \int_\Omega \hspace{0.01in}
\frac{A}{\pi^3 r_1 r_2 r_3} \exp{\Biggl(
-
\biggl(\frac{x_1}{r_1}\biggr)^2-\biggl(\frac{x_2}{r_2}\biggr)^2-\biggl(\frac{x_3}{r_3}\biggr)^2}
\Biggr )
\Delta t \hspace{0.02in} d \hspace{0.01in} V \hspace{0.04in} ,
\end{equation}
where $\Omega$ represents the domain.  Note that $\Delta
E_{\hspace{0.005in}\Omega} = A\Delta t$.

\item {\bf Picewise Constant:}\\
This type of source is the sum of scaled characteristic functions of mutually disjoint subdomains $\Omega_i$, $i=1,\ldots,M$ of the domain $\Omega$, such that
\begin{equation}
q(x,y,z) = \sum_{i=1}^M  A_i \chi_{\Omega_i}(x,y,z),
\end{equation}
where $A_i$ is the value of the volumetric heat source in subdomain $\Omega_i$. The change in energy, $\Delta E$ in a cell due to the heat source over a time step $\Delta t$ is given by
\begin{equation}
\Delta E = q \Delta t V,
\end{equation}
where $V$ is the cell volume. In other words, if the cell is contained in $\Omega_i$, then we have
\begin{equation}
\Delta E = A_i \Delta t V.
\end{equation}
The total amount of energy, $\Delta E_\Omega$, over a time step is determined by the equation \begin{equation} \Delta E_\Omega = \int_\Omega q \Delta t dV = \sum_{i=1}^M A_i \int_{\Omega_i} dV. \end{equation} We note that the special case, where $M=1$ and $\Omega_1 = \Omega$ is possible. This is the case where a constant volumetric heat source is specified over the entire domain, $q(x,y,z) = A$.

\end{itemize}




