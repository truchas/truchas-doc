{% local definitions
\newcommand{\R}{\mathbb{R}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\W}{\mathcal{W}}
\newcommand{\norm}[1]{\lVert#1\rVert}

\chapter{Nonlinear Solution Methods}
\LAPP{NonLinSolve}

A continually recurring problem in \truchas\ is the need to find an approximate
solution to
\begin{equation}\label{F=0} 
  F(u) = 0,
\end{equation}
where $F\!:\!\Omega\subset\mathbb{R}^m\to\mathbb{R}^m$ is a nonlinear mapping
and $m$ is order of the mesh size.  Methods of solving (\ref{F=0}) when $F$
is smooth%
\footnote{Strictly speaking, the nonlinear systems encountered in \truchas\
may not be smooth everywhere owing, for example, to nonsmoothness in phase
diagrams or the solid mechanics contact model.  Nevertheless it is useful in
in practice to ignore this fact, recognizing that the nonlinear solver
convergence behavior will be adversely effected in a neighborhood of the
nonsmoothness.}
are typically some variant of Newton's method: if $u_k$ is the current
approximate solution, the next approximate solution is $u_{k+1} :=
u_k + \delta u_{k+1}$, where $\delta u_{k+1}$ is the solution of the
\emph{correction equation}
\begin{equation}\LEQ{Ncorr}
  J_k \delta u_{k+1} = - F(u_k)
\end{equation}
with Jacobian $J_k := F'(u_k)$.  Unfortunately, it may be very expensive,
or even practically impossible, to compute the Jacobian and/or solve the
correction equation exactly, especially for larger systems.  In such
situations, one can instead seek an approximate solution to (\ref{eq:Ncorr}),
possibly using an approximation for $J_k$, which gives rise to an
\emph{inexact Newton method}.  \truchas\ provides two such methods: the
Jacobian-free Newton-Krylov method and an accelerated inexact Newton method.

\section{Jacobian-Free Newton-Krylov Method}

The Generalized Minimal RESidual (GMRES) algorithm~\cite{saad} is used
to solve \EQ{Ncorr}.  GMRES (or any other Krylov method such as
conjugate gradients) defines an initial linear residual, \( {\mathbf
r}_{0} \) given an initial guess, \( \mathbf{\delta u}_{0} \),
\begin{equation} {\mathbf r}_{0} = -{\mathbf F ({\mathbf u})} - 
{\mathbf J} \mathbf{\delta u}_{0}.
\end{equation}
Note that the nonlinear iteration index, \(k \), has been dropped.

The \(l^{th}\) GMRES iteration 
minimizes \(\parallel \mathbf{ J} \mathbf{\delta u}_{l} +
\mathbf{ F} (\mathbf{u}) \parallel_{2} \) with a least squares
approach.  \( \mathbf{\delta u}_{l} \) is constructed from a linear
combination of the Krylov vectors \(\{{\mathbf
r}_{0}, {\mathbf J}{\mathbf r}_{0}, ({\mathbf J})^{2} {\mathbf r}_{0},
..., ({\mathbf J})^{l-1} {\mathbf r}_{0}\}\), which were constructed
during the previous \( l - 1\) GMRES iterations.  This linear
combination of Krylov vectors can be written as,
\begin{equation}
\mathbf{\delta u}_{l} = \mathbf{\delta u}_{0}
+ \sum_{j=0}^{l-1} \alpha_{j} ({\mathbf J})^{j} {\mathbf r}_{0},
\LEQ{sum}
\end{equation}

where evaluating the scalars \(\alpha_{j} \) is part of the GMRES
iteration.
Upon examining \EQ{sum} we see that 
GMRES  requires the action of the
Jacobian only in the form of matrix-vector products,
which can be approximated by~\cite{nk}
\begin{equation}
{\mathbf J} {\mathbf v} \approx
[{\mathbf F}({\mathbf u} + \epsilon
{\mathbf v}) - {\mathbf F}({\mathbf u}) ] \; / \; \epsilon ,
\LEQ{mfree}
\end{equation}
where \({\mathbf v} \) is a Krylov vector (i.e. one of \(\{{\mathbf r}_{0},
{\mathbf J}{\mathbf r}_{0}, ({\mathbf J})^{2} {\mathbf r}_{0}, ...,
({\mathbf J})^{l-1} {\mathbf r}_{0}\}\)), and $\epsilon$ is a small perturbation.

\EQ{mfree} is a first order Taylor series expansion approximation to
the Jacobian, \(\mathbf J \), times a vector, \(\mathbf v\).  For
illustration consider the two coupled nonlinear equations
\(F_{1}(u_{1},u_{2}) = 0, \; F_{2}(u_{1},u_{2}) = 0 \).
The Jacobian for this problem  is;

\[ {\mathbf J}  = \Large  \left[ \begin{array}{cc}
\frac{\partial F_{1}}{\partial u_{1}} & \frac{\partial F_{1}}{\partial u_{2}} \\
     &         \\
\frac{\partial F_{2}}{\partial u_{1}} & \frac{\partial F_{2}}{\partial u_{2}} \\
\end{array} \right]. \]

\noindent
Working backwards from \EQ{mfree}, we have;

\[
\frac{{\mathbf F}({\mathbf u} + \epsilon{\mathbf v}) 
- {\mathbf F}({\mathbf u})}{\epsilon}  =
\large
\left(
\begin{array}{c}
\frac{1}{\epsilon}\Big[{F_{1}( u_{1} +\epsilon v_{1}, u_{2} +
   \epsilon v_{2}) - F_{1}(u_{1},u_{2})}\Big] \\
 \\
\frac{1}{\epsilon}\Big[{F_{2}( u_{1} +\epsilon v_{1}, u_{2} +
   \epsilon v_{2}) - F_{2}(u_{1},u_{2})}\Big] \\
\end{array}
\right).
\]

\noindent
Approximating \( {\mathbf F}({\mathbf u} + \epsilon{\mathbf v}) \)
with a first order Taylor series expansion about \( {\mathbf u} \), we have;

\[
\frac{{\mathbf F}({\mathbf u} + \epsilon{\mathbf v}) 
  - {\mathbf F}({\mathbf u})}{\epsilon}  \approx
\large
\left(
\begin{array}{c}
\frac{1}{\epsilon}\Big[{F_{1}(u_{1},u_{2}) 
+\epsilon v_{1} \frac{\partial F_{1}}{\partial u_{1}} 
+\epsilon v_{2} \frac{\partial F_{1}}{\partial u_{2}}
- F_{1}(u_{1},u_{2})}\Big] \\
\\
\frac{1}{\epsilon}\Big[{F_{2}(u_{1},u_{2})
+\epsilon v_{1} \frac{\partial F_{2}}{\partial u_{1}} 
+\epsilon v_{2} \frac{\partial F_{2}}{\partial u_{2}}
- F_{2}(u_{1},u_{2})}\Big] \\
\end{array}
\right). \]

\noindent
This expression can be  simplified to;

\[ \large \left( \begin{array}{c}
 v_{1} \frac{\partial F_{1}}{\partial u_{1}}
+ v_{2} \frac{\partial F_{1}}{\partial u_{2}} 
 \\
 \\
 v_{1} \frac{\partial F_{2}}{\partial u_{1}}
+ v_{2} \frac{\partial F_{2}}{\partial u_{2}} 
 \\
\end{array} \right) = {\mathbf J v} \]

This matrix-free approach, besides its obvious memory advantage, has
many unique capabilities.  Namely, Newton-like nonlinear convergence
without {\it forming} or {\it inverting} the true Jacobian.

To complete the description of this technique we provide a
prescription for evaluating the scalar perturbation. In this study
$\epsilon$ is given by,

\begin{equation}
\epsilon = \frac {1}{N || {\mathbf v} ||_2} \sum_{m=1}^{N}b | u_m |  ,
\LEQ{pert}
\end{equation}
where \( N \) is the linear system dimension and \( b \) is a constant
whose magnitude is approximately the square root of machine roundoff
(\( b = 10^{-5}\) for most of this study).

We employ right preconditioning and thus we are solving,
\begin{equation}
({\mathbf J P^{-1}}) (\mathbf{P \delta u}) = -{\mathbf F ({\mathbf u})}.
\end{equation}
\( {\mathbf P}\) symbolically represents the preconditioning
matrix and \({\mathbf P}^{-1} \) the inverse of preconditioning matrix.
In practice, this inverse is only approximately realized through
some standard iterative method, and thus we may think of it more
as \(\tilde{{\mathbf P}}^{-1} \).

The right preconditioned matrix-free algorithm is:

\begin{equation}
{\mathbf J} \tilde{{\mathbf P}}^{-1} {\mathbf v} \approx
[{\mathbf F}({\mathbf u} + \epsilon \tilde{{\mathbf P}}^{-1}
{\mathbf v}) - {\mathbf F}({\mathbf u}) ] \; / \; \epsilon ,
\LEQ{Pmfree}
\end{equation}

This is done is actually done in two steps;
\begin{enumerate}

\item Solve (iteratively, and not to convergence)
\( {\mathbf P y}= {\mathbf v} \) for \({\mathbf y}\)

\item Perform \( {\mathbf J} {\mathbf y} \approx
[{\mathbf F}({\mathbf u} + \epsilon
{\mathbf y}) - {\mathbf F}({\mathbf u}) ] \; / \; \epsilon , \)

\end{enumerate}
Thus only the matrix \( {\mathbf P} \) is formed and only the matrix
\( {\mathbf P} \) is iteratively inverted.

There are two choices to be made;
\begin{enumerate}

\item What linearization should be used to form \( {\mathbf P } \) ?

\item  What linear iterative method should be used to solve
\( {\mathbf P y}= {\mathbf v} \) ?

\end{enumerate}


\section{An Accelerated Inexact Newton Method}
In this section we consider a nonlinear Krylov acceleration procedure for
inexact Newton's method that was introduced in \cite{Carlson98a} (see also
\cite{Miller05}).  This method falls into a broad class of accelerated
inexact Newton (AIN) schemes discussed in \cite{Fokkema98}, though it
differs from the specific methods described there.

In the inexact Newton method the correction equation (\ref{eq:Ncorr}) is solved
only approximately, and often with an approximation $M_k$ for the Jacobian
$J_k$ as well.  Formally we may express this inexact correction $v_{k+1}$ as
\begin{equation}\label{ain:INcorr}
  v_{k+1} := -P(u_k)^{-1} F(u_k),
\end{equation}
where $P(u_k)^{-1}$ is the \emph{preconditioner} for the nonlinear system.
Commonly $P^{-1}$ is only realized as an iterative procedure applied to the
system $M_k v = -F(u_k)$; neither $P$ nor $P^{-1}$ need ever be explicitly
formed.  The ideal preconditioner would be the exact inversion of $J_k$,
yielding Newton's method.

Defining $G(u) = - P(u)^{-1} F(u)$, we recognize that this inexact Newton
iteration is simply the standard fixed point iteration
$u_{k+1} := u_k + G(u_k)$, which converges if $G'$ is sufficiently close
to $-I$ in a neighborhood of the root.
Indeed, if $P(u) = F'(u)$ then $G' = -I$ at the root.

With this fixed point iteration view of inexact Newton as a reference,
the accelerated method proceeds with successive approximations 
$u_{k+1}=u_k+v_{k+1}$, $k=0,1,\ldots$  At iteration $k$ we have accumulated
from previous iterations the correction space
$\V_k=\operatorname{span}\{v_1,\dots,v_k\}$
and corresponding $G$-difference space
$\W_k=\operatorname{span}\{w_1,\dots,w_k\}$,
where $w_n=G(u_n)-G(u_{n-1})$.  The fixed point iteration chooses
$v_{k+1}=G(u_k)$ as the next correction.  The ideal correction would solve
$G(u_k+v_{k+1})=0$, which isn't feasible, but linearizing one could choose
$v_{k+1}$ such that
\begin{equation}\label{ain:Lcorr}
  0 = G(u_k) + G'(u_k) v_{k+1}.
\end{equation}
The fixed point iteration, not knowing anything about $G'$, invokes the
assumption that $G'\approx-I$ and so substitutes $-I$ for $G'$.  However,
if $G'$ is nearly constant in a neighborhood of the root then
\begin{equation}\label{ain:wv}
  w_n \approx G' v_n
\end{equation}
so that one does (approximately) know $G'$ on the space $\V_k$.  Hence we
split the correction in (\ref{ain:Lcorr}) into two pieces: $v_{k+1}=p+q$
with $p\in\V_k$, for which we know the action of $G'$ by (\ref{ain:wv}),
and leftover piece $q$, for which we do not and so will replace $G'$ by $-I$
as in the fixed point iteration.  Introducing the matrices
$V_k = [v_1\ldots v_k]$ and $W_k=[w_1\ldots w_k]$, this split correction is
\begin{equation}
  p = V_k y, \quad 0 = G(u_k) + W_k y - q,
\end{equation}
where $y\in\R^k$ is chosen so that $W_k y$ is the best $l_2$ fit to $-G(u_k)$.
The accelerated correction is thus
\begin{equation}\label{ain:Acorr}
  v_{k+1} := G(u_k) - (V_k + W_k)(W_k^T W_k)^{-1} W_k^T G(u_k).
\end{equation}
For the first iteration ($k=0$) the spaces $\V_0$ and $\W_0$ are empty,
and the correction $v_1$ is identical to the fixed point iteration
correction $G(u_0)$.

Notice that there is nothing that requires one to use \emph{all} the previous
corrections $v_1,\dots,v_k$ (and their corresponding $G$-differences) when
computing the accelerated correction (\ref{ain:Acorr}).  In fact, since $G$
is nonlinear, we might expect only the most recent corrections to yield
decently accurate information about $G'$.  Also there is nothing to
guarantee that the $w_n$ vectors are linearly independent.  The accelerated
method, therefore, assembles the pair of acceleration spaces $(\V_k, \W_k)$
by taking the vectors in reverse order, starting with $(v_k, w_k)$, and
dropping any pair $(v_n, w_n)$ whenever $w_n$ is nearly in the span of the
preceding $w$-vectors.  Moreover, the number of vectors used is limited to a
maximum number, which is typically very small so that the cost, in time and
memory, to compute the accelerated correction is quite modest.




\section{Preconditioning}

}
