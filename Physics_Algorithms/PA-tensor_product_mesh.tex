\chapter{Tensor Product Mesh Generation}

A \textit{tensor product} mesh is characterized by cells that are
orthogonal quadrilaterals (rectangles) in 2-D or orthogonal hexahedra
(bricks) in 3-D. In addition, the edges of each (hex or quad) cell in
a tensor product mesh possess lengths that are in general different. A
\textit{uniform} tensor product mesh is one in which all cell edge
lengths are identical. While a uniform mesh is attractive from a
numerical discretization point of view (e.g., it is easier to quantify
discretization errors), it is frequently more computationally
efficient to use a nonuniform mesh so that cells are concentrated in a
particular region interest, such as in an area where solution
gradients are steeper. Although there are several methods for
generating a nonuniform mesh, described herein is a method which is
preferred in the \telluride project -- and implemented in the \truchas
code -- namely a mesh in which the ratios of widths or any pairs of
adjacent cells is a constant~\cite{wilson}.  This type of mesh,
described below, is term a ``ratio-zoned'' mesh.

\section{Description of a 1-D Ratio-Zoned Mesh}

Assume an interval of width $w$ must be descretized,
\begin{equation}
X_0 \leq X \leq X_0+w\, ,
\end{equation}
with a ``stretched'' grid of $N$ cells having the property
\begin{equation}
{X_{i+1}-X_i\over X_i-X_{i-1}} = \beta\, ,
\end{equation}
for $1<i<N$. The above equation can be written in continuous form
as
\begin{equation}
{dX\over di}=a\,\beta^i\, , \qquad 0\leq i \leq N\, ,
\LEQ{xcont}
\end{equation}
where $a$ is a constant. Integrating \EQ{xcont}:
\begin{equation}
\int_{X_0}^X{dX}=\int_0^ia\beta^i\, di\, ,
\end{equation}
gives
\begin{equation}
X_i - X_0 = {a\over\ln\beta}\left(\beta^i-1\right)\, .
\LEQ{xsoln}
\end{equation}
To simplify the discussion, assume $X_0=0$ and divide \EQ{xsoln}
by $w$ to give
\begin{equation}
x_i = {a\over\ln\beta}\left(\beta^i-1\right)\, ,
\LEQ{xnorm}
\end{equation}
where $x=X/w$ and $a$ is now a dimensionless constant. The location of the mesh
intervals, $x_i$, is given in \EQ{xnorm} as a function of three
variables: $a$, $\beta$, and $i$. Two auxiliary equations are further required
to solve \EQ{xnorm}. Given the problem definition, we have the
first:
\begin{equation}
x(a,\beta,i=N)=1\, ;
\LEQ{x1}
\end{equation}
and, with loss of generality, the width of the first cell ($\Delta$) is assumed
to be known, giving the second:
\begin{equation}
x(a,\beta,i=1)=\Delta\, .
\LEQ{x2}
\end{equation}
\EQ{xnorm}, subject to the requirement of \EQ{x2},
can be solved for $a$:
\begin{equation}
a = {\Delta\ln\beta\over\beta - 1}\, ;
\LEQ{a}
\end{equation}
which allows \EQ{xnorm} to be written as
\begin{equation}
x_i = {\Delta\over\beta-1}\left(\beta^i-1\right)\, ,\qquad 0\leq i \leq N\, .
\LEQ{xfinal}
\end{equation}
\EQ{xfinal}, which is the formula for the sum of the first
$i$ terms of a geometric series, may be written as 
\begin{equation}
f = {\Delta\over\beta-1}\left(\beta^N-1\right)-1=0\, .
\LEQ{f}
\end{equation}
using the requirement of \EQ{x1}. The method used to solve
\EQ{f} depends upon whether $\beta$ or $N$ is known. Each
case is considered in the following.

\subsection{Case 1: $N$ is Given; Find $\beta$}

Because $f(\beta)$ is a monotonic function (for $\beta\geq 0$, $N>0$),
\EQ{f} may be readily solved for $\beta$ using a Newton-Raphson
(NR) method:
\begin{equation}
\beta_{k+1} = \beta_k - {f_k\over f^{\prime}_k}\, ,
\LEQ{beta}
\end{equation}
where
\begin{equation}
f^{\prime} = {\partial f\over\partial\beta} = {\Delta\over\beta - 1}
\left[N\beta^{N-1}-{\beta^N-1\over\beta - 1}\right]
\end{equation}
and $k$ is the iteration count.

\subsection{Case 2: $\beta$ is Given; Find $N$}

If $\beta$ is known, \EQ{f} may be solved directly for $N$:
\begin{equation}
N^* = {\ln\left(1 + {\beta - 1\over \Delta}\right)\over\ln\beta}\, .
\LEQ{nstar}
\end{equation}
The value of $N^*$ given by \EQ{nstar} is in general not
an integer, so $N$ is found by rounding $N^*$ up to the next whole number:
\begin{equation}
N=\texttt{int}(N^* + 1)\, .
\LEQ{nint}
\end{equation}

\subsection{Bounds for $\beta$}

Although $f(\beta)$ is a monotonic function, the magnitude of $f$ and its
derivatives can be large in the neighborhood of the root if the solution
to \EQ{f} is close to unity. This can cause difficulties
for the iteration algorithm (\EQ{beta}) unless a suitable
initial guess, $\beta_0$, is given.

If $\beta < 1$, then the size of the last cell will be smaller than the
average cell size:
\begin{equation}
\Delta\beta^{N-1} < 1/N\, , \qquad\textrm{or} \qquad \beta <
\left({1\over\Delta N}\right)^{1/N-1}\, .
\LEQ{betamin}
\end{equation}
A lower bound for $\beta$ (when $\beta < 1$) can be found by allowing $N$ to
become unbounded in \EQ{f}:
\begin{equation}
\lim_{N\to\infty} {\Delta\over\beta_\mathrm{\, min} - 1}\bigl[\beta_\mathrm{\, min}^N - 1\bigr] - 1\, ,
\qquad\textrm{or} \qquad \beta_{\mathrm{\, min}} = 1 - \Delta\, .
\end{equation}
Thus $N$ may be computed from \EQA{nstar}{nint} provided
$\beta > \beta_\mathrm{\, min}$.

If $\beta > 1$, the size of the last cell will be larger than the average cell size:
\begin{equation}
\Delta\beta^{N-1} > 1/N\, , \qquad\textrm{or} \qquad \beta >
\left({1\over\Delta N}\right)^{1/N-1}\, .
\end{equation}
An upper bound for the $\beta > 1$ case follows by observing that the sum of the
widths of the first $N-1$ cells is larger than $N-1$ times the smallest cell size:
\begin{equation}
1-\Delta\beta^{N-1} > (N-1)\Delta\, , \textrm{or} \quad \beta <
\left({1-(N-1)\Delta\over\Delta}\right)^{1/N-1}\, .
\LEQ{betamax}
\end{equation}

Bounds for $\beta$, given in \EQT{betamin}{betamax}, may be summarized as
\begin{equation}
1-\Delta < \beta< \biggl[{1\over\Delta}\biggr]^{1/N-1}\, \textrm{for}\quad \beta < 1\, ,
\LEQ{betaboundlow}
\end{equation}
and
\begin{equation}
\biggl[{1\over\Delta N}\biggr]^{1/N-1} < \beta < \biggl[{1-(N-1)\Delta\over\Delta}\biggr]^{1/N-1}
\, \textrm{for}\quad \beta > 1\, .
\LEQ{betaboundhigh}
\end{equation}

\section{Parameterizing the 1-D Ratio-Zoned Mesh}

Six parameters characterize the 1-D ratio-zoned tensor product
mesh in the previous section:
\begin{description}
\item{$\bullet$} $\beta$ - the ratio of widths of any pair of adjacent cells;
\item{$\bullet$} $\Delta_\mathrm{L}$ - the first cell width in the mesh interval;
\item{$\bullet$} $\Delta_\mathrm{R}$ - the last cell width in the mesh interval;
\item{$\bullet$} $N$ - the number of cells in the mesh interval;
\item{$\bullet$} $w$ - the total width of the mesh interval; and
\item{$\bullet$} $a$ - the mesh constant given by \EQ{a}.
\end{description}
A simple program can be written to compute three out of the six parameters above,
with the other three having been specified. Six combinations of
of user-specified (known) input and computed (unknown) output
are possible:
\begin{enumerate}
\item Given $\Delta_\mathrm{L}$, $N$, and $w$;
      compute $\Delta_\mathrm{R}$, $a$, and $\beta$.
\item Given $\Delta_\mathrm{R}$, $N$, and $w$;
      compute $\Delta_\mathrm{L}$, $a$, and $\beta$.
\item Given $\Delta_\mathrm{L}$, $\beta$, and $w$;
      compute $\Delta_\mathrm{R}$, $a$, and $N$.
\item Given $\Delta_\mathrm{R}$, $\beta$, and $w$;
      compute $\Delta_\mathrm{L}$, $a$, and $N$.
\item Given $\Delta_\mathrm{L}$, $\beta$, and $N$;
      compute $\Delta_\mathrm{R}$, $a$, and $w$.
\item Given $\Delta_\mathrm{R}$, $\beta$, and $N$;
      compute $\Delta_\mathrm{L}$, $a$, and $w$.
\end{enumerate}
To insure that the iteration procedure in \EQ{beta} converges,
$\beta_0$ is chosen to be the upper bound from \EQ{betaboundlow}
or \EQ{betaboundhigh}. When solving for $N^*$ in \EQ{nstar},
the program uses $\beta=1.01\beta_\mathrm{\, min}$ if the input value of $\beta$
is less than $\beta_\mathrm{\, min}$.

\section{Summary}

An interval of width $w$ is meshed with $N$ cells using an exponential function:
\begin{equation}
X_i = {\Delta_\mathrm{L}(\beta^i - 1)\over\beta - 1}\quad \textrm{or}\quad
X_i = w - {\Delta_\mathrm{R}(\alpha^{N-i} - 1)\over\alpha - 1}\,
\LEQ{Xi}
\end{equation}
where $0\leq i\leq N$, $0\leq X\leq w$, and
\begin{eqnarray}
X_i   & = & \textrm{coordinate of grid point}\, i\, ,         \nonumber\\
w     & = & \textrm{width of the mesh interval}\, ,           \nonumber\\
N     & = & \textrm{number of cells in the mesh interval}\, , \nonumber\\
\beta & = & \textrm{ratio} = (X_{i+1}-X_i)/(X_i-X_{i-1})\, ,  \nonumber\\
\alpha & = & 1/\beta,                                                       \nonumber\\
\Delta_\mathrm{L} & = & \textrm{width of left (first) cell} = X_1 - X_0\, , \nonumber\\
\Delta_\mathrm{R} & = & \textrm{width of right (last) cell} = X_N - X_{N-1}\, .
\end{eqnarray}
Setting $i=N$ and $i=0$ in \EQ{Xi} gives
\begin{equation}
X_N = w = {\Delta_\mathrm{L}(\beta^N - 1)\over\beta - 1}
        = {\Delta_\mathrm{R}(\alpha^N - 1)\over\alpha - 1}\, .
\LEQ{XN}
\end{equation}
Setting $i=N-1$ in \EQ{Xi} gives
\begin{equation}
X_{N-1} = {\Delta_\mathrm{L}(\beta^{N-1} - 1)\over\beta - 1}\, ,
\LEQ{XN1}
\end{equation}
which, when subtracted from \EQ{XN}, gives
\begin{equation}
\Delta_\mathrm{R} = \Delta_\mathrm{L}\beta^{N-1}\, .
\LEQ{DeltaR}
\end{equation}
When any three variables of the set $(\beta,N,\Delta_\mathrm{R},\Delta_\mathrm{L},w)$
are known, \EQA{XN}{DeltaR} may be used to find the unknown variables.

\section{Specifying a Tensor Product Mesh for \truchas}

In \truchas, the current set of allowed input parameters are $\beta$, $N$, and $w$,
which gives simple algebraic expressions for $\Delta_\mathrm{L}$ and $\Delta_\mathrm{R}$,
namely:
\begin{equation}
\Delta_\mathrm{L} = w{\beta - 1\over\beta^N-1}\, \quad\textrm{and}\quad
\Delta_\mathrm{R} = \Delta_\mathrm{L}\beta^{N-1}=w(\beta - 1)\, .
\end{equation}
