\chapter{Sensitivity Analysis}
\LAPP{sensitivityanalysis}

Sensitivity analysis plays a vital role in trade-off, reliability, inverse/identification,
and optimization studies.   For example, in a trade-off study one can
view the temperature sensitivity field to determine where the most drastic
temperature changes will occur if one alters the value of a prescribed Neumann temperature boundary
condition.


 By definition, the sensitivities are the derivatives of the
system response with respect to the model parameters.  To compute the sensitivities in \truchas
  the direct differentiation and semi-analytical methods are combined
resulting in  efficient and accurate computations that require minimal code modifications.

To explain this sensitivity analysis, consider  an analysis which is descretized into a series of cell equations
 and then assembled into a  residual vector (cf. equation \ref{F=0})
\({\mathbf F ({\mathbf u})} \) where  \({\mathbf u} \) is the discretized state vector.
The nonlinear equation \({\mathbf F ({\mathbf u})}  = \mathbf{0} \) is then solved for  \({\mathbf u} \)
in the \textsl{primal analysis }
by using the nonlinear solver  discussed in \APP{NonLinSolve}.
Of course, if we change a model parameter $s$, then the residual   and hence the
state change.  To reflect this dependency, the residual equation is expressed as
\begin{equation}
{\mathbf F ({\mathbf u}(s), s)}  = \mathbf{0}
\LEQ{sens1}
\end{equation}
where it is understood that the parameter $s$ is a known input.
To determine the sensitivity of the state with respect to a parameter $s$, one could perturb the
parameter by the amount $\delta$, i.e. set $s \longrightarrow s + \delta  $,  solve the residual equation
\begin{equation}
{\mathbf F ({\mathbf u}(s+ \delta ), s+ \delta  )}  = \mathbf{0}
\LEQ{sens2}
\end{equation}
and compute the derivative from the approximation
\begin{equation}
\mathbf{u}'(s) \approx \frac{1}{\delta  } \left[ \mathbf{u}(s+\delta  ) - \mathbf{u}(s) \right]
\LEQ{sens3}
\end{equation}
This \textsl{finite difference } sensitivity analysis is simple to implement as  the analysis is merely repeated
with the perturbed parameter.  However, it is computationally unattractive since it
requires an additional  nonlinear analysis for each parameter $s$, for which the sensitivities are desired.
Moreover, it is susceptible to truncation errors if $ \delta $ is too large and
round-off errors if  $ \delta $ is too small.  Of course the truncation error could be reduced by
using a second-order accurate approximation, i.e.
\begin{equation}
\mathbf{u}'(s) \approx \frac{1}{2 \, \delta  } \left[ \mathbf{u}(s+\delta  ) - \mathbf{u}(s-\delta) \right]
\LEQ{sens3s}
\end{equation}
however, this would require another nonlinear analysis to evaluate the perturbed state $\mathbf{u}(s-\delta)$.


Two  methods, the \textsl{adjoint} and \textsl{direct differentiation},  have been used to
efficiently and accurately evaluate sensitivities for a wide variety of systems.
The direct approach has been adopted in \truchas to avoid the
cumbersome backward time mappings that are required by the adjoint method
\cite{Michaleris93:plast}.  Due to the transient nature of the system
it is  necessary to rewrite the  residual equation as
\begin{equation}
{\mathbf F ({\mathbf u}_{n}(s),{\mathbf u}_{n-1}(s), s)}  = \mathbf{0}
\LEQ{sens4}
\end{equation}
In the above equation, the state ${\mathbf u}_{n}(s)$ evaluated at time $t=t_{n}$ is determined
in the primal analysis 
given the known inputs: the state ${\mathbf u}_{n-1}(s)$  at time previous time step $t=t_{n-1}$
and the parameter $s$. Of course, ${\mathbf u}_{n-1}(s)$ is known as it is evaluated
during the primal analysis at the previous time step and for the initial time, i.e. $t=t_{0}$,
${\mathbf u}_{0}(s)$ is the known initial condition.  And, of course ${\mathbf u}_{n-1}(s)$
is a function of $s$ as it  changes if $s$ changes.

Differentiating  \EQ{sens4} with respect to $s$ and rearranging gives the \textsl{pseudo problem }
\begin{equation}
{\mathbf J} \,  {\mathbf u}_{n}' = -\left\{
\frac{ \partial \mathbf{F}}{\partial \mathbf{u}_{n-1}} \, \mathbf{u}_{n-1}' +
\frac{ \partial \mathbf{F}}{\partial s}
\right\}
\LEQ{sens5}
\end{equation}
where ${\mathbf J}$ is the Jacobian of \EQ{Ncorr}.  The \textsl{pseudo load},
i.e. the right-had side of \EQ{sens5} is known since we know the dependency of the residual
on $\mathbf{u}_{n-1}$ and $s$ and since the sensitivity $\mathbf{u}_{n-1}'(s)$ is evaluated
during the pseudo  analysis at the previous time step and since for the initial time
${\mathbf u}_{0}'(s)$ is the known initial condition sensitivity.

Note that the pseudo problem is linear in the sensitivity $ {\mathbf u}_{n}'(s)$
so the computation of the sensitivity $ {\mathbf u}_{n}'(s)$
requires a  linear solve even though  the
computation of the state $ {\mathbf u}_{n}(s)$ requires
 a nonlinear solve.
 In total, one linear pseudo problem is solved for each parameter $s$ for which the
 sensitivity is desired.

Also note that  the  linear solve uses the same coefficient matrix that appears in \EQ{Ncorr}.
Thus, the same solver that is used to resolve  \EQ{Ncorr} can be used to resolve
 \EQ{sens5}.  In the best case scenario, the Jacobian  ${\mathbf J}$ is assembled
and factored so that the solution of the each  pseudo problem (one pseudo problem is
solved for each design parameter) only requires a back solve.  However, this is 
seldom the case.  So in an effort to hasten the pseudo analyses, one may wish 
to invest extra effort to develop  an effective preconditioner for
 ${\mathbf J}$ during the primal analysis  so it  can also  be utilized for 
 the subsequent pseudo analyses.

The challenge of the sensitivity analysis is the ability to evaluate the
pseudo load, i.e. the right-hand side of \EQ{sens5}.
The derivatives can be computed analytically or by symbolic differentiation
software.  However, in \truchas the \textsl{semi-analytical} method is
used whereby the pseudo load is approximated from the second-order accurate
finite difference approximation
\begin{eqnarray}
\LEQ{sens6}
&&
\left. \left\{
\frac{ \partial \mathbf{F}}{\partial \mathbf{u}_{n-1}} \,
            \mathbf{u}_{n-1}' +
\frac{ \partial \mathbf{F}}{\partial s}
\right\}  \right|_{(\mathbf{u}_n, \mathbf{u}_{n-1}, s) = (\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s )}
 \\
&& \hspace{.25in}
\approx
\frac{1}{2 \, \delta} \left\{
\left[ \mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) + \delta \, \mathbf{u}_{n-1}'(s), s ) +
\mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s + \delta) \right]  -
\right. \nonumber \\ && \hspace{.75in} \left.
\left[ \mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) - \delta \, \mathbf{u}_{n-1}'(s), s ) +
\mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s - \delta) \right]
\right\}
\nonumber
\end{eqnarray}
Combining  \EQ{sens5} and  \EQ{sens6} gives the pseudo problem
\begin{eqnarray}
{\mathbf J} \,  {\mathbf u}_{n}' &\! = \! &
\frac{1}{2 \, \delta} \left\{
\left[ \mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) + \delta \, \mathbf{u}_{n-1}'(s), s ) +
\mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s + \delta) \right]  -
\right. \nonumber \\ && \hspace{.15in} \left.
\left[ \mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) - \delta \, \mathbf{u}_{n-1}'(s), s ) +
\mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s - \delta) \right]
\right\}
\LEQ{sens7}
\end{eqnarray}
%which resembles, but is not to be confused with,  the matrix-free GMRES operation of \EQ{mfree}.
%Indeed, the GMRES algorithm is  used to resolve \EQ{sens7} while its supporting routines
%are used to evaluate the pseudo load.

Several variables must be carefully selected to accurately and efficiently compute the sensitivities.
As previously mentioned, improper choices of $\delta$ can lead to round-off or truncation
errors so  the
  \texttt{Sens\_Variable-- Sens\_Variable\_Pert}
%\tvars{Sens\_Variable}{Sens\_Variable\_Pert}
variable must be carefully prescribed.  To reduce  computation for the pseudo load evaluation
a forward difference approximation can be used in place of the default  central difference
approximation via the \\ \texttt{NUMERICS--Energy\_Sensitivity\_Pseudo\_Load} variable.  
Naturally the forward difference approximation will also result in  decreased accuracy. 
Because the sensitivity analysis assumes that $\mathbf{F}(\mathbf{u}_n(s), \mathbf{u}_{n-1}(s) , s ) = \mathbf{0}$,
cf. \EQ{sens4}, the convergence criterion for the nonlinear solver, i.e. the
  \texttt{NONLINEAR\_SOLVER-- Convergence\_Criterion} (of the \texttt{NUMERICS-- Energy\_Nonlinear\_Solution}
namelist),
%\tvars{Sens\_Variable}{Sens\_Variable\_Pert}
variable, must also be carefully prescribed 
(of course, this is good practice to  ensure accurate computation of the state $\mathbf{u}$).  
Finally, the ability to
accurately and efficiently solve the linear pseudo problem  of \EQ{sens7} requires
careful consideration of the
\texttt{LINEAR\_SOLVER-- Convergence\_Criterion, 
Maximum\_Iterations, Preconditioning\_Method, 
Preconditioning\_Preconditioner, 
\\
Preconditioning\_Scope, Preconditioning\_Steps, 
\\
Relaxation\_Parameter,  and Stopping\_Criterion}\\ (of the \texttt{NUMERICS-- Energy\_Sensitivity\_Solution}
namelist) variables.
%The aforementioned linear solver parameters are given default values which may be overridden by 
%defining a separate \texttt{LINEAR\_SOLVER} via the \textxx{NUMERICS--Energy\_Sensitivity\_Solution}
%variable.

%The sensitivity analysis is easily extended to coupled problems.  If only weak coupling exists between the systems,
%then the sensitivity analysis is similar to the transient sensitivity analysis described
%above.  I.e., for each time step, the primal and then pseudo analyses for the first system are sequentially completed followed
%by the primal and  pseudo analyses for the second system which use as
% inputs the states and state sensitivities from the first system and so on.
