\chapter{Parallelism}
\LCHAP{parallel}

This chapter presents the the parallel programming model in \truchas.  

\section{Background on Parallel Programming}

One goal of the \truchas code is to run simulations with large data
sets and complicated, coupled, physical processes.  To make that
possible, we developed the code so that it can use multiple computer
processors simultaneously.  Because multiple CPUs are working
together, at the same time, on the same simulation, this is called
``parallel programming''.  An introduction to many of the topics
discussed in this section can be found in~\cite{hayes88}.

\subsection{Parallel Computer}
A computer which supports paralell programming is often called a
parallel computer.  Many different types of parallel computers are in
use today.  While all parallel computers have multiple CPUs, one major
distinction between them is whether all the CPUs have direct access to
a common memory system or not.  CPUs that share memory are ``Shared
Memory'' parallel computers.  Parallel computers where each CPU has
direct access to only its own, local, memory system are ``Distributed
Memory'' computers.  That is, the memory is distributed among the
CPUs.  

\subsection{Shades of Grey}

Shared memory and distributed memory parallel computers represent two
ends of a spectrum.  Most modern parallel computers fall somewhere in
between.  Large Silicon Graphics, Inc.  (SGI) systems are
``distributed, shared memory''.  On those systems, CPUs have their own
memory system, so memory is distributed among the processors.
However, a combination of hardware and software allows any processor
to directly read or write the memory of any other processor.  So the
memory is both distributed and shared.

Many of the parallel computers are built as a cluster of processing
nodes.  The processing nodes may themselves be shared memory parallel
computers.  For instance, we sometimes run on a cluster of
dual-processor PCs.  The Compaq computer system, the latest large
scale supercomputer at Los Alamos, is a cluster of shared-memory
nodes. 

\subsection{Programming for Distributed Memory Parallel Computers}

\truchas is developed to run on distributed memory parallel
computers.  Every interesting, modern parallel computer can be
efficiently used with that model.  All non-local reading and writing
of data is done through explicit library calls.  (The library may make
use of the fact that high-speed, shared memory hardware is available
and deliver high performance, but \truchas does not assume that
exists.)

There are some disadvantages to limiting ourselves to a distributed
memory model.  One significant disadvantage is that it is often easier
to program for shared memory systems than for distributed memory
systems.  Another disadvantage is that shared memory systems admit
higher performance algorithms than distributed memory systems.  So, by
rejecting a shared memory programming model we are making our
development task harder, and potentially decreasing our performance.

We decided that these disadvantages were outweighed by the portability
we get by assuming only distributed memory.  We can run on cheaper
systems, since shared memory hardware is expensive.  So \truchas can
run on inexpensive clusters of PCs.  Also, there has not yet emerged
an ubiquitous shared memory programming model that delivers high
performance on large systems across vendors.  Our portability would be
limited if we assumed one vendor's particular shared memory paradigm.


\section{SPMD Programming Model}

\truchas uses the SPMD (Single Program, Multiple Data) programming
paradigm.  We write our code using common, single processor
programming languages (mostly FORTRAN95), and make calls to a library
to move data between processors.  The same executable runs on each
processor (that's the ``Single Program'' part of SPDM).  Each
processor has different data (different regions of the mesh, for
example), and hence computes different results (that's the ``Multiple
Data'' part of SPMD).  Many of the algorithms for computing across the
processors are ``data parallel algorithms''~\cite{SteeleHillis86}.


\subsection{MPI}

Of course, the different processors have to exchange data in order to
execute the desired algorithms.  For instance, computing the gradient
of a field for a cell requires field data from surrounding cells.  The
data for those surrounding cells may be on different processors.
Therefore, we need some way to transfer data between processors.  By
choice we have limited ourselves to programming languages that do not
know anything about moving data between processors.  Therefore, we
need to use a special-purpose library.  The library we chose for
Truchas is the Message Passing Interface (MPI)~\cite{MPIhome, MPIref,
useMPI}.  PVM is an alternative communication library.

\subsection{Communication Library: PGSLib}

MPI provides the fundamental capability to move data from one process
to another.  For instance, the routine \verb+ MPI_Send+ sends a buffer
of data from one processor to another.  However, the MPI interface is
at a very low level - it refers to real or integer buffers, word
sizes, and processor numbers.  \truchas uses a communication
library, \pgslib~\cite{PGSLibhome}, which provides the abstractions
appropriate for unstructed mesh algorithms.  For instance, \pgslib
provides routines to gather data from all surrounding cells.  A
developer using this routine does not have to concern themselves with
the detail of where the surrounding cell data is stored - local on the
same processor, or on other processors.

\pgslib provides abstractions for all the functionality that might
require interprocessor communication.  MPI routines are never called
directly in \truchas, only indirectly through \pgslib~.  One nice
benefit of that is that \pgslib provides a ``serial emulator''.  That
is, if somebody is running on a serial computer (that is, they are
using only a single processor), by linking with the serial version of
\pgslib then they can avoid using MPI altogether.  That simplifies
debugging, and also simplifies installation, since if you do not plan
to run on multiple processors there is no need to install MPI.

\section{Developing Code In Truchas}

In this section we review the fundamentals for developing code in
Truchas, with exclusive emphasis on assuring that the code will run
properly in parallel.

There are two concepts, which, if kept clearly in mind, can assure
that correct code is generated, in most cases.

\begin{itemize}
\item[] All code executes locally, on a single processor, except
explicit calls into the \pgslib library.
\item[] All processors must execute the same calls, in the same order,
into \pgslib.
\end{itemize}

Code which involves all the processors (calls into \pgslib) is
called \textbf{global}.  Code which involves only a single processor is
called \textbf{local}.  Hence, our parallel programming paradigm is often
called \textbf{global--local programming}.  The general flow of control
involves mostly local code, interspersed with global calls into
\pgslib.

\subsection{What Is Local Data, and What Is Global Data?}

Since we are assuming distributed memory, all data is ``owned'' by one
and only one processor.  However, sometimes we want to perform global
operations on what seems to be a single, global, data set.

Local data is data which is operated on only by a single processor.  A
temporary variable inside a loop is local.  Global data is a union of
local data sets, together with a (possibly conceptual) description of
how the local data sets are ordered and joined.

For instance, scattering a scalar field, {\em e.g.} temperature, from
cell centers to cell vertices is a global operation on global data.
The cell centers and the cell vertices are global data sets because we
want to consider the scatter operation on the whole mesh, regardless
of how it may be distributed to different processors.  We consider
that each processor contains the data for a portion of the mesh.
Clearly, the scatter operation cannot complete without all processors
participating.  The code for this example might be:

\begin{verbatim}
   real, dimension(ncells):: Temperature
   real, dimension(nnodes):: Node_Sum_Temperature

   call SUM_SCATTER(Node_Avg_Temperature, Temperature)
\end{verbatim}

\verb+SUM_SCATTER+ is a global subroutine, so this fragment of code is
global.


The distinction between local data and global data is frequently one
of context.  For instance, if we want to scale cell centered
temperature data by some constant, we might write:

\begin{verbatim}
   real, dimension(ncells) :: Temperature
   
   Temperature = scale_factor * Temperature
\end{verbatim}

In this case Temperature is local data, since we are operating on each
processor's portion of the mesh independently.  

\subsection{Compute Locally}

Once again, because our machine model assumes distributed memory, and
because we do not assume any native machine capability to access
memory of other processors, all computation is local.  That is, every
processor can only compute on data which it owns, {\em i.e.} local
data.  This means that most of the Truchas code looks like single
processor, serial code.  The main distinction is that any operation
which involves the whole mesh must use special routines.  Most of the
physics algorithms are local, however.

\subsection{Communication is Global}

Any non-local algorithm requires communication between processors, and
hence must use one the \pgslib routines.  Common global operations
include gathering data from vertices to cell centers, scattering (with
a combiner such as addition) data from cell centers to vertices and
computing the dot product of two vectors.  Input and output is also
global.

\subsection{Partitioning The Data}

So far we have mentioned that operations on the whole mesh, such as
computing the derivative of a field, are global operations.
(Derivatives require neighbor cell information, and hence are
non-local.)  However, users supply Truchas with a single mesh, and so
the question is how to transition from a single mesh to a distributed,
partitioned, mesh.

We divide the mesh among processors using a mesh partitioner.  In the
general case we use a program called Chaco, from Sandia National
Laboratory.  That program looks at the mesh connectivity and returns a
permutation array which tells us how to reorder the mesh so that we
can get the number of partitions we want with as little communication
between partitions as possible.  It also tries to make the partitions
as evenly sized as possible so that all processors will have the same
amount of work to do.  (Obviously, returning one partition containing
the whole mesh and all other partitions containing no data at all
would minimize communication between partitions.  That would not help
us distribute the computation between processors, though.)

In same cases, rather than using Chaco we use a simple algorithm which
divides the mesh into blocks.  This works well if the mesh is a
rectilinear mesh that has been generated by Truchas' internal mesh
generator.  It does not work well for any other kind of mesh.

\subsection{Common Pitfalls}

The most common mistake that inhibits parallel operation is to treat a
global operation as if it were local.  For instance:

\begin{verbatim}
   real, dimension(ncells):: Temperature
   real, dimension(nnodes):: Node_Sum_Temperature

   if (MAXVAL(Temperature) <= Temp_Min) then
      call SUM_SCATTER(Node_Sum_Temperature, Temperature)
   end if
\end{verbatim}

The error here is that each processor will have a different result for
\verb+MAXVAL(Temperature)+, so some processors may enter the if clause
and others will not.  Since \verb+SUM_SCATTER+ is a global operation,
our symantics require that all processors must execute it.  If some do
and some do not, then the operation will not complete properly, and
program will crash or hang, in the best case, or return incorrect
results in the worst case.

One thing that makes this such a nefarious bug is that it shows up
only when some processors enter the if clause and some do not.  It is
likely that for many data sets no processors or all processors will
enter the if clause, and hence the code will seem to be correct.  In
addition, for a given data set, the bug may show up on some number of
processors and not on other numbers of processors.  (The bug will
never show up on one processor.)

One way to find this sort of bug is with a debugger such as
Totalview.  If the program is crashing or hanging, you can examine it
with Totalview and notice that some processors are executing one piece
of code and other subroutines are hung inside the if clause.

One correct way to code this example is:

\begin{verbatim}
   real, dimension(ncells):: Temperature
   real, dimension(nnodes):: Node_Avg_Temperature

   if (PGSLib_Global_MAXVAL(Temperature) <= Temp_Min) then
      call SUM_SCATTER(Node_Avg_Temperature, Temperature)
   end if
\end{verbatim}

By making the \verb+MAXVAL+ global the same value is returned to all
processors.  Assuming that \verb+Temp_Min+ is the same on all
processors, then either all processors or no processor will enter the
if clause.  
