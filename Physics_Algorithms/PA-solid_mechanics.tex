
\chapter{Solid Mechanics}

The solid mechanics capability in \truchas\ is described in this
chapter.  The current release can calculate displacements, elastic
stresses and both elastic and plastic strains for an isotropic
material, including stresses and deformations caused by temperature
changes and gradients.  The volume changes associated with solid state
phase changes can also be included in the solution.  A variety of
traction and displacement boundary conditions can be specified, and
this release includes sliding interfaces and contact, restricted to
``small'' displacements.  The model for plastic flow uses a flow
stress that depends on strain rate and temperature with no work
hardening.  Other material behavior such as porosity formation may be
added in future versions of the software.

\section{Notation}
\begin{tabbing}
VariableXXXX \= DescriptionXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \=   \kill
$A$ \> Control volume face area \\
$\mathbf{A}$ \> linear elastic operator \\
$a_i$, $b_i$, $c_i$, $d_i$ \> coefficients for tetrahedral linear interpolation \\
$\mathbf{b}$, $b_i$ \> body force components \\
$d$ \> displacement boundary condition value (scalar) \\
$E$ \> Young's modulus\\
$e^{el}$ \> elastic strain tensor \\
$e^{pc}$ \> phase change strain tensor \\
$\bar e^{pl}$ \> effective plastic strain (scalar) \\
$e^{pl}$ \> plastic strain tensor \\
$e^{th}$ \> thermal strain tensor \\
$e^{tot}$ \> total strain tensor \\
$G$ \> second Lame$^\prime$ constant \\
$\mathbf{J}$ \> Jacobian matrix\\ 
$N_i$ \> Shape functions for a cell or element\\
$\hat{n}$, $\hat{n}_i$ \> unit normal vector and components \\
$\mathbf{P}$ \> preconditioning matrix \\
$r$, $s$, $t$ \> logical coordinates for element shape functions\\
$\mathbf{r}$,$r_i$ \> right hand side vector for the linear elastic system \\
$T$ \> temperature \\
$T_{ref}$ \> stress reference temperature \\
$t$ \> time \\
$\mathbf{u}$, $u_i$, $[u,v,w]$ \> displacement vector or components \\
$x_i$ or $[x,y,z]$ \> global coordinates \\
$\alpha$ \> linear coefficient of thermal expansion\\
$\delta_{ij}$ \> Kronecker delta \\
$\lambda$ \> first Lame$^\prime$ constant \\
$\nu$ \> Poisson's ratio \\
$\phi$ \> a displacement component \\
$\rho$ \> current density of the material (not accounting for thermal expansion) \\
$\rho_0$ \> initial density of the material \\
$\sigma$ \> Cauchy stress tensor\\
$\bar \sigma$ \> Effective stress (second invariant of $\sigma$,
scalar) \\
$\sigma^\prime$ \> deviatoric stress tensor \\
$\sigma^{th}$ \> thermal stress tensor \\
$\tau_i$ \> traction components \\
$\tau_n$ \> traction component normal to the surface \\
$\theta$ \> temperature difference relative to a stress-free
temperature \\
\end{tabbing}

More notation specific to the mechanical threshold stress (MTS) model
and the contact algorithm are listed in \SEC{material-properties} and
\SEC{boundary_and_initial_cond}.  Unless otherwise indicated, repeated
indices denote summation, i.e. $e_{kk} \equiv e_{11} +e_{22} +e_{33}$.

\section{Physics}

\subsection{Assumptions}

The current solid mechanics implementation is an application of linear
thermoelastic continuum mechanics with small strain viscoplastic flow.
It is assumed that the solid material is a continuum, and the
discretized strain field satisfies compatibility.  The current
formulation uses infinitesimal strains, and is therefore accurate only
for small strains and rotations.  The material behavior is assumed to
be linear elastic with isotropic $J_2$ plasticity.  The deformation is
assumed to be quasi-static.  Body forces and isotropic dilatation due
to phase change can be optionally included in the current
implementation.

\subsection{Equations}

The basic conservation law to be satisfied is that of equilibrium
($\sigma_{ij}$ are stress components, $x_j$ are coordinates, $b_i$ are
body force components): 
\begin{equation}
\frac{\partial{\sigma_{ij}}}{\partial{x_j}} + b_i = 0
\LEQ{equilibrium}
\end{equation}
For isotropic elasticity:
\begin{equation}
\sigma_{ij} = \lambda e^{el}_{kk} \delta_{ij} + 2 G e^{el}_{ij} 
\LEQ{elastic_stress}
\end{equation}
where $\lambda$ and $G$ are the first and second Lam\'{e} constants,
respectively.

The strain tensor $e^{el}_{ij}$ is the elastic strain defined by
decomposing the total strain into elastic, thermal, plastic and phase change
portions.
\begin{equation}
e^{tot}_{ij} = e^{el}_{ij} + e^{th}_{ij} + e^{pl}_{ij} + e^{pc}_{ij}
\LEQ{total_strain}
\end{equation}
The thermal strain is defined as
\begin{equation}
e^{th}_{ij} = \alpha \delta_{ij} \theta
\LEQ{thermal_strain}
\end{equation}
where $\alpha$ is the coefficient of thermal expansion, and
$\theta$ is the temperature difference relative to a stress-free
reference temperature.  For the case of a temperature dependent 
coefficient of thermal expansion, \EQ{thermal_strain} becomes
\begin{equation}
e^{th}_{ij} = \int^T_{T_{ref}} \alpha(T) \delta_{ij}\; \mathrm{d} T
\LEQ{thermal_strain_2}
\end{equation}

The phase change strain is also assumed to be an isotropic expansion
or contraction calculated from the density change associated with the
phase change.  Assuming small strains, we use:
\begin{equation}
e^{pc}_{ij} = \left [ \left (\frac{\rho_0}{\rho} \right )^{\frac{1}{3}} - 1\right ] \delta_{ij}
\LEQ{pc_strain}
\end{equation}


The total strain is the infinitesimal strain tensor defined in terms
of the displacement gradient: 
\begin{equation}
e^{tot}_{ij} = \frac{1}{2} \left( \frac{\partial{u_i}}{\partial{x_j}} +
\frac{\partial{u_j}}{\partial{x_i}} \right)
\LEQ{small_strains}
\end{equation}
where $u_i$ are material displacement components.  The displacement
field is approximated by a finite volume discretization, which is
described in \SEC{sm:algorithms}.  The displacements at cell
vertices are the primary solution variables.

For isotropic thermo-elasticity with phase change, \EQ{elastic_stress},
\EQ{total_strain}, \EQ{thermal_strain_2} and \EQ{pc_strain} give
\begin{equation}
\sigma_{ij} = \lambda e^{tot}_{kk} \delta_{ij} + 2 G e^{tot}_{ij} -
(3\lambda + 2G) \left ( \int^T_{T_{ref}} \alpha(T) \; \mathrm{d} T + 
\left [ \left (\frac{\rho_0}{\rho} \right )^{\frac{1}{3}} - 1\right ] \right ) \delta_{ij}
\LEQ{thermo-elas}
\end{equation}

The plastic strain is calculated from the users choice of viscoplastic
model of the form 
\begin{equation}
\frac{\mathrm{d} {\bar{e}}^{pl}}{\mathrm{d} t} = \mbox{f}(\bar{\sigma}, T)
\LEQ{vp-generic}
\end{equation}
where $\bar{e}^{pl}$ is the effective plastic strain and
$\bar{\sigma}$ is the second invariant of the stress tensor ($J_2$),
or effective stress.  The specific viscoplastic models available are
described in \SEC{material-properties}.  The effective plastic
strain $\bar{e}^{pl}$ and effective stress $\bar{\sigma}$ are scalars,
and the components of the plastic strain increment $\Delta
e^{pl}_{ij}$ are calculated from the Prandtl-Reuss equations
\begin{equation}
\Delta e^{pl}_{ij} = \frac{3}{2} \frac{\Delta \bar{e}^{pl}}{\bar{\sigma}} 
\sigma^\prime_{ij}
\LEQ{Prandtl-Reuss}
\end{equation}
where $\sigma^\prime_{ij}$ is the deviatoric stress and $\Delta
\bar{e}^{pl}$ is the increment in effective plastic strain obtained by
integrating \EQ{vp-generic} over a time step.

The deviatoric stress is the elastic stress tensor minus the
hydrostatic component.
\begin{equation}
\sigma^\prime_{ij} = \sigma_{ij} - \delta_{ij} (\sigma_{kk}/3)
\LEQ{dev-stress}
\end{equation}
The plastic strain components are proportional to the deviatoric
stress components, so the volumetric plastic strain ($e^{pl}_{kk}$) is
zero. Including plastic strains in the stress calculation gives
\begin{equation}
\sigma_{ij} = \lambda e^{tot}_{kk} \delta_{ij} + 2 G e^{tot}_{ij} -
(3\lambda + 2G)\left ( \int^T_{T_{ref}} \alpha(T) dT + 
\left [ \left (\frac{\rho_0}{\rho} \right )^{\frac{1}{3}} - 1\right ] \right ) \delta_{ij}  - 2 G e^{pl}_{ij}
\LEQ{thermo-elas-vp}
\end{equation}


\subsection{Boundary and Initial Conditions}
\LSEC{boundary_and_initial_cond}

\subsubsection{Notation}

\begin{tabular}{l c  p{6in}}
$\mathbf{u}$ & - & the displacement vector for the entire domain \\
$\hat{n}$ & - & a unit vector normal to and pointing away from the surface \\
$\vec{u}_j$ & - & the displacement vector (in ndim dimensions) for node $j$\\
$\vec{f}_j$ & - & the force vector at node $j$ that is a function of the 
                  displacement vector $\mathbf{u}$ and source terms\\
$\Lambda$ & - & a contact function for a gap interface that is equal
                to 1 if the surfaces are in contact or penetrated and
                equal to 0 if the surfaces are separated.
\end{tabular}

\subsubsection{Boundary Conditions}
\LSEC{boundary-conditions}

Boundary conditions may be specified in terms of displacements $u_i$
or surface tractions.  Since the primary solution variables are
displacements, specifying displacements as Dirichlet conditions is
straight forward.  Tractions $\tau_j$ are defined as a force per unit
area on a surface or interface, and are related to the bulk stress:
\begin{equation}
\tau_j = \sigma_{ij} \cdot \hat{n}_i
\LEQ{tractions}
\end{equation}
where $\tau_j$ are the traction components and $\hat{n}_i$ are the surface
normal components.  The algorithm used in \truchas\ allows surface
tractions to be specified directly on control volume faces.
Displacements and tractions can be specified in either global
Cartesian coordinates or in the direction normal to the surface or
interface.

If a displacement normal to the surface is specified, the constraint
equation is
\begin{equation}
\vec{u} \cdot \hat{n} = d
\LEQ{normal_disp}
\end{equation}
where $\mathbf{u}$ is the displacement vector, $\mathbf{n}$ is the
unit normal vector at the node where the constraint is applied, and
$d$ is the scalar displacement value with a positive value in the
direction pointing outward from the body.

Since the equilibrium equations for a node are for a force vector in
three dimensions, the general case requires that the three equations
for a node be decomposed into components normal and tangential to the surface.  The
equilibrium equations can be expressed as
\begin{equation}
 \vec{f}_j = 0
\LEQ{node_equilibrium}
\end{equation}
The displacement constraint can be applied by taking the component of the vector
orthogonal to the specified displacement and adding the portion
satisfying \EQ{normal_disp}:
\begin{equation}
([I] - [\hat{n}\hat{n}^T]) \vec{f}_j - c[\hat{n}\hat{n}^T](\vec{u}_j - d \hat{n}) = 0
\LEQ{norm_decomp_disp}
\end{equation}
where $[\hat{n}\hat{n}^T]$ is the $3 \times 3$ orthogonal projection matrix onto 
the normal direction, and $[I] - [\hat{n}\hat{n}^T]$ the orthogonal projection 
onto the plane normal to $ \hat{n}$.

If a traction boundary condition normal to the surface is specified,
the traction components in Cartesian coordinates are given by
\begin{equation}
\tau_i = \tau_{n} \hat{n}_i
\LEQ{normal_trac}
\end{equation}
where $\tau_i$ is the traction component in Cartesian coordinates, $\tau_{n}$
is the specified normal traction and $\hat{n}_i$ is the the $i$ component of
the unit normal vector.  If $\tau_{n}$ is positive, the resulting force is
in the direction pointing outward from the body.

\subsubsection{Sliding Interfaces and Contact}
\LSEC{interfaces}

Small displacement sliding interfaces can be specified, with or
without a contact algorithm.  The interface is defined with gap
elements, that are currently constructed by duplicating mesh nodes and
element faces on a surface and constructing elements of zero thickness
by connecting the coincident faces and nodes in the mesh definition.
In the future, elements of finite thickness may be designated as gap
elements.  The gap elements are currently only used to provide
connectivity information to the sliding and contact algorithms,
facilitating the parallel implementation.

Sliding interfaces specified without contact (designated ``normal
constraint'' interfaces) allow coincident nodes across an interface to
move relative to each other tangential to the surface but not normal
to the surface.  This is implemented by treating the nodes on the
interface as if they are on a free surface and adding constraints that
are dependent on the displacement vector of the coincident node on the
other side of the interface.  The constraints must satisfy two
conditions: (1) the relative normal displacement is zero. For coincident
nodes $j$ and $k$ on either side of the interface, and unit normal
vector $\hat{n}$:
\begin{equation}
(\vec{u}_k - \vec{u}_j) \cdot \hat{n} = 0
\LEQ{n_disp_scalar}
\end{equation}
or in vector form
\begin{equation}
[\hat{n}\hat{n}^T](\vec{u}_k - \vec{u}_j) = 0
\LEQ{n_disp_vector}
\end{equation}
and (2) the sum of the forces normal to the interface on the two nodes must be zero:
\begin{equation}
[\hat{n}\hat{n}^T](\vec{f}_j + \vec{f}_k) = 0
\LEQ{force_balance}
\end{equation}

To implement the sliding constraint, the equations for the force
vectors acting on nodes $j$ and $k$ can be modified to be (for
$\hat{n}$ pointing away from node $j$):
\begin{eqnarray}
([I] - [\hat{n}\hat{n}^T]) \vec{f}_j + [\hat{n}\hat{n}^T](\vec{f}_j + 
\vec{f}_k) + c [\hat{n}\hat{n}^T](\vec{u}_k - \vec{u}_j) = 0 \\
([I] - [\hat{n}\hat{n}^T]) \vec{f}_k + [\hat{n}\hat{n}^T](\vec{f}_j + 
\vec{f}_k) + c [\hat{n}\hat{n}^T](\vec{u}_j - \vec{u}_k) = 0
\LEQ{norm_constraint_1}
\end{eqnarray}
or
\begin{eqnarray}
\vec{f}_j + [\hat{n}\hat{n}^T](\vec{f}_k + c(\vec{u}_k - \vec{u}_j)) = 0 \\
\vec{f}_k + [\hat{n}\hat{n}^T](\vec{f}_j + c(\vec{u}_j - \vec{u}_k)) = 0
\LEQ{norm_constraint_2}
\end{eqnarray}
The nodal equations and constraints are in the form of the equation for
a node on a free surface plus the force applied by the node across the interface
plus a term that penalizes penetration or separation of the two nodes
normal to the interface.  The constant $c$ is chosen to scale the
penalty function appropriately.

\EQ{norm_constraint_2} can be modified with a contact function
$\Lambda$ to make the normal constraint non-symmetric, penalizing
penetration but allowing the nodes to separate.  $\Lambda$ is equal to
1 if the surfaces are in contact or penetrated and equal to 0 if the
surfaces are separated.  Evaluating $\Lambda$ at a node is an
important implementation issue.
\begin{eqnarray}
\vec{f}_j + \Lambda [\hat{n}\hat{n}^T](\vec{f}_k + c(\vec{u}_k - \vec{u}_j)) = 0 \\
\vec{f}_k + \Lambda[\hat{n}\hat{n}^T](\vec{f}_j + c(\vec{u}_j - \vec{u}_k)) = 0
\LEQ{contact_1}
\end{eqnarray}

\subsubsection{Initial Conditions}

The temperature field is required for the thermoelastic solution,
and initial temperatures that are specified in the input file or
overwrite routine are used as initial conditions.  A stress-free
reference temperature must be specified for each material in the
problem. 

\subsection{Interaction with Other Physics}

The solid mechanics solution depends on the temperature field and
volume fractions of the various solid phases.  The volume fractions
and temperature field at the beginning and end of the time step are
used to calculate the thermal strain increments and plastic strain
increments.  Calculation of the phase change strain requires the
change in volume from the enthalpy calculation.  None of the other
physics capabilities are dependent on the stresses, strains and
displacements in this release.

\subsection{Material Properties}
\LSEC{material-properties}

\subsubsection{Linear Elasticity}

The isotropic thermoelastic model requires two elastic constants and a
linear coefficient of thermal expansion.  These properties are
generally temperature dependent, and can be specified as functions of
temperature in the input.  Currently the elastic constants are
specified as the Lam\'{e} constants $\lambda$ and $G$, as defined in
\EQ{thermo-elas}.  The relationship between Lam\'{e} constants and
Young's modulus $E$ and Poisson's ratio $\nu$ are given here:
\begin{equation}
\lambda = \frac{E\nu}{(1+\nu)(1-2\nu)}
\LEQ{Lame1}
\end{equation}
\begin{equation}
G = \frac{E}{2(1+\nu)}
\LEQ{Lame2}
\end{equation}

\subsubsection{MTS Viscoplastic Model}

The mechanical threshold stress (MTS) model \cite{FollansbeeKocks88}
was developed to model plastic deformation of metals based on
thermally activated deformation mechanisms.  Although this model may
not be the most appropriate for small strains and high temperatures,
data for a number of materials of interest to the \telluride project
have been fitted to this model.

If we ignore any terms related to work hardening, the equations for
the MTS model with a strain rate and temperature dependent yield
strength are as follows:
\begin{equation}
\frac{\sigma}{\mu} = \frac{\sigma_a}{\mu} +
S_i(\dot{\epsilon},T)\frac{\hat{\sigma_i}}{\mu_0}
\LEQ{MTS1}
\end{equation}
\begin{equation}
\mu = \mu_0 - \frac{D}{\exp{\left(\frac{T_0}{T}\right)} - 1}
\LEQ{mu-temp}
\end{equation}
\begin{equation}
S_i(\dot{\epsilon},T) = \left\{ 1 - \left[ \frac{kT}{\mu b^3 g_{0i}}
\ln{\left(\frac{\dot{\epsilon}_{0i}}{\dot{\epsilon}}\right)}
\right]^{\frac{1}{q_i}} \right\}^{\frac{1}{p_i}} 
\LEQ{MTS2}
\end{equation}

The algorithm in \truchas\ requires $\dot{\epsilon}$ as a function of
$T$ and $\sigma$. \EQ{MTS1} and \EQ{MTS2} can be solved for
$\dot{\epsilon}$ to give
\begin{equation}
\dot{\epsilon} = \frac{\dot{\epsilon}_{0i}}{\exp{\left\{ \left[ 1 - 
\left(\frac{\mu_0}{\mu \hat{\sigma}_i} (\sigma - \sigma_a)\right)^{p_i}
\right]^{q_i} \frac{\mu b^3 g_{0i}}{kT} \right\}}}
\LEQ{MTS3}
\end{equation}


The terms in the equations and corresponding \truchas\ input variables
are as follows:
\begin{tabbing}
VariableXXX \= DescriptionXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \=   \kill
Variable  \>   Description  \> Input Variable \\ 
$\sigma$   \> Effective flow stress used in \EQ{vp-generic}  \> not an
input variable \\
$\dot{\epsilon}$ \> Effective plastic strain rate used in \EQ{vp-generic}  \> not an
input variable \\
$\mu$ \> Temperature dependent shear modulus \> not an input variable \\
$T$ \> Temperature  \> not an input variable \\
$\mu_0$ \> Reference shear modulus \> MTS\_mu\_0 \\
$\sigma_a$ \> Athermal stress term \> MTS\_sig\_a \\
$\hat{\sigma_a}$ \> Stress term for thermally activated yield stress \> MTS\_sig\_i \\
$D$ \> Constant for temperature dependent shear modulus \> MTS\_d \\
$T_0$ \> Reference temperature for shear modulus \> MTS\_temp\_0 \\
$k$ \> Boltzmann's constant in appropriate units \> MTS\_k \\
$b$ \> Burger's vector magnitude \> MTS\_b \\
$g_{0i}$ \> Dimensionless constant \> MTS\_g\_0i \\
$\dot{\epsilon}_{0i}$ \> Reference strain rate \> MTS\_edot\_0i \\
$p_i$ \> Dimensionless constant \> MTS\_p\_i \\
$q_i$ \> Dimensionless constant \> MTS\_q\_i \\
\end{tabbing}

\subsubsection{Power Law Viscoplastic Model}

The MTS model is not generally valid at high temperatures (relative to
the melting point) and low strain rates.  Accurate data for metals in
this regime are often not available.  A simple power law model is
available to fit or estimate the plastic behavior in this regime.  The
relation between strain rate, stress and temperature is of the form
\begin{equation}
\dot{\epsilon} = A \bar{\sigma}^n \exp{\frac{-Q}{RT}}
\LEQ{power_law}
\end{equation}

where $A$, $n$ and $Q$ are material parameters.

\section{Algorithms}
\LSEC{sm:algorithms}

\subsection{Discretization}
The discretization method used to solve \EQ{thermo-elas} or
\EQ{thermo-elas-vp} is based on a node-centered control volume
discretization~\cite{Fryer91,Bailey95}.  This algorithm was chosen
because it allows the efficient use of the existing mesh data
structures and parallel gather-scatter routines.  Control volumes are
constructed for each node or cell vertex using data from the mesh for
fluid flow and heat transfer.  Each cell is decomposed into
sub-volumes with faces defined by connecting cell centroids, face
centroids and edge midpoints, as shown in
\FIG{cv_faces}.

\begin{figure}[ht]
\centering
\includegraphics[scale=0.3]{figures/hex_cv_face_2a.pdf}
\caption[Control sub-volumes and faces in a single mesh cell] {Faces
 defining control sub-volumes for a hex cell and a degenerate prism cell.}
\LFIG{cv_faces}
\end{figure}

\begin{figure}[h]
\includegraphics[scale=0.20]{figures/cv_face_int_2.pdf}
\hspace{0.5in}
\includegraphics[scale=0.20]{figures/cv_face_srf.pdf}
\caption[Control volume] {Faces defining control
volumes for a node surrounded by hex cells and a node on the
surface of a regular hex mesh.}
\LFIG{control_volumes}
\end{figure}

Combining the faces of the sub-volumes from all cells that share a
given node gives the surface of a control volume for that node.
Control volumes for nodes that are on the surface of the mesh are
enclosed by dividing the external cell face into smaller faces bounded
by the internal control volume faces and cell edges.  Examples for a
structured hex mesh are shown in \FIG{control_volumes}.

The simplest derivation for the method used in \truchas\ is to start
with the equilibrium equation, \EQ{equilibrium}, and integrate over
the control volume:
\begin{equation}
\int_V \frac{\partial{\sigma_{ij}}}{\partial{x_j}}\; \mathrm{d} v = - \int_V
b_i\; \mathrm{d} v
\LEQ{equilib_int}
\end{equation}

The integral on the left hand side can be converted to a surface
integral using the divergence theorem,
\begin{equation}
\int_V \frac{\partial{\sigma_{ij}}}{\partial{x_j}}\; \mathrm{d} v = 
\int_S \sigma_{ij} \cdot \hat{n}_j \; \mathrm{d} s
\LEQ{surface_int}
\end{equation}
where the integral is now defined over the surface of the control
volume.

Combining with \EQ{thermo-elas-vp}, moving the thermal stresses and
phase change stresses to the right hand side gives 
\begin{multline}
\int_S (\lambda e^{tot}_{kk} \delta_{ij} + 2 G (e^{tot}_{ij} -
e^{pl}_{ij})) 
\cdot \hat{n}_j \; \mathrm{d} s = \\
\int_S \left [ \left ( \int^T_{T_{ref}} \alpha(T) \; \mathrm{d} T + 
\left [ \left (\frac{\rho_0}{\rho} \right )^{\frac{1}{3}} - 1\right ]
\right ) \delta_{ij} \right ] \cdot \hat{n}_j \; \mathrm{d} s - \int_V
b_i\; \mathrm{d} v
\LEQ{basic-eq-1}
\end{multline}
for each control volume.  Bailey and Cross~\cite{Bailey95} claim that
\EQ{basic-eq-1} can also be derived from a weighted residual method
where the weight function is unity within the control volume and zero
elsewhere.

\subsection{Displacement Gradients}

The total strain $e^{tot}_{ij}$ is evaluated at the face centroids by
standard finite element techniques for tri-linear hexahedral elements.
The displacement field within an mesh cell is related to nodal
displacements by
\begin{equation}
\phi(r,s,t) = \sum_{i=1}^{m} N_i(r,s,t) \phi_i
\LEQ{shape_func}
\end{equation}
where $\phi(r,s,t)$ is a displacement component at logical coordinates
$r,s,t$ within a reference element, $N_i$ are shape functions at those
same coordinates, and $\phi_i$ are the corresponding displacements at
the nodes.  The global coordinates $x,y,z$ are related to $r,s,t$
through the Jacobian matrix
\begin{equation}
\mathbf{J} = 
\left( \begin{array}{ccc}
 \frac{\partial x}{\partial r} &\frac{\partial y}{\partial r}
&\frac{\partial z}{\partial r} \\
\\
 \frac{\partial x}{\partial s} &\frac{\partial y}{\partial s}
&\frac{\partial z}{\partial s} \\
\\
 \frac{\partial x}{\partial t} &\frac{\partial y}{\partial t}
&\frac{\partial z}{\partial t} \\
\end{array} \right)
\LEQ{celljac}
\end{equation}
where
\begin{equation}
\frac{\partial x}{\partial r} = \sum_{i=1}^{m} \frac{\partial N_i}{
\partial r} x_i \quad \mathrm{etc.}
\LEQ{sf_deriv}
\end{equation}
After inverting $\mathbf{J}$, displacement gradient components in
global coordinates can be calculated:
\begin{equation}
\left[ \begin{array}{c}
\frac{\partial \phi}{\partial x} \\
\\
\frac{\partial \phi}{\partial y} \\
\\
\frac{\partial \phi}{\partial z} \\
\end{array}\right]
= \mathbf{J^{-1}}
\left[ \begin{array}{c}
\frac{\partial \phi}{\partial r} \\
\\
\frac{\partial \phi}{\partial s} \\
\\
\frac{\partial \phi}{\partial t} \\
\end{array}\right]
\LEQ{disp_deriv}
\end{equation}

\subsection{Solution Algorithm for Quasi-Static Stresses and Strains}

The left hand side of \EQ{basic-eq-1} is evaluated from the strains at
the centroid of each control volume face and the normal at the face
centroid using a single quadrature point.  A linear thermo-elastic
solution is computed on initialization if solid mechanics is active.
The initial solution uses either the Newton-Krylov or accelerated
inexact Newton nonlinear solver for the displacements.

The solution algorithm computes all stress and strain quantities at
all integration points for all cells.  These quantities are also
recalculated at cell centroids for output purposes only.

\subsubsection{Initialization}

Since the current implementation is a small strain Lagrangian
formulation, the Jacobians are only calculated once upon
initialization.  Initialization for the solid mechanics solution
includes the following:
\begin{itemize}
\item Calculate and store the area $A$, centroid coordinates and
normal vector $\hat{n}_i$ for each control volume face in each cell.
\item Calculate and store the inverse of the Jacobian matrix,
$\mathbf{J^{-1}}$ for each control volume face in each cell.
\item Identify external cell faces that have traction boundary conditions
specified, and calculate areas $A$ and normal vectors for
control volume faces on those cell faces.
\item Identify nodes for displacement boundary conditions.
\item If gap elements are present, identify pairs of nodes for
interface and contact constraints.  Also calculate and store the
various normal and tangential vectors for the contact algorithm.
\end{itemize}

\subsubsection{Initial Thermo-Elastic Solution}

The solvers used in \truchas\ do not require the formation of a
stiffness matrix, except for preconditioning, as described in
\SEC{TMPreconditioning}.  The solution method for the initial linear
thermo-elastic stresses, strains and displacements is as follows:

\begin{itemize}
\item Calculate the source terms that do not depend on the
displacement vector in \EQ{basic-eq-1}, denoted as $r$:
  \begin{itemize} 
  \item For each cell, calculate the thermal stress contribution from
  the cell temperature:
  \begin{equation}
  \sigma_{ij}^{th} \delta_{ij} = (3\lambda + 2G) \left ( \int^T_{T_{ref}} \alpha(T) \; \mathrm{d} T \right ) 
  \LEQ{thermal_stress}
  \end{equation} 
  \item For each cell, calculate the phase change stress contribution from
  the volume change calculated as part of the enthalpy solution:
  \begin{equation}
  \sigma_{ij}^{pc} \delta_{ij} = (3\lambda + 2G) \left[ \left
  (\frac{\rho_0}{\rho} \right )^{\frac{1}{3}} - 1\right ]
  \LEQ{phase_change_stress}
  \end{equation} 
  \item Accumulate the right hand side surface integrals for each
  displacement component $i$ for each node for each cell by summing
  over the number of control volume faces for the node (nfaces).
  Using values at the control volume face centroid (with no summation
  over index i):
  \begin{equation} 
  r_i = \sum_{m=1}^{\mathrm{nfaces}} (\sigma_{ii}^{th} +
  \sigma_{ii}^{pc})  \hat{n}_i^m  A^m 
  \LEQ{thermal_pc_rhs}
  \end{equation}
  \item Add contributions from traction boundary conditions $\tau_i$:
  \begin{equation} r_i = r_i +
  \sum_{m=1}^{\mathrm{nfaces}} \tau_i^m  A^m
  \LEQ{traction_rhs}
  \end{equation}
  \item Add gravitational body force terms if required
  \begin{equation}
  b_i = V_n  \rho_n  \vec{g}_i
  \LEQ{body-force}  
  \end{equation}
  where $V_n$ is the volume of the control volume for the node,
  $\rho_n$ is the average density of the control volume for the node
  and $\vec{g}$ is the gravitational acceleration vector
  \item Enforce displacement, sliding interface and contact boundary conditions 
        described in \SEC{boundary-conditions} and \SEC{interfaces}.  The detailed 
        equations for the supported combinations of displacement, sliding and contact 
        constraints are in \APP{contact}.
  \item Scatter the contributions to the right hand side for all cells
  to the nodes.
  \end{itemize}
\item Call the non-linear solver package to calculate displacements at the
  nodes.  The non-linear residual routine is called by the solver
  routines.  Although the initial solution is a linear elastic solution, if 
  contact boundary conditions are specified, the problem is non-linear. 
\item Calculate the total strain field at the cell centroid and at
  each integration point in each cell from the displacements.
  (\EQ{small_strains}).
\item Subtract the thermal and phase change strains to get the elastic strain:
  \begin{equation}
  e_{ij}^{el} = e_{ij}^{tot} - \alpha \theta \delta_{ij}
  \LEQ{elastic_strain}
  \end{equation}
\item Calculate the elastic stress at the cell centroid and at
  each integration point in each cell from the displacements using
\EQ{elastic_stress}.
\item Calculate the deviatoric stress using \EQ{dev-stress}.
\item Calculate the effective stress at the cell centroid and at
  each integration point in each cell using
  \begin{equation}
   \bar{\sigma} = \left[ \left( (\sigma_{11} - \sigma_{22})^2 + 
                                (\sigma_{22} - \sigma_{33})^2 +
                                (\sigma_{33} - \sigma_{11})^2 +
                            6 (\sigma_{12} ^2 + \sigma_{13} ^2 + 
                                 \sigma_{23} ^2)\right) / 2 \right]^{\frac{1}{2}}
   \LEQ{effective_stress}
  \end{equation}
\item Calculate and store the plastic strain rate at the cell centroid and at
  each integration point in each cell.
\end{itemize}

%\textbf{Matrix vector product $\mathbf{Au}$:}
%\begin{itemize}
%\item At each control volume face centroid in each cell, calculate the
%  total strain from the displacement vector, using
%  \EQ{celljac} to \EQ{disp_deriv} and \EQ{small_strains}. 
%\item Calculate an elastic stress $\sigma_{ij}^$ from the total
%  strain at each control volume face centroid as in
%  \EQ{elastic_stress}.
%\item Accumulate the vector $Y = \mathbf{Au}$ for each displacement component
%  $i$ for each node for each cell by summing over the number of
%  control volume faces for the node (nfaces).  Using values at the
%  control volume face centroid (one point quadrature):  
%  \begin{equation}
%  Y_j = [\mathbf{Au}]_i = \sum_{m=1}^{\mathrm{nfaces}} \sigma_{ij}^{m} * n_i^m * A^m
%  \LEQ{y_eq_ax}
%  \end{equation}
%\item Scatter the contributions to $Y$ for all cells to the nodes.
%\item Enforce displacement boundary conditions by setting $Y_i$ to
%  $u_i$ times a penalty parameter $p$. 
%\end{itemize}

\subsubsection{Non-Linear Thermo-Elastic-Viscoplastic Solution}
After the initial linear thermoelastic solution, it is assumed that
the \EQ{basic-eq-1} is non-linear with material properties that may be
temperature dependent.  The solution procedure for each time step is
as follows: 
\begin{itemize}
\item Store the elastic stress, plastic strain and total strain
components for each integration point for each cell from the initial
solution or the previous time step. 
\item Calculate the deviatoric stress at each integration point using
\EQ{dev-stress}.
\item Calculate the effective stress using \EQ{effective_stress}.
\item Calculate and store the portion of the residual that does not
depend on the displacement field.  This currently includes the thermal
strain terms, phase change strain terms.  These terms are also modified 
by the displacement, sliding and contact constraints.
\item Call the non-linear solver to calculate the
displacement field.  The non-linear solver calls routines for the
residual calculation and possibly a numerical approximation of the Jacobian
matrix-vector product as described below.  The stresses and strains at
the integration points are updated automatically when the residual is
computed for the convergence check.
\item Calculate the total strain, plastic strain, elastic stress, and 
plastic strain rate at the cell centroids.
\end{itemize}

\subsubsection{Residual Calculation:}

The accelerated inexact Newton and Newton-Krylov methods both require
the calculation of the solution residual for a given displacement
vector. The residual is given by
\begin{equation}
\mathbf{F}(\mathbf{u})= \frac{\partial{\sigma_{ij}}}{\partial{x_i}} + \mathbf{b}
%  \mathbf{F}(\mathbf{u})= \frac{\partial{\sigma_{ij}}}{\partial{x_i}}
  \LEQ{residual}
\end{equation}
where $\sigma_{ij}$ is given by \EQ{thermo-elas-vp}.
The residual is calculated by evaluating $\sigma_{ij}$ at each
integration point and integrating over the control volume as for the
linear elastic solution.  
\begin{equation}
  F_j(\mathbf{u}) = \sum_{m=1}^{\mathrm{nfaces}} \sigma_{ij}^m
\hat{n}_i^m A^m + b_j
  \LEQ{discrete_residual}
\end{equation}
Note that $\sigma_{ij}$ is calculated from the elastic strain, which
requires calculating the total strain, thermal strain and plastic
strain at each control volume face.
\begin{equation}
  e^{el}_{ij} = e^{tot}_{ij} - e^{th}_{ij} - e^{pl}_{ij} - e^{pc}_{ij}
  \LEQ{elastic_strain}
\end{equation}

The total strain $e^{tot}$ is calculated from the displacement field
as described above, and the thermal and phase change strain terms are
computed and stored once for each time step.  The plastic strain is
calculated by integrating the plastic strain rate at each integration
point over the time step.  The integration of the plastic strain is
implicit, assuming a linear change in total strain over the time step
and a constant straining direction equal to that at the beginning of
the time step.  If the plastic strain rate is small enough a midpoint method
is used to calculate an average strain rate.  Otherwise an ODE
integrator (BDF2) is used to accurately integrate the plastic strain
over the time step at an integration point.

%\textbf{Nonlinear Matvec:}
%
%The Newton-Krylov solver requires a routine to calculate the matrix
%vector product
%\begin{equation}
%{\mathbf J} {\mathbf v} \approx
%[{\mathbf F}({\mathbf u} + \epsilon
%{\mathbf v}) - {\mathbf F}({\mathbf u}) ] \; / \; \epsilon ,
%\LEQ{nlmatvec}
%\end{equation}
%where $\mathbf J$ is the Jacobian matrix 
%\begin{equation}
% J_{i,j} = \frac{ \partial F_{i}(\mathbf{u})}{\partial u _{j}}.
% \LEQ{fjac}
%\end{equation}
%This is straight forward, requiring two residual calculations. 
\subsubsection{Boundary Conditions}
Traction boundary conditions are applied directly to the control
volume faces on the boundary by substituting $\tau_j$ for $(\sigma_{ij}
\hat{n}_i$) in \EQ{discrete_residual}.

Displacement boundary conditions in Cartesian coordinates are applied
by replacing the equations for the node with \EQ{norm_decomp_disp}.
Sliding and contact interface conditions are imposed according to
\EQ{norm_constraint_2} and \EQ{contact_1}.  The details of the
projections of the force vectors for displacement and interface
boundary conditions are in \APP{contact}. 

\subsection{Preconditioning}
\LSEC{TMPreconditioning}

For efficient convergence the nonlinear solvers require an
approximation of the Jacobian matrix
($\PARTIALOFWRT{\mathbf{F}}{\mathbf{u}}$) for preconditioning.
Currently \truchas\ uses an approximation of the elastic stiffness
matrix $\mathbf{A}$ for preconditioning of the linear and nonlinear
solvers.  The method for approximating $\mathbf{A}$ developed for
\truchas\ uses the same control volumes and surface integral as for
the full operator $\mathbf{A}$, but the displacement gradients are
approximated using only neighboring nodes connected by edges.  Instead
of using the full tri-linear interpolation functions and all eight
nodes from a cell, three adjacent nodes from a single cell are used to
construct a tetrahedron (\FIG{tet_grad}), where stresses are to be
calculated for the surface of the volume surrounding the node labeled
1.

\begin{figure}[ht]
\centering
\includegraphics[scale=0.2]{figures/tet_grad_geo.pdf}
\caption[Construction of tetrahedra for preconditioning]{Definition
of tetrahedra for displacement gradient approximation.}
\LFIG{tet_grad}
\end{figure}

The displacement components (represented by $u,v,w$) are
assumed to vary linearly over the tetrahedron:
\begin{eqnarray}
  u & = & \sum_{i=1}^4 (a_i + b_i x + c_i y + d_i z) u_i \nonumber \\
  v & = & \sum_{i=1}^4 (a_i + b_i x + c_i y + d_i z) v_i \nonumber \\
  w & = & \sum_{i=1}^4 (a_i + b_i x + c_i y + d_i z) w_i
%  u & = & (a_1 + b_1 x + c_1 y + d_1 z) u_1 + 
%          (a_2 + b_2 x + c_2 y + d_2 z) u_2 + \nonumber \\
%    &   & (a_3 + b_3 x + c_3 y + d_3 z) u_3 +
%          (a_4 + b_4 x + c_4 y + d_4 z) u_4   \nonumber \\
%  v & = & (a_1 + b_1 x + c_1 y + d_1 z) v_1 + 
%          (a_2 + b_2 x + c_2 y + d_2 z) v_2 + \nonumber \\
%    &   & (a_3 + b_3 x + c_3 y + d_3 z) v_3 +
%          (a_4 + b_4 x + c_4 y + d_4 z) v_4   \nonumber \\
%  w & = & (a_1 + b_1 x + c_1 y + d_1 z) w_1 + 
%          (a_2 + b_2 x + c_2 y + d_2 z) w_2 + \nonumber \\
%    &   & (a_3 + b_3 x + c_3 y + d_3 z) w_3 +
%          (a_4 + b_4 x + c_4 y + d_4 z) w_4 
 \LEQ{tet_disp}
\end{eqnarray}
The displacement gradients are then constant over the element,
\begin{eqnarray}
\PARTIALOFWRT{u}{x} & = & \sum_{i=1}^4 b_i u_i \nonumber \\
\PARTIALOFWRT{u}{y} & = & \sum_{i=1}^4 c_i u_i \nonumber \\
     & \vdots & \nonumber \\
\PARTIALOFWRT{w}{z} & = & \sum_{i=1}^4 d_i w_i
% \frac{\partial{u}}{\partial{x}} & = & b_1 u_1 + b_2 u_2 + 
%                                     b_3 u_3 + b_4 u_4 \nonumber \\
% \frac{\partial{u}}{\partial{y}} & = & c_1 u_1 + c_2 u_2 + 
%                                     c_3 u_3 + c_4 u_4 \nonumber \\
%     & \vdots & \nonumber \\
% \frac{\partial{w}}{\partial{z}} & = & d_1 w_1 + d_2 w_2 + 
%                                      d_3 w_3 + d_4 w_4 
  \LEQ{tet_disp}
\end{eqnarray}
where $(u_1, u_2, u_3, u_4)$ are the $u$ displacements at nodes 1-4 in
\FIG{tet_grad}, etc.  The coefficients $(a_i, b_i, c_i, d_i)$ are
calculated from the coordinates at nodes 1-4, as described in various
finite element references, such as \cite{zienkiewicz}. The elastic
equilibrium equation $\mathbf{A}\mathbf{u} = \mathbf{b}$ can then be
expanded using an approximation of $\mathbf{A}$ in terms of $(a_i,
b_i, c_i, d_i)$ and the control volume face areas and normals.  A
single stress value based on the tetrahedron strain gradients is used
for all control volume faces associated with node 1 in that cell.

This procedure results in a matrix $\mathbf{P}$, an approximation to
$\mathbf{A}$.  For hexahedral meshes $\mathbf{P}$ usually has a much
smaller bandwidth than $\mathbf{A}$.  If the mesh is fixed, most of
the work to calculate $\mathbf{P}$ (evaluation of $a_i, b_i, c_i,
d_i$) is done once at the beginning of the simulation.  If $\lambda$
and $G$ are temperature dependent or if material volume fractions in a
cell change, $\mathbf{P}$ must be recalculated.

The linear solver package uses $\mathbf{P}$ for preconditioning of the
linear system.  Currently the only preconditioning methods for solid
mechanics are symmetric successive over-relaxation (SSOR) and diagonal
scaling.  SSOR generally results in a reduction of the number of
iterations by a factor of 3 or 4 compared to no preconditioning.  The
preconditioning routine itself is quite efficient, and the reduction
in computation time is substantial.

For tetrahedral meshes $\mathbf{P}$ should be equal to $\mathbf{A}$.
Computations on tetrahedral meshes can be quite efficient, but the
accuracy of linear tetrahedra are quite poor compared to tri-linear
hexes for the same number of nodes.  It should also be noted that
tetrahedral meshes generally have a much larger number of cells for a
given number of nodes.  Since the algorithms in \truchas\ generally loop
over cells for the construction of $\mathbf{A}$ and $\mathbf{P}$, the
benefits of tet meshes are reduced.
