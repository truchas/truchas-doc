%----------------------------------------------------------------
%
% Filename: MLHcommands.sty (excerpts from...)
% Author:   Michael L. Hall, <Hall@LANL.gov>
%
% Created on: circa 1986
% CVS Info:   Id: MLHcommands.sty,v 6.0 2002/12/04 19:41:19 hall Exp 
%
% Comments removed for brevity -- see original for explanation.

\newcommand{\bracket}[1]{\left[ #1 \right]}
\newcommand{\fn}[1]{\left( #1 \right)}
\newcommand{\dx}[1]{\,d#1}
\def\vec#1{\mbox{$\stackrel{^{\mathstrut}\smash{\longrightarrow}}{#1}$}}
\def\div{\mbox{$\vec{\nabla} \cdot$}}
\newcommand{\grad}{\mbox{$\vec{\nabla}$}}
\newcommand{\trans}[1]{{#1}^{\mathrm{T}}}
\newcommand{\mtrans}[1]{{#1}^{-\mathrm{T}}}
\newcommand{\mat}[1]{\mathbf{#1}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\explain}[1]{\mbox{\hspace{2em} #1}}
\newcommand{\Augustus}{{\textsc{Augustus}}}

% End of excerpts from: MLHcommands.sty
%----------------------------------------------------------------

\chapter{Support-Operators}
\LAPP{support_ops}

The support-operators method \cite{Shashkov02,morel01} is a means of 
constructing discrete analogs of invariant differential operators, 
like the divergence and the gradient. 
In \truchas, a mixed hybrid formulation of the support-operators 
method is used within the species diffusion component.  It is also 
used as a discretization scheme for the electromagnetics component 
(disguised as finite elements).  
The heat transfer component and the projection step
in the fluid flow component, uses the support-operators discretization
of the \Augustus\ code package \cite{hall98e,hall99c,morel01}.

There are several formulations of the support-operators
methodology, and the version used by the species diffusion and 
electromagnetics component are different from the version used by
the  heat transfer and and the fluid flow components.
We include both formulations here for elucidation and comparison.

\section{Species Diffusion Component Support-Operators Formulation}

\renewcommand{\grad}{\mathbf{grad} \,}
\renewcommand{\div}{\mathrm{div} \, }

This formulation is used for the species diffusion component and its
description was adapted from \cite{berndt04}.

To provide an overview of support-operators, we consider the following
standard diffusion equation given by
\begin{equation} \label{so:diffeq}
- \div( K \, \grad T ) = f \; \; \forall \; (x,y,z) \in \Omega \subset \Re^3,
\end{equation}
where $T(x,y,z)$ represents concentration, $K(x,y,z)$ is the
conductivity, and $f$ is a source term.  In general, $K$ may 
be a symmetric positive-definite tensor that can vary discontinuously
in space.  The problem becomes well-posed when we enforce boundary
conditions.  Here we only consider homogeneous Dirichlet boundary
conditions, i.e., 
\begin{equation} \label{so:bcs} 
\hspace*{0.5cm} T(x,y,z) = 0, \; \; \forall (x,y,z) \in \Omega.
\end{equation}
Next, we reduce the second-order operator given in (\ref{so:diffeq})
to a more natural set of first-order operators, given by
\begin{equation} \label{so:firstord}
\div \mathbf{W} = f, \hspace*{1cm} \mathbf{W} = -K \, \grad T,
\end{equation}
where the first equation is an expression of conservation and the
second is an expression of {\it Fourier's Law}.  

In the {\bf first step} of the support-operators discretization method,
we specify discrete degrees of freedom for the scalar and vector
unknowns. With {\sc Truchas} in mind, we assume a mesh consisting of
hexahedral elements.  Let $\Omega^h$ be such a mesh consisting of
hexahedral elements $\left\{ {\sc H}_c \right\}_{c=1}^{ncells}$ such
that 
\[ 
\Omega = \bigcup_{c=1,ncells}  {\sc H}_c. 
\]
We use the geometric cell- and face-centers to define the
locations of the scalar and vector unknowns, respectively. 
Specifically, the face-centered vector unknowns are represented only
by their normal component $\mathbf{W} \cdot \mathbf{n}$.

In the {\bf second step} of the support operator discretization
method, we equip the discrete spaces $HC$ and ${\cal HF}$ of scalar
and  vector unknowns, respectively, with scalar products. These
scalar products are simple extensions of their continuous
analogues. We define the the inner product for discrete scalar
functions $u$ and $v$ in $HC$ as 
\begin{equation} \label{so:scal-innerprod} 
\left[ u, v \right]_{HC} \stackrel{\mathrm{def}}{=}
\sum_{c=1}^{ncells} u_c \, v_c \, | H_c |,
\end{equation}
where $u_c$ and $v_c$ are the cell-centered values, and $|H_c|$ is the
element volume.  For the
space of discrete vector unknowns, the scalar product is not as
straightforward and we provide only a brief description here.  See
\cite{Shashkov02} for a more careful description of the vector
unknown space scalar product.  Nonetheless, we define 
the inner product by
\begin{equation} \label{so:vec-innerprod}
\left[ \mathbf{A}, \mathbf{B} \right]_{\cal HF}
\stackrel{\mathrm{def}}{=}
\sum_{H_c\in \Omega^h} \left[ \mathbf{A}, \mathbf{B} \right]_{{\cal HF},
H_c}
\end{equation}
with
\begin{equation}
\left[ \mathbf{A}, \mathbf{B} \right]_{{\cal HF},H_c}
\stackrel{\mathrm{def}}{=} 
\sum_{n=1}^8 \, (K_n^c)^{-1} ( \mathbf{A}_n^c, \mathbf{B}_n^c )\, V_n^c.
\end{equation}
Here, $n$ loops over all vertices of the hexahedron $H_c$, and $K_n^c$ is
the diffusion coefficient local to $H_c$ at its vertex $n$.
$V_n^c$ denotes a ``nodal'' volume, and $\mathbf{A}_n^c$ and
$\mathbf{B}_n^c$ denote cartesian vectors located at vertex $n$ of
$H_c$.
These nodal vectors are constructed using face-centered vector
unknowns from faces that are adjacent to the node and belong to $H_c$.

The {\bf third step} of the support operator discretization method is
to define a discrete analogue of the divergence operator
$\mathbf{DIV}:\cal{HF} \rightarrow HC$.  
Based on Gauss' divergence theorem, we have the 
coordinate invariant definition of $\div$ given by
\begin{equation} \label{so:div-op-defn}
\div \mathbf{W} = \lim_{V \rightarrow 0} \frac{1}{V} \oint_{\partial V}
  \mathbf{W} \cdot \mathbf{n} \; \mathrm{d}S.
\end{equation}
We use this expression to define a discrete divergence over a
hexahedron in $\Omega^h$. Denote by $F$ a particular face with area
$A_F$. Then the discrete analogue of (\ref{so:div-op-defn}) on hexahedron
$H_c$ is
\begin{equation}
(\mathbf{DIV}\; \mathbf{W})_{H_c} = \frac{1}{|H_c|} \sum_{F \in H_c}
A_F\, \mathbf{W} \cdot \mathbf{n}_{F,c},
\end{equation}
where $\mathbf{n}_{F,c}$ is the outward normal to face $F$ of
hexahedron $H_c$.

Finally, in the {\bf fourth step} of the support operator discretization
method, we derive a discrete flux operator  ${\cal G}$ (as the
discrete analogue of the operator  $-K \, \nabla$) that is adjoint to
the discrete divergence operator $\mathbf{DIV}$ with respect to the
two scalar products (\ref{so:scal-innerprod}) and
(\ref{so:vec-innerprod}), i.e.
\begin{equation}
\left[ \mathbf{DIV} W, u \right]_{\cal HF} = \left[ W, {\cal G} u 
\right]_{HC},\quad \forall W \in {\cal HF},\; \forall u \in HC. 
\end{equation}
To derive an explicit formula for ${\cal G}$, we consider an auxiliary
scalar product $<\cdot,\cdot>$ and relate it to scalar products
(\ref{so:scal-innerprod}) and (\ref{so:vec-innerprod}). Denote by
$<\cdot,\cdot>$ the standard vector dot product, then
\[
\left[ u, v \right]_{HC} = < {\cal D} u, v >,\quad 
\mbox{and}\;
\left[ U, W \right]_{\cal HF} = < {\cal M} U, W >, 
\]
where ${\cal D}$ is a diagonal matrix ${\cal D} = \mbox{diag}\{ |H_1|,
|H_2|, \ldots, |H_{ncells}|\}$, with the volumes of all hexahedrons on the
diagonal, and ${\cal M}$ is a sparse symmetric mass matrix. 
Combining these last two formulas, we get
\[
\left[ W, \mathbf{DIV}^\star u\right]_{\cal HF} = < W, {\cal M}
\mathbf{DIV}^\star u >  
= \left[ \mathbf{DIV} W, u \right]_{HC} = < W, \mathbf{DIV}^T {\cal
D} u >.
\]
for all $W \in {\cal HF}$ and $u \in HC$. Here, $\mathbf{DIV}^T$ is
the adjoint of $\mathbf{DIV}$ with respect to the auxiliary scalar
product. 
Therefore, 
\[
{\cal M} \mathbf{DIV}^\star = \mathbf{DIV}^T {\cal D} 
\]
which implies
\begin{equation}\label{SO:gradient}
{\cal G} = \mathbf{DIV}^\star = {\cal M}^{-1} \mathbf{DIV}^T {\cal D}.
\end{equation}
The support operator discretization of the first order system
(\ref{so:firstord}) is now given by
\[
\mathbf{DIV} W = f^h, \quad \mbox{and}\; W = {\cal G} T,
\]
where $W\in {\cal HF}$, $T \in HC$, and $f^h$ is a vector of integral
averages of $f$, each component taken over one hexahedron in the mesh.

Note that $\mathbf{DIV}^T u$ is defined on faces and represents the
difference in concentration between two adjacent cells. This operator
forms the basis of the gradient in the {\sc ortho} algorithm used in heat
transfer calculations.


\subsection{Mixed Hybrid Formulation}

In the mixed hybrid formulation, we localize the face centered vector 
unknowns from step two above to cells by introducung two separate ones,
one for each cell adjacent to a face. Continuity of the normal flux across
a face is enforced
by introducing additional equality constraints: We require the face
flux on a face belonging to a cell to equal that of the equivalent face
on its neighbor cell. 
These additional equality constraints are introduced by means of Lagrange 
multipliers which results in additional scalar degrees of freedom, one
for each face. It turns out that these unknowns represent in fact the
concentration on faces.

The algebraic equations that  must be solved can be written as a $3\times 3$
block matrix, where the blocks derive from dividing the unknowns into
cell centered concentrations, face centered concentrations, and face 
centered normal fluxes. The mixed hybrid linear system of equations
arises when the face centered normal flux variables are eliminated (via
block elimination). We go one step further and then eliminate the 
face centered concentration variables as well.



\section{\Augustus\ Support-Operators Formulation}

This formulation is used for the heat transfer component and the
fluid flow component of Truchas. The following description was
adapted from \cite{hall98e,hall99c}.

We start with the following diffusion equation 
\be
  - \div D \grad \phi = S \, , \LEQ{analytic diffusion}
\ee
which can be written
\bea
  \div \vec{F} = S \, , \\
  \vec{F} = - D \grad \phi \, , 
\eea
where $\phi$ represents the temperature, $D$ represents the
thermal conductivity, $\vec{F}$ represents the heat flux and $S$
represents a heat source in the heat transfer solution. In the
projection step of the fluid flow solution, $\phi$ represents a
change in the pressure, $D$ represents the inverse of the density,
$\vec{F}$ represents an ``acceleration'' flux and $S$ represents an
``acceleration gradient'' source.

To discretize this equation, we integrate over a cell ($c$), applying
Gauss's Theorem to change the volume integral into a surface integral
over the faces ($f$):
\be
  \sum_f \vec{F_{c,f}} \cdot \vec{A_{c,f}} = S_c V_c 
  \LEQ{discretized diffusion}
\ee
This is the basic form used by \truchas. The gradient terms
($\vec{F_{c,f}} \cdot \vec{A_{c,f}}$) are calculated separately from
the solution of the conservation equation, so that a matrix-free
solution procedure may be followed. This matrix-free solution
method requires that the gradient terms be calculated when given
the $\phi$-values of a trial solution, so that is the form derived
by the Support Operator Method.

Note that this discretization will be inherently conservative,
and that no derivatives are taken across material boundaries --
a rigorous treatment.  We locate unknowns for $\phi$ at the cell
centers and the cell faces.  The $\vec{F_{c,f}} \cdot \vec{A_{c,f}}$
(gradient) terms, on each face of a cell, must be defined in terms
of the $\phi$'s within that cell.

In summary, the Support Operator Method represents the diffusion
term ($\div D \grad \phi$) as the divergence ($\div$) of a gradient
($\grad$), explicitly defines one of the operators (in this case,
the divergence operator), and then defines the remaining operator
(in this case, the gradient operator) as the discrete adjoint of
the first operator. The last step is accomplished by discretizing a
portion of a vector identity. In other words, the first operator is
set up explicitly, and the second operator is defined in terms of
the first operator's definition. The rest of this section derives
the Support Operator Method in more detail.

To derive the Support Operator Method used in \Augustus, we start
with this vector identity,
\be
  \div \fn{\phi \vec{W}} = \phi \div \vec{W} 
                           + \vec{W} \cdot \grad \phi \; ,
\ee
where $\phi$ is the scalar variable to be diffused and $\vec{W}$
is an arbitrary vector, and integrate over a cell volume:
\be
  \int_c \div \fn{\phi \vec{W}} \dx{V} = 
  \int_c \phi \div \vec{W} \dx{V} +
  \int_c \vec{W} \cdot \grad \phi \dx{V} \; . 
  \LEQ{SO Vector Identity}
\ee
Each term in the equation above will be treated separately.

The first term in \EQ{SO Vector Identity} can be transformed via Gauss's
Theorem into a surface integral,
\be
  \int_c \div \fn{\phi \vec{W}} \dx{V} = 
  \oint_S \fn{\phi \vec{W}} \cdot \,\vec{dA} \; .
\ee
This is discretized into values defined on each face of the hexahedral cell, 
\be
  \oint_S \fn{\phi \vec{W}} \cdot \,\vec{dA} \approx
  \sum_f \phi_f \vec{W_f} \cdot \vec{A_f} \; .
\ee

The second term in \EQ{SO Vector Identity} is approximated by first
assuming that $\phi$ is constant over the cell (at the center value),
and then performing a discretization similar to the previous one
for the first term:
\bea
  \int_c \phi \div \vec{W} \dx{V}
  & \approx & \phi_c \int_c \div \vec{W} \dx{V} \; , \\
  & =       & \phi_c \oint_S \vec{W} \cdot \,\vec{dA} \; , \\
  & \approx & \phi_c \sum_f \vec{W_f} \cdot \vec{A_f} \; .  
\eea

Turning to the final (third) term in \EQ{SO Vector Identity},
we insert the definition of the flux,
\be
  \vec{F} = -D \grad \phi \; ,
\ee
to get
\be
  \int_c \vec{W} \cdot \grad \phi \dx{V} =
  - \int_c D^{-1} \vec{W} \cdot \vec{F} \dx{V} \; .
\ee
Note that by defining the flux in terms of the remainder of the
equation, the gradient is being defined in terms of the divergence.

The third term is discretized by evaluating the integrand at each
of the cell nodes (octants of the hexahedral cells, represented by
$n$) and summing:
\be
  - \int_c D^{-1} \vec{W} \cdot \vec{F} \dx{V} \approx 
  - \sum_n D^{-1}_n \vec{W_n} \cdot \vec{F_n} V_n \; .
\ee

Combining all of the discretized terms of \EQ{SO Vector Identity} and
changing to a linear algebra representation gives
\be
  \sum_f \phi_f \trans{\mat{W}}_f \mat{A}_f =
  \phi_c \sum_f \trans{\mat{W}}_f \mat{A}_f  
  - \sum_n D^{-1}_n \trans{\mat{W}}_n \mat{F}_n V_n \; .
\ee

Rearranging terms gives
\be
  \sum_n D^{-1}_n \trans{\mat{W}}_n \mat{F}_n V_n = 
  \sum_f \fn{\phi_c - \phi_f} \trans{\mat{W}}_f \mat{A}_f 
  \; .
\ee

Note that the right hand side is a sum over the six faces, but the
left hand side is a sum over the eight nodes.

In order to express the node-centered vectors, $\mat{W}_n$ and $\mat{F}_n$,
in terms of their face-centered counterparts, define
\be
  \trans{\mat{J}}_n \mat{W}_n \equiv \bracket{
     \begin{array}{c} \trans{\mat{W}}_{f1} \mat{A}_{f1} \\[1em]
                      \trans{\mat{W}}_{f2} \mat{A}_{f2} \\[1em]
                      \trans{\mat{W}}_{f3} \mat{A}_{f3}
     \end{array}} \; ,
\ee
where $f1$, $f2$, and $f3$ are the faces adjacent to node $n$
and the Jacobian matrix is the square matrix given by
\be
  \mat{J}_n = \bracket{\begin{array}{ccc}
                       \mat{A}_{f1} & \mat{A}_{f2} & \mat{A}_{f3}
                       \end{array}} \; .
\ee

Using this definition for the node-centered vectors $\mat{W}_n$ and $\mat{F}_n$
and performing some algebraic manipulations results in
\be \LEQ{discrete2}
  \sum_n D^{-1}_n V_n \trans{\bracket{\begin{array}{c}
                       \trans{\mat{W}}_{f1} \mat{A}_{f1} \\[1em] 
                       \trans{\mat{W}}_{f2} \mat{A}_{f2} \\[1em] 
                       \trans{\mat{W}}_{f3} \mat{A}_{f3}
                       \end{array}}}
    \mat{J}_n^{-1} \mtrans{\mat{J}}_n
                       \bracket{\begin{array}{c}
                       \trans{\mat{F}}_{f1} \mat{A}_{f1} \\[1em]
                       \trans{\mat{F}}_{f2} \mat{A}_{f2} \\[1em]
                       \trans{\mat{F}}_{f3} \mat{A}_{f3}
                       \end{array}}
  = \trans{\widetilde{\mat{W}}} \widetilde{\mat{\Phi}}
  \; ,
\ee
where the sum over faces has been written as a dot product of
$\widetilde{\mat{W}}$ and $\widetilde{\mat{\Phi}}$, which are defined by
\be
  \widetilde{\mat{W}}  =  
        \bracket{\begin{array}{c}
          \trans{\mat{W}}_1 \mat{A}_1 \\[1em]
          \trans{\mat{W}}_2 \mat{A}_2 \\[1em]
          \trans{\mat{W}}_3 \mat{A}_3 \\[1em]
          \trans{\mat{W}}_4 \mat{A}_4 \\[1em]
          \trans{\mat{W}}_5 \mat{A}_5 \\[1em]
          \trans{\mat{W}}_6 \mat{A}_6
        \end{array}} \; , \; \; \; 
  \widetilde{\mat{\Phi}}  =  
        \bracket{\begin{array}{c}
          \fn{\phi_c - \phi_1} \\[1em]
          \fn{\phi_c - \phi_2} \\[1em]
          \fn{\phi_c - \phi_3} \\[1em]
          \fn{\phi_c - \phi_4} \\[1em]
          \fn{\phi_c - \phi_5} \\[1em]
          \fn{\phi_c - \phi_6}
        \end{array}} \; .
\ee

To convert the short vectors involving the faces adjacent to a particular
node into sparse long vectors involving all of the faces of the cell, define
permutation matrices for each node, $\mat{P}_n$, such that
\be
  \bracket{\begin{array}{c}
             \trans{\mat{W}}_{f1} \mat{A}_{f1} \\[1em] 
             \trans{\mat{W}}_{f2} \mat{A}_{f2} \\[1em] 
             \trans{\mat{W}}_{f3} \mat{A}_{f3}
           \end{array}}
  = \mat{P}_n 
    \bracket{\begin{array}{c}
      \trans{\mat{W}}_1 \mat{A}_1 \\[1em]
      \trans{\mat{W}}_2 \mat{A}_2 \\[1em]
      \trans{\mat{W}}_3 \mat{A}_3 \\[1em]
      \trans{\mat{W}}_4 \mat{A}_4 \\[1em]
      \trans{\mat{W}}_5 \mat{A}_5 \\[1em]
      \trans{\mat{W}}_6 \mat{A}_6
    \end{array}}
  = \mat{P}_n \widetilde{\mat{W}} \; ,
\ee
where, for example, 
\be
  \mat{P}_n = 
  \bracket{\begin{array}{cccccc}
             0 & 0 & 1 & 0 & 0 & 0 \\[1em] 
             0 & 0 & 0 & 0 & 1 & 0 \\[1em] 
             0 & 1 & 0 & 0 & 0 & 0
           \end{array}} 
  \explain{\begin{tabular}{l}
             if $f1\fn{n} = 3$, \\
             $f2\fn{n} = 5$, \\
             and $f3\fn{n} = 2$.
           \end{tabular}}
\ee

Note that $\mat{P}_n$ is rectangular, with a size of
$N_{\textit{dimensions}} \times N_{\textit{local faces}}$ ($3 \times
6$ for 3-D, $2 \times 4$ for 2-D, $1 \times2$ for 1-D).

Using the permutation matrices, and defining $\widetilde{\mat{F}}$ in a
fashion similar to $\widetilde{\mat{W}}$ ($\widetilde{\mat{F}}$ is a vector
of $\trans{\mat{F}}_f \mat{A}_f$ for each cell face), gives
\be
  \sum_n D^{-1}_n V_n 
    \trans{\widetilde{\mat{W}}} \trans{\mat{P}}_n
    \mat{J}_n^{-1} \mtrans{\mat{J}}_n
    \mat{P}_n \widetilde{\mat{F}} 
  = \trans{\widetilde{\mat{W}}} \widetilde{\mat{\Phi}} \; ,
\ee
or
\be
  \trans{\widetilde{\mat{W}}} 
     \bracket{\sum_n D^{-1}_n V_n \trans{\mat{P}}_n
     \mat{J}_n^{-1} \mtrans{\mat{J}}_n
     \mat{P}_n}
  \widetilde{\mat{F}} 
  = \trans{\widetilde{\mat{W}}} \widetilde{\mat{\Phi}} \; ,
\ee
or
\be
  \trans{\widetilde{\mat{W}}} \mat{S}
  \widetilde{\mat{F}} 
  = \trans{\widetilde{\mat{W}}} \widetilde{\mat{\Phi}} \; ,
\ee
where
\be
  \mat{S} = 
    \sum_n D^{-1}_n V_n \trans{\mat{P}}_n
    \mat{J}_n^{-1} \mtrans{\mat{J}}_n
    \mat{P}_n \; .
\ee
The original vector $\vec{W}$ (on which $\mat{W}_f$ and
$\widetilde{\mat{W}}$ are based) was an arbitrary vector. It can
now be eliminated from the equation to give
\be
  \mat{S} \widetilde{\mat{F}} = \widetilde{\mat{\Phi}} \; . \LEQ{SO S matrix}
\ee
This equation could be easily inverted ($\mat{S}$ is $6 \times 6$
in 3-D), and the resultant relationships between $\vec{F_{c,f}}
\cdot \vec{A_{c,f}}$ and $\phi$ for each cell combined using
\EQ{discretized diffusion} and flux equality at each face into a
sparse matrix for the entire problem, which could be solved once
for the solution to the original \EQ{analytic diffusion}. This is
what is done inside \Augustus\/.

However, due to historical reasons, \truchas\ takes a different
approach. \truchas\ combines all of the cell equations like \EQ{SO
S matrix} into a single (block-diagonal) equation. When the outer
nonlinear iteration needs the gradients ($\vec{F_{c,f}} \cdot
\vec{A_{c,f}}$), this block-diagonal matrix equation is solved. Note
that no conservation equation is solved when determining these
gradients, and the fluxes on either side of a face are not set
equal. Also, instead of iteratively solving for the unknown $\Phi$
values on the faces, these are set to the cell-center value across
the face (i.e. the neighbor cell value) once at the beginning of
the inner matrix solve for the gradients. 
The \truchas\ solution method will be changed in the future.

