\chapter{\texttt{FLOW} Namelist}
\tnamelistdef{FLOW}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{FLOW} namelist specifies the parameters for the fluid flow model
and algorithm. This namelist is read whenever the \tnamelist{PHYSICS} namelist
option \tvar{PHYSICS}{Flow} is enabled.
Parameters for the linear solvers employed by the algorithm are specified
using \tnamelist{FLOW_VISCOUS_SOLVER} and \tnamelist{FLOW_PRESSURE_SOLVER}
namelists.
Flow boundary conditions are defined using \tnamelist{FLOW_BC} namelists.

\section*{\texttt{FLOW} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required when flow physics is enabled.
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}
\subsection*{Physics Options}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{FLOW}{inviscid}
\end{itemize}
\subsection*{Numerical parameters}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{FLOW}{courant_number}
\item \tvar{FLOW}{viscous_number}
\item \tvar{FLOW}{viscous_implicitness}
\item \tvar{FLOW}{track_interfaces}
\item \tvar{FLOW}{material_priority}
\item \tvar{FLOW}{vol_track_subcycles}
\item \tvar{FLOW}{nested_dissection} (expert)
\item \tvar{FLOW}{vol_frac_cutoff} (expert)
\item \tvar{FLOW}{fischer_dim} (expert)
\item \tvar{FLOW}{fluid_frac_threshold} (expert)
\item \tvar{FLOW}{min_face_fraction} (expert)
\item \tvar{FLOW}{void_collapse} (experimental)
\item \tvar{FLOW}{void_collapse_relaxation} (experimental)
\item \tvar{FLOW}{wisp_redistribution} (experimental)
\item \tvar{FLOW}{wisp_cutoff} (experimental)
\item \tvar{FLOW}{wisp_neighbor_cutoff} (experimental)
\end{itemize}


\section*{\texttt{inviscid}}
\tvardef{FLOW}{inviscid}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  This option omits the viscous forces from the flow equations. In this case
  there is no viscous system to solve and the \tnamelist{FLOW_VISCOUS_SOLVER}
  namelist is not required.
\item[Type:] \logical
\item[Default:] \false
\end{description}


\section*{\texttt{courant_number}}
\tvardef{FLOW}{courant_number}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  This parameter sets an upper bound on the time step that is associated
  with stability of the explicit fluid advection algorithm. A value of 1
  corresponds (roughly) to the stability limit, with smaller values resulting
  in proportionally smaller allowed time steps. Truchas uses the largest time
  step possible subject to this and other limits.
\item[Type: \real]
\item[Default: $0.5$]
\item[Valid values:] $(0.0,1.0]$
\item[Notes:]
The Courant number for a cell is the dimensionless value $C_i =
u_i\Delta t/\Delta x_i$ where $\Delta t$ is the time step, $u_i$ the fluid
velocity magnitude on the cell, and $\Delta x_i$ a measure of the cell size.
The time step limit is the largest $\Delta t$ such that $\max\{C_i\}$ equals
the value of \texttt{courant_number}.

The interpretation of $u_i$ and $\Delta x_i$ for a general cell is somewhat
sticky. Currently a ratio $u_f/h_f$ is computed for each face of a cell and
the maximum taken for the value of $u_i/\Delta x_i$. Here $u_f$ is the normal
fluxing velocity on the face, and $h_f$ is the inscribed cell height at the
face; that is, the minimum normal distance between the face and cell nodes
not belonging to the face.
\end{description}


\section*{\texttt{viscous_number}}
\tvardef{FLOW}{viscous_number}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
This parameter sets an upper bound on the time step that is associated with
stability of an explicit treatment of viscous flow stress tensor. A value of
1 corresponds roughly to the stability limit, with smaller values resulting
in proportionally smaller allowed time steps. Truchas uses the largest time
step possible subject to this and other limits.

For an implicit treatment of the viscous flow stress tensor with
\tvar{FLOW}{viscous_implicitness} at least $\tfrac12$, which is always strongly
recommended, the viscous discretization is unconditionally stable and \emph{no
time step limit is needed}. In this case, the parameter can still be used to
limit the time step for \emph{accuracy}. A value of 0 will disable this limit
entirely.
\item[Type:] \real
\item[Default:] $0$
\item[Valid values:] ${}\ge0$
\item[Notes:]
The viscous number for a cell is the dimensionless value $V_i =
\nu_i\Delta t/{\Delta x_i}^2$, where $\Delta t$ is the time step, $\nu_i$
the kinematic viscosity ($\mu/\rho$) on the cell, and $\Delta x_i$ a measure
of the cell size. The time step limit is the largest $\Delta t$ such that
$\max\{V_i\}$ equals the value of \texttt{viscous_number}. Currently the
measure of cell size mirrors that used in the definition of the
\tvar{FLOW}{courant_number}, namely that $\Delta x_i$ is taken as the
minimum of the inscribed heights $h_f$ for the faces of the cell.
\end{description}


\section*{\texttt{viscous_implicitness}}
\tvardef{FLOW}{viscous_implicitness}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The degree of time implicitness $\theta$ used for the velocity field in
the discretization of the viscous flow stress tensor in the fluid momentum
conservation equation. The velocity is given by the $\theta$-method,
$\mathbf{u}_\theta = (1-\theta)\mathbf{u}_n -  \theta \mathbf{u}_{n+1}$:
$\theta=0$ gives an explicit discretization and $\theta=1$ a fully implicit
discretization. In practice only the values 1, $\tfrac12$ (trapezoid method),
and 0 are useful, and use of the latter explicit discretization is generally
not recommended. Note that an implicit discretization, $\theta>0$, will
require the solution of a linear system; see \tnamelist{FLOW_VISCOUS_SOLVER}.
This parameter is not relevant to \tvar{FLOW}{inviscid} flow problems.
\item[Type:] \real
\item[Default:] $1$
\item[Valid values:] $[0,1]$
\item[Notes:]
The discretization is first order except for the trapezoid method
($\theta=\frac12$) which is second order. However note that the flow
algorithm overall is only first order irrespective of the choice of $\theta$.

The advanced velocity $\mathbf{u}_{n+1}$ is actually the predicted velocity
$\mathbf{u}^\star_{n+1}$ from the predictor stage of the flow algorithm.
\end{description}


\section*{\texttt{track_interfaces}}
\tvardef{FLOW}{track_interfaces}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
This option enables the tracking of material interfaces. The default is to
track interfaces whenever the problem involves more than one material. If the
problem involves a single fluid and it is known a priori that there will
never be any mixed material cells containing fluid, then this option can be
set to false to short-circuit some unnecessary work, but otherwise the default
should be used.
\item[Type:] \logical
\item[Default:] \true
\item[Notes:]
~
\todo[inline]{Say something about diffuse advection for multi-materials.}
\end{description}


\section*{\texttt{material_priority}}
\tvardef{FLOW}{material_priority}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
A list of material names that defines the priority order in which material
interfaces are reconstructed within a cell for volume tracking. All fluid
material names must be listed, and if the problem includes any non-fluid
materials, this list must include the case-sensitive keyword \texttt{"SOLID"},
which stands for all non-fluid materials lumped together. The default is the
list of fluid materials in input file order, followed by \texttt{"SOLID"} for
the lumped non-fluids.
\item[Type:] string list
\item[Notes:]
Different priorities will result in somewhat different results. Unfortunately
there are no hard and fast rules for selecting the priorities.
\todo[inline]{We should try to give some guidance here, and/or give some
indication of the issues involved.}
\end{description}


\section*{\texttt{vol_track_subcycles}}
\tvardef{FLOW}{vol_track_subcycles}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The number of sub-time steps $n$ taken by the volume tracker for every time
step of the flow algorithm. If the flow time step size is $\Delta t$ then
the volume tracker will take $n$ time steps of size $\Delta t/n$ to compute
the net flux volumes and advance the volume fractions for the flow step.
\item[Type:] \integer
\item[Default:] 2
\item[Notes:]
With the current unsplit advection algorithm~\cite{rk97} it is necessary to
sub-cycle the the volume tracking time integration method in order to obtain
good ``corner coupling'' of the volume flux terms.
\end{description}


\section*{\texttt{nested_dissection}\quad(Expert Parameter)}
\tvardef{FLOW}{nested_dissection}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
This option enables use of the nested dissection algorithm to reconstruct
material interfaces in cells containing 3 or more materials. If set false
the less accurate and less expensive onion skin algorithm will be used.
\item[Type:] \logical
\item[Default:] \true
\end{description}


\section*{\texttt{vol_frac_cutoff}\quad(Expert Parameter)}
\tvardef{FLOW}{vol_frac_cutoff}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The smallest material volume fraction allowed. If a material volume fraction
drops below this cutoff, the material is removed entirely from the cell, and
its volume fraction replaced by proportional increases to the volume fractions
of the remaining materials, or if the cell contains void, by increasing the
void volume fraction alone.
\item[Type:] \real
\item[Valid values:] $(0,1)$
\item[Default:] $10^{-8}$
\end{description}


\section*{\texttt{fischer_dim}\quad(Expert Parameter)}
\tvardef{FLOW}{fischer_dim}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The dimension $d$ of the subspace used in Fischer's projection
method~\cite{fischer1998projection} for computing an initial guess for
pressure projection system based on previous solutions. Memory requirements
for the method are $2(d+1)$ cell-based vectors. Set this variable to 0
to disable use of this method.
\item[Type:] \integer
\item[Default:] 6
\end{description}


\section*{\texttt{fluid_frac_threshold}\quad(Expert Parameter)}
\tvardef{FLOW}{fluid_frac_threshold}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
Cells with a total fluid volume fraction less than this threshold are ignored
by the flow solver, being regarded as `solid' cells.
\item[Type:] \real
\item[Valid values:] $(0,1)$
\item[Default:] $10^{-2}$
\end{description}


\section*{\texttt{min_face_fraction}\quad(Expert Parameter)}
\tvardef{FLOW}{min_face_fraction}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The variable sets the minimum value of the fluid density associated with a
face for the pressure projection system. It is specified as a fraction of the
minimum fluid density (excluding void) of any fluid in the problem.
\item[Type:] \real
\item[Default:] $10^{-3}$
\end{description}


\section*{\texttt{void_collapse}\quad(Experimental)}
\tvardef{FLOW}{void_collapse}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The volume-of-fluid algorithm effectively treats small fragments of void
entrained in fluid as incompressible, resulting in unphysical void
``bubbles'' that persist in the flow. A model that drives the collapse of
these void fragments will be enabled when this variable is set to true.
See \mbox{\tvar{FLOW}{void_collapse_relaxation}} for a model parameter.
\item[Type:] \logical
\item[Default:] \false
\end{description}


\section*{\texttt{void_collapse_relaxation}\quad(Experimental)}
\tvardef{FLOW}{void_collapse_relaxation}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The relaxation parameter in the void collapse model.
See \tvar{FLOW}{void_collapse}.
\item[Type:] \real
\item[Default:] $0.1$
\item[Valid values:] $[0,1]$
\item[Notes:]
The relaxation factor is roughly inversely proportional to the number
of timesteps required for all the void in a cell to collapse as dictated
by inertial forces. Thus a relaxation factor of 1 would allow for all the
void in a cell to collapse over a single timestep. A factor of 0.1 would
allow for the void in a cell to collapse over the course of 10 timesteps.
Larger values tend to cause more mass loss (on the order of 0.5\%),
although the results do improve with increased subcycling.
\end{description}

\section*{\texttt{wisp_redistribution}\quad(Experimental)}
\tvardef{FLOW}{wisp_redistribution}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
A cell containing a small amount of fluid (wisps) can sometimes trigger pathological behavior,
manifesting as a perpetual acceleration that drives the timestep to 0.
 A model that redistributes these wisps to other other fluid cells will be enabled when this variable is set to true.
See \mbox{\tvar{FLOW}{wisp_cutoff}} and \mbox{\tvar{FLOW}{wisp_neighbor_cutoff}} model parameters.
\item[Type:] \logical
\item[Default:] \false
\end{description}


\section*{\texttt{wisp_cutoff}\quad(Experimental)}
\tvardef{FLOW}{wisp_cutoff}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
Fluid cells with a fluid volume fraction below this value may be treated as wisps and have their fluid material moved around to other fluid cells.
Generally, moving fluid around the domain can have an undesirable effect on the flow physics.  It is therefore advisable to keep this value as small as possible.  Based on 
numerical experimentation, the recommended value is $0.05$.  Smaller values, such as, 
$0.01$ did not result in robust simulations.
See \mbox{\tvar{FLOW}{wisp_redistribution}}.
\item[Type:] \real
\item[Default:]  $0.05$
\end{description}

\section*{\texttt{wisp_neighbor_cutoff}\quad(Experimental)}
\tvardef{FLOW}{wisp_neighbor_cutoff}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
Fluid cells with a fluid volume fraction below \texttt{wisp_cutoff} can only be considered a wisp if the amount of fluid in the neighboring cells is also "small". This definition of "small" is controlled by \texttt{wisp_neighbor_cutoff}.
In addition, for a a cell to receive wisp material, the fluid volume fraction of it and its neighbors must be larger than \texttt{wisp_neighbor_cutoff}.
See \mbox{\tvar{FLOW}{wisp_redistribution}}.
\item[Type:] \real
\item[Default:] $0.25$
\end{description}
