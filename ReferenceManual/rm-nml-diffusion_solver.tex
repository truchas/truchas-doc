\chapter{\texttt{DIFFUSION_SOLVER} Namelist}
\tnamelistdef{DIFFUSION_SOLVER}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{DIFFUSION_SOLVER} namelist sets the parameters that are specific
to the heat and species transport solver.  The namelist is read when either
of the \tnamelist{PHYSICS} namelist options \tvar{PHYSICS}{Heat_Transport}
or \tvar{PHYSICS}{Species_Transport} are enabled.

The solver has two time integration methods which are selected by the variable
\tvar{DIFFUSION_SOLVER}{Stepping_Method}.  The default is a variable 
step-size, implicit second-order BDF2 method that controls the local truncation
error of each step to a user-defined tolerance by adaptively adjusting the step size.
The step size is chosen so that an a priori estimate of the error will be
within tolerance, and steps are rejected when the actual error is too large.
A failed step may be retried with successively smaller step sizes.

The other integration method is a non-adaptive, implicit first-order BDF1 method
specifically designed to handle the exceptional difficulties that arise when
heat transfer is coupled to a fluid flow system that includes void.  In this
context the heat transfer domain changes from one step to the next because of
the moving void region, and mesh cells may only be partially filled with material.
For this method the time step is controlled by flow or other physics models.

Both methods share a common nonlinear solver and preconditioning options.

The initial step size and upper and lower bounds for the step size are set in
the \tnamelist{NUMERICS} namelist.  In addition, the step size selected by the
adaptive solver may be further limited by other physics models or by the
\texttt{NUMERICS} variables \tvar{NUMERICS}{Dt_Grow} and \tvar{NUMERICS}{Dt_Constant}.
When only diffusion solver physics are enabled, it is important that these variables
be set appropriately so as not to unnecessarily impede the normal functioning
of the diffusion solver.

\section*{\texttt{DIFFUSION_SOLVER} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional] Required when heat transport and/or species transport physics is enabled.
\item[Single/Multiple Instances] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}
\item \tvar{DIFFUSION_SOLVER}{Abs_Conc_Tol}
\item \tvar{DIFFUSION_SOLVER}{Abs_Enthalpy_Tol}
\item \tvar{DIFFUSION_SOLVER}{Abs_Temp_Tol}
\item \tvar{DIFFUSION_SOLVER}{Cond_Vfrac_Threshold}
\item \tvar{DIFFUSION_SOLVER}{Hypre_AMG_Debug}
\item \tvar{DIFFUSION_SOLVER}{Hypre_AMG_Logging_Level}
\item \tvar{DIFFUSION_SOLVER}{Hypre_AMG_Print_Level}
\item \tvar{DIFFUSION_SOLVER}{Max_NLK_Itr}
\item \tvar{DIFFUSION_SOLVER}{Max_NLK_Vec}
\item \tvar{DIFFUSION_SOLVER}{Max_Step_Tries}
\item \tvar{DIFFUSION_SOLVER}{NLK_Preconditioner}
\item \tvar{DIFFUSION_SOLVER}{NLK_Tol}
\item \tvar{DIFFUSION_SOLVER}{NLK_Vec_Tol}
\item \tvar{DIFFUSION_SOLVER}{PC_AMG_Cycles}
\item \tvar{DIFFUSION_SOLVER}{PC_Freq}
\item \tvar{DIFFUSION_SOLVER}{PC_SSOR_Relax}
\item \tvar{DIFFUSION_SOLVER}{PC_SSOR_Sweeps}
\item \tvar{DIFFUSION_SOLVER}{Rel_Conc_Tol}
\item \tvar{DIFFUSION_SOLVER}{Rel_Enthalpy_Tol}
\item \tvar{DIFFUSION_SOLVER}{Rel_Temp_Tol}
\item \tvar{DIFFUSION_SOLVER}{Residual_Atol}
\item \tvar{DIFFUSION_SOLVER}{Residual_Rtol}
\item \tvar{DIFFUSION_SOLVER}{Stepping_Method}
\item \tvar{DIFFUSION_SOLVER}{Verbose_Stepping}
\item \tvar{DIFFUSION_SOLVER}{Void_Temperature}
\end{itemize}


\tvardef{DIFFUSION_SOLVER}{Abs_Conc_Tol}
\section*{\texttt{Abs_Conc_Tol}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\epsilon$ for the absolute error component
  of the concentration error norm used by the BDF2 integrator.  If
  $\delta\mathbf{c}$ is a concentration field increment with reference
  concentration field $\mathbf{c}$, then this error norm is
  \[ |||\delta\mathbf{c}|||\equiv\max_j\,|\delta c_j|/(\epsilon+\eta |c_j|). \]
  The relative error tolerance $\eta$ is given by
  \tvar{DIFFUSION_SOLVER}{Rel_Conc_Tol}.
  This variable is only relevant to the adaptive integrator and to diffusion
  systems that include concentration as a dependent variable.
\item[Physical dimension:] same as the `concentration' variable
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $\ge0$
\item[Notes:]  The error norm is dimensionless and normalized.  The BDF2
  integrator will accept time steps where the estimated truncation error
  is less than $2$, and chooses the next suggested time step so that its
  prediction of the next truncation error is $\tfrac12$.\par
  For $c_j$ sufficiently small the norm approximates an absolute norm with
  tolerance $\epsilon$, and for $c_j$ sufficiently large the norm approximates
  a relative norm with tolerance $\eta$.  If $\epsilon=0$ then the norm is a
  pure relative norm and the concentration must be bounded away from $0$. \par
  The same tolerance is used for all concentration components.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Abs_Enthalpy_Tol}
\section*{\texttt{Abs_Enthalpy_Tol}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\epsilon$ for the absolute error component
  of the enthalpy error norm used by the BDF2 integrator.  If
  $\delta\mathbf{H}$ is a enthalpy field increment with reference
  enthalpy field $\mathbf{H}$, then this error norm is
  \[ |||\delta\mathbf{H}|||\equiv\max_j\,|\delta H_j|/(\epsilon+\eta |H_j|). \]
  The relative error tolerance $\eta$ is given by
  \tvar{DIFFUSION_SOLVER}{Rel_Enthalpy_Tol}.
  This variable is only relevant to the adaptive integrator and to diffusion
  systems that include enthalpy as a dependent variable.
\item[Physical dimension:] \AUenergy\per(\AUtemperature\usk\AUvolume)
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $\ge0$
\item[Notes:]  The error norm is dimensionless and normalized.  The BDF2
  integrator will accept time steps where the estimated truncation error
  is less than $2$, and chooses the next suggested time step so that its
  prediction of the next truncation error is $\tfrac12$.\par
  For $H_j$ sufficiently small the norm approximates an absolute norm with
  tolerance $\epsilon$, and for $H_j$ sufficiently large the norm approximates
  a relative norm with tolerance $\eta$.  If $\epsilon=0$ then the norm is a
  pure relative norm and the enthalpy must be bounded away from $0$.
\end{description}

\tvardef{DIFFUSION_SOLVER}{Abs_Temp_Tol}
\section*{\texttt{Abs_Temp_Tol}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\epsilon$ for the absolute error component
  of the temperature error norm used by the BDF2 integrator.  If
  $\delta\mathbf{T}$ is a temperature field increment with reference
  temperature field $\mathbf{T}$, then this error norm is
  \[ |||\delta\mathbf{T}|||\equiv\max_j\,|\delta T_j|/(\epsilon+\eta |T_j|). \]
  The relative error tolerance $\eta$ is given by
  \tvar{DIFFUSION_SOLVER}{Rel_Temp_Tol}.
  This variable is only relevant to the adaptive integrator and to diffusion
  systems that include temperature as a dependent variable.
\item[Physical dimension:] \AUtemperature
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $\ge0$
\item[Notes:]  The error norm is dimensionless and normalized.  The BDF2
  integrator will accept time steps where the estimated truncation error
  is less than $2$, and chooses the next suggested time step so that its
  prediction of the next truncation error is $\tfrac12$.\par
  For $c_j$ sufficiently small the norm approximates an absolute norm with
  tolerance $\epsilon$, and for $c_j$ sufficiently large the norm approximates
  a relative norm with tolerance $\eta$.  If $\epsilon=0$ then the norm is a
  pure relative norm and the temperature must be bounded away from $0$.
\end{description}

\tvardef{DIFFUSION_SOLVER}{Cond_Vfrac_Threshold}
\section*{\texttt{Cond_Vfrac_Threshold}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  Material volume fraction threshold for inclusion in heat conduction
  when using the non-adaptive integrator.
\item[Type:] \real
\item[Default:] $0.001$
\item[Valid values:] $(0,1)$
\item[Note:]
  Fluid flow systems that include void will result in partially filled
  cells, often times with only a tiny fragment of material.  Including
  such cells in the heat conduction problem can cause severe numerical
  difficulties.  By excluding cells with a material volume fraction
  less than this threshold from participation in heat conduction we can
  obtain a much better conditioned system.  Note that we continue to
  track enthalpy for such cells, including enthalpy that may be advected
  into or out of the cell; we just do not consider diffusive transport
  of enthalpy.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Hypre_AMG_Debug}
\section*{\texttt{Hypre_AMG_Debug}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Enable debugging output from Hypre's BoomerAMG
  solver.  Only relevant when \\ \tvar{DIFFUSION_SOLVER}{NLK_Preconditioner} is
  set to \texttt{'Hypre_AMG'}.
\item[Type:] \logical
\item[Default:] \false\ (off)
\item[Note:] See \texttt{HYPRE_BoomerAMGSetDebugFlag} in
  the Hypre Reference Manual.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Hypre_AMG_Logging_Level}
\section*{\texttt{Hypre_AMG_Logging_Level}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Enable additional diagnostic computation by Hypre's BoomerAMG
  solver.  Only relevant when \tvar{DIFFUSION_SOLVER}{NLK_Preconditioner} is
  set to \texttt{'Hypre_AMG'}.
\item[Type:] \integer
\item[Default:] 0 (none)
\item[Valid values:] 0, none; ${}>0$, varying amounts.  Refer to the Hypre
  Reference Manual description of \texttt{HYPRE_BoomerAMGSetLogging} for details.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Hypre_AMG_Print_Level}
\section*{\texttt{Hypre_AMG_Print_Level}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The diagnostic output verbosity level of Hypre's BoomerAMG
  solver.  Only relevant when \tvar{DIFFUSION_SOLVER}{NLK_Preconditioner} is
  set to \texttt{'Hypre_AMG'}.
\item[Type:] \integer
\item[Default:] 0
\item[Valid values:]\qquad
  \begin{tabular}[t]{|c|l|}\hline
  0& no output\\
  1& write setup information\\
  2& write solve information\\
  3& write both setup and solve information \\ \hline
  \end{tabular}
  
  See \texttt{HYPRE_BoomerAMGSetPrintLevel} in the Hypre Reference Manual.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Max_NLK_Itr}
\section*{\texttt{Max_NLK_Itr}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  The maximum number of NLK nonlinear solver iterations allowed.
\item[Type:] \integer
\item[Default:] 5
\item[Valid values:] $\ge2$
\item[Notes:] This variable is used by both the adaptive and non-adaptive
  integrators, though the appropriate values differ significantly.
  
  For the adaptive integrator, the failure of a nonlinear iteration to converge
  \emph{is not} necessarily fatal; the BDF2 integration procedure expects that
  this will occur, using it as an indication that the preconditioner for the
  nonlinear system needs to be updated.  If still unsuccessful, the step may
  be retried with a halved time step size, perhaps repeatedly.  Therefore it
  is important that the maximum number of iterations not be set too high, as
  this merely delays the recognition that some recovery strategy needs to be
  taken, and can result in much wasted effort.
  
  By contrast, a nonlinear solver convergence failure \emph{is} fatal for the
  non-adaptive solver.  Thus the maximum number of iterations should be set to
  some suitably large value; if the number of iterations ever exceeds this value
  the simulation is terminated.
  
  The default value is appropriate for the adaptive integrator.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Max_NLK_Vec}
\section*{\texttt{Max_NLK_Vec}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  The maximum number of acceleration vectors to use in the NLK nonlinear solver.
\item[Type:] \integer
\item[Default:] $\text{\texttt{Max_NLK_Itr}} - 1$
\item[Valid values:] $>0$
\item[Notes:]  The acceleration vectors are derived from the difference
  of successive nonlinear function iterates accumulated over the course of
  a nonlinear solve.  Thus the maximum possible number of acceleration vectors
  available is one less than the maximum number of NLK iterations, and so
  specifying a larger number merely wastes memory.  If a large number of
  NLK iterations is allowed (as when using the non-adaptive integrator) then it may
  be appropriate to use a smaller value for this parameter, otherwise the
  default value is fine.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Max_Step_Tries}
\section*{\texttt{Max_Step_Tries}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The maximum number of attempts to successfully take a
  time step before giving up.  The step size is reduced between each try.
  This is only relevant to the adaptive solver.
\item[Type:] \integer
\item[Default:] 10
\item[Valid values:] $\ge1$
\item[Notes:] If other physics is enabled then
  this variable is effectively assigned the value 1, overriding the input
  value.  This is required for compatibility with the other physics solvers
  which currently have no way of recovering from a failed step.
\end{description}


\tvardef{DIFFUSION_SOLVER}{NLK_Preconditioner}
\section*{\texttt{NLK_Preconditioner}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The choice of preconditioner for the NLK iteration.
  There are currently two preconditioners to choose from: SSOR and 
  HYPRE_AMG. The former is symmetric over relaxation, and the later is
  an algebraic multigrid preconditioner from the \emph{hypre} library.
\item[Type:] \texttt{string}
\item[Default:] \texttt{'SSOR'}
\item[Valid values:] \texttt{'SSOR'} or \texttt{'Hypre_AMG'}
\item[Notes:] If SSOR is the chosen as the preconditioner, the user
  can set  \tvar{DIFFUSION_SOLVER}{PC_SSOR_Relax} to the over
  relaxation parameter and \tvar{DIFFUSION_SOLVER}{PC_SSOR_Sweeps}
  to the number of SSOR sweeps.
  If Hypre_AMG is the chosen as the preconditioner, the user can set
  \tvar{DIFFUSION_SOLVER}{PC_AMG_Cycles} to the number of AMG cycles
  per preconditioning step.
\end{description}


\tvardef{DIFFUSION_SOLVER}{NLK_Tol}
\section*{\texttt{NLK_Tol}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The convergence tolerance for the NLK nonlinear solver.
  The nonlinear system is considered solved by the current iterate if the
  BDF2 integrator norm of the last solution correction is less than this value.
  This variable is only relevant to the adaptive integrator.
\item[Type:] \real
\item[Default:] 0.1
\item[Valid values:] $(0,1)$
\item[Notes:]  This tolerance is relative to the dimensionless and normalized
  BDF2 integrator norm; see \tvar{DIFFUSION_SOLVER}{Abs_Conc_Tol}, for
  example.  The nonlinear system only needs to be solved to an accuracy equal
  to the acceptable local truncation error for the step, which is roughly 1.
  Solving to a greater accuracy is wasted effort.  Using a tolerance in the
  range $(0.01, 0.1)$ is generally adequate to ensure a sufficently converged
  nonlinear iterate.
\end{description}


\tvardef{DIFFUSION_SOLVER}{NLK_Vec_Tol}
\section*{\texttt{NLK_Vec_Tol}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The NLK vector drop tolerance.  When
  assembling the acceleration subspace vector by vector, a vector is
  dropped when the sine of the angle between the vector and the subspace
  less than this value.
\item[Type:] \real
\item[Default:] 0.001
\item[Valid values:] $>0$
\end{description}


\tvardef{DIFFUSION_SOLVER}{PC_AMG_Cycles}
\section*{\texttt{PC_AMG_Cycles}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The number of V-cycles to take per preconditioning step
  of the nonlinear iteration.
\item[Physical Dimension:] dimensionless
\item[Type:] \integer
\item[Default:] 2
\item[Valid values:] $\ge1$ 
\item[Notes:] We use standard $V(1,1)$ cycles. Parameters other than 
  the number of V cycles cannot be controlled by the user.
\end{description}


\tvardef{DIFFUSION_SOLVER}{PC_Freq}
\section*{\texttt{PC_Freq}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  This controls how frequently the preconditioner is updated in the adaptive
  BDF2 integrator. A value of $N$ will allow a preconditioner to be used for
  as many as $N$ consecutive time steps before being updated, although it may
  be updated more frequently based on other criteria. A value of 1 causes
  the preconditioner to be updated every time step. The default behavior is to
  not require any minimum update frequency.
\item[Type:] \integer
\item[Default:] $\infty$
\item[Valid values:] $\ge1$ 
\item[Notes:]
  A basic strategy of the adaptive BDF2 integrator is to use a preconditioner
  for as many time steps as possible, and only update it when a nonlinear
  time step iteration fails to converge. This generally works quite well.
  But if you find that the integrator is thrashing --- evidenced by the number
  of times a step failed with an old preconditioner and was retried (this is
  the \texttt{NNR} diagnostic value in the terminal output) being a significant
  fraction of the number of time steps --- it may be more cost effective to set
  this value to 1, for example.
\end{description}


\tvardef{DIFFUSION_SOLVER}{PC_SSOR_Relax}
\section*{\texttt{PC_SSOR_Relax}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The relaxation parameter used in the SSOR preconditioning
  of the nonlinear system.
\item[Physical Dimension:] dimensionless
\item[Type:] \real
\item[Default:] 1.4
\item[Valid values:] $(0,2)$
\item[Notes:]  A value less than 1 gives under-relaxation and a value greater
  than 1 over-relaxation.
\end{description}


\tvardef{DIFFUSION_SOLVER}{PC_SSOR_Sweeps}
\section*{\texttt{PC_SSOR_Sweeps}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The number of sweeps used in the SSOR preconditioning
  of the nonlinear system.
\item[Type:] \integer
\item[Default:] 4
\item[Valid values:] $\ge1$
\item[Notes:]  The effectiveness of the SSOR preconditioner (measured by
  the convergence rate of the nonlinear iteration) improves as the number
  of sweeps increases, though at increasing cost.   For especially large
  systems where the effectiveness of SSOR deteriorates, a somewhat larger
  value than the default 4 sweeps may be required.  Using fewer than 4
  sweeps is generally not recommended.
\end{description}


\section*{\texttt{Rel_Conc_Tol}}
\tvardef{DIFFUSION_SOLVER}{Rel_Conc_Tol}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\eta$ for the relative error component
  of the concentration error norm used by the BDF2 integrator.  If
  $\delta\mathbf{c}$ is a concentration field increment with reference
  concentration field $\mathbf{c}$, then this error norm is
  \[ |||\delta\mathbf{c}|||\equiv\max_j\,|\delta c_j|/(\epsilon+\eta |c_j|). \]
  The absolute error tolerance $\epsilon$ is given by
  \tvar{DIFFUSION_SOLVER}{Abs_Conc_Tol}.
  This variable is only relevant to the adaptive solver and to diffusion
  systems that include concentration as a dependent variable.
\item[Physical Dimension:] dimensionless
\item[Type:] \real
\item[Default:] 0.0
\item[Valid values:] $[0,1)$
\item[Notes:] See the notes for \tvar{DIFFUSION_SOLVER}{Abs_Conc_Tol}.
\end{description}


\section*{\texttt{Rel_Enthalpy_Tol}}
\tvardef{DIFFUSION_SOLVER}{Rel_Enthalpy_Tol}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\eta$ for the relative error component
  of the enthalpy error norm used by the BDF2 integrator.  If
  $\delta\mathbf{c}$ is a enthalpy field increment with reference
  enthalpy field $\mathbf{H}$, then this error norm is
  \[ |||\delta\mathbf{H}|||\equiv\max_j\,|\delta H_j|/(\epsilon+\eta |H_j|). \]
  The absolute error tolerance $\epsilon$ is given by
  \tvar{DIFFUSION_SOLVER}{Abs_Enthalpy_Tol}.
  This variable is only relevant to the adaptive solver and to diffusion
  systems that include enthalpy as a dependent variable.
\item[Physical Dimension:] dimensionless
\item[Type:] \real
\item[Default:] 0.0
\item[Valid values:] $[0,1)$
\item[Notes:] See the notes for \tvar{DIFFUSION_SOLVER}{Abs_Enthalpy_Tol}.
\end{description}


\section*{\texttt{Rel_Temp_Tol}}
\tvardef{DIFFUSION_SOLVER}{Rel_Temp_Tol}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The tolerance $\eta$ for the relative error component
  of the temperature error norm used by the BDF2 integrator.  If
  $\delta\mathbf{T}$ is a temperature field increment with reference
  temperature field $\mathbf{T}$, then this error norm is
  \[ |||\delta\mathbf{T}|||\equiv\max_j\,|\delta T_j|/(\epsilon+\eta |T_j|). \]
  The absolute error tolerance $\epsilon$ is given by
  \tvar{DIFFUSION_SOLVER}{Abs_Temp_Tol}.
  This variable is only relevant to the adaptive solver and to diffusion
  systems that include temperature as a dependent variable.
\item[Physical Dimension:] dimensionless
\item[Type:] \real
\item[Default:] 0.0
\item[Valid values:] $[0,1)$
\item[Notes:] See the notes for \tvar{DIFFUSION_SOLVER}{Abs_Temp_Tol}.
\end{description}


\section*{\texttt{Residual_Atol}}
\tvardef{DIFFUSION_SOLVER}{Residual_Atol}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  The absolute residual tolerance $\epsilon_1$ used by the iterative nonlinear
  solver of the non-adaptive integrator. If $r_0$ denotes the initial nonlinear
  residual, iteration stops when the current residual $r$ satisfies
  $||r||_2 \le \max\{\epsilon_1, \epsilon_2||r_0||_2\}$.
\item[Type:] \real
\item[Default:] 0
\item[Valid values:] $\ge0$
\item[Note:]
  Ideally this tolerance should be set to 0, but in some circumstances,
  especially at the start of a simulation, the initial residual may be so
  small that it is impossible to reduce it by the factor $\epsilon_2$ due
  to finite precision arithmetic.  In such cases it is necessary to provide
  this absolute tolerance.  It is impossible, however, to say what a suitable
  value would be, as this depends on the nature of the particular nonlinear
  system.  Some guidance can be obtained through trial-and-error by enabling
  \tvar{DIFFUSION_SOLVER}{Verbose_Stepping} and observing the magnitude
  of the residual norms in the resulting diagnostic output file.
\end{description}


\section*{\texttt{Residual_Rtol}}
\tvardef{DIFFUSION_SOLVER}{Residual_Rtol}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  The relative residual tolerance $\epsilon_2$ used by the iterative nonlinear
  solver of the non-adaptive integrator. If $r_0$ denotes the initial nonlinear
  residual, iteration stops when the current residual $r$ satisfies
  $||r||_2 < \max\{\epsilon_1, \epsilon_2||r_0||_2\}$.
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $(0,1)$
\end{description}


\tvardef{DIFFUSION_SOLVER}{Stepping_Method}
\section*{\texttt{Stepping_Method}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The choice of time integration method.
\item[Type:] string
\item[Default:] \texttt{'Adaptive BDF2'}
\item[Valid values:] \texttt{'Adaptive BDF2'} or \texttt{'Non-adaptive BDF1'}
\item[Note:]
  The non-adaptive integrator must be selected when fluid flow is enabled
  and void material is present.  Otherwise use the default adaptive integrator.
\end{description}


\tvardef{DIFFUSION_SOLVER}{Verbose_Stepping}
\section*{\texttt{Verbose_Stepping}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] A flag that enables the output of detailed BDF2 time
  stepping information.  The human-readable information is written to a file
  with the suffix \texttt{.bdf2.out} in the output directory.
\item[Type:] \logical
\item[Default:] \false
\end{description}


\tvardef{DIFFUSION_SOLVER}{Void_Temperature}
\section*{\texttt{Void_Temperature}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] An arbitrary temperature assigned to cells that contain
  only the void material. The value has no effect on the simulation, and is
  only significant to visualization.
\item[Type:] \real
\item[Default:] $0$
\end{description}
