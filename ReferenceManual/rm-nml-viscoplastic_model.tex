\chapter{\texttt{VISCOPLASTIC_MODEL} Namelist}
\tnamelistdef{VISCOPLASTIC_MODEL}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}
The \texttt{VISCOPLASTIC_MODEL} namelist defines the viscoplastic model
to be used for a particular solid material phase in material stress-strain
calculations.
Two viscoplastic models are available: a mechanical threshold stress (MTS)
model and a power law model.  When no model is given for a solid material
phase, it is modeled as a purely elastic material.  The models specify a
relation for the effective plastic strain rate $\dot\epsilon$ as a function
of temperature $T$ and von Mises stress $\sigma$.  For more details on the
models see the \PA\ manual.  Briefly:

\textbf{Power Law Model}.\enspace
In the simple power law model, the strain rate relation is
\begin{equation}\label{eq:pwr}
  \dot\epsilon = A\,\exp(-Q/RT)\,\sigma^n,
\end{equation}
where $A$, $n$, $Q$, and $R$ are parameters given by this namelist.

\textbf{MTS Model}.\enspace
The MTS model uses the strain rate relation
\begin{equation}\label{eq:mts}
  \dot\epsilon = \dot\epsilon_{0i}\,\exp\left[-\frac{\mu b^3 g_{0i}}{kT}
  \biggl(1-\Bigl(\frac{\mu_0}{\mu\,\sigma_i}(\sigma-\sigma_a)\Bigr)^{p_i}
      \biggr)^{q_i}\right],\qquad
  \mu = \mu_0 - \frac{D}{\exp(T_0/T) - 1}
\end{equation}
where $\dot\epsilon_{0i}$, $g_{0i}$, $b$, $k$, $D$, $\mu_0$, $T_0$,
$\sigma_i$, $\sigma_a$, $p_i$, and $q_i$ are parameters given by this namelist.
When $\sigma < \sigma_a$ we instead use $\dot\epsilon = K\,\sigma^5$,
where $K$ is chosen to give continuity with the previous relation at
$\sigma=\sigma_a$.  And when $\sigma-\sigma_a>\mu\,\sigma_i/\mu_0$
we take $\dot\epsilon=\dot\epsilon_{0i}$.

\section*{\texttt{SURFACE_TENSION} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:]
  Optional; only relevant when \tvar{PHYSICS}{Solid_Mechanics} is true.
\item[Single/Multiple Instances:]
  Multiple; at most one per solid material phase.
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{VISCOPLASTIC_MODEL}{Phase}
\item \tvar{VISCOPLASTIC_MODEL}{Model}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_b}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_d}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_edot_0i}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_g_0i}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_k}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_mu_0}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_p_i}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_q_i}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_sig_a}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_sig_i}
\item \tvar{VISCOPLASTIC_MODEL}{MTS_temp_0}
\item \tvar{VISCOPLASTIC_MODEL}{Pwr_Law_A}
\item \tvar{VISCOPLASTIC_MODEL}{Pwr_Law_N}
\item \tvar{VISCOPLASTIC_MODEL}{Pwr_Law_Q}
\item \tvar{VISCOPLASTIC_MODEL}{Pwr_Law_R}
\end{itemize}


\tvardef{VISCOPLASTIC_MODEL}{Phase}
\section*{\texttt{Phase}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of the material \tnamelist{PHASE} to which this viscoplastic model
  applies.
\item[Type:] case-sensitive string
\item[Default:] none
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{Model}
\section*{\texttt{Model}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The type of viscoplastic strain rate model.
\item[Type:] case-insensitive string
\item[Default:] none
\item[Valid values:] \texttt{"MTS"}, \texttt{"power law"}, \texttt{"elastic"}
\item[Notes:]
  The effect of the \texttt{"elastic"} option is equivalent to not specifying
  a viscoplastic model at all; it is provided as a convenience.
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_b}
\section*{\texttt{MTS_b}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Burger's vector length $b$ in (\ref{eq:mts}).
\item[Physical dimension:] \AUlength
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_d}
\section*{\texttt{MTS_d}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Constant $D$ used in (\ref{eq:mts}).
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] none
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_edot_0i}
\section*{\texttt{MTS_edot_0i}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Reference strain rate $\dot{\epsilon}_{0i}$ used
  in (\ref{eq:mts}).
\item[Physical dimension:] \reciprocal\AUtime
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_g_0i}
\section*{\texttt{MTS_g_0i}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Material constant $g_{0i}$ used in (\ref{eq:mts}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_k}
\section*{\texttt{MTS_k}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Boltzmann's constant $k$ used in (\ref{eq:mts}).
\item[Physical dimension:] \AUenergy\per\AUtemperature
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\item[Note:]
  Temperature should be expressed in Kelvin, or other temperature scale
  where 0 corresponds to absolute zero.  If SI units are being used, $k$
  should be $1.38\times10^{-23}$.  Use a value appropriate to the units
  used in (\ref{eq:mts}).
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_mu_0}
\section*{\texttt{MTS_mu_0}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Reference value $\mu_0$ for the temperature
  dependent shear modulus used in (\ref{eq:mts}).
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_p_i}
\section*{\texttt{MTS_p_i}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Exponent term $p_i$ used in (\ref{eq:mts}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_q_i}
\section*{\texttt{MTS_q_i}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Exponent term $q_i$ used in (\ref{eq:mts}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_sig_a}
\section*{\texttt{MTS_sig_a}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The athermal stress term $\sigma_a$ in (\ref{eq:mts}).
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] none
\item[Valid values:] $\ge0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_sig_i}
\section*{\texttt{MTS_sig_i}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A stress term $\sigma_i$ related to obstacles to
  dislocation motion in (\ref{eq:mts}).
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{MTS_temp_0}
\section*{\texttt{MTS_temp_0}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Constant $T_0$ used in the temperature dependent
  shear modulus equation in (\ref{eq:mts}).
\item[Physical dimension:] \AUtemperature
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{Pwr_Law_A}
\section*{\texttt{Pwr_Law_A}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Constant term $A$ in (\ref{eq:pwr}).
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] none
\item[Valid values:] $\ge0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{Pwr_Law_N}
\section*{\texttt{Pwr_Law_N}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Stress exponent term $n$ in (\ref{eq:pwr}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{Pwr_Law_Q}
\section*{\texttt{Pwr_Law_Q}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Activation energy $Q$ in (\ref{eq:pwr}).
\item[Physical dimension:] \AUenergy\per\mole
\item[Type:] real
\item[Default:] none
\item[Valid values:] $\ge0$
\end{description}


\tvardef{VISCOPLASTIC_MODEL}{Pwr_Law_R}
\section*{\texttt{Pwr_Law_R}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Gas constant $R$ in (\ref{eq:pwr}).
\item[Physical dimension:] \AUenergy\per(\AUtemperature\usk\mole)
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\item[Note:]
  Temperature should be expressed in Kelvin, or other temperature scale
  where 0 corresponds to absolute zero.  Use the value for $R$ appropriate
  to the units used in (\ref{eq:pwr}).
\end{description}
