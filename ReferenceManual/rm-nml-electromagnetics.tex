\chapter{\texttt{ELECTROMAGNETICS} Namelist}
\tnamelistdef{ELECTROMAGNETICS}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{ELECTROMAGNETICS} namelist sets most of the parameters used
by the electromagnetic (EM) solver to calculate the Joule heat used in
induction heating simulations.  Exceptions are the electrical conductivity,
electric susceptibility, and magnetic susceptibility which are defined for
each material phase using the \tnamelist{MATERIAL} namelist, and the induction
coils hat produce an external magnetic source field, which are specified in
\tnamelist{INDUCTION_COIL} namelists.
The EM calculations are
performed on a tetrahedral mesh specified by the \tnamelist{ALTMESH} namelist,
which is generally different than the main mesh used throughout the rest of
Truchas.

{\bf A Remark on Units:} The EM solver assumes SI units by default.  In
particular, the result of the Joule heat calculation is a \emph{power
density}---W/m${}^3$ in SI units.  To use a different system of units,
the user must supply appropriate values for the free-space constants
\tvar{PHYSICAL_CONSTANTS}{Vacuum_Permittivity} and
\tvar{PHYSICAL_CONSTANTS}{Vacuum_Permeability}.
In any case, the user must ensure that a consistent set of units is used
throughout Truchas.

\section*{\texttt{ELECTROMAGNETICS} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}
\item \tvar{ELECTROMAGNETICS}{CG_Stopping_Tolerance}
\item \tvar{ELECTROMAGNETICS}{EM_Domain_Type}
\item \tvar{ELECTROMAGNETICS}{Graphics_Output}
\item \tvar{ELECTROMAGNETICS}{Material_Change_Threshold}
\item \tvar{ELECTROMAGNETICS}{Maximum_CG_Iterations}
\item \tvar{ELECTROMAGNETICS}{Maximum_Source_Cycles}
\item \tvar{ELECTROMAGNETICS}{Num_Etasq}
\item \tvar{ELECTROMAGNETICS}{Output_Level}
\item \tvar{ELECTROMAGNETICS}{Source_Frequency}
\item \tvar{ELECTROMAGNETICS}{Source_Times}
\item \tvar{ELECTROMAGNETICS}{SS_Stopping_Tolerance}
\item \tvar{ELECTROMAGNETICS}{Steps_Per_Cycle}
\item \tvar{ELECTROMAGNETICS}{Symmetry_Axis}
\item \tvar{ELECTROMAGNETICS}{Uniform_Source}
\end{itemize}


\section*{\texttt{CG_Stopping_Tolerance}}
\tvardef{ELECTROMAGNETICS}{CG_Stopping_Tolerance}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Tolerance used to determine when the conjugate gradient
  (CG) iteration has converged.  The criterion used is that
  $\|\mathbf{r}\|/\|\mathbf{r}_0\|<\texttt{CG_Stopping_Tolerance}$.
  The electromagnetics solver uses its own special preconditioned CG linear
  solver.
\item[Type:] \real
\item[Default:] $10^{-5}$
\item[Valid values:] $(0, 0.1)$
\item[Notes:] The numerical characteristics of the electromagnetic system
  require that the linear systems be solved to significantly greater
  accuracy than would otherwise be required.  Too loose a tolerance will
  manifest itself in a significant build-up of noise in the solution of
  the electric field over the course of the simulation.  This input variable
  should not be greater than $10^{-4}$.
\end{description}


\section*{\texttt{EM_Domain_Type}}
\tvardef{ELECTROMAGNETICS}{EM_Domain_Type}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A flag specifying the type of domain geometry that is
  discretized by the computational mesh.
\item[Type:] string
\item[Default:] none
\item[Valid values:] \texttt{'Full_Cylinder'}, \texttt{'Half_Cylinder'},
  \texttt{'Quarter_Cylinder'}
\item[Notes:]
  At this time there is not yet a facility for specifying general boundary
  conditions for the electromagnetic simulation.  Consequently, the
  computational domain $\Omega$ is limited to the following special cases
  when \tvar{ELECTROMAGNETICS}{Symmetry_Axis}=\texttt{'z'}:
  \[\begin{array}{ll}
    \mbox{\texttt{'Full_Cylinder'}:} &
      \Omega = \{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;z_1 \le z \le z_2 \} \\
    \mbox{\texttt{'Half_Cylinder'}:} &
      \Omega = \{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;x \ge 0,  \;z_1 \le z \le z_2 \} \\
    \mbox{\texttt{'Quarter_Cylinder'}:} &
      \Omega = \{(x,y,z) \;|\; x^2 + y^2 \le r^2, \;x,y \ge 0, \;z_1 \le z \le z_2 \} \\
  \end{array}\]
  The values $r>0$, $z_1<z_2$ are inferred from the mesh and are not specified
  directly.  Dirichlet source field conditions are imposed on the boundaries
  $\{ x^2 + y^2 = r^2 \}$ and $\{ z=z_1,z_2 \}$, and symmetry conditions on
  the symmetry planes $\{ x=0 \}$ and $\{ y=0 \}$ if present.  See the User
  Manual for more details.
  
  The analogous definitions for the other possible symmetry axes, \texttt{'x'}
  and \texttt{'y'}, are obtained by the appropriate cyclic permutation of the
  coordinates.
  
  For the computational mesh used in the EM simulation, see the
  \tnamelist{ALTMESH} namelist.
  
  \textbf{Experimental Features}.  The value \texttt{'Frustum'} specifies that
  the domain is a frustum of a right cone
  \[ \Omega = \{(x,y,z) \;|\; x^2 + y^2 \le m^2 (z-z_0)^2, \;z_1 \le z \le z_2 \}\]
  or an angular wedge of a frustum.  As with the other domain types the values
  $m>0$, $z_0$ and $z_1<z_2$ are inferred from the mesh and are not specified
  directly.  For wedges of a frustum, the wedge sides can lie on any plane from
  the family of 30 degree increment planes that includes the $x=0$ plane.  The
  preceding description is for the $z$-axis symmetry case, but the analogous
  functionality for the other symmetry axes is also provided.
\end{description}


\section*{\texttt{Graphics_Output}}
\tvardef{ELECTROMAGNETICS}{Graphics_Output}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Controls the graphics output of the electromagnetic solver.
\item[Type:] \logical
\item[Default:] \false
\item[Valid values:] \true or \false
\item[Notes:]
  If \true, the electromagnetic solver will generate its own graphics
  data files in OpenDX format (see \texttt{http://www.opendx.org}).  The files
  contain the material parameter fields, the averaged Joule heat field, and the
  time series of the electromagnetic fields.  The files are identified by the
  suffixes \texttt{-EM.dx} and \texttt{-EM.bin}.  The value of
  \texttt{Graphics_Output} has no impact on the normal graphics output
  generated by Truchas, which is determined elsewhere, and the averaged Joule
  heat field used in heat transport will be output there in either case.
\end{description}


\section*{\texttt{Material_Change_Threshold}}
\tvardef{ELECTROMAGNETICS}{Material_Change_Threshold}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:] Controls, at each step, whether the Joule heat is
  recalculated in response to temperature-induced changes in the EM material
  parameter values.  The Joule heat is recalculated whenever the difference
  between the current parameter values and those when the Joule heat was last
  computed exceeds this threshold value.  Otherwise the previously calculated
  Joule heat is used.  The maximum relative change is used as the difference
  measure.
\item[Type:] \real
\item[Default:] 0.3
\item[Valid values:] $(0.0, \infty)$
\item[Notes:]
  The electric conductivity and magnetic permeability are the only values
  whose changes are monitored.  The electric permittivity only enters the
  equations through the displacement current term, which is exceedingly
  small in this quasi-magnetostatic regime and could be dropped entirely.
  Thus the Joule heat is essentially independent of the permittivity and
  so any changes in its value are ignored.

  For electric conductivity, only the conducting region (where the value
  is positive) is considered when computing the difference.  An underlying
  assumption is that this region remains fixed throughout the simulation.
\end{description}


\section*{\texttt{Maximum_CG_Iterations}}
\tvardef{ELECTROMAGNETICS}{Maximum_CG_Iterations}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Maximum number of conjugate gradient (CG) iterations
  allowed.  The electromagnetics solver uses its own special preconditioned
  CG linear solver.
\item[Type:] \integer
\item[Default:] 500
\item[Valid values:] $(0.0, \infty)$
\end{description}


\section*{\texttt{Maximum_Source_Cycles}}
\tvardef{ELECTROMAGNETICS}{Maximum_Source_Cycles}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The electromagnetic field equations are integrated in
  time toward a periodic steady state.  This input variable specifies the
  time limit, measured in cycles of the sinusoidal source field, for the
  Joule heat calculation.
\item[Type:] \integer
\item[Default:] 10
\item[Valid values:] $(0.0, \infty)$
\item[Notes:]
  To avoid ringing, the amplitude of the external source field is ramped and
  is not at full strength until after approximately two cycles have passed.
  Consequently this input variable should not normally be $<3$.
  Convergence to a periodic steady state is usually attained within 5 cycles;
  see \tvar{ELECTROMAGNETICS}{SS_Stopping_Tolerance}.
  If convergence is not attained within the limit allowed by this input
  variable, the last result is returned and a warning issued, but execution
  continues with the rest of the physics simulation.
  
  The electromagnetic field equations are solved on an \emph{inner} time
  distinct from that of the rest of the physics; see
  \tvar{ELECTROMAGNETICS}{SS_Stopping_Tolerance}.
\end{description}


\section*{\texttt{Num_Etasq}}
\tvardef{ELECTROMAGNETICS}{Num_Etasq}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] This value is used for the displacement current
  coefficient $\eta^2$, in the low-frequency, nondimensional scaling of
  Maxwell's equations, when its value exceeds the physical value.
\item[Physical dimension:] dimensionless
\item[Type:] \real
\item[Default:] 0
\item[Valid values:] $(0.0, \infty)$
\item[Notes:]
  The quasi-magnetostatic regime is characterized by $\eta^2\ll 1$.  Since
  this value can become exceedingly small (resulting in a difficult-to-solve,
  ill-conditioned system), it may be helpful to use a numerical value instead,
  say $\eta^2 = 10^{-6}$ or $10^{-8}$, without having any discernable
  effect on the solution.  However, it is generally safe to ignore this
  variable and let the solver use the physical value.  See the \PA\ 
  for more details.
\end{description}


\section*{\texttt{Output_Level}}
\tvardef{ELECTROMAGNETICS}{Output_Level}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] Controls the verbosity of the electromagnetic solver
\item[Type:] \integer
\item[Default:] 1
\item[Valid values:] 1, 2, 3 or 4
\item[Notes:]
  At the default level, 1, a status message is output at the end of each
  source field cycle showing the progress toward steady state.  Level 2
  adds a summary of the CG iteration for each time step.  Level 3 adds
  the norm of the difference between the solution and extrapolated predictor
  for each time step.  This gives an indication of the (time) truncation error,
  and if noise is accumulating in the system it will be seen here; see
  \tvar{ELECTROMAGNETICS}{CG_Stopping_Tolerance}. Level 4 adds convergence
  info for each CG iterate.  Levels 1 and 2 are typical.
\end{description}


\section*{\texttt{Source_Frequency}}
\tvardef{ELECTROMAGNETICS}{Source_Frequency}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Frequency $f$ (cycles per unit time) of the
  sinusoidally-varying magnetic source fields that drive the Joule heat
  calculation.
\item[Physical dimension:] \reciprocal\AUtime
\item[Type:] \real
\item[Default:] none
\item[Valid values:] Any single positive value, or any sequence of positive
  values.
\item[Notes:]  A sequence of up to 32 values may be assigned to this variable
  in order to specify a time-dependent frequency; see
  \tvar{ELECTROMAGNETICS}{Source_Times} and Fig.~\ref{EM-fig1}.
  
  The source fields are due to induction coils specified through
  \tnamelist{INDUCTION_COIL} namelists, and a spatially uniform source
  field specified by \tvar{ELECTROMAGNETICS}{Uniform_Source}.  All
  operate at the common frequency specified by this variable, and a common
  phase.  The phase value is irrelevant due to the time averaging of the
  calculated Joule heat.
  
  The Joule heat calculation is coupled to the rest of the physics in a manner
  that assumes the time scale of the electromagnetic fields, $f^{-1}$ is
  \emph{much smaller} than the time scale of the other physics (as defined by
  the characteristic time step).  Consequently, the frequency $f$ must not be
  too small.
\end{description}


\section*{\texttt{Source_Times}}
\tvardef{ELECTROMAGNETICS}{Source_Times}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A sequence of times that define the time partition of
  the piecewise-constant functional form used in the case of time-dependent
  source field parameters.
\item[Physical dimension:] \AUtime
\item[Type:] \real
\item[Default:] none
\item[Valid values:] Any strictly increasing sequence of values.
\item[Notes:] If this variable is not specified, then the source field
  parameters are assumed to be constant in time, with a \emph{single} value
  assigned to \tvar{ELECTROMAGNETICS}{Source_Frequency},
  \tvar{ELECTROMAGNETICS}{Uniform_Source}, and each
  \tvar{INDUCTION_COIL}{Current} variable in any \tnamelist{INDUCTION_COIL}
  namelists.  Otherwise, if $n$ values are assigned to \texttt{Source_Times},
  then $n+1$ values must be assigned to each of those variables.  See
  Fig.~\ref{EM-fig1} for a description of the functional form described by
  these values.
  
  At most $31$ values may be specified for this variable.
\end{description}


\section*{\texttt{SS_Stopping_Tolerance}}
\tvardef{ELECTROMAGNETICS}{SS_Stopping_Tolerance}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]  The electromagnetic field equations are integrated in time
  toward a periodic steady state.  Convergence to this steady state is measured
  by comparing the computed Joule heat field averaged over the last source
  field cycle, $q_{\mbox{last}}$, with the result from the previous cycle,
  $q_{\mbox{prev}}$.  When 
  $\|q_{\mbox{last}} - q_{\mbox{prev}}\|_{\mbox{max}} / \|q_{\mbox{last}}\|_{\mbox{max}} < $
  \texttt{SS_Stopping_Tolerance},
  the Joule heat calculation is considered converged and $q_{\mbox{last}}$ returned.
\item[Type:] \real
\item[Default:] $10^{-2}$
\item[Valid values:] $(0.0, \infty)$
\item[Notes:] Depending on the accuracy of the other physics, $10^{-2}$ or
  $10^{-3}$ are adequate values.  If the value is taken too small (approaching
  machine epsilon) convergence cannot be attained.
  
  It is assumed that the time scale of the electromagnetic fields is
  \emph{much shorter} than the time scale of the other physics; see
  \tvar{ELECTROMAGNETICS}{Source_Frequency}.
  In this case it suffices to solve the electromagnetic field equations to a
  periodic steady state, while temporarily freezing the other physics, and
  averaging the rapid temporal variation in the Joule heat field over a cycle.
  In effect, the electromagnetic field equations are solved over an
  \emph{inner} time distinct from that of the rest of the physics.
\end{description}


\section*{\texttt{Steps_Per_Cycle}}
\tvardef{ELECTROMAGNETICS}{Steps_Per_Cycle}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:] The number of time steps per cycle of the external source
  field used to integrate the electromagnetic field equations
\item[Type:] \integer
\item[Default:] 20
\item[Valid values:] $(0.0, \infty)$
\item[Notes:]
  Increasing the number of time steps per cycle increases the accuracy of the
  Joule heat calculation, while generally increasing the execution time.
  A reasonable range of values is $[10,40]$; anything less than 10 is
  \emph{severely} discouraged.
  
  The electromagnetic field equations are solved on an \emph{inner} time
  distinct from that of the rest of the physics; see
  \tvar{ELECTROMAGNETICS}{SS_Stopping_Tolerance}.
\end{description}


\section*{\texttt{Symmetry_Axis}}
\tvardef{ELECTROMAGNETICS}{Symmetry_Axis}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A flag that specifies which axis is to be used as the
  problem symmetry axis for the Joule heat simulation.
\item[Type:] string
\item[Default:] \texttt{'z'}
\item[Valid values:] \{ \texttt{'x'}, \texttt{'y'}, \texttt{'z'} \}
\item[Notes:] The value of this variable determines the orientation of the
  uniform magnetic source field specified by
  \tvar{ELECTROMAGNETICS}{Uniform_Source}, and the coils specified by the
  \tnamelist{INDUCTION_COIL} namelists, if any.  It also determines the
  assumed orientation of the computational domain; see
  \tvar{ELECTROMAGNETICS}{EM_Domain_Type}.
\end{description}


\section*{\texttt{Uniform_Source}}
\tvardef{ELECTROMAGNETICS}{Uniform_Source}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Amplitude of a sinusoidally-varying, uniform magnetic
  source field that drives the Joule heat computation.  The field is
  directed along the problem symmetry axis as specified by
  \tvar{ELECTROMAGNETICS}{Symmetry_Axis}.
\item[Physical dimension:] \AUcurrent\per\AUlength
\item[Type:] \real
\item[Default:] 0
\item[Valid values:] Any single value, or any sequence of values.
\item[Notes:]
  A sequence of up to 32 values may be assigned to this variable in order to
  specify a time-dependent amplitude; see \tvar{ELECTROMAGNETICS}{Source_Times}
  and Fig.~\ref{EM-fig1}.  If this variable is not specified, its value or
  values, as appropriate, are assumed to be zero.

  The total external magnetic source field that drives the Joule heat
  computation will be the superposition of this field and the fields due to
  the coils specified in the \tnamelist{INDUCTION_COIL} namelists, if any.
  
  For reference, the magnitude of the magnetic field within an infinitely-long,
  finely-wound coil with current \emph{density} $I$ is simply $I$.  The field
  magnitude at the center of a circular current loop of radius $r$ with
  current $I$ is $I/2r$.  In both cases the field is directed along the axis
  of the coil/loop.
\end{description}


\begin{figure}[b]
  \begin{center}
    \includegraphics[scale=0.7]{figures/em-pc.pdf}
  \end{center}
  \caption{Piecewise constant function form showing the constant values
    $f_0,f_1,\ldots,f_n$ in relation to the time partition $t_1,t_2,\ldots,t_n$.}
  \label{EM-fig1}
\end{figure}

