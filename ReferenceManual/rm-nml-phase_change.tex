\chapter{\texttt{PHASE_CHANGE} Namelist}
\tnamelistdef{PHASE_CHANGE}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

\newcommand{\Tsol}{T_\text{sol}}
\newcommand{\Tliq}{T_\text{liq}}
\newcommand{\fsol}{f_\text{sol}}

The \texttt{PHASE_CHANGE} namelist defines the phase change model used for
the transformation between two phases of a multi-phase material. An instance
of this namelist is required for each consecutive pair of phases specified
by the \tvar{MATERIAL}{phases} variable of a \tnamelist{MATERIAL} namelist.

Truchas models the transformation from low temperature phase ("solid")
to high temperature phase ("liquid") using a "solid fraction" function
$\fsol(T)$ that gives the fraction of low temperature phase as a function
of temperature. There are two temperatures $\Tsol < \Tliq$ such that
$\fsol=1$ for $T\le\Tsol$, $\fsol=0$ for $T\ge\Tliq$, and $0<\fsol<1$ for
$\Tsol<T<\Tliq$. There are two alternative methods for specifying this
function.

The first simple, but non-physical, method uses a smooth Hermite cubic
polynomial to interpolate $\fsol$ between $\Tsol$ and $\Tliq$. This is
pictured below.
The second method uses a table of $(T,\fsol)$ values to define $\fsol(T)$.
This allows physics-based phase change models like lever and Scheil to be
defined, and data from CALPHAD-type tools to be used directly.

Note that while described in terms of solid and liquid, these phase
change models also apply to solid-solid transformations with the obvious
reinterpretation of notation.

\section*{Namelist Variables}

\begin{nmlvar}{PHASE_CHANGE}{low_temp_phase,high_temp_phase}{}
These are the names of the two material phases.
\end{nmlvar}

\newlength\picwidth\setlength\picwidth{2in}

\begin{nmlvar}{PHASE_CHANGE}{solidus_temp,liquidus_temp}{}
  The solidus and liquidus temperatures, $\Tsol$ and $\Tliq$. If these
  variables are defined, the simple solid fraction model is used, which
  interpolates the solid fraction over the interval $[\Tsol,\Tliq]$ using
  a smooth Hermite cubic polynomial. These variables are incompatible with
  \ivar{solid_frac_table}.
  %\par\centering
  \begin{center}
    \includegraphics[width=\picwidth]{figures/fs-smooth.pdf}
  \end{center}
\end{nmlvar}

\pagebreak[3]

\begin{nmlvar}{PHASE_CHANGE}{solid_frac_table}{}
A table of temperature-solid fraction values. The format is the same as
described for the \tnamelist{FUNCTION} namelist \tvar{FUNCTION}{Tabular_Data}
variable. However the table may be given in either increasing or decreasing
temperature order, and the solid fraction values must be strictly monotone.
The table endpoints must specify the 1 and 0 solid fractions, and the
corresponding temperatures are taken to be $\Tsol$ and $\Tliq$, respectively.
The solid fraction function is interpolated from this table using Akima
smoothing; see the \tnamelist{FUNCTION} namelist
\tvar{FUNCTION}{Tabular_Interp}. Additional data points outside the interval
$[\Tsol,\Tliq]$ are automatically added to ensure that the solid fraction is
constant outside the transformation interval. This variable is incompatible
with \ivar{solidus_temp} and \ivar{liquidus_temp}.
\begin{center}
\begin{minipage}{0.4\linewidth}\hfill
  \begin{Verbatim}[frame=single,gobble=3,commandchars=\\\{\}]
    solid_frac_table = 930.0 1.00
                       931.0 0.95
                       ...
                       940.0 0.00
  \end{Verbatim}
\end{minipage}\hfill
\begin{minipage}{0.5\linewidth}
  \includegraphics[width=\picwidth]{figures/fs-table.pdf}
\end{minipage}
\end{center}
\end{nmlvar}

\begin{nmlvar}{PHASE_CHANGE}{latent_heat}{}
The energy per unit mass $L$ absorbed during the transformation of the
material from the low temperature phase to the high temperature phase at
the liquidus temperature $T_\text{liq}$.
This property is required when the specific enthalpy of the high temperature
phase is defined from its specific heat. Otherwise it is ignored.
\begin{center}
  \includegraphics[width=2in]{figures/latent-heat.pdf}
\end{center}
\end{nmlvar}
