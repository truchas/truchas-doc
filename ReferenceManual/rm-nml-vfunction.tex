\chapter{\texttt{VFUNCTION} Namelist}
\tnamelistdef{VFUNCTION}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

Similar to the \tnamelist{FUNCTION} namelist, the \texttt{VFUNCTION} namelist
is used to define a vector-valued function that can be used in some situations
where vector-valued data is needed, such as the specification of the boundary
velocity in a flow boundary condition.

These are general vector-valued functions of one or more variables,
$\vec{y} = (y_1,\ldots,y_m) = \vec{f}(x_1,\ldots,x_n)$.
The dimension of the value, the number of variables, and the unknowns they
represent (i.e., time, position, temperature, etc.) all depend on the context
in which the function is used, and that is described in the documentation of
those namelists where these functions can be used.
Currently only a single function type can be defined:
\begin{description}
\item[Tabular Function.] This is a continuous, single-variable function 
  $\vec{y}=\vec{f}(s)$ linearly interpolated from a sequence of data points
  $(s_i,\vec{y}_i)$, $i=1,\dots,p$, with $s_i<s_{i+1}$. The variable $x_d$
  that is identified with $s$ is specified by \tvar{VFUNCTION}{tabular_dim}.
\end{description}


\section*{\texttt{FUNCTION} Namelist Features}
\begin{description} \setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}
\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{VFUNCTION}{Name}
\item \tvar{VFUNCTION}{Type}
\item \tvar{VFUNCTION}{Tabular_Data}
\item \tvar{VFUNCTION}{Tabular_Dim}
\end{itemize}


\tvardef{VFUNCTION}{Name}
\section*{\texttt{Name}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  A unique name by which this vector function can be referenced by other
  namelists.
\item[Type:] A case-sensitive string of up to 31 characters.
\item[Default:]  None
\end{description}


\section*{\texttt{Type}}
\tvardef{VFUNCTION}{Type}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The type of function defined by the namelist.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Valid values:] \texttt{"tabular"} for a tabular function
\end{description}


\tvardef{VFUNCTION}{Tabular_Data}
\section*{\texttt{Tabular_Data}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The table of values $(s_i,\vec{y}_i)$ defining a tabular function 
  $\vec{y}=\vec{f}(s)$. Use \tvar{VFUNCTION}{Tabular_Dim} to set the
  variable identified with $s$.
\item[Type:] real array
\item[Default:] none
\item[Notes:]
  This is a $(m+1)\times p$ array that is most easily specified in the
  following manner
  \begin{Verbatim}[commandchars=\\\{\}]
    Tabular_Data(:,1) = \(s_1, y_{11}, \dots, y_{m1}\)
    Tabular_Data(:,2) = \(s_2, y_{12}, \dots, y_{m2}\)
                      \(\vdots\)
    Tabular_Data(:,p) = \(s_p, y_{1p}, \dots, y_{mp}\)
  \end{Verbatim}
\end{description}


\tvardef{VFUNCTION}{Tabular_Dim}
\section*{\texttt{Tabular_Dim}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The dimension in the $m$-vector of independent variables that serves
  as the independent variable for the single-variable tabular function.
\item[Type:] integer
\item[Default:] 1
\end{description}
