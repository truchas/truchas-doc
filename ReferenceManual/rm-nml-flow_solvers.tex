\chapter{\texttt{FLOW_PRESSURE_SOLVER} and \texttt{FLOW_VISCOUS_SOLVER} Namelists}
\tnamelistdef{FLOW_PRESSURE_SOLVER}\tnamelistdef{FLOW_VISCOUS_SOLVER}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The flow algorithm requires the solution of two linear systems at each time
step: the implicit viscous velocity update system and the pressure Poisson
system. Truchas uses the hybrid solver from the HYPRE software library
\cite{hypre} to solve these systems. 

The hybrid solver first uses a diagonally-scaled iterative Krylov solver.
If it determines that convergence is too slow, the solver switches to a more
expensive but more effective preconditioned Krylov solver that uses an
algebraic multigrid (AMG) preconditioner (BoomerAMG).

The \texttt{FLOW_VISCOUS_SOLVER} namelist sets the HYPRE hybrid solver
parameters for the solution of the implicit viscous velocity update system,
and the \texttt{FLOW_PRESSURE_SOLVER} namelist sets the solver parameters for
the solution of the pressure Poisson system. The same variables are used in
both namelists.

\section*{\texttt{FLOW_VISCOUS_SOLVER} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required only for viscous flow with
\tvar{FLOW}{viscous_implicitness}${}>0$.
\item[Single/Multiple Instances:] Single
\end{description}

\section*{\texttt{FLOW_PRESSURE_SOLVER} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Single
\end{description}


\section*{\texttt{krylov_method}}
\tvardef{flow_solvers}{krylov_method}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Selects the Krylov method used by the HYPRE hybrid solver. The options are
  \texttt{"cg"} (default), \texttt{"gmres"}, and \texttt{"bicgstab"}.
\end{description}


\section*{\texttt{krylov_dim}}
\tvardef{flow_solvers}{krylov_dim}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The Krylov subspace dimension for the restarted GMRES method. 
\item[Type:] \integer
\item[Valid values:] $>0$
\item[Default:] \texttt{5}
\end{description}


\section*{\texttt{conv_rate_tol}}
\tvardef{flow_solvers}{conv_rate_tol}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The convergence rate tolerance $\theta$ where the hybrid solver switches
  to the more expensive AMG preconditioned Krylov solver. The average
  convergence rate after $n$ iterations of the diagonally-scaled Krylov solver
  is $\rho_n = \bigl(\|r_n\|/\|r_0\|\bigr)^{1/n}$, where $r_n = A x_n - b$ is
  the residual of the linear system, and its convergence is considered too
  slow when
  \begin{equation*}
    \left[1-\frac{|\rho_n-\rho_{n-1}|}{\max(\rho_n,\rho_{n-1})}\right]\rho_n
      > \theta
  \end{equation*}
\item[Type:] \real
\item[Valid values:] $(0,1)$
\item[Default:] $0.9$
\end{description}


\section*{\texttt{abs_tol}, \texttt{rel_tol}}
\tvardef{flow_solvers}{abs_tol}
\tvardef{flow_solvers}{rel_tol}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The absolute and relative error tolerances $\epsilon_1$ and $\epsilon_2$
  for the solution of the linear system. The test for convergence is
  $\|r\| \le \max\{\epsilon_1, \epsilon_2 \|b\|\}$, where $r = Ax -b$ is
  the residual of the linear system.
\item[Type:] \real
\item[Default:] None
\item[Note:] \quad
\todo[inline]{Any guidance here? Pressure vs viscous solve?}
\end{description}


\section*{\texttt{max_ds_iter}}
\tvardef{flow_solvers}{max_ds_iter}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The maximum number of diagonally scaled Krylov iterations allowed.
  If convergence is not achieved within this number of iterations the
  hybrid solver will switch to the preconditioned Krylov solver.
\item[Type:] \integer
\item[Default:]
\end{description}


\section*{\texttt{max_amg_iter}}
\tvardef{flow_solvers}{}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The maximum number of preconditioned Krylov iterations allowed.
  \todo[inline]{What happens if convergence is not finally achieved
  within this number of iterations?}
\item[Type:] \integer
\item[Default:]
\end{description}


\section*{\texttt{print_level}}
\tvardef{flow_solvers}{print_level}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Set this parameter to 2 to have HYPRE write diagnostic data to the terminal
  for each solver iteration. This is only useful in debugging situations. The
  default is 0, no output.
\end{description}


\section*{Additional HYPRE parameters (Expert)}
Some additional HYPRE solver parameters and options can be set using these
namelists. Nearly all of these are associated with the BoomerAMG
preconditioner, and all have reasonable defaults set by HYPRE. See the
\emph{ParCSR Hybrid Solver} section in the HYPRE reference manual
\cite{hypre-rm} for details. The HYPRE user's manual \cite{hypre-um} has
some additional information. The variables that can be set are listed below.
Note that the variables correspond to similarly-named HYPRE library functions
and not actual HYPRE variables. Also note that there are many parameters and
options that cannot currently be set by the namelists.

\begin{quote}
\begin{description}\setlength{\itemsep}{0pt}
\item[\texttt{cg_use_two_norm}] (logical)
\item[\texttt{amg_strong_threshold}] (real)
\item[\texttt{amg_max_levels}] (integer)
\item[\texttt{amg_coarsen_method}] (integer)
\item[\texttt{amg_smoothing_sweeps}] (integer)
\item[\texttt{amg_smoothing_method}] (integer)
\item[\texttt{amg_interp_method}] (integer)
\end{description}
\end{quote}
