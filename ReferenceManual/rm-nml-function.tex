\chapter{\texttt{FUNCTION} Namelist}
\tnamelistdef{FUNCTION}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

There is often a need to specify phase properties, boundary condition
data, source data, etc., as functions rather than constants.  The
\texttt{FUNCTION} namelist provides a means for defining functions that
can be used in many situations.

The namelist can define several types of functions: a multi-variable polynomial,
a continuous piecewise linear function defined by a table of values,
a smooth step function, and with certain Truchas build configurations, a general
user-provided function from a shared object library that is
dynamically loaded at runtime.  The functions are functions of $m$ variables.
The expected number of variables and what unknowns they represent (i.e.,
temperature, time, $x$-coordinate, etc.) depends on the context in which
the function is used, and this will be detailed by the documentation of
those namelists where these functions can be used.

\begin{description}
\item[Polynomial Function.]
  This function is a polynomial in the variables $\mathbf{v}=(v_1,\dots,v_m)$
  of the form
  \begin{equation}\label{polyfun}
    f(\mathbf{v}) = \sum_{j=1}^{n} c_j \prod_{i=1}^{m}(v_i-a_i)^{e_{ij}}
  \end{equation}
  with coefficients $c_j$, integer-valued exponents $e_{ij}$, and arbitrary
  reference point $\mathbf{a}=(a_1,\dots,a_m)$.  The coefficients are
  specified by \tvar{FUNCTION}{Poly_Coefficients}, the exponents by
  \tvar{FUNCTION}{Poly_Exponents}, and the reference point by
  \tvar{FUNCTION}{Poly_Refvars}.
\item[Tabular Function.] This is a continuous, single-variable function $y=f(x)$
  interpolated from a sequence of data points $(x_i,y_i)$, $i=1,\dots,n$, with
  $x_i<x_{i+1}$.  A smooth interpolation method is available in addition to the
  standard linear interpolation; see \tvar{FUNCTION}{Tabular_Interp}. There are
  also two different methods for extrapolating on $x<x_1$ and $x>x_n$; see
  \tvar{FUNCTION}{Tabular_Extrap}.
\item[Smooth Step Function.]
  This function is a smoothed ($C_1$) step function in a single variable
  $\mathbf{v}=(x)$ of the form
  \begin{equation}\label{step}
    f(x) = \begin{cases}
      y_0 \text{ if $x\le x_0$}, \\
      y_0 + (y_1-y_0) s^2(3-2s), s\equiv(x-x_0)/(x_1-x_0), \text{ when $x\in(x_0,x_1)$}, \\ 
      y_1 \text{ if $x\ge x_1$},
    \end{cases}
  \end{equation}
  with parameters $x_0$, $x_1$, $y_0$, and $y_1$.
\item[Shared Library Function.]  This is a function from a shared object
  library having a simple Fortran~77 or C compatible interface.  Written
  in Fortran~77 the function interface must look like
  \begin{Verbatim}
    double precision function myfun (v, p) bind(c)
      double precision v(*), p(*)
  \end{Verbatim}
  where \texttt{myfun} can, of course, be any name. The equivalent C interface is
  \begin{Verbatim}
    double myfun (double v[], double p[]);
  \end{Verbatim}
  The vector of variables $\mathbf{v}=(v_1,\dots,v_m)$ is passed in the
  argument \texttt{v} and a vector of parameter values specified by
  \tvar{FUNCTION}{Parameters} is passed in the argument \texttt{p}.
  Instructions for compiling the code and creating a shared object library
  can be found in the \IG.  The path to the library is given by
  \tvar{FUNCTION}{Library_Path} and the name of the function
  (\texttt{myfun}, e.g.) is given by \tvar{FUNCTION}{Library_Symbol}.
  Note that the \texttt{bind(c)} attribute on the function declaration
  inhibits the Fortran compiler from mangling the function name
  (by appending an underscore, for example) as it normally would.
  \todo[inline]{Add instructions for building a shared library? To Installation Guide?}
\end{description}


\section*{\texttt{FUNCTION} Namelist Features}
\begin{description} \setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}
\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{FUNCTION}{Name}
\item \tvar{FUNCTION}{Type}
\item \tvar{FUNCTION}{Library_Path}
\item \tvar{FUNCTION}{Library_Symbol}
\item \tvar{FUNCTION}{Parameters}
\item \tvar{FUNCTION}{Poly_Coefficients}
\item \tvar{FUNCTION}{Poly_Exponents}
\item \tvar{FUNCTION}{Poly_Refvars}
\item \tvar{FUNCTION}{Smooth_Step_X0}
\item \tvar{FUNCTION}{Smooth_Step_X1}
\item \tvar{FUNCTION}{Smooth_Step_Y0}
\item \tvar{FUNCTION}{Smooth_Step_Y1}
\item \tvar{FUNCTION}{Tabular_Data}
\item \tvar{FUNCTION}{Tabular_Dim}
\item \tvar{FUNCTION}{Tabular_Extrap}
\item \tvar{FUNCTION}{Tabular_Interp}
\end{itemize}


\tvardef{FUNCTION}{Name}
\section*{\texttt{Name}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  A unique name by which this function can be referenced by other namelists.
\item[Type:] A case-sensitive string of up to 31 characters.
\item[Default:]  None
\end{description}


\section*{\texttt{Type}}
\tvardef{FUNCTION}{Type}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The type of function defined by the namelist.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Valid values:] \texttt{"polynomial"} for a polynomial function,
  \texttt{"tabular"} for a tabular function,
  \texttt{"smooth step"} for a smooth step function, or 
  \texttt{"library"} for a function from a shared object library.
  The \texttt{"library"} value is not available with a Truchas executable
  built with the ``dynamic loading'' option disabled.
\end{description}


\tvardef{FUNCTION}{Library_Path}
\section*{\texttt{Library_Path}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The path to the shared object library that contains the function.
\item[Type:] A string of up to 128 characters.
\item[Default:] none
\end{description}


\tvardef{FUNCTION}{Library_Symbol}
\section*{\texttt{Library_Symbol}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The symbol name of the function within the shared object file.
\item[Type:] A string of up to 128 characters.
\item[Default:] none
\item[Notes:]
  Unless the Fortran function is declared with the \texttt{BIND(C)} attribute,
  which is the recommended practice, a Fortran compiler will almost always
  mangle the name of the function so that the symbol name is not quite the
  same as the name in the source code.  Use the UNIX/Linux command-line utility \texttt{nm} to list the symbol
  names in the library file to determine the correct name to use here.
\end{description}


\section*{\texttt{Parameters}}
\tvardef{FUNCTION}{Parameters}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  Optional parameter values to pass to the shared library function.
\item[Type:] real vector of up to 16 values
\item[Default:] None
\end{description}


\tvardef{FUNCTION}{Poly_Coefficients}
\section*{\texttt{Poly_Coefficients}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The coefficients $c_j$ of the polynomial (\ref{polyfun}).
\item[Type:] real vector of up to 64 values
\item[Default:] None
\end{description}


\tvardef{FUNCTION}{Poly_Exponents}
\section*{\texttt{Poly_Exponents}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The exponents $e_{ij}$ of the polynomial (\ref{polyfun}).
\item[Type:] integer array
\item[Default:] None
\item[Notes:] Namelist array input is very flexible.  The syntax
  \begin{Verbatim}[commandchars=\\\{\}]
    Poly_Exponents(\(i\),\(j\)) = \(\ldots\)
  \end{Verbatim}
  defines the value for exponent $e_{ij}$.  All the variable exponents
  for coefficient $j$ can be defined at once by listing their values with
  the syntax
  \begin{Verbatim}[commandchars=\\\{\}]
    Poly_Exponents(:,\(j\)) = \(\ldots\)
  \end{Verbatim}
  In some circumstances it is possible to omit providing $0$-exponents for
  variables that are unused.  For example, if the function is expected to
  be a function of $(t,x,y,z)$, but a polynomial in only $t$ is desired,
  one can just define a 1-variable polynomial and entirely ignore the remaining
  variables.  On the other hand, if a polynomial in $z$ is desired, one must
  specify $0$-valued exponents for all the preceding variables.
\end{description}


\tvardef{FUNCTION}{Poly_Refvars}
\section*{\texttt{Poly_Refvars}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The optional reference point $\mathbf{a}$ of the polynomial (\ref{polyfun}).
\item[Type:] real vector
\item[Default:] 0.0
\end{description}


\tvardef{FUNCTION}{Tabular_Data}
\section*{\texttt{Tabular_Data}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The table of values $(x_i,y_i)$ defining a tabular function $y=f(x)$.
  See also \tvar{FUNCTION}{Tabular_Dim}, \tvar{FUNCTION}{Tabular_Interp},
  and \tvar{FUNCTION}{Tabular_Extrap} for additional variables that define
  the function.
\item[Type:] real array
\item[Default:] none
\item[Notes:]
  This is a $2\times n$ array with $n\le100$.  Namelist array input is very
  flexible and the values can be specified in several ways.  For example,
  the syntax
  \begin{Verbatim}[commandchars=\\\{\}]
    Tabular_Data(1,:) = \(x_1, x_2, \dots, x_n\)
    Tabular_Data(2,:) = \(y_1, y_2, \dots, y_n\)
  \end{Verbatim}
  specifies the $x_i$ and $y_i$ values as separate lists.  Or the values
  can be input naturally as a table
  {
  \newsavebox{\foo}
  \sbox{\foo}{\begin{array}[t]{cc}x_1,&y_1\\x_2,&y_2\\\multicolumn{2}{c}{\ldots}\\x_n,&y_n\end{array}}
  \begin{Verbatim}[commandchars=\\\{\}]
    Tabular_Data = \usebox{\foo}
  \end{Verbatim}
  }
  The line breaks are unnecessary, of course, and are there only for
  readability as a table.
\end{description}


\tvardef{FUNCTION}{Tabular_Dim}
\section*{\texttt{Tabular_Dim}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The dimension in the $m$-vector of independent variables that serves
  as the independent variable for the single-variable tabular function.
\item[Type:] integer
\item[Default:] 1
\item[Notes:]
  Functions defined by this namelist are generally functions of $m$
  variables $(v_1, v_2, \dots, v_m)$.  The number of variables and
  the unknowns to which they correspond depend on the context where
  the function is used.  One of these variables needs to be selected
  to be the independent variable used for the tabular function.  In
  typical use cases the desired tabular function will depend on time
  or temperature.  Those unknowns are often the first variable, and
  the default value of \texttt{Tabular_Dim} is appropriate.
\end{description}


\tvardef{FUNCTION}{Tabular_Extrap}
\section*{\texttt{Tabular_Extrap}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the method used for extrapolating outside the range of tabular
  function data points.
\item[Type:] case-insensitive string
\item[Default:] \texttt{"nearest"}
\item[Valid values:] \texttt{"nearest"}, \texttt{"linear"}
\item[Notes:]
  Nearest extrapolation continues the $y$ value at the first or last data
  point.  Linear extrapolation uses the slope of the first or last data
  interval, or if Akima smoothing is used (see \tvar{FUNCTION}{Tabular_Interp})
  the computed slope at the first or last data point.
\end{description}


\tvardef{FUNCTION}{Tabular_Interp}
\section*{\texttt{Tabular_Interp}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the method used for interpolating between tabular function
  data points.
\item[Type:] case-insensitive string
\item[Default:] \texttt{"linear"}
\item[Valid values:] \texttt{"linear"}, \texttt{"akima"}
\item[Notes:]
  Akima interpolation \cite{akima} uses Hermit cubic interpolation on each
  data interval, with the slope at each data point computed from the linear
  slopes on the neighboring four intervals.  The resulting function is $C^1$
  smooth. To determine the slope at the first two and last two data points,
  two virtual data intervals are generated at the beginning and at the end
  using quadratic extrapolation.

  The algorithm seeks to avoid undulations in the interpolated function where
  the data suggests a flat region though its choice of slopes at data points.
  Figure~\ref{func-fig1}A shows the typical smooth Akima interpolation. If the
  first interval was expected to be flat, the exhibited undulation would
  likely be unacceptable.  By inserting an additional data point to create
  successive intervals with the same slope, as in Figure~\ref{func-fig1}BC, the
  algorithm identifies it as a flat region and preserves it in the interpolation.
  Where two flat regions with differing slopes meet, it is impossible to
  simultaneously retain smoothness and preserve flatness. In this case a
  modification to the Akima algorithm used by Matlab's \texttt{tablelookup}
  function is adopted, which gives preference to the region with smaller slope
  as shown in Figure~\ref{func-fig1}D.
\end{description}

\begin{figure}[h]
  \begin{center}
    \includegraphics[height=2.0in]{figures/akima.png}
  \end{center}
  \caption{Examples of smooth Akima interpolation.}
  \label{func-fig1}
\end{figure}


\tvardef{FUNCTION}{Smooth_Step_X0}
\section*{\texttt{Smooth_Step_X0}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The parameter $x_0$ of the function (\ref{step}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] Require only $x_0 < x_1$.
\end{description}


\tvardef{FUNCTION}{Smooth_Step_X1}
\section*{\texttt{Smooth_Step_X1}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The parameter $x_1$ of the function (\ref{step}).
\item[Type:] real
\item[Default:] none
\item[Valid values:] Require only $x_0 < x_1$.
\end{description}


\tvardef{FUNCTION}{Smooth_Step_Y0}
\section*{\texttt{Smooth_Step_Y0}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The parameter $y_0$ of the function (\ref{step}).
\item[Type:] real
\item[Default:] none
\end{description}


\tvardef{FUNCTION}{Smooth_Step_Y1}
\section*{\texttt{Smooth_Step_Y1}}
\begin{description} \setlength{\itemsep}{0pt}
\item[Description:]
  The parameter $y_1$ of the function (\ref{step}).
\item[Type:] real
\item[Default:] none
\end{description}
