\chapter{\texttt{BODY} Namelist}
\tnamelistdef{BODY}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{BODY} namelists define initial material distributions and conditions.
The \texttt{BODY} namelists are processed in the order they appear, and identify
the specified part of the computational domain not claimed by any preceding
\texttt{BODY} namelist.  Any ``background'' type \texttt{BODY} must be listed
last.

Each namelist is used to specify a geometry and initial state. The geometry is
specified via the variables using an acceptable combination of
\tvar{BODY}{surface_name}, \tvar{BODY}{axis}, \tvar{BODY}{fill},
\tvar{BODY}{height}, \tvar{BODY}{length}, \tvar{BODY}{mesh_material_number},
\tvar{BODY}{radius}, \tvar{BODY}{rotation_angle}, \tvar{BODY}{rotation_pt}, and
\tvar{BODY}{translation_pt}, hereafter referred to as geometry-type
parameters. The initial state is specified using \tvar{BODY}{material_number},
\tvar{BODY}{velocity}, \tvar{BODY}{phi}, and \tvar{BODY}{temperature} or
\tvar{BODY}{temperature_function}. %, hereafter referred to as state-type parameters.

\section*{\texttt{BODY} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}

\item \tvar{BODY}{axis}
\item \tvar{BODY}{fill}
\item \tvar{BODY}{height}
\item \tvar{BODY}{length}
\item \tvar{BODY}{material_name}
\item \tvar{BODY}{mesh_material_number}
\item \tvar{BODY}{phi}
\item \tvar{BODY}{radius}
\item \tvar{BODY}{rotation_angle}
\item \tvar{BODY}{rotation_pt}
\item \tvar{BODY}{surface_name}
\item \tvar{BODY}{temperature}
\item \tvar{BODY}{temperature_function}
\item \tvar{BODY}{translation_pt}
\item \tvar{BODY}{velocity}
\end{itemize}

\section*{\texttt{axis}}
\tvardef{BODY}{axis}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
The axis to be used for defining a cylinder or plane.
\item[Type:] string
\item[Default:] (none)
\item[Valid values:] \texttt{'x', 'y', 'z'}
\end{description}

\section*{\texttt{fill}}
\tvardef{BODY}{fill}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
The side of the surface to which material is to be inserted for this body.
\item[Type:] string
\item[Default:] \texttt{'inside'}
\item[Valid values:] \texttt{'inside', 'outside'}
\end{description}

\section*{\texttt{height}}
\tvardef{BODY}{height}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Height of a cylinder body.
\item[Physical Dimension:] \AUlength
\item[Type:] \real
\item[Default:] (none)
\item[Valid values:] $(0.0, \infty)$
\end{description}

\section*{\texttt{length}}
\tvardef{BODY}{length}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Length of each side of the box body, or the coefficients of an ellipse or
ellipsoid body.
\item[Physical Dimension:] \AUlength
\item[Type:] \real \ triplet
\item[Default:] (none)
\item[Valid values:] $(0, \infty)$
\end{description}

\section*{\texttt{material_name}}
\tvardef{BODY}{material_name}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Name of the material, or material phase in the case of a multi-phase material,
that occupies the volume of this body.
\item[Type:] string
\item[Default:] (none)
\end{description}

\section*{\texttt{mesh_material_number}}
\tvardef{BODY}{mesh_material_number}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  List of material numbers (element block IDs) associated with the cells as
defined in the mesh file. This parameter is only meaningful when surface_name =
'from mesh file'.
\item[Type:] \integer\ list (16 max)
\item[Default:] (none)
\item[Valid values:] Existing material numbers in mesh file (if the mesh file is
in Exodus/Genesis format, this is the mesh block number).
\end{description}

\section*{\texttt{phi}}
\tvardef{BODY}{phi}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Initial value of the diffusion solver's multi-component scalar field in the
material body.
\item[Physical Dimension:] varies
\item[Type:] \real\ vector of \tvar{DIFFUSION_SOLVER}{Num_Species} values
\item[Default:] 0.0
\item[Valid values:] $(-\infty, \infty)$
\end{description}

\section*{\texttt{radius}}
\tvardef{BODY}{radius}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Radius of the geometric body (cylinder, sphere, ellipsoid).
\item[Physical Dimension:] \AUlength
\item[Type:] \real
\item[Default:] (none)
\item[Valid values:] $(0.0, \infty)$
\end{description}

\section*{\texttt{rotation_angle}}
\tvardef{BODY}{rotation_angle}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Angle (degrees) about the $(x,y,z)$ axes this body is to be rotated.
This variable is only supported for 'plane' and 'cylinder' body types.
\item[Type:] \real \ triplet
\item[Default:] 0.0, 0.0, 0.0
\item[Valid values:] $(-\infty, \infty)$
\end{description}

\section*{\texttt{rotation_pt}}
\tvardef{BODY}{rotation_pt}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Location of the point about which this body is to be rotated.
This variable is only supported for 'plane' and 'cylinder' body types.
\item[Physical Dimension:] \AUlength
\item[Type:] \real \ triplet
\item[Default:] 0.0, 0.0, 0.0
\item[Valid values:] $(-\infty, \infty)$
\end{description}

\section*{\texttt{surface_name}}
\tvardef{BODY}{surface_name}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Type of surface characterizing the interface topology for this body. The
available options are:
\begin{description}\setlength{\itemsep}{0pt}
\item[\texttt{"background"}]
  A background body will occupy all space which has not been claimed by
  previously listed \texttt{BODY} namelists. If provided, it must be the final
  \texttt{BODY} namelist provided. When specified, no other geometry-type
  parameters are relevant.
\item[\texttt{"plane"}]
  A plane is specified using \tvar{BODY}{axis}, \tvar{BODY}{rotation_angle},
  \tvar{BODY}{rotation_pt}, and \tvar{BODY}{fill} to define the normal direction,
  and \tvar{BODY}{translation_pt} to provide a point on the plane surface. The
  normal vector is an 'outward' normal, such that the region defined is in the
  opposite direction of the normal vector unless \tvar{BODY}{fill} = 'outside'.
\item[\texttt{"box"}]
  A box is specified using \tvar{BODY}{translation_pt} as the center,
  \tvar{BODY}{length} for the length of $x$, $y$, and $z$ sides respectively, and
  \tvar{BODY}{fill} to invert the shape. This shape does not support rotation.
\item[\texttt{"sphere"}]
  A sphere is specified using \tvar{BODY}{translation_pt} as the center,
  \tvar{BODY}{radius}, and \tvar{BODY}{fill} to invert the shape.
\item[\texttt{"ellipsoid"}]
  An ellipsoid of the form
  \begin{equation*}
    \frac{(x-x_0)^2}{l_1^2} + \frac{(y-y_0)^2}{l_2^2} + \frac{(z-z_0)^2}{l_3^2} \le 1
  \end{equation*}
  is specified using \tvar{BODY}{translation_pt} as the center,
  \tvar{BODY}{length} for $l_1$, $l_2$, and $l_3$, , and \tvar{BODY}{fill} to
  invert the shape. This shape does not support rotation.
\item[\texttt{"ellipse"}]
  An infinitely long elliptic cylinder of the form
  \begin{equation*}
    \frac{(x-x_0)^2}{l_1^2} + \frac{(y-y_0)^2}{l_2^2} \le 1
  \end{equation*}
  is specified using \tvar{BODY}{translation_pt} as the center,
  \tvar{BODY}{length} for $l_1$ and $l_2$, , and \tvar{BODY}{fill} to invert the
  shape. This shape does not support rotation, and will be aligned with the $z$
  axis.
\item[\texttt{"cylinder"}]
  A cylinder is specified using \tvar{BODY}{translation_pt} as the center of the
  base, \tvar{BODY}{axis}, \tvar{BODY}{rotation_angle}, and
  \tvar{BODY}{rotation_pt} to define the orientation, \tvar{BODY}{radius},
  \tvar{BODY}{height}, and \tvar{BODY}{fill} to invert the shape.
\item[\texttt{"from mesh file"}]
  This option is used to specify cells associated with element blocks in the
  input mesh file. \tvar{BODY}{mesh_material_number} is used to list the desired
  element blocks. \tvar{BODY}{fill} may be used to invert the selection.
\end{description}
\item[Type:] string
\item[Default:] (none)
\end{description}

\section*{\texttt{temperature}}
\tvardef{BODY}{temperature}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Initial constant temperature of the material body.
\item[Physical dimension:] \AUtemperature
\item[Type:] real
\item[Default:] none
\item[Note:]
  Either \texttt{temperature} or \tvar{BODY}{temperature_function} must
  specified, but not both.
\end{description}

\section*{\texttt{temperature_function}}
\tvardef{BODY}{temperature_function}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
The name of a \tnamelist{FUNCTION} namelist that defines the initial
temperature function for the material body.  That function is expected
to be a function of $(x,y,z)$.
\item[Type:] string
\item[Default:] none
\item[Note:]
  Either \texttt{temperature_function} or \tvar{BODY}{temperature} must
  specified, but not both.
\end{description}

\section*{\texttt{translation_pt}}
\tvardef{BODY}{translation_pt}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Location to which each surface origin of this body is translated.
\item[Physical Dimension:] \AUlength
\item[Type:] \real \ triplet
\item[Default:] 0.0, 0.0, 0.0
\item[Valid values:] $(-\infty, \infty)$
\end{description}

\section*{\texttt{velocity}}
\tvardef{BODY}{velocity}

\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
Initial velocity of the material body.
\item[Physical Dimension:] \AUlength\per\AUtime
\item[Type:] \real \ triplet
\item[Default:] 0.0, 0.0, 0.0
\item[Valid values:] $(-\infty, \infty)$
\end{description}
