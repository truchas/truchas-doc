\chapter{\texttt{ALTMESH} Namelist}
\tnamelistdef{ALTMESH}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{ALTMESH} namelist specifies the alternate mesh used by the
induction heating solver.  This is a 3D tetrahedral mesh imported from
an ExodusII format disk file.

\todo[inline]{Need a discussion of the EM computational domain that
must be meshed.  See EM_Domain_Type and the discussions in the P\&A
and User manuals.}

\todo[inline]{Need a discussion of how this mesh must relate to the
primary mesh insofar as grid mapping is concerned; things like the
choice of element blocks.}

\section*{\texttt{ALTMESH} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required when \tvar{PHYSICS}{Electromagnetics} is true.
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{ALTMESH}{Altmesh_Coordinate_Scale_Factor}
\item \tvar{ALTMESH}{Altmesh_File}
\item \tvar{ALTMESH}{First_Partition}
\item \tvar{ALTMESH}{Grid_Transfer_File}
\item \tvar{ALTMESH}{Partitioner}
\item \tvar{ALTMESH}{Partition_File}
\item \tvar{ALTMESH}{rotation_angles}
\item \tvar{ALTMESH}{data_mapper_kind}
\end{itemize}


\tvardef{ALTMESH}{Altmesh_Coordinate_Scale_Factor}
\section*{\texttt{Altmesh_Coordinate_Scale_Factor}}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Description:]
  An optional factor by which to scale all mesh node coordinates.
\item[Type:] real
\item[Default:] 1.0
\item[Valid values:] $>0$
\end{description}


\tvardef{ALTMESH}{Altmesh_File}
\section*{\texttt{Altmesh_File}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the path to the ExodusII mesh file.  If not an absolute path,
  it will be interpreted relative the the Truchas input file directory.
\item[Type:] case-sensitive string
\item[Default:] none
\end{description}


\tvardef{ALTMESH}{Grid_Transfer_File}
\section*{\texttt{Grid_Transfer_File}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Certain fields must be mapped between the main and alternative meshes during
  the course of a simulation.  These mappings are accomplished using some fixed
  grid-mapping data that depend only on the two meshes.  This optional variable
  specifies the path of a file containing this grid mapping data. If specified,
  and if the file exists, it will be read and its mapping data checked to
  ensure that it corresponds to the two meshes being used.  If it corresponds,
  the data will be used for the calculation.  Otherwise, the grid mapping data
  is computed and written to the file \texttt{altmesh_mapping_data.bin} in
  the output directory for use in future calculations, avoiding a needless and
  potentially costly recomputation of the same data.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Note:]
  The mapping data depends on the internal ordering of the nodes and cells
  of each mesh, in addition to the meshes themselves.  Thus it is recommended
  that this file be named in such a way that reflects the identity of the
  two meshes \emph{and} the number of processors used to compute the mapping
  data; mapping data computed with one number of processors will not be
  usable in a calculation with a different number of processors, even when
  the same pair of meshes is used.
\end{description}


\tvardef{ALTMESH}{Partitioner}
\section*{\texttt{Partitioner}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The partitioning method used to generate the parallel
  decomposition of the EM mesh.
\item[Type:] case-insensitive string
\item[Default:] \texttt{"chaco"}
\item[Valid values:] \texttt{"chaco"}, \texttt{"metis"}, \texttt{"file"}, \texttt{"block"}
\item[Notes:] See the \texttt{MESH} namelist variable \tvar{MESH}{Partitioner}
  for a description of the options and their associated input variables.
\end{description}


\tvardef{ALTMESH}{Partition_File}
\section*{\texttt{Partition_File}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the path to the EM mesh cell partition file, and is required
  when \tvar{ALTMESH}{Partitioner} is \texttt{"file"}. If not an absolute
  path, it will be interpreted as a path relative to the Truchas input
  file directory.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Notes:] See the Notes for the \texttt{MESH} namelist variable
  \tvar{MESH}{Partition_File}.
\end{description}


\tvardef{ALTMESH}{First_Partition}
\section*{\texttt{First_Partition}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the number given the first partition in the numbering convention
  used in the partition file. Either 0-based or 1-based numbering is allowed. 
\item[Type:] integer
\item[Default:] 0
\item[Valid values:] 0 or 1
\end{description}


\tvardef{ALTMESH}{rotation_angles}
\section*{\texttt{rotation_angles}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of 3 angles, given in degrees, specifying the amount of
  counter-clockwise rotation about the x, y, and z-axes to apply to the mesh.
  The rotations are done sequentially in that order. A negative angle
  is a clockwise rotation, and a zero angle naturally implies no rotation.
\item[Type:] real 3-vector
\item[Default:] $(0.0, 0.0, 0.0)$
\end{description}


\tvardef{ALTMESH}{data_mapper_kind}
\section*{\texttt{data_mapper_kind}\quad(Experimental)}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  This specifies the tool that will be used to map fields between the main
  heat transfer mesh and this alternative mesh. If \texttt{"portage"} is
  selected, an experimental data mapper based on the Portage toolkit,
  \url{https://laristra.github.io/portage}, will be used. This data mapper
  is capable of handling main meshes containing prism and pyramid cells, but
  it has not yet been thoroughly vetted. Otherwise the normal data mapping
  tool will be used by default.
\item[Type:] string
\item[Default:] \texttt{"default"}
\item[Valid values:] \texttt{"default"}, \texttt{"portage"}
\end{description}
