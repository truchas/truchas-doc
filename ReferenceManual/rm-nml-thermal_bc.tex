\chapter{\texttt{THERMAL_BC} Namelist}
\tnamelistdef{THERMAL_BC}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{THERMAL_BC} namelist is used to define boundary conditions for
the heat transfer model at external boundaries and internal interfaces.
Each instance of the namelist defines a particular condition to impose over
a subset of the domain boundary. The boundary subset $\Gamma$ is specified
using mesh face sets. The namelist variable \tvar{THERMAL_BC}{face_set_ids}
takes a list of face set IDs, and the boundary condition is imposed on all
faces belonging to those face sets. Note that ExodusII mesh side sets are
imported into Truchas as face sets with the same IDs.

\subsubsection*{External boundaries}
The following types of external boundary conditions can be defined.
The outward unit normal to the boundary $\Gamma$ is denoted $\hat{n}$.
\begin{itemize} \setlength{\itemsep}{0pt}
\item \textit{Temperature.}
  A temperature Dirichlet condition
  \begin{equation}
    T = T_b \text{ on $\Gamma$}
  \end{equation}
  is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"temperature"}.
  The boundary value $T_b$ is specified using either \tvar{THERMAL_BC}{temp}
  for a constant value, or \tvar{THERMAL_BC}{temp_func} for a function.
\item \textit{Total Flux.}
  A heat flux condition
  \begin{equation}
    -\kappa\nabla T\cdot\hat{n} = q_b \text{ on $\Gamma$}
  \end{equation}
  is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"flux"}. The heat
  flux $q_b$ is specified using either \tvar{THERMAL_BC}{flux}
  for a constant value, or \tvar{THERMAL_BC}{flux_func} for a function.
\item \textit{Heat Transfer.}
  An external heat transfer flux condition
  \begin{equation}
    -\kappa\nabla T\cdot\hat{n} = \alpha(T-T_\infty) \text{ on $\Gamma$}
  \end{equation}
  is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"htc"}.
  The heat transfer coefficient $\alpha$ is specified using either
  \tvar{THERMAL_BC}{htc} for a constant value, or
  \tvar{THERMAL_BC}{htc_func} for a function, and the ambient temperature
  $T_\infty$ is specified using either \tvar{THERMAL_BC}{ambient_temp}
  for a constant value, or \tvar{THERMAL_BC}{ambient_temp_func} for a function.
\item \textit{Ambient Radiation.}
  A simple ambient thermal radiation condition
  \begin{equation}
    -\kappa\nabla T\cdot\hat{n} =
      \epsilon\sigma\bigl((T-T_0)^4-(T_\infty-T_0)^4\bigr) \text{ on $\Gamma$}
  \end{equation}
  is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"radiation"}. The
  emissivity $\epsilon$ is specified using either \tvar{THERMAL_BC}{emissivity}
  for a constant value or \tvar{THERMAL_BC}{emissivity_func} for a function, and
  the temperature of the ambient environment $T_\infty$ is specified using either
  \tvar{THERMAL_BC}{ambient_temp} for a constant value, or
  \tvar{THERMAL_BC}{ambient_temp_func} for a function.
  Here $\sigma$ is the Stefan-Boltzmann constant and $T_0$ is the absolute-zero
  temperature, both of which can be redefined if the problem units differ from
  the default SI units using the \tvar{PHYSICAL_CONSTANTS}{Stefan_Boltzmann}
  and \tvar{PHYSICAL_CONSTANTS}{Absolute_Zero} components of the
  \tnamelist{PHYSICAL_CONSTANTS} namelist.
\end{itemize}

The specified boundary conditions are not generally allowed to overlap.
It is not permitted, for example, to imposed both a temperature and
a flux condition on the same part of boundary.  The one exception is that
heat transfer and ambient radiation conditions can be superimposed; the net
flux in this case will be the sum of the heat transfer and radiation fluxes.

It is also generally required that the specified boundary conditions completely
cover the computational boundary. However, when enclosure radiation systems are
present no boundary condition need be imposed on any part of the boundary that
belongs to an enclosure. Either temperature or heat transfer conditions may
still be imposed there, and in the latter case the net heat flux is the sum
of the heat transfer and radiative (from enclosure radiation) fluxes.

\subsubsection*{Internal interfaces}
Internal interfaces are merely coincident pairs of conforming external mesh
boundaries. These are modifications to the mesh created by Truchas and are
defined using the \tvar{MESH}{Interface_Side_Sets} parameter from the
\tnamelist{MESH} namelist. Only the face set IDs referenced there can be used
in the definition of the following interface conditions. The following types
of internal interface conditions can be defined.
\begin{itemize} \setlength{\itemsep}{0pt}
\item \textit{Interface Heat Transfer.}
  An interface heat transfer condition models heat transfer across an
  imperfect contact between two bodies or across a thin subscale material
  layer lying along an interface $\Gamma$. It imposes continuity of the
  heat flux $-\kappa\nabla T\cdot\hat{n}$ across the interface $\Gamma$
  and gives this flux as
  \begin{equation}
    -\kappa\nabla T\cdot\hat{n} = - \alpha [T] \text{ on $\Gamma$,}
  \end{equation}
  where $[T]$ is the jump in $T$ across $\Gamma$ in the direction $\hat{n}$.
  It is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"interface-htc"}.
  The heat transfer coefficient $\alpha$ is specified using either
  \tvar{THERMAL_BC}{htc} for a constant value, or
  \tvar{THERMAL_BC}{htc_func} for a function.
\item \textit{Gap Radiation.} 
  A gap radiation condition models radiative heat transfer across a thin open
  gap lying along an interface $\Gamma$. It imposes continuity of the heat flux
  $-\kappa\nabla T\cdot\hat{n}$ across $\Gamma$ and gives the flux as
  \begin{equation}
    -\kappa\nabla T\cdot\hat{n} =
        \epsilon_{\Gamma}\sigma\bigl((T_{\!-}-T_0)^4-(T_{\!+}-T_0)^4\bigr)
        \text{ on $\Gamma$,}
  \end{equation}
  where $T_{\!-}$ and $T_{\!+}$ denote the values of $T$ on the inside and
  outside gap surfaces with respect to the normal $\hat{n}$ to $\Gamma$.
  It is defined by setting \tvar{THERMAL_BC}{type} to \texttt{"gap-radiation"}.
  The gap emissivity $\epsilon_{\Gamma}$ is specified using either
  \tvar{THERMAL_BC}{emissivity} for a constant value, or
  \tvar{THERMAL_BC}{emissivity_func} for a function.
  The effective gap emissivity $\epsilon_{\Gamma}$ depends on the emissivities
  $\epsilon_{-}$ and $\epsilon_+$ of the surfaces on either side of the gap
  and is given by
  \begin{equation}
    \epsilon_{\Gamma} = \frac{\epsilon_- \epsilon_+}%
      {\epsilon_- +\epsilon_+ - \epsilon_- \epsilon_+}.
  \end{equation}
  The value of the Stefan-Boltzmann constant $\sigma$ and the absolute-zero
  temperature $T_0$ can be redefined if the problem units differ from the
  default SI units using the \tvar{PHYSICAL_CONSTANTS}{Stefan_Boltzmann} and
  \tvar{PHYSICAL_CONSTANTS}{Absolute_Zero} components of the
  \tnamelist{PHYSICAL_CONSTANTS} namelist.
\end{itemize}

\section*{\texttt{THERMAL_BC} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{THERMAL_BC}{name}
\item \tvar{THERMAL_BC}{face_set_ids}
\item \tvar{THERMAL_BC}{type}
\item \tvar{THERMAL_BC}{temp}
\item \tvar{THERMAL_BC}{temp_func}
\item \tvar{THERMAL_BC}{flux}
\item \tvar{THERMAL_BC}{flux_func}
\item \tvar{THERMAL_BC}{htc}
\item \tvar{THERMAL_BC}{htc_func}
\item \tvar{THERMAL_BC}{ambient_temp}
\item \tvar{THERMAL_BC}{ambient_temp_func}
\item \tvar{THERMAL_BC}{emissivity}
\item \tvar{THERMAL_BC}{emissivity_func}
\end{itemize}


\section*{\texttt{name}}
\tvardef{THERMAL_BC}{name}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name used to identify a particular instance of this namelist.
\item[Type:] string (31 characters max)
\item[Default:] none
\end{description}


\section*{\texttt{face_set_ids}}
\tvardef{THERMAL_BC}{face_set_ids}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of face set IDs that define the portion of the boundary where the
  boundary condition will be imposed.
\item[Type:] integer list (32 max)
\item[Default:] none
\end{description}


\section*{\texttt{type}}
\tvardef{THERMAL_BC}{type}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The type of boundary condition. The available options are:
  \begin{description}\setlength{\itemsep}{0pt}
  \item[\texttt{"temperature"}]
    Temperature is prescribed on the boundary. Use \tvar{THERMAL_BC}{temp} or
    \tvar{THERMAL_BC}{temp_func} to specify its value.
  \item[\texttt{"flux"}]
    Outward heat flux is prescribed on the boundary. Use \tvar{THERMAL_BC}{flux}
    or \tvar{THERMAL_BC}{flux_func} to set its value.
  \item[\texttt{"htc"}]
    External heat transfer condition. Use \tvar{THERMAL_BC}{htc} or
    \tvar{THERMAL_BC}{htc_func} to set the heat transfer coefficient, and
    \tvar{THERMAL_BC}{ambient_temp} or \tvar{THERMAL_BC}{ambient_temp_func}
    to set the ambient temperature.
  \item[\texttt{"radiation"}]
    A simple ambient thermal radiation condition. Use \tvar{THERMAL_BC}{emissivity} or
    \tvar{THERMAL_BC}{emissivity_func} to set the emissivity, and
    \tvar{THERMAL_BC}{ambient_temp} or \tvar{THERMAL_BC}{ambient_temp_func} to
    set the temperature of the ambient environment.
  \item[\texttt{"interface-htc"}]
    An internal interface heat transfer condition. Use \tvar{THERMAL_BC}{htc}
    or \tvar{THERMAL_BC}{htc_func} to set the heat transfer coefficient.
  \item[\texttt{"gap-radiation"}]
    A gap thermal radiation condition. Use \tvar{THERMAL_BC}{emissivity} or
    \tvar{THERMAL_BC}{emissivity_func} to set the emissivity.
  \end{description}
\item[Type:] string
\item[Default:] none
\end{description}


\section*{\texttt{temp}}
\tvardef{THERMAL_BC}{temp}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of boundary temperature for a temperature-type boundary
  condition. To specify a function, use \tvar{THERMAL_BC}{temp_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{temp_func}}
\tvardef{THERMAL_BC}{temp_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the boundary temperature for a temperature-type boundary condition. The
  function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{flux}}
\tvardef{THERMAL_BC}{flux}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the outward boundary heat flux for a flux-type boundary
  condition. To specify a function, use \tvar{THERMAL_BC}{flux_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{flux_func}}
\tvardef{THERMAL_BC}{flux_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the outward boundary heat flux for a flux-type boundary condition. The
  function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{htc}}
\tvardef{THERMAL_BC}{htc}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the heat transfer coefficient for either an external
  or interface heat transfer-type boundary condition. To specify a function,
  use \tvar{THERMAL_BC}{htc_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{htc_func}}
\tvardef{THERMAL_BC}{htc_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the heat transfer coefficient for either an external or interface heat
  transfer-type boundary condition. The function is expected to be a function
  of $(t,x,y,z)$ for an external heat transfer-type boundary condition, and
  a function of $(T,t,x,y,z)$ for an interface heat transfer-type boundary
  condition. In the latter case $T$ is taken to be the maximum of the two
  temperatures on either side of the interface.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{ambient_temp}}
\tvardef{THERMAL_BC}{ambient_temp}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the ambient temperature for external heat transfer or
  radiation-type boundary condition. To specify a function, use
  \tvar{THERMAL_BC}{ambient_temp_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{ambient_temp_func}}
\tvardef{THERMAL_BC}{ambient_temp_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the ambient temperature for external heat transfer or radiation-type boundary
  condition. The function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{emissivity}}
\tvardef{THERMAL_BC}{emissivity}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of emissivity for a radiation-type boundary condition.
  To specify a function, use \tvar{THERMAL_BC}{emissivity_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{emissivity_func}}
\tvardef{THERMAL_BC}{emissivity_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the emissivity for a radiation-type boundary condition. The function is
  expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}
