\chapter{\texttt{ENCLOSURE_RADIATION} Namelist}
\tnamelistdef{ENCLOSURE_RADIATION}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}
\todo[inline]{Need an overview of the namelist.}


\section*{\texttt{ENCLOSURE_RADIATION} Namelist Features}
\begin{description}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple, one for each enclosure.
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{ENCLOSURE_RADIATION}{Name}
\item \tvar{ENCLOSURE_RADIATION}{Enclosure_File}
\item \tvar{ENCLOSURE_RADIATION}{Coord_Scale_Factor}
\item \tvar{ENCLOSURE_RADIATION}{Skip_Geometry_Check}
\item \tvar{ENCLOSURE_RADIATION}{Ambient_Constant}
\item \tvar{ENCLOSURE_RADIATION}{Ambient_Function}
\item \tvar{ENCLOSURE_RADIATION}{Error_Tolerance}
\item \tvar{ENCLOSURE_RADIATION}{Precon_Method}
\item \tvar{ENCLOSURE_RADIATION}{Precon_Iter}
\item \tvar{ENCLOSURE_RADIATION}{Precon_Coupling_Method}
\item \tvar{ENCLOSURE_RADIATION}{toolpath}
\end{itemize}


\section*{\texttt{Name}}
\tvardef{ENCLOSURE_RADIATION}{Name}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name for this enclosure radiation system.
\item[Type:] string (31 characters max)
\item[Default:] none
\end{description}


\section*{\texttt{Enclosure_File}}
\tvardef{ENCLOSURE_RADIATION}{Enclosure_File}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The path to the enclosure file.  This is interpreted relative to the
  Truchas input file unless this is an absolute path.  If operating in
  moving enclosure mode (see \ivar{toolpath}) the file name is the base
  name for a collection of enclosure files.
\item[Type:] string (255 characters max)
\item[Default:] none
\item[Notes:]
  The \texttt{genre} program from the \textsc{RadE} tool suite can be used
  to generate this file.
\end{description}


\section*{\texttt{Coord_Scale_Factor}}
\tvardef{ENCLOSURE_RADIATION}{Coord_Scale_Factor}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  An optional factor with which to scale the node coordinates of the
  enclosure surface.
\item[Type:] real
\item[Default:] $1.0$
\item[Valid values:] $>0.0$
\item[Notes:]
  The faces of the enclosure surface must match faces from the Truchas mesh.
  If the coordinates of the mesh are being scaled, it is likely that the same
  scaling needs to be applied to the enclosure surface.
\end{description}


\section*{\texttt{Ambient_Constant}}
\tvardef{ENCLOSURE_RADIATION}{Ambient_Constant}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the ambient environment temperature.
\item[Physical dimension:] \AUtemperature
\item[Type:] real
\item[Default:] none
\item[Valid values:] $\ge{}$\tvar{PHYSICAL_CONSTANTS}{Absolute_Zero}
\item[Notes:]
  Either \texttt{Ambient_Constant} or
  \tvar{ENCLOSURE_RADIATION}{Ambient_Function} must be specified, but not
  both.  Currently this is necessary even for full enclosures, although in
  that case the value will not be used and any value is acceptable.
\end{description}


\section*{\texttt{Ambient_Function}}
\tvardef{ENCLOSURE_RADIATION}{Ambient_Function}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist that defines the ambient
  environment temperature function.  That function is expected to be a
  function of $t$ alone.
\item[Type:] string
\item[Default:] none
\item[Valid values:]
\item[Notes:]
  Either \texttt{Ambient_Function} or
  \tvar{ENCLOSURE_RADIATION}{Ambient_Constant} must be specified, but not
  both.  Currently this is necessary even for full enclosures, although in
  that case the value will not be used and any constant value is acceptable.
\end{description}


\section*{\texttt{Error_Tolerance}}
\tvardef{ENCLOSURE_RADIATION}{Error_Tolerance}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The error tolerance $\epsilon$ for the iterative solution of the linear
  radiosity system $Aq=b$.  Iteration stops when the approximate radiosity
  $q$ satisfies $||b-Aq||_2<\epsilon||b||_2$.
\item[Type:] real
\item[Default:] \texttt{1.0e-3}
\item[Valid values:] $>0$
\item[Notes:]
  The Chebyshev iterative method is used when solving the radiosity system 
  in isolation with given surface temperatures.  However the usual case has
  the radiosity system as just one component of a larger nonlinear system
  that is solved by a Newton-like iteration, and this condition on the
  radiosity component is one necessary condition of the complete stopping
  criterion of the iteration.
\end{description}


\section*{\texttt{toolpath}}
\tvardef{ENCLOSURE_RADIATION}{toolpath}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{TOOLPATH} namelist that defines a time-dependent
  motion that has been suitably partitioned. If this variable is specified
  it enables the moving enclosure mode of operation. This works in concert
  with the \texttt{genre} program.
\end{description}


\section*{\texttt{Precon_Method}\quad(Expert Parameter)}
\tvardef{ENCLOSURE_RADIATION}{Precon_Method}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Preconditioning method for the radiosity system.
  \emph{Use the default.}
\item[Type:] string
\item[Default:] \texttt{"jacobi"}
\item[Valid values:] \texttt{"jacobi"}, \texttt{"chebyshev"}
\item[Notes:]
  The preconditioner for the fully-coupled heat transfer/enclosure radiation
  system NLK solver is built from smaller preconditioning pieces, one of which
  is a preconditioner for the radiosity system alone.  The least costly and
  seemingly most effective is Jacobi.
\end{description}


\section*{\texttt{Precon_Iter}\quad(Expert Parameter)}
\tvardef{ENCLOSURE_RADIATION}{Precon_Iter}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The number of iterations of the \tvar{ENCLOSURE_RADIATION}{Precon_Method}
  method to apply as the radiosity system preconditioner.
  \emph{Use the default.}
\item[Type:] integer
\item[Default:] 1
\item[Valid values:] $\ge1$
\end{description}


\section*{\texttt{Precon_Coupling_Method}\quad(Expert Parameter)}
\tvardef{ENCLOSURE_RADIATION}{Precon_Coupling_Method}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Method for coupling the radiosity and heat transfer
  system preconditioners.
  \emph{Use the default.}
\item[Type:] string
\item[Default:] \texttt{"backward GS"}
\item[Valid values:] \texttt{"jacobi"}, \texttt{"forward GS"},
                     \texttt{"backward GS"}, \texttt{"factorization"}
\item[Notes:]
  There are several methods for combining the independent preconditionings
  of the radiosity system and heat transfer system to obtain a preconditioner
  for the fully-coupled system.  If we view it as a block system with the
  the radiosity system coming first, the first three methods correspond to
  block Jacobi, forward block Gauss-Seidel, and backward Gauss-Seidel updates.
  The factorization method is an approximate Schur complement update, that
  looks like block forward Gauss-Seidel followed by the second half of block
  backward Gauss-Seidel.
\end{description}


\section*{\texttt{Skip_Geometry_Check}\quad(Expert Parameter)}
\tvardef{ENCLOSURE_RADIATION}{Skip_Geometry_Check}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Normally the geometry of the enclosure surface faces are compared with
  boundary faces of the heat conduction mesh to ensure they actually match.
  Setting this variable to false will disable this check, which may be
  necessary in some unusual use cases.
\item[Type:] \logical
\item[Default:] \texttt{.false.}
\end{description}
