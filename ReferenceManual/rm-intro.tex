\chapter{Introduction}

\section{Invoking Truchas}

Truchas is executed in serial using a command of the form
\begin{Verbatim}[frame=single,commandchars=\\\{\}]
 truchas [-h] [-d[:\textit{n}]] [-o:\textit{outdir}] [-r:\textit{rstfile}] \textit{infile}
\end{Verbatim}
assuming \verb|truchas| is the name of the executable.
The brackets denote optional arguments that are described in
Table~\ref{cl-options}.  The only required argument is the path to the input
file \texttt{\textit{infile}}.  This file name must end with the extension
``\texttt{.inp}'' (without the quotes).  The general format of the input file
is described in the next section, and the following chapters describe the
various Fortran namelists that go into the input file to describe the problem
to be simulated.

All of the output files are written to directory whose name is generated
from the base name of the input file.  For example, if the input file is
\texttt{myprob.inp}, the output directory will be named \verb|myprob_output|.
The name of the output directory can be overridden using the \verb|-o| option.
The directory will be created if necessary.

The precise manner of executing Truchas in parallel depends on the MPI
implementation being used.  This may be as simple as prefixing the serial
invocation above with ``\Verb|mpirun -np n|'', where \verb|n| is the number
of processes.  But this varies widely and providing specific instructions is
beyond the scope of this document. There is no difference in the Truchas 
arguments between serial and parallel, however.

\begin{table}[h]
\caption{Truchas command line options}\label{cl-options}
\begin{tabularx}{\linewidth}{lX}
\hline
Option & Description \\
\hline
\verb|-h| & Print a usage summary of the command line options and exit. \\
\verb|-d[:|\texttt{\textit{n}}\verb|]| &
   Sets the debug output level \texttt{\textit{n}}. The default level is 0,
   which produces no debug output, with levels 1 and 2 producing progressively
   more debug output.  \verb|-d| is equivalent to \verb|-d:1|. \\
\verb|-o:|\texttt{\textit{outdir}} &
  Causes all output files to be written to the directory \texttt{\textit{outdir}}
  instead of the default directory.  The directory is created if necessary. \\
\verb|-r:|\texttt{\textit{rstfile}} &
  Executes in restart mode, restarting from the data in the file
  \texttt{\textit{rstfile}}.  This file is generated from the output of a
  previous Truchas simulation using post-processing utilities. \\
\hline
\end{tabularx}
\end{table}

\subsection{Stopping Truchas}
There are occasions where one would like to gracefully terminate a running
Truchas simulation before it has reached the final simulation time given
in the input file.  This is easily done by sending the running process
the \verb|SIGURG| signal:
\begin{Verbatim}[frame=single,commandchars=\\\{\}]
 kill -s SIGURG \textit{pid}
\end{Verbatim}
where \texttt{\textit{pid}} is the process id.  When Truchas receives
this signal, it continues until it reaches the end of the current time step,
where it writes the final solution and then exits normally.


\begin{figure}[t]
\begin{Verbatim}[frame=single]
 This is a comment.  Anything outside a namelist input is ignored.

 &MESH
   ! Within a namelist input "!" introduces a comment.
   mesh_file = "my-big-mesh.exo" ! character string value
 /

 Another comment.

 &PHYSICS
   heat_conduction = .true.     ! logical values are .true./.false.
   body_force = 0.0, 0.0, -9.8  ! assigning values to an array
   !This would be an equivalent method ...
   !body_force(1) =  0.0
   !body_force(2) =  0.0
   !body_force(3) = -9.8
 /

 Newlines in a namelist are optional.
 &PHYSICAL_CONSTANTS stefan_boltzmann=0.1, absolute_zero=0.0 /
\end{Verbatim}
\caption{Fragment of a Truchas input file illustrating namelist input syntax.}
\label{fig:inpfile}
\end{figure}

\section{Input File Format}

The Truchas input file is composed of a sequence of Fortran namelist inputs.
Each namelist input has the form
\begin{Verbatim}[frame=single,commandchars=\\\{\},fontfamily=auto]
 &\textrm{\textit{namelist-group-name}}
   \textrm{\textit{namelist-input-body}}
 /
\end{Verbatim}
The input begins with a line where the first nonblank is the character \&
immediately followed by the name of the namelist group.  The input continues
until the / character.  The body of the namelist input consists of a sequence
of \textit{name} = \textit{value} pairs, separated by commas or newlines, that
assign values to namelist variables.  Namelist input is a feature of Fortran
and a complete description of the syntax can be found in any Fortran reference,
however the basic syntax is very intuitive and a few examples like those in
Fig.~\ref{fig:inpfile} should suffice to explain its essentials.

The namelists and the variables they contain are described in the following
chapters.  A particular namelist will be required or optional, and it may
appear only once or multiple times.  Not all variables of a namelist need
to be specified.  Some may not be relevant in a given context, and others
may have acceptable default values; only those that are used and need to be
assigned a value need to be specified.

The order of the namelist inputs in the input file is not significant;
they may appear in any order.  Any text outside of a namelist input is
ignored and can be regarded as a comment; see Fig.~\ref{fig:inpfile}.

Fortran is case-insensitive when interpreting the namelist group names
and variable names; they may be written in any mixture of upper and lower
case.  However character string \emph{values}, which are interpreted by
Truchas, are case-sensitive unless documented otherwise.

In the event of a namelist input syntax error, Truchas will report that
it was unable to read the namelist, but unfortunately it is not able to
provide any specific information about the error because Fortran does not
make such information available.  In such cases the user will need to scan
the namelist input for syntax errors: look for misspelt variable names,
variables that don't belong to the namelist, blank written in place of an
underscore, etc.


\section{Physical Units}

Truchas does not require the use any particular system of physical units, nor
does it provide a means for the user to specify the dimension of a numerical
value.  The user is simply expected to ensure that all input values are given
using a consistent system of units.  To assist in this end, the dimension for
all dimensional quantities is documented using the following abstract units:
mass \AUmass, length \AUlength, time \AUtime, thermodynamic temperature
\AUtemperature, and electric current \AUcurrent.  Thus mass density, for
example, will be documented as having dimension \AUmass\per\AUvolume. The
following derived abstract units are also used: force \AUforce\ ($={}$\AUforcebase)
and energy \AUenergy\ ($={}$\AUenergybase).

There are a few physical constants, like the Stefan-Boltzmann constant, that
have predefined values in SI units.  These constants are referenced by a few
specific models, and where a model uses one of these constants this fact is
noted.  Use the \tnamelist{PHYSICAL_CONSTANTS} namelist to redefine the value
of these constants where necessary.


\section{Working With Output Files}

As described earlier, Truchas writes its output files to the directory named
in the \verb|-o| option, or if omitted, to a directory whose name is generated
from the base name of the input file: \verb|myprob_output| if \verb|myprob.inp|
is the input file, for example.  Two primary files are written, a \verb|.log|
file that is a copy of the terminal output, and a \verb|.h5| HDF5 file that
contains all the simulation results.  HDF5 is a widely-used format for storing
and managing data, and there are a great many freely-available tools for working
with these files.  In this release, which is the first to feature HDF5 output,
we provide only a few essential tools, described below, for processing the
\verb|.h5| file.  We expect to provide additional tools in future releases.

\subsection{\texttt{write_restart}}
The program \verb|write_restart| is used to create Truchas restart files
using data from an \verb|.h5| output file.  The command syntax is
\begin{quote}
\begin{Verbatim}[commandchars=\\\{\}]
write_restart [\textit{options}] \textit{H5FILE}
\end{Verbatim}
\end{quote}
where \texttt{\textit{H5FILE}} is the \verb|.h5| output file and the possible
options are
\begin{quote}
\begin{tabularx}{\linewidth}{@{}>{\ttfamily}lX@{}}
  -h & Display usage help and exit. \\
  -l & Print a list of the available cycles from which the restart file can be
       created.  No restart file is written. \\
  -n \textit{N} & Data from cycle \texttt{\textit{N}} is used to create the
      restart file; if not specified, the last cycle is used. \\
  -o \textit{FILE} & Write restart data to \texttt{\textit{FILE}}.  If not
      specified, \texttt{\textit{FILE}} is taken to be the
      \texttt{\textit{H5FILE}} name with the \verb|.h5| suffix replaced by
      \texttt{.restart.\textit{N}} where \texttt{\textit{N}} is the cycle
      number. \\
  -m \textit{FILE} & Create a mapped restart file using the specified ExodusII
      mesh \texttt{\textit{FILE}} as the target mesh. \\
  -s \textit{FLOAT} & Scale the mapped restart mesh by the factor
      \texttt{\textit{FLOAT}}. \\
\end{tabularx}\end{quote}
\todo[inline]{Need a discussion of what mapped restarts are and the limitations.}

\subsection{\texttt{write_probes}}
The \verb|write_probes| utility extracts probe data (see the \tnamelist{PROBE}
namelist) from an \verb|.h5| output file and writes it to the terminal (where
it can be redirected as needed) in a multicolumn format suitable for many line plotting
programs.  The command syntax is
\begin{quote}
\begin{Verbatim}[commandchars=\\\{\}]
write_probes \{ -h | -l | -n \textit{N} \} \textit{H5FILE}
\end{Verbatim}
\end{quote}
where  \texttt{\textit{H5FILE}} is the \verb|.h5| output file and the available
options are
\begin{quote}
\begin{tabularx}{\linewidth}{@{}>{\ttfamily}lX@{}}
%\begin{tabular}{@{}>{\ttfamily}ll@{}}
  -h & Display usage help and exit. \\
  -l & Print a list of the available probes. \\
  -n \textit{N} & Data for probe index \texttt{\textit{N}} is written.
\end{tabularx}
%\end{tabular}
\end{quote}

\subsection{\texttt{truchas-gmv-parser.py}}
The python script \verb|truchas-gmv-parser.py| is used to create input files
for the GMV visualization tool.  Formerly distributed gratis,
GMV has been commercialized (\url{http://www.generalmeshviewer.com}).
Earlier free versions of the tool can still be found on the internet, however,
and it remains available within LANL.  The command syntax is
\begin{quote}
\begin{Verbatim}[commandchars=\\\{\}]
python truchas-gmv-parser.py [\textit{options}] \textit{H5FILE}
\end{Verbatim}
\end{quote}
where \texttt{\textit{H5FILE}} is the \verb|.h5| output file.  Use the option
\verb|-h| to get a full list of the available options.


\todo[inline]{Need documentation for the RadE tool suite.}
