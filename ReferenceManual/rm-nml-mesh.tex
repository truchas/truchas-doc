\chapter{\texttt{MESH} Namelist}
\tnamelistdef{MESH}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{MESH} namelist specifies the common mesh used by all physics
models other than the induction heating model, which uses a separate
tetrahedral mesh specified by the \tnamelist{ALTMESH} namelist.
For simple demonstration problems, a rectilinear hexahedral mesh of a brick
domain can be defined, but for most applications the mesh will need to be
generated beforehand by some third party tool or tools and saved as a file that
Truchas will read. At this time Exodus~II \cite{exodus} is the only supported
mesh format (also sometimes known as Genesis). This well-known format is used
by some mesh generation tools (Cubit \cite{cubit}, for example) and utilities
exist for translating from other formats to Exodus~II. The unstructured 3D mesh
may be a general mixed-element mesh consisting of non-degenerate hexehedral,
tetrahedral, pyramid, and wedge/prism elements. The Exodus~II format supports
a partitioning of the elements into \emph{element blocks} and also supports
the definition of \emph{side sets}, which are collections of oriented element
faces that describe mesh surfaces, either internal or boundary. Extensive use
is made of this additional mesh metadata in assigning materials, initial
conditions, boundary conditions, etc., to the mesh.

\section*{\texttt{MESH} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\subsection*{External mesh file}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{MESH}{mesh_file}
\item \tvar{MESH}{interface_side_sets}
\item \tvar{MESH}{gap_element_blocks}
\item \tvar{MESH}{exodus_block_modulus}
\end{itemize}
\subsection*{Internally generated mesh}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{MESH}{x_axis}
\item \tvar{MESH}{y_axis}
\item \tvar{MESH}{z_axis}
\item \tvar{MESH}{noise_factor}
\end{itemize}
\subsection*{Common parameters}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{MESH}{coordinate_scale_factor}
\item \tvar{MESH}{rotation_angles}
\item \tvar{MESH}{partitioner}
\item \tvar{MESH}{partition_file}
\item \tvar{MESH}{first_partition}
\end{itemize}


\section*{External Mesh File}
In typical usage, the mesh will be read from a specified Exodus~II mesh
file. Other input variables that follow specify optional modifications
that can be made to the mesh after it is read.

\tvardef{MESH}{mesh_file}
\section*{\texttt{mesh_file}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the path to the Exodus~II mesh file.  If not an absolute path,
  it will be interpreted as a path relative to the Truchas input file
  directory.
\item[Type:] case-sensitive string
\item[Default:] none
\end{description}


\tvardef{MESH}{interface_side_sets}
\section*{\texttt{interface_side_sets}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of side set IDs from the ExodusII mesh identifying internal mesh
  surfaces that will be treated specially by the heat/species transport solver.
\item[Type:] integer list
\item[Default:] An empty list of side set IDs.
\item[Valid values:] Any side set ID whose faces are internal to the mesh.
\item[Notes:]  The heat/species transport solver requires that boundary
  conditions are imposed along the specified surface.  Typically these will
  be interface conditions defined by \tnamelist{THERMAL_BC}
  namelists, but in unusual use cases they could also be external boundary
  conditions defined by the same namelists.  In the
  latter case it is necessary to understand that the solver views the mesh as
  having been sliced open along the specified internal surfaces creating
  matching pairs of additional external boundary and, where interface
  conditions are not imposed, boundary conditions must be imposed on
  \emph{both} sides of the interface.  
\end{description}


\tvardef{MESH}{gap_element_blocks}
\section*{\texttt{gap_element_blocks}\quad(deprecated)}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A list of element block IDs from an Exodus~II mesh that
  are to be treated as gap elements.
\item[Type:] integer list
\item[Default:] An empty list of element block IDs.
\item[Valid values:] Any element block ID.
\item[Notes:] Any element block ID in the mesh file can be specified, 
  but elements that are not connected such that they can function as gap
  elements or are not consistent with side set definitions will almost
  certainly result in incorrect behavior.  The code does not check for
  these inconsistencies. \par
  The heat/species transport solver drops these elements from its view of
  the mesh and treats them instead as an internal interface; see the notes
  to \tvar{MESH}{interface_side_sets}. The block IDs specified here can be
  used as values for \tvar{THERMAL_BC}{face_set_ids} from the
  \tnamelist{THERMAL_BC} namelist.
\end{description}


\tvardef{MESH}{exodus_block_modulus}
\section*{\texttt{exodus_block_modulus}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  When importing an Exodus~II mesh, the element block IDs are replaced by their
  value modulo this parameter. Set the parameter to 0 to disable this procedure.
\item[Type:] integer
\item[Default:] 10000
\item[Valid values:] $\ge0$
\item[Notes:]
  This parameter helps solve a problem posed by mixed-element meshes created
  by Cubit and Trelis.  In those tools a user may define an element block
  comprising multiple element types. But when exported in the Exodus~II format,
  which doesn't support blocks with mixed element types, the element block
  will be written as multiple Exodus~II blocks, one for each type of element.
  One of the blocks will retain the user-specified ID of the original block.
  The IDs of the others will be that ID plus an offset specific to the element
  type.  For example, if the original block ID was 1, hexahedra in the block
  will be written to a block with ID 1, tetrahedra to a block with ID 10001,
  pyramids to a block with ID 100001, and wedges to a block with ID 200001.
  These are the default offset values, and they can be set in Cubit/Trelis;
  see their documentation for details on how the IDs are generated.  It is
  important to note that this reorganization of element blocks occurs silently
  and so the user may be unaware that it has happened.  In order to reduce the
  potential for input errors, Truchas will by default convert the block IDs to
  congruent values modulo $N$ in the interval $[1, N-1]$ where $N$ is the value
  of this parameter.  The default value $10000$ is appropriate for the default
  configuration of Cubit/Trellis, and restores the original user-specified
  block IDs.  Note that this effectively limits the range of element block IDs
  to $[1,N-1]$.
  
  The element block IDs are modified immediately after reading the file.
  Any input parameters that refer to block IDs must refer to the modified IDs.
\end{description}


\section*{Internally Generated Mesh}
A rectilinear hexahedral mesh for a brick domain $[x_\text{min},x_\text{max}]
\times[y_\text{min},y_\text{max}]\times[z_\text{min},z_\text{max}]$ can be
generated internally as part of a Truchas simulation using the following
input variables. The mesh is the tensor product of 1D grids in each of the
coordinate directions. Each coordinate grid is defined by a coarse grid
whose intervals are subdivided into subintervals, optionally with biased sizes.
The generated Exodus~II mesh consists of a single element block with ID~1,
and a side set is defined for each of the six sides of the domain with IDs~1
through 6 for the $x=x_\text{min}$, $x=x_\text{max}$, $y=y_\text{min}$,
$y=y_\text{max}$, $z=z_\text{min}$, and $z=z_\text{max}$ sides, respectively.
Note that while the mesh is formally structured, it is represented internally
as a general unstructured mesh.

\section*{\texttt{x_axis}, \texttt{y_axis}, \texttt{z_axis}}
\tvardef{MESH}{x_axis}\tvardef{MESH}{y_axis}\tvardef{MESH}{z_axis}
%\begin{description}\setlength{\itemsep}{0pt}
%\item[Description:]
  Data that describes the grid in each of the coordinate directions. The
  tensor product of these grids define the nodes of the 3D mesh. The data
  for each coordinate grid consists of these three component arrays:
  \begin{description}
    \item[\texttt{\%coarse_grid}:]
      A strictly increasing list of two or more real values that define the
      points of the coarse grid for the coordinate direction. The first and
      last values define the extent of the domain in this direction.
    \item[\texttt{\%intervals}:]
      A list of postive integers defining the number of subintervals into
      which each corresponding coarse grid interval should be subdivided.
      The number of values must be one less than the number of coarse grid
      points.
    \item[\texttt{\%ratio}:]
      An optional list of positive real values that define the ratio of the
      lengths of successive subintervals for each coarse grid interval. The
      default is to subdivide into equal length subintervals. If specified,
      the number of values must be one less than the number of coarse grid
      points.
  \end{description}
  See \FIG{rect-mesh} for an example.
%\item[Type:] real
%\item[Default:] none
%\end{description}


\begin{figure}\LFIG{rect-mesh}
\newlength{\Colsep}
\setlength{\Colsep}{20pt}
\begin{minipage}[t]{\textwidth}
\begin{minipage}[c]{\dimexpr0.48\textwidth-0.5\Colsep\relax}
\includegraphics[width=\linewidth]{figures/grid-plot.png}
\end{minipage}\hfil
\begin{minipage}[c]{\dimexpr0.52\textwidth-0.5\Colsep\relax}
\begin{Verbatim}[frame=single,fontsize=\small]
 x_axis%coarse_grid = 0.0, 0.67, 1.33, 2.0
 x_axis%intervals   = 3, 1, 4
 x_axis%ratio       = 1.3, 1.0, 0.7
 y_axis%coarse_grid = 0.0, 0.5, 1.0
 y_axis%intervals   = 1, 3
 y_axis%ratio       = 0.7
 z_axis%coarse_grid = 0.0, 1.0
 z_axis%intervals   = 1
\end{Verbatim}
\end{minipage}
\end{minipage}
\caption{Top $xy$ surface of the rectilinear mesh generated by the
         example input shown.}
\end{figure}


\section*{\texttt{noise_factor}\quad(expert)}%
\tvardef{MESH}{noise_factor}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  If specified with a positive value, the coordinates of each mesh node
  will be perturbed by uniformly distributed random amount whose magnitude
  will not exceed this value times the local cell size at the node. Nodes
  on the boundary are not perturbed in directions normal to the boundary.
  This is only useful for testing.
\item[Valid values:] $\in[0, 0.3]$
\item[Default:] 0
\end{description}


\section*{Common Variables}

The following variables apply to both types of meshes.

\tvardef{MESH}{coordinate_scale_factor}
\section*{\texttt{coordinate_scale_factor}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  An optional factor by which to scale all mesh node coordinates.
\item[Type:] real
\item[Default:] 1.0
\item[Valid values:] $>0$
\end{description}

\tvardef{MESH}{rotation_angles}
\section*{\texttt{rotation_angles}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of 3 angles, given in degrees, specifying the amount of
  counter-clockwise rotation about the x, y, and z-axes to apply to the mesh.
  The rotations are done sequentially in that order. A negative angle
  is a clockwise rotation, and a zero angle naturally implies no rotation.
\item[Type:] real 3-vector
\item[Default:] $(0.0, 0.0, 0.0)$
\end{description}


\tvardef{MESH}{partitioner}
\section*{\texttt{partitioner}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The partitioning method used to generate the parallel
  decomposition of the mesh.
\item[Type:] case-insensitive string
\item[Default:] \texttt{"chaco"}
\item[Valid values:] \texttt{"chaco"}, \texttt{"metis"}, \texttt{"file"}, \texttt{"block"}
\item[Notes:]~
  \begin{description}
  \item[\texttt{"chaco"}]
    uses a graph partitioning method from the Chaco library \cite{chaco}
    to compute the mesh decomposition at run time. This is the standard
    method long used by Truchas.
  \item[\texttt{"metis"}]
    uses the well-known METIS library \cite{metis} to partition the dual
    graph of the mesh at run time. This method has a number of options
    which are described below.
  \item[\texttt{"file"}]
    reads the partitioning of the mesh cells from a disk file; see
    \tvar{MESH}{partition_file}.
  \item[\texttt{"block"}]
    partitions the mesh cells into nearly equal-sized blocks of consecutively
    numbered cells according their numbering in the mesh file.  The quality
    of this naive decomposition entirely depends on the given ordering of mesh
    cells, and thus this option is not generally recommended.
  \end{description}
\end{description}


\tvardef{MESH}{partition_file}
\section*{\texttt{partition_file}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the path to the mesh cell partition file, and is required
  when \tvar{MESH}{partitioner} is \texttt{"file"}. If not an absolute
  path, it will be interpreted as a path relative to the Truchas input
  file directory.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Notes:]
  The format of this text file consists of a sequence of integer values,
  one value or multiple values per line. The first value is the partition
  number of the first cell, the second value the partition number of the
  second cell, and so forth.  The number of values must equal the number
  of mesh cells.  The file may use either a 0-based or 1-based numbering
  convention for the partitions.  Popular mesh partitioning tools typically
  use 0-based partition numbering, and so the default is to assume 0-based
  numbering; use \tvar{MESH}{first_partition} to specify 1-based numbering.
\end{description}


\tvardef{MESH}{first_partition}
\section*{\texttt{first_partition}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Specifies the number given the first partition in the numbering convention
  used in the partition file. Either 0-based or 1-based numbering is allowed. 
\item[Type:] integer
\item[Default:] 0
\item[Valid values:] 0 or 1
\end{description}


\section*{METIS Mesh Partitioning}
When \texttt{"metis"} is specified for \ivar{partitioner}, a graph
partitioning procedure from the METIS library is used to partition
the dual graph of the mesh. This is the graph whose nodes are the
mesh cells and edges are the faces shared by cells. The partitioning
procedures have the following integer-valued options that may be
specified, though all have reasonable defaults so that none must be
specified. See the METIS documentation \cite{metis} for more details
on these options.
\begin{description}\setlength{\itemsep}{0pt}
\item[\ivar{metis_ptype}] specifies the partitioning method. Possible
  values are:
  \begin{itemize}\setlength{\itemsep}{0pt}
  \item[0:] Multilevel recursive bisection (default)
  \item[1:] Multilevel $k$-way partitioning
  \end{itemize}
\item[\ivar{metis_iptype}] specifies the algorithm used during initial
  partitioning (recursive bisection only). Possible values are:
  \begin{itemize}\setlength{\itemsep}{0pt}
  \item[0:] Grows a bisection using a greedy strategy (default)
  \item[1:] Computes a bisection at random followed by a refinement
  \end{itemize}
\item[\ivar{metis_ctype}] specifies the matching scheme to be used during
  coarsening. Possible values are:
  \begin{itemize}\setlength{\itemsep}{0pt}
  \item[0:] Random matching
  \item[1:] Sorted heavy-edge matching (default)
  \end{itemize}
\item[\ivar{metis_ncuts}] specifies the number of different partitionings
  that will be computed. The final partitioning will be the one that achieves
  the best edgecut. The default is 1.
\item[\ivar{metis_niter}] specifies the number of iterations of the
  refinement algorithm at each stage of the uncoarsening process.
  The default is 10.
\item[\ivar{metis_ufactor}] specifies the maximum allowed load imbalance
  among the partitions. A value of $n$ indicates that the allowed load
  imbalance is $(1+n)/1000$. The default is 1 for recursive bisection
  (i.e., an imbalance of $1.001$) and the default value is $30$ for $k$-way
  partitioning (i.e., an imbalance of $1.03$).
\item[\ivar{metis_minconn}] specifies whether the partitioning procedure
  should seek to minimize the maximum degree of the subdomain graph (1); or
  not (0, default). The subdomain graph is the graph in which each partition
  is a node, and edges connect subdomains with a shared interface.
\item[\ivar{metis_contig}] specifies whether the partitioning procedure
  should produce partitions that are contiguous (1); or not (0, default).
  If the dual graph of the mesh is not connected this option is ignored.
\item[\ivar{metis_seed}] specifies the seed for the random number generator.
\item[\ivar{metis_dbglvl}] specifies the amount and type of diagnostic
  information that will be written to stderr by the partitioning procedure.
  The default is 0, no output. Use 1 to write some basic information. Refer
  to the METIS documentation for the many other possible values and the
  output they generate.
\end{description}
