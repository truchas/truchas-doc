\chapter{\texttt{BC} Namelist}
\tnamelistdef{BC}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{BC} namelist is used to define boundary conditions for the
solid mechanics model at external boundaries and internal material interfaces.

The preferred method for specifying the mesh surface where a boundary
condition applies is to reference a side set from the ExodusII-format mesh.
Alternatively, the mesh surface can specified using a conic surface:
\begin{equation}\label{eq:conic}
  0 = p(x,y,z) = c_0 + c_x x + c_y y + c_z z + c_{xx} x^2 + c_{yy} y^2 + 
      c_{zz} z^2 + c_{xy} xy + c_{xz} xz + c_{yz} yz
\end{equation}
A face belongs to the mesh surface whenever its centroid lies on this
surface (see \tvar{BC}{Conic_Tolerance}).  The coefficients are specified
using the \texttt{Conic_*} variables.  Another method for solid mechanics
is to specify nodes.  The
method is selected using \tvar{BC}{Surface_Name}.  The specified surface
may also be restricted to lie within a bounding box.

\section*{\texttt{BC} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{BC}{BC_Name}
\item \tvar{BC}{BC_Table}
\item \tvar{BC}{BC_Type}
\item \tvar{BC}{BC_Value}
\item \tvar{BC}{BC_Variable}
\item \tvar{BC}{Bounding_Box}
\item \tvar{BC}{Conic_Constant}
\item \tvar{BC}{Conic_Tolerance}
\item \tvar{BC}{Conic_X}
\item \tvar{BC}{Conic_XX}
\item \tvar{BC}{Conic_XY}
\item \tvar{BC}{Conic_XZ}
\item \tvar{BC}{Conic_Y}
\item \tvar{BC}{Conic_YY}
\item \tvar{BC}{Conic_YZ}
\item \tvar{BC}{Conic_Z}
\item \tvar{BC}{Conic_ZZ}
\item \tvar{BC}{Mesh_Surface}
\item \tvar{BC}{Node_Disp_Coords}
\item \tvar{BC}{Surface_Name}
\end{itemize}


\tvardef{BC}{BC_Name}
\section*{\texttt{BC_Name}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
A name used to identify a particular instance of this namelist.
\item[Type:] case-sensitive string
\item[Default:] none
\item[Note:] This is optional and used for logging purposes only.
\end{description}


\tvardef{BC}{BC_Variable}
\section*{\texttt{BC_Variable}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of the variable to which this boundary condition applies.
\item[Type:] case-insensitive string
\item[Default:] none
\item[Valid values:]
  \texttt{"displacement"}
\end{description}


\tvardef{BC}{BC_Type}
\section*{\texttt{BC_Type}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The type of boundary condition.
\item[Type:] string
\item[Default:] Depends on \texttt{BC_Variable}:
   \item \texttt{'displacement'}\: \texttt{'x-traction', 'y-traction', 'z-traction'}
   \item[Valid values:] Depends on \texttt{BC_Variable}:
   \item \texttt{'displacement'}\: \texttt{'x-traction', 'y-traction', 'z-traction', \\
   'x-displacement', 'y-displacement', 'z-displacement',\\
   'normal-displacement', 'normal-traction', 'free-interface', \\
   'normal-constraint', 'contact'}
\item[Notes:]
\begin{itemize}
\item The solid mechanics displacement solution defaults to a
traction-free surface with no displacement constraints
(\texttt{'x-traction', 'y-traction',} and \texttt{'z-traction'} set to
zero.)
\item The \texttt{'free-interface'}, \texttt{'normal-constraint'} and {'contact'} 
types can only be specified for interfaces with gap elements.
\end{itemize}
\end{description}


\tvardef{BC}{BC_Value}
\section*{\texttt{BC_Value}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value(s) for the constant(s) used in this BC definition.
  See also \tvar{BC}{BC_Table}.
\item[Physical dimension:] varies
\item[Type:] \real \ (up to 24 values depending on \tvar{BC}{BC_Type})
\item[Default:] 0.0
\item[Notes:]
  The meaning of the items in the BC_Value list depends on the
  particular boundary condition:
\end{description}

\small
\begin{tabular}{|l|l|p{1.5 in}|c|c|} \hline
 \tvar{BC}{BC_Variable} & \tvar{BC}{BC_Type} & \textbf{Value Description} &
 \textbf{Physical} & \textbf{Number} \\
 & & & \textbf{Dimension} & \textbf{of values} \\
 \hline
 \texttt{"displacement"} & \texttt{"x-displacement"} & displacement & \AUlength & 1 \\
 \texttt{"displacement"} & \texttt{"y-displacement"} & displacement & \AUlength & 1 \\
 \texttt{"displacement"} & \texttt{"z-displacement"} & displacement & \AUlength & 1 \\
 \texttt{"displacement"} & \texttt{"x-traction"} & traction (force/area) & \AUforce\per\AUarea & 1 \\
 \texttt{"displacement"} & \texttt{"y-traction"} & traction (force/area) & \AUforce\per\AUarea & 1 \\
 \texttt{"displacement"} & \texttt{"z-traction"} & traction (force/area) & \AUforce\per\AUarea & 1 \\
 \texttt{"displacement"} & \texttt{"normal-displacement"} & displacement & \AUlength & 1 \\
 \texttt{"displacement"} & \texttt{"free-interface"} & not used & --- & 0 \\
 \texttt{"displacement"} & \texttt{"normal-constraint"} & not used & --- & 0 \\
 \texttt{"displacement"} & \texttt{"contact"} & not used & --- & 0 \\
 \hline
\end{tabular}


\tvardef{BC}{Bounding_Box}
\section*{\texttt{Bounding_Box}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The extents in each dimension of a bounding box that restricts the extent
  of the mesh surface where the boundary condition is applied.  This does not
  apply in the case  \tvar{BC}{Surface_Name} is \texttt{"node set"}.
\item[Physical dimension:] \AUlength
\item[Type:] A real array $(x_\text{min}, x_\text{max}, y_\text{min},
  y_\text{max}, z_\text{min}, z_\text{max})$.
\item[Default:] Unlimited in each dimension.
\end{description}


\tvardef{BC}{Conic_Constant}
\section*{\texttt{Conic_Constant}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_0$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


%\section*{\texttt{Conic_Relation}}
%\tvardef{BC}{Conic_Relation}
%
%\begin{description}
%\setlength{\itemsep}{0pt}
%\item[Description:]
%The equality or inequality relation used in the conic equation defining the BC surface.
%See \tvar{BC}{Conic Equation} for the form of the equation.
%\item[Type:] string
%\item[Default:] (none)
%\item[Valid values:] '$=$', '$<$', '$>$'
%\end{description}


\tvardef{BC}{Conic_Tolerance}
\section*{\texttt{Conic_Tolerance}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
A mesh face is considered to lie on the conic surface when the absolute
value of the conic polynomial (\ref{eq:conic}) at the face centroid is less
than the value of this parameter.  Only relevant when using a conic
polynomial to define the boundary condition surface.
\item[Type:] real
\item[Default:] $10^{-6}$
\item[Valid values:] $>0$
\item[Notes:]
  It is important to note that this is not a tolerance on the distance of
  a centroid from the conic surface, but merely a tolerance on the value
  of the conic polynomial.  Its dimension depends on that of the coefficients
  in the polynomial. \par
  Most mesh generators place nodes on a bounding surface.  For non-planar
  surfaces, this has the consequence that face centroids will not lie exactly
  on the surface, making the choice of this tolerance rather significant.
  \todo[inline]{Change the criterion}
\end{description}


\tvardef{BC}{Conic_X}
\section*{\texttt{Conic_X}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_x$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_XX}
\section*{\texttt{Conic_XX}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{xx}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_XY}
\section*{\texttt{Conic_XY}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{xy}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_XZ}
\section*{\texttt{Conic_XZ}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{xz}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_Y}
\section*{\texttt{Conic_Y}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_y$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_YY}
\section*{\texttt{Conic_YY}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{yy}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_YZ}
\section*{\texttt{Conic_YZ}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{yz}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_Z}
\section*{\texttt{Conic_Z}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_z$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Conic_ZZ}
\section*{\texttt{Conic_ZZ}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Value of the coefficient $c_{zz}$ in the conic polynomial (\ref{eq:conic}).
\item[Type:] real
\item[Default:] 0.0
\end{description}


\tvardef{BC}{Mesh_Surface}
\section*{\texttt{Mesh_Surface}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Identifier of a side set defined in the ExodusII-format mesh.
  Only relevant when \tvar{BC}{Surface_Name} is \texttt{"from mesh file"}.
\item[Type:] integer
\item[Default:] none
\end{description}


\tvardef{BC}{Node_Disp_Coords}
\section*{\texttt{Node_Disp_Coords}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  List of points that identify mesh nodes where a displacement
  boundary condition is applied.  Up to 50 points can be specified
  as a list of $(x,y,z)$ coordinates.
  Only relevant when \tvar{BC}{Surface_Name} is \texttt{"node set"}.
\item[Physical dimension:] \AUlength
\item[Type:] real
\end{description}


\tvardef{BC}{Surface_Name}
\section*{\texttt{Surface_Name}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Selects the method of specifying the mesh surface where the boundary
  condition will be applied.
\item[Type:] case-insensitive string
\item[Default:] none
\item[Valid values:]\quad\par%

  \begin{center}\begin{tabularx}{\linewidth}{lX}
    \hline
    Value & Associated variables \\
    \hline
    \texttt{"from mesh file"} & \tvar{BC}{Mesh_Surface}.  Requires that the
        mesh is imported from an ExodusII-format mesh file. \\
    \texttt{"conic"} & \tvar{BC}{Conic_Tolerance}, \tvar{BC}{Conic_Constant},
        \tvar{BC}{Conic_X}, \tvar{BC}{Conic_Y}, \tvar{BC}{Conic_Z},
        \tvar{BC}{Conic_XX}, \tvar{BC}{Conic_YY}, \tvar{BC}{Conic_ZZ},
        \tvar{BC}{Conic_XY}, \tvar{BC}{Conic_XZ}, \tvar{BC}{Conic_YZ} \\
    \texttt{"node set"} & \tvar{BC}{Node_Disp_Coords} \\
    \hline
  \end{tabularx}\end{center}
\end{description}
