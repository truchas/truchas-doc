\chapter{\texttt{PHYSICS} Namelist}
\tnamelistdef{PHYSICS}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{PHYSICS} namelist specifies which physics models are active
in the simulation.  The models are implemented by the four primary physics
kernels --- fluid flow, heat/species transport, induction heating, and solid
mechanics --- which are weakly coupled using time splitting.
A brief overview of the physics kernels follows; see \PA\ for more details.

\paragraph*{Fluid Flow.}
The fluid flow physics model simulates multi-material, incompressible flow with
interface tracking.  A gravitational body force is defined using the
\tvar{PHYSICS}{Body_Force_Density} variable.
See the \tnamelist{MATERIAL} namelist for a description of the material
properties required by the fluid flow model.
 
\paragraph*{Heat and Species Transport.}
The heat and species transport physics kernel models both heat conduction
with thermal (view factor) radiation, and solutal species diffusion and
thermodiffusion.  These (primarily) diffusive transport processes are fully
coupled; advection of enthalpy and solutal species are handled by the fluid
flow physics kernel and incorporated as loosely-coupled source terms.

Heat transport is enabled using the \tvar{PHYSICS}{Heat_Transport} flag,
and solves the heat equation
\begin{equation}
  \frac{\partial H}{\partial t} = \nabla\cdot K\nabla T + Q
  + Q_{\text{joule}} + Q_{\text{adv}},
\end{equation}
with dependent variables temperature $T$ and enthalpy density $H$.
The enthalpy density is algebraically related to temperature as $H=f(T)$
where $f'(T) = \rho\,c_p$ is the volumetric heat capacity.
See the \tnamelist{MATERIAL} namelist for a description of the material
properties required by the heat equation.
The optional volumetric heat source $Q$ is defined through the
\tnamelist{DS_SOURCE} namelist using \texttt{"temperature"} as the
equation name.
The Joule heating source $Q_{\text{joule}}$ is computed by the induction
heating kernel, and the advected heat $Q_{\text{adv}}$ by the flow
kernel.
The boundary conditions on $T$ are defined through the
\tnamelist{THERMAL_BC} namelists.
The initial value of $T$ are defined through the \tvar{BODY}{Temperature}
variable of the \tnamelist{BODY} namelists.  View factor radiation systems
which couple to the heat equation are defined using
\tnamelist{ENCLOSURE_RADIATION} namelists.

Solutal species transport is enabled using the \tvar{PHYSICS}{Species_Transport}
flag, which solves the $n$ coupled equations
\begin{equation}
  \frac{\partial\phi_i}{\partial t}
    = \nabla\cdot D_i(\nabla\phi_i \left[+ S_i\nabla T\right])
    + Q_i + Q_{i,\text{adv}}, \quad i=1,\dots,n,
\end{equation}
for species concentrations $\phi_i$.  The number of components $n$ is
defined by \tvar{PHYSICS}{Number_of_Species}.  The thermodiffusion term in
$[\cdot]$ is only included when coupled with heat transport.
See the \tnamelist{MATERIAL} namelist for defining the diffusivities $D_i$
and Soret coefficients $S_i$.
The optional volumetric source $Q_i$ is defined through the
\tnamelist{DS_SOURCE} namelist using \texttt{"concentration\textit{i}"}
as the equation name.
The advected species source $Q_{i,\text{adv}}$ is computed by the flow
kernel.
Boundary conditions on $\phi_i$ are defined through the
\tnamelist{SPECIES_BC} namelists.
The initial value of the $\phi_i$ are defined through the
\tvar{BODY}{Phi} variable of the \tnamelist{BODY} namelists.

\paragraph*{Induction Heating.}
The induction heating physics kernel solves for the Joule heat that is used
as a source in heat transport.  It is enabled using the
\tvar{PHYSICS}{Electromagnetics} flag.
See the \tnamelist{MATERIAL} namelist for a description of the material
properties required by the electromagnetics solver.
The \tnamelist{ELECTROMAGNETICS} namelist is used to describe
the induction heating problem. 

\paragraph*{Solid Mechanics.}
The solid mechanics physics kernel models small strain elastic and plastic
deformation of solid material phases, including deformations induced by
temperature changes and solid state phase changes.  It is enabled using
the \tvar{PHYSICS}{Solid_Mechanics} flag.
See the \tnamelist{MATERIAL} namelist for a description of the material
properties required by the solid mechanics kernel.
Parameters which define
the plasticity model are defined using the \tnamelist{VISCOPLASTIC_MODEL}
namelist.
Displacement and traction boundary conditions are defined using the
\tnamelist{BC} namelist.  The effect of the gravitational body force
defined by \tvar{PHYSICS}{Body_Force_Density} can be included by enabling the
\tvar{SOLID_MECHANICS}{Solid_Mechanics_Body_Force} flag.

\section*{\texttt{PHYSICS} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}
\item \tvar{PHYSICS}{Body_Force_Density}
\item \tvar{PHYSICS}{Electromagnetics}
\item \tvar{PHYSICS}{Flow}
\item \tvar{PHYSICS}{Heat_Transport}
\item \tvar{PHYSICS}{Materials}
\item \tvar{PHYSICS}{Number_of_Species}
\item \tvar{PHYSICS}{Solid_Mechanics}
\item \tvar{PHYSICS}{Species_Transport}
\end{itemize}


\tvardef{PHYSICS}{Materials}
\section*{\texttt{Materials}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
A list of materials to include in the simulation. These are material names
defined in \tnamelist{MATERIAL} namelists. The list must include all materials
assigned to a region in a \tnamelist{BODY} namelist, or specified as an
\tvar{FLOW_BC}{inflow_material} in a fluid flow boundary condition, but it
need not include all materials defined in the input file. Use the reserved
name \texttt{"VOID"} to refer to the built-in void pseudo-material.
\item[Type:] string list
\end{description}


\tvardef{PHYSICS}{Flow}
\section*{\texttt{Flow}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
Enables the simulation of fluid flow.
\item[Type:] logical
\item[Default:] false
\end{description}


\tvardef{PHYSICS}{Body_Force_Density}
\section*{\texttt{Body_Force_Density}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A constant force per unit mass, $\vec{g}$, that acts throughout material volumes.
  The net force on a volume is the integral of its density times $\vec{g}$ over the
  volume. Typically $\vec{g}$ is the gravitational acceleration.
\item[Physical dimension:] \AUlength\per\AUtime\squared
\item[Type:] real 3-vector
\item[Default:] (0, 0, 0)
\item[Note:]
  The fluid flow model always includes this body force. \par
  The solid mechanics model has the option of including this body force or not;
  see \tvar{SOLID_MECHANICS}{Solid_Mechanics_Body_Force}.
\end{description}

 
\tvardef{PHYSICS}{Heat_Transport}
\section*{\texttt{Heat_Transport}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Enables the calculation of heat conduction, advection, and radiation
  using the heat/species transport physics kernel.
\item[Type:] logical
\item[Default:] false
\end{description}

 
\tvardef{PHYSICS}{Species_Transport}
\section*{\texttt{Species_Transport}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Enables the calculation of species diffusion and advection using the
  heat/species transport physics kernel.  The number of species components
  must be specified using \tvar{PHYSICS}{Number_of_Species}.
\item[Type:] logical
\item[Default:] false
\end{description}

 
\tvardef{PHYSICS}{Number_of_Species}
\section*{\texttt{Number_of_Species}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The number of species components.  Required when
\tvar{PHYSICS}{Species_Transport} is enabled.
\item[Type:] integer
\item[Default:] 0
\item[Valid values:] ${}>0$
\end{description}

 
\tvardef{PHYSICS}{Solid_Mechanics}
\section*{\texttt{Solid_Mechanics}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Enables the calculation of solid material stresses and strains.
\item[Type:] logical
\item[Default:] false
\end{description}

 
\tvardef{PHYSICS}{Electromagnetics}
\section*{\texttt{Electromagnetics}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Enables the calculation of Joule heating.
\item[Type:] logical
\item[Default:] false
\end{description}
