\chapter{\texttt{SOLID_MECHANICS} Namelist}
\tnamelistdef{SOLID_MECHANICS}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{SOLID_MECHANICS} namelist sets parameters that are specific to
the solid mechanics model and algorithm. This namelist is read whenever the
\tnamelist{PHYSICS} namelist option \tvar{PHYSICS}{Solid_Mechanics} is enabled.
Parameters for the nonlinear solver used by the algorithm and its
preconditioner are specified in \tnamelist{NONLINEAR_SOLVER} and \
\tnamelist{LINEAR_SOLVER} namelists. Optional material viscoplasticity models
are defined in \tnamelist{VISCOPLASTIC_MODEL} namelists.


\section*{\texttt{SOLID_MECHANICS} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional] Required when solid mechanics physics is enabled.
\item[Single/Multiple Instances] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}
\item \tvar{SOLID_MECHANICS}{Contact_Distance}
\item \tvar{SOLID_MECHANICS}{Contact_Norm_Trac}
\item \tvar{SOLID_MECHANICS}{Contact_Penalty}
\item \tvar{SOLID_MECHANICS}{Displacement_Nonlinear_Solution}
\item \tvar{SOLID_MECHANICS}{Solid_Mechanics_Body_Force}
\item \tvar{SOLID_MECHANICS}{Stress_Reduced_Integration}
\item \tvar{SOLID_MECHANICS}{Strain_Limit}
\item \tvar{SOLID_MECHANICS}{Convergence_Criterion}
\item \tvar{SOLID_MECHANICS}{Maximum_Iterations}
\item \tvar{SOLID_MECHANICS}{NLK_Vector_Tolerance}
\item \tvar{SOLID_MECHANICS}{NLK_Max_Vectors}
\end{itemize}



\section*{\texttt{Contact_Distance}}
\tvardef{SOLID_MECHANICS}{Contact_Distance}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A length scale parameter $\beta$ for the contact function 
%\[  \lambda = \left\{\begin{array}{ll}
%	1 & \mbox{if $s < 0$ and $\tau_n < 0$} \\
%	0 & \mbox{if $s > \beta$ or $\tau_n > \tau^*$} \\ 
%	2 (\frac{s}{\beta} - 1)^3 + 3 (\frac{s}{\beta} - 1)^2 & 
%		\mbox{if $0 < s < \beta$ and $\tau_n < 0$} \\ 
%	2 (\frac{\tau_n}{\tau^*} - 1)^3 + 3 (\frac{\tau_n}{\tau^*} - 1)^2 & 
%		\mbox{if $0 < \tau_n < \tau^*$ and $s < 0$} \\ 
%	(2 (\frac{s}{\beta} - 1)^3 + 3 (\frac{s}{\beta} - 1)^2) *
%		(2 (\frac{\tau_n}{\tau^*} - 1)^3 + 3 (\frac{\tau_n}{\tau^*} - 1)^2) & 
%		\mbox{if $0 < \tau_n < \tau^*$ and $0< s < \beta$}
%	\end{array}
%	\right.
%\]

\[ \lambda = \lambda_s * \lambda_{\tau} \]
where
\[ \lambda_s = \left\{\begin{array}{ll}
	1 & \mbox{if $s < 0$} \\
	0 & \mbox{if $s > \beta$} \\ 
	2 (\frac{s}{\beta} - 1)^3 + 3 (\frac{s}{\beta} - 1)^2 & 
		\mbox{if $0 < s < \beta$}
	\end{array}
	\right.
\]
and
\[  \lambda_{\tau} = \left\{\begin{array}{ll}
	1 & \mbox{if $\tau_n < 0$} \\
	0 & \mbox{if $\tau_n > \tau^*$} \\ 
	2 (\frac{\tau_n}{\tau^*} - 1)^3 + 3 (\frac{\tau_n}{\tau^*} - 1)^2 & 
		\mbox{if $0 < \tau_n < \tau^*$}
	\end{array}
	\right.
\]
$s = \hat{n} \cdot (u_k - u_j)$
\item[Physical dimension:] \AUlength
\item[Type:] real
\item[Default:] 1.0e-7
\item[Valid values:] $(0,\infty]$
\item[Notes:] The default value is usually a good value for mesh cell sizes in
the 1 - 10 mm size range.
\end{description}


\section*{\texttt{Contact_Norm_Trac}}
\tvardef{SOLID_MECHANICS}{Contact_Norm_Trac}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A parameter $\tau^*$ for the contact function 
\[ \lambda = \lambda_s * \lambda_{\tau} \]
where
\[ \lambda_s = \left\{\begin{array}{ll}
	1 & \mbox{if $s < 0$} \\
	0 & \mbox{if $s > \beta$} \\ 
	2 (\frac{s}{\beta} - 1)^3 + 3 (\frac{s}{\beta} - 1)^2 & 
		\mbox{if $0 < s < \beta$}
	\end{array}
	\right.
\]
and
\[  \lambda_{\tau} = \left\{\begin{array}{ll}
	1 & \mbox{if $\tau_n < 0$} \\
	0 & \mbox{if $\tau_n > \tau^*$} \\ 
	2 (\frac{\tau_n}{\tau^*} - 1)^3 + 3 (\frac{\tau_n}{\tau^*} - 1)^2 & 
		\mbox{if $0 < \tau_n < \tau^*$}
	\end{array}
	\right.
\]
$\tau_n$ is the normal traction at the interface where a positive value corresponds
to a tensile force normal to the surface.
\item[Physical dimension:] \AUforce\per\AUarea
\item[Type:] real
\item[Default:] 1.0e4
\item[Valid values:] $[0,\infty]$
\item[Notes:] The default value is probably appropriate for materials with 
elastic constants in the range $10^9$ - $10^{11}$.  This parameter should probably 
be scaled proportionately for elastic constants that differ from this range.
\end{description}


\section*{\texttt{Contact_Penalty}}
\tvardef{SOLID_MECHANICS}{Contact_Penalty}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A penalty factor for the penetration constraint in the
contact algorithm.  Changing this is probably not a good idea in the current
version.  
\item[Physical dimension:] dimensionless
\item[Type:] real
\item[Default:] 1.0e3
\item[Valid values:] $[0,\infty]$
\item[Notes:] 
\end{description}


\section*{\texttt{Displacement_Nonlinear_Solution}}
\tvardef{SOLID_MECHANICS}{Displacement_Nonlinear_Solution}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A character string pointer to the nonlinear
solution algorithm parameters to be used in a Newton-Krylov solution
of the nonlinear thermo-elastic viscoplastic equations. This string
``points'' to a particular \tnamelist{NONLINEAR_SOLVER} namelist if
it matches the \tvar{NONLINEAR_SOLVER}{Name} input variable string
in the \tnamelist{NONLINEAR_SOLVER} namelist.
\item[Type:] string
\item[Default:] \texttt{"default"}
\item[Valid values:] arbitrary string
\item[Notes:] If this string does not match a
\tvar{NONLINEAR_SOLVER}{Name} input variable string specified in a
\tnamelist{NONLINEAR_SOLVER} namelist, then the default set of
nonlinear solution algorithm parameters is used for the thermo-elastic
viscoplastic equations.
\end{description}


\tvardef{SOLID_MECHANICS}{Solid_Mechanics_Body_Force}
\section*{\texttt{Solid_Mechanics_Body_Force}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Body forces will be included in the solid mechanics calculation. 
\item[Physical dimension:]
\item[Type:] \logical
\item[Default:] \false
\end{description}


\section*{\texttt{Strain_Limit}}
\tvardef{SOLID_MECHANICS}{Strain_Limit}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] This parameter controls the use of the ODE
integrator in the plastic strain calculation.  It should be set to the
minimum significant value of the plastic strain increment for a time
step.  If convergence seems poor when a viscoplastic material model is
used, it may help to reduce the value.
\item[Physical dimension:] \AUlength\per\AUlength
\item[Type:] real
\item[Default:] 1.0e-10
\item[Valid values:] $\ge0$
\item[Notes:] This parameter can not be currently used to control the
time step.  It may be used for such purposes in future releases.
\end{description}


\section*{\texttt{Convergence_Criterion}}
\tvardef{SOLID_MECHANICS}{Convergence_Criterion}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Tolerance used to determine when nonlinear convergence has
  been reached.
\item[Type:] real
\item[Default:] 1e-12
\item[Valid values:] $(0,0.1)$
\item[Note:] We refer to the input value of
  \tvar{NONLINEAR_SOLVER}{Convergence_Criterion} as $\epsilon$. $F(x) = 0$ is the
  nonlinear system being solved.

  The nonlinear iteration is stopped when \emph{either} of the
  following two conditions are met:
  \begin{itemize}
  \item reduction in the 2-norm of the nonlinear residual meets the criterion, i.e.\:

    \begin{displaymath}
      \frac{\| F(x_{k+1}) \|_2}{\| F(x_0) \|_2} < \gamma
    \end{displaymath}

  \item the relative change in the max-norm of the solution meets the criterion, i.e.\:

    \begin{displaymath}
      \frac{\| \delta x \|_{\infty}}{\| x_{k+1} \|_{\infty}} < \gamma
    \end{displaymath}

  \end{itemize}

  where $\gamma$ is the input desired tolerance, modified using an
  estimate of the convergence rate, i.e.:

  \begin{displaymath}
    \gamma = (1 - \rho) \epsilon
  \end{displaymath}

  and:

  \begin{displaymath}
    \rho = \frac{ \| x_{k+1} - x_k \|_{\infty} / \| x_{k+1} \|_{\infty}}
    { \| x_k - x_{k-1} \|_{\infty} / \| x_k \|_{\infty} }
  \end{displaymath}

  This is an attempt to prevent false convergence if the solution stagnates,
  but allow iteration to stop if the solution is acceptable.
\end{description}


\section*{\texttt{Maximum_Iterations}}
\tvardef{SOLID_MECHANICS}{Maximum_Iterations}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Maximum allowed number of iterations of the nonlinear
  solver.
\item[Type:] \integer
\item[Default:] 100
\item[Valid values:] $[0,\infty)$
\end{description}


\section*{\texttt{NLK_Vector_Tolerance}}
\tvardef{SOLID_MECHANICS}{NLK_Vector_Tolerance}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The vector drop tolerance for the NLK method. When
  assembling the acceleration subspace vector by vector, a vector is dropped
  when the sine of the angle between the vector and the subspace less than this
  value.
\item[Type:] \real
\item[Default:] 0.01
\item[Valid values:] $(0,1)$
\end{description}


\section*{\texttt{NLK_Max_Vectors}}
\tvardef{SOLID_MECHANICS}{NLK_Max_Vectors}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] For the NLK method, the maximum number of acceleration
  vectors to be used.
\item[Type:] \integer
\item[Default:] 20
\item[Valid values:] $[0,\infty)$
\end{description}
