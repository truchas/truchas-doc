\chapter{\texttt{MATERIAL} and \texttt{PHASE} Namelists}
\tnamelistdef{MATERIAL}\tnamelistdef{PHASE}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

A database of materials, their properties and attributes are defined using
the \texttt{MATERIAL}, \texttt{PHASE}, and \linebreak \tnamelist{PHASE_CHANGE}
namelists. Not all materials are necessarily included in the simulation, but
only those specified by the \texttt{PHYSICS} namelist variable
\tvar{PHYSICS}{materials}.
This allows one to reuse material input blocks without needing to prune out
unused materials which would otherwise negatively impact performance.

In Truchas usage, one or more phases comprise a material. A single-phase
material is defined by a \texttt{MATERIAL} namelist, and the namelist
specifies all properties and attributes of the material. A multi-phase
material is defined by a \texttt{MATERIAL} namelist and an optional
\texttt{PHASE} namelist for each of the material phases. Properties and
attributes that apply to all phases can be defined in the \texttt{MATERIAL}
namelist. Properties and attributes specific to a given phase are defined
in the \texttt{PHASE} namelist for the phase, and these supercede any that
might be defined in the \texttt{MATERIAL} namelist for the phase. Additional
information that defines the transformation between phases is specified using
the \tnamelist{PHASE_CHANGE} namelist.

%\newenvironment{property}[2]%
%    {\begin{list}{}{%
%        \setlength{\labelsep}{0pt}%\setlength{\leftmargin}{0pt}%
%        \setlength{\topsep}{0pt}\setlength{\partopsep}{2\itemsep}%
%        \setlength{\labelwidth}{\leftmargin}\setlength{\listparindent}{\parindent}}
%        \item[\texttt{\textbf{#1}}\hfill]\hfill\makebox[0.3\linewidth][l]{#2}\item[]}%
%    {\end{list}}

%\newcommand{\ivar}[1]{\texttt{\textbf{#1}}}


\section*{The \texttt{MATERIAL} Namelist}
\addcontentsline{toc}{section}{The \texttt{MATERIAL} Namelist}
The \texttt{MATERIAL} namelist defines a material, either single-phase or
multi-phase, that is available to be used in a simulation. In addition to
the property and attribute variables described below, the namelist has the
following variables.

\begin{nmlvar}{MATERIAL}{name}{}
A unique name for the material used to reference it.
\end{nmlvar}

\begin{nmlvar}{MATERIAL}{phases}{}
A multi-phase material is defined by assigning a value to \ivar{phases}.
This is a list of two or more unique phase names that comprise the material.
The phases must be listed in order from low to high temperature phases. The
phase names will be referenced by \tnamelist{PHASE_CHANGE} namelists and by
optional \texttt{PHASE} namelists. Consecutive pairs of phases must be
accompanied by a corresponding \tnamelist{PHASE_CHANGE} namelist.
\end{nmlvar}


\section*{The \texttt{PHASE} Namelist}
\addcontentsline{toc}{section}{The \texttt{PHASE} Namelist}
The \texttt{PHASE} namelist defines properties and attributes specific to one
phase of a multi-phase material. It is optional. Any properties or attributes
defined here supersede those defined in the parent \texttt{MATERIAL} namelist.
In addition to the property and attribute variables described below, the
\texttt{PHASE} namelist has the following variable.

\begin{nmlvar}{PHASE}{name}{}
The name of the phase. This is one of the names assigned to the \ivar{phases}
variable of the \texttt{MATERIAL} namelist for the parent material.
\end{nmlvar}


\section*{Property and Attribute Variables}
\addcontentsline{toc}{section}{Property and Attribute Variables}

The following variables specify the values of material properties and
attributes. Unless otherwise noted, they may appear in both the
\texttt{MATERIAL} and \texttt{PHASE} namelists. Many properties can be
either constant-valued or a function. Variables ending with the suffix
\ivar{_func} specify the name of a \tnamelist{FUNCTION} namelist that
defines a function that computes the value of the property.

\subsection*{Thermodynamic Properties}
The following property variables are used by the heat transport model:
\begin{itemize}\setlength{\itemsep}{0pt}
\item \ivar{density}
\item \ivar{specific_heat}, \ivar{specific_heat_func}, \ivar{ref_temp},
      and \ivar{ref_enthalpy}
\item \ivar{specific_enthalpy_func}
\item \ivar{conductivity}, \ivar{conductivity_func}
\end{itemize}

The material mass density (mass per volume) is specified by
\ivar{density}. It is limited to constant values and all phases in a
multi-phase material are currently constrained to have the same density
(but see \linebreak\ivar{density_delta_func} for flow and \ivar{tm_linear_cte}
for solid mechanics.) Consequently this variable may only appear in the
\texttt{MATERIAL} namelist.

There are two options for defining the temperature-dependent specific
enthalpy function $h(T)$ of a material or phase. The first specifies the
specific heat $C_p(T)$ (energy per unit mass per degree temperature) using
\ivar{specific_heat} for a constant or \ivar{specific_heat_func} for a
function of temperature. In the latter case it must either be a tabular
function or a polynomial without a $T^{-1}$ term. An analytic antiderivative
of the specific heat will be generated by Truchas and used for $h(T)$.
For a single-phase material or the lowest-temperature phase of a
multi-phase material, there is an arbitrary constant of integration.
By default it is chosen such that $h(0)=0$. It can be chosen instead such
that $h(T_\text{ref}) = h_\text{ref}$ by specifying values for the optional
variables \ivar{ref_temp} and \ivar{ref_enthalpy}, which default to 0.
These latter two variables may only appear in the \texttt{MATERIAL} namelist.

The second option is to specify the specific enthalpy function $h(T)$ (energy
per unit mass) directly using \ivar{specific_enthalpy_func}. The function
must be strictly increasing, and when used for a phase of a multi-phase
material, it must incorporate the latent heat associated with the
transformation from the adjacent lower-temperature phase (when there is one).

The thermal conductivity (power per unit length per degree temperature) of
a material or phase is specified by \ivar{conductivity} for a constant or
\ivar{conductivity_func} for a function. The function is assumed to be a
function of temperature $T$, or $(T,\phi_1,\dots,\phi_n)$ when coupled with
solutal species transport.


\subsection*{Fluid Flow Properties}
The following attribute and property variables are used by the fluid flow model:
\begin{itemize}\setlength{\itemsep}{0pt}
\item \ivar{is_fluid}
\item \ivar{density}
\item \ivar{density_delta_func}
\item \ivar{viscosity}, \ivar{viscosity_func}
\end{itemize}

The boolean attribute \ivar{is_fluid} is used to indicate whether or not
the material or phase is a fluid. Its default value is \texttt{F} (or
\texttt{.false.}) Materials and phases marked as fluid are included in the
fluid flow model.

The constant reference density $\rho_0$ of a fluid material or phase is
specified by \ivar{density}. This is the same density value used for heat
transport. A temperature-dependent fluid density $\rho(T)$ can be defined
by giving its deviation from the reference density, $\delta\rho(T) =
\rho(T)-\rho_0$ using the variable \ivar{density_delta_func}. This is used
only to compute the buoyancy body force of the Boussinesq approximation in
the flow model. If not specified, no deviation from the reference density
is assumed.

The dynamic viscosity (mass per length per time) of a material or phase
is specified by \ivar{viscosity} for a constant or \ivar{viscosity_func}
for a function of temperature. This is required for viscous flow problems
(\tnamelist{FLOW} namelist variable \texttt{inviscid = F}).

\subsection*{Electromagnetic Properties}
The following property variables are used by the induction heating model:
\begin{itemize}\setlength{\itemsep}{0pt}
\item \ivar{electrical_conductivity}, \ivar{electrical_conductivity_func}
\item \ivar{electric_susceptibility}, \ivar{electric_susceptibility_func}
\item \ivar{magnetic_susceptibility}, \ivar{magnetic_susceptibility_func}
\end{itemize}

The electrical conductivity of a material or phase is specified using either
\ivar{electrical_conductivity} for a constant, or
\ivar{electrical_conductivity_func} for a function of temperature. The
default value is 0. The electromagnetics solver assumes SI units by default
and the units of electrical conductivity are Siemens per meter. To use
different units, the values for the \tnamelist{PHYSICAL_CONSTANTS} namelist
variables \linebreak \ivar{vacuum_permeability} and \ivar{vacuum_permittivity}
must be redefined appropriately.

The electric susceptibility $\chi_e$ of a material or phase is specified
using either \ivar{electric_susceptibility} for a constant, or
\ivar{electric_susceptibility_func} for a function of temperature.
The relative permittivity is $1+\chi_e$. This property has a default value
of zero, which is appropriate in most cases.

The magnetic susceptibility $\chi_m$ of a material or phase is specified
using either \ivar{magnetic_susceptibility} for a constant, or
\ivar{magnetic_susceptibility_func} for a function of temperature.
The relative permeability is $1+\chi_m$. This property has a default value
of zero, which is appropriate in most cases.


\subsection*{Thermomechanical Properties}
The following properties are used by the solid mechanics model, and are only
relevant to non-fluid materials and phases.
Additional viscoplasticity  parameters may be defined using the
\tnamelist{VISCOPLASTIC_MODEL} namelist.
\begin{itemize}\setlength{\itemsep}{0pt}
\item \ivar{tm_ref_density}
\item \ivar{tm_ref_temp}
\item \ivar{tm_linear_cte}, \ivar{tm_linear_cte_func}
\item \ivar{tm_lame1}, \ivar{tm_lame1_func}
\item \ivar{tm_lame2}, \ivar{tm_lame2_func}
\end{itemize}

The temperature at which a material or phase is stress-free is specified
by \ivar{tm_ref_temp} and its density (mass per volume) at that temperature
specified by \ivar{tm_ref_density}. Its linear coefficient of thermal
expansion (inverse time) is specified by \ivar{tm_linear_cte} for a constant
or \ivar{tm_linear_cte_func} for a function of temperature.

The first and second Lam\'e constants $\lambda$ and $G$ (force per area)
for a material or phase is specified by \ivar{tm_lame1} and \ivar{tm_lame2}
for constants or \ivar{tm_lame1_func} and \ivar{tm_lame2_func} for functions
of temperature.


\subsection*{Species Transport Properties}
The following property variables are relevant to the solutal species
transport model. The model allows for an arbitrary number of species with
concentrations $\{\phi_i\}_{i=1}^n$, and the variables are arrays of
length~$n$ with each element being the property for the corresponding species.

\begin{itemize}\setlength{\itemsep}{0pt}
\item \ivar{diffusivity}, \ivar{diffusivity_func}
\item \ivar{soret}, \ivar{soret_func}
\end{itemize}

The diffusivity (area per time) of a species component in a material or phase
is specified by the corresponding element of \ivar{diffusivity} for a constant
or \ivar{diffusivity_func} for a function. A function is expected to be a
function of all the species concentrations $(\phi_1,\dots,\phi_n)$, or when
coupled with heat transfer, a function of temperature and concentrations
$(T,\phi_1,\dots,\phi_n)$.

When coupled with heat transfer, the Soret coefficient (inverse temperature)
of the thermodiffusion term for a species component in a material or phase
is specified by the corresponding element of \ivar{soret} for a constant or
\ivar{soret_func} for a function. A function is expected to be a function of
temperature and concentrations $(T,\phi_1,\dots,\phi_n)$. This is optional.
If not specified, thermodiffusion of the species component is not included in
the model, but if defined for one phase of a multi-phase material, it must
be defined for all its phases.
