\chapter{\texttt{FLOW_BC} Namelist}
\tnamelistdef{FLOW_BC}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{FLOW_BC} namelist is used to define boundary conditions for the
fluid flow model at external boundaries. At inflow boundaries it also specifies
the value of certain intensive material quantities, like temperature, that may
be associated with other physics models.

Each instance of the namelist defines a particular condition to impose over a
subset $\Gamma$ of the domain boundary. The boundary subset $\Gamma$ is
specified using mesh face sets. The namelist variable
\tvar{FLOW_BC}{face_set_ids} takes a list of face set IDs, and the
boundary condition is imposed on all faces belonging to those face sets. Note
that ExodusII mesh sides sets are imported into Truchas as face sets with the
same IDs. The following common types of boundary conditions can be defined:
\begin{itemize}\setlength{\itemsep}{0pt}
\item \textit{Pressure.}
  A pressure Dirichlet condition $p = p_b$ on $\Gamma$ is defined by setting
  \tvar{FLOW_BC}{type} to \texttt{"pressure"}. The boundary value $p_b$ is
  specified using either \tvar{FLOW_BC}{pressure} for a constant value, or
  \tvar{FLOW_BC}{pressure_func} for a function.
\item \textit{Velocity.}
  A velocity Dirichlet condition $\mathbf{u}=\mathbf{u}_b$ on $\Gamma$ is
  defined by setting \tvar{FLOW_BC}{type} to \texttt{"velocity"}. The boundary
  value $\mathbf{u}_b$ is specified using either \tvar{FLOW_BC}{velocity} for
  a constant value, or \tvar{FLOW_BC}{velocity_func} for a function.
\item \textit{No slip.}
  The special velocity Dirichlet condition $\mathbf{u}=0$ on $\Gamma$ is
  defined by setting \tvar{FLOW_BC}{type} to \texttt{"no-slip"}.
\item \textit{Free slip.}
  A free-slip condition where fluid is not permitted to penetrate the boundary,
  $\hat{n}\cdot\mathbf{u}=0$ on $\Gamma$, where $\hat{n}$ is the unit normal to
  $\Gamma$, but is otherwise free to slide along the boundary (no tangential
  traction) is defined by setting \tvar{FLOW_BC}{type} to \texttt{"free-slip"}.
\item \textit{Tangential surface tension.}
\end{itemize}

These boundary condition types are mutually exclusive: namely, no two types
may be defined on overlapping subsets of the boundary. Any subset of the
boundary not explicitly assigned a boundary condition will be implicitly
assigned a free-slip condition.

Currently it is only possible to assign boundary conditions on the external
mesh boundary. However in many multiphysics applications the boundary of the
fluid flow domain will not coincide with the boundary of the larger problem
mesh. In some cases the boundary will coincide with an internal
mesh-conforming interface that separates fluid cells and solid (non-fluid)
cells, where a boundary condition could conceivably be assigned. In other
cases, typically those involving phase change, the boundary is only implicit,
passing through mixed fluid/solid cells, and will not conform to the mesh.
In either case, the flow algorithm aims to impose an effective no-slip
condition for viscous flows, or a free-slip condition for inviscid flows.
A possible modeling approach in the former mesh-conforming case is to define
an internal mesh interface using the \tnamelist{MESH} namelist variable
\tvar{MESH}{interface_side_sets}. This effectively creates new external mesh
boundary where flow boundary conditions can be assigned.

\section*{\texttt{FLOW_BC} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}
\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{FLOW_BC}{name}
\item \tvar{FLOW_BC}{face_set_ids}
\item \tvar{FLOW_BC}{type}
\item \tvar{FLOW_BC}{pressure}
\item \tvar{FLOW_BC}{pressure_func}
\item \tvar{FLOW_BC}{velocity}
\item \tvar{FLOW_BC}{velocity_func}
\item \tvar{FLOW_BC}{dsigma}
\item \tvar{FLOW_BC}{inflow_material}
\item \tvar{FLOW_BC}{inflow_temperature}
\end{itemize}


\section*{\texttt{name}}
\tvardef{FLOW_BC}{name}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name used to identify a particular instance of this namelist
\item[Type:] string (31 characters max)
\item[Default:] none
\end{description}


\section*{\texttt{face_set_ids}}
\tvardef{FLOW_BC}{face_set_ids}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of face set IDs that define the portion of the boundary where
  the boundary condition will be imposed.
\item[Type:] integer list (32 max)
\item[Default:] none
\end{description}


\section*{\texttt{type}}
\tvardef{FLOW_BC}{type}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The type of boundary condition. The available options are:
  \begin{description}\setlength{\itemsep}{0pt}
  \item[\texttt{"pressure"}]
    Pressure is prescribed on the boundary.
    Use \tvar{FLOW_BC}{pressure} or \tvar{FLOW_BC}{pressure_func} to specify
    its value.
  \item[\texttt{"velocity"}]
    Velocity is prescribed on the boundary.
    Use \tvar{FLOW_BC}{velocity} or \tvar{FLOW_BC}{velocity_func} to specify
    its value.
  \item[\texttt{"no-slip"}]
    0-velocity is imposed on the boundary.
    This is incompatible with inviscid flow.
  \item[\texttt{"free-slip"}]
    No velocity normal to the boundary, but the tangential velocity is
    otherwise free (no traction forces).
  \item[\texttt{"marangoni"}]
    Like "free-slip" except a tangential traction is applied that is due to
    temperature dependence of surface tension. Use \tvar{FLOW_BC}{dsigma}
    to specify the value of $d\sigma/dT$. This is incompatible with inviscid
    flow.
  \end{description}
\item[Type: string]
\item[Default: none]
\item[Notes:]
The different boundary condition types are mutually exclusive; no two can
be specified on a common portion of the boundary.
\end{description}


\section*{\texttt{pressure}}
\tvardef{FLOW_BC}{pressure}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of boundary pressure for a pressure-type boundary
  condition. To specify a function, use \tvar{FLOW_BC}{pressure_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{pressure_func}}
\tvardef{FLOW_BC}{pressure_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the boundary pressure for a pressure-type boundary condition. The
  function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{velocity}}
\tvardef{FLOW_BC}{velocity}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of boundary velocity for a velocity-type boundary
  condition. To specify a function, use \tvar{FLOW_BC}{velocity_func} instead.
\item[Default:] none
\item[Type:] real 3-vector
\end{description}


\section*{\texttt{velocity_func}}
\tvardef{FLOW_BC}{velocity_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{VFUNCTION} namelist defining a function that gives
  the boundary velocity for a velocity-type boundary condition. The
  function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{dsigma}}
\tvardef{FLOW_BC}{dsigma}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of $d\sigma/dT$ for the marangoni-type condition. Here
  $\sigma(T)$ is the temperature dependent surface tension coefficient.
\item[Default:] none
\item[Type:] real
\item[Units:] ???
\end{description}


\section*{\texttt{inflow_material}}
\tvardef{FLOW_BC}{inflow_material}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Velocity and pressure boundary conditions may result in fluid flow into
  the domain across the boundary. This parameter specifies the name of the
  fluid material to flux in. If not specified, materials are fluxed
  into a cell through a boundary face in the same proportion as the material
  volume fractions present in the cell.
\item[Default:] none
\end{description}


\section*{\texttt{inflow_temperature}}
\tvardef{FLOW_BC}{inflow_temperature}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Velocity and pressure boundary conditions may result in fluid flow into
  the domain across the boundary. This parameter specifies the temperature
  of the material fluxed in. If not specified, materials are fluxed into a
  call through a boundary face at the same temperature as the cell.
\item[Default:] none
\end{description}
