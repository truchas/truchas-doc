\chapter{\texttt{NUMERICS} Namelist}
\tnamelistdef{NUMERICS}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{NUMERICS} namelist specifies general numerical parameters not
specific to any particular physics, especially those controlling the overall
time stepping of the Truchas model.

\section*{\texttt{NUMERICS} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Single
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{NUMERICS}{Alittle}
\item \tvar{NUMERICS}{Cutvof}
\item \tvar{NUMERICS}{Cycle_Max}
\item \tvar{NUMERICS}{Cycle_Number}
\item \tvar{NUMERICS}{Discrete_Ops_Type}
\item \tvar{NUMERICS}{Dt_Constant}
\item \tvar{NUMERICS}{Dt_Grow}
\item \tvar{NUMERICS}{Dt_Init}
\item \tvar{NUMERICS}{Dt_Max}
\item \tvar{NUMERICS}{Dt_Min}
\item \tvar{NUMERICS}{t}
\end{itemize}



\section*{\texttt{Alittle}}
\tvardef{NUMERICS}{Alittle}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A small, positive real number (relative to unity) used to
avoid division by zero or to compare against other numbers to deduce relative significance.
\item[Physical dimension:] dimensionless
\item[Type:] real
\item[Default:] \texttt{EPSILON}$(x)$, where $x$ is of type \real. If the precision
of $x$ is double, \texttt{EPSILON}$(x)$ returns $1.0^{-16}$ for most
combinations of software (Fortran 90 compiler) and hardware platforms tested.
\item[Valid values:] (0.0, 0.001]
\end{description}


\section*{\texttt{Cutvof}}
\tvardef{NUMERICS}{Cutvof}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The value of a material cell volume fraction
below which that material is ignored. If any material has a cell
volume fraction less than \tvar{NUMERICS}{Cutvof}, then that material is
deleted from that cell, with all other materials present receiving a
proportional increase in volume fraction. The only exception is the
``background'' material, which if present receives the entire
allocation (equal to the volume fraction deleted).
\item[Physical dimension:] dimensionless
\item[Type:] real
\item[Default:] $10^{-8}$
\item[Valid values:] $(0.0, 1.0)$
\item[Notes:] Relative to most other volume-fraction-based algorithms,
\tvar{NUMERICS}{Cutvof} is defaulted and used at a \textit{much}
lower value. If a prototypical value of
\tvar{NUMERICS}{Cutvof} equal to $10^{-4}$ were used, as is the case in most
commercial software, local and global mass conservation would suffer,
and lack of algorithmic robustness would be masked. The default
$10^{-8}$ value for \tvar{NUMERICS}{Cutvof} yields good results, hence setting
it higher is generally not necessary.
\end{description}


\section*{\texttt{Cycle_Max}}
\tvardef{NUMERICS}{Cycle_Max}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The maximum cycle number allowed in the given
simulation, where one ``cycle'' corresponds to one integration time
step over all physical model equations. The simulation will terminate
gracefully when the cycle number reaches \tvar{NUMERICS}{Cycle_Max}.
\item[Type:] integer
\item[Default:] 1000000
\item[Valid values:] $(0, \infty)$
\item[Notes:] A simulation will also terminate gracefully if the last entry in the
\tvar{OUTPUTS}{Output_T} input variable (\real) array (in the \tnamelist{OUTPUTS}
namelist) is exceeded by the current simulation time \tvar{NUMERICS}{t}.
\end{description}


\section*{\texttt{Cycle_Number}}
\tvardef{NUMERICS}{Cycle_Number}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The cycle number to be used just prior to
the first actual cycle (time step) taken in the given simulation.  The
first simulation cycle number is then taken to be
$\texttt{cycle_number}+1$.
\item[Type:] integer
\item[Default:] 0
\item[Valid values:] $[1, \infty)$
\item[Notes:] The default value of $0$ results in the first computational cycle
taken to be $1$. This input variable is most useful when starting
simulations from a restart file that already contains a restart cycle
number $>0$.
\end{description}


\section*{\texttt{Discrete_Ops_Type}}
\tvardef{NUMERICS}{Discrete_Ops_Type}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Flag for choosing the numerical reconstruction
method used in estimating face-centered (located at cell face
centroids) spatial gradients and values of discrete cell-centered
data. Specifically, face pressure gradients, face velocity values,
and face velocity gradients are all controlled by this flag.
\item[Type:] string
\item[Default:] \texttt{"default"}
\item[Valid values:] \texttt{"default"}, \texttt{"ortho"}, \texttt{"nonortho"}
\item[Notes:] Face-centered pressure gradients are needed for
cell-centered estimates of the pressure Laplacian (used in the
pressure Poisson solve) and for enforcement of solenoidal
face-centered velocities. Face-centered velocity gradients are needed
for cell-centered estimates of the stress tensor, and face-centered
velocity values are needed for estimation of fluxing
velocities. If this variable does
not appear in the \verb|NUMERICS| namelist, or if it appears and is set to
\verb|'default'|, the type of discrete operators to use is chosen
by testing the orthogonality of the mesh cells.
When \emph{all} mesh cells are found to be
orthogonal (to roundoff error) and thus hexahedral, then
\tvar{NUMERICS}{Discrete_Ops_Type} defaults to an \texttt{'ortho'}
method. Otherwise, \tvar{NUMERICS}{Discrete_Ops_Type} defaults to a
\texttt{'nonortho'} method, namely the Least Squares Linear Reconstruction
(LSLR) method. For a detailed discussion of discrete
operators in Truchas, consult the \PA.  If \verb|'ortho'| or
\verb|'nonortho'| is set, the chosen operator type is used, whatever the mesh.

The user can set an overall discrete operator type as above and choose an
alternative discrete operator type for the projection step in fluid flow (FF).
To override the overall default or explicit overall setting, set the variable
\tvar{LEGACY_FLOW}{FF_Discrete_Ops_Type} to the desired value.
Setting this variable leaves the overall
setting \verb|Discrete_Ops_Type| unmodified for all remaining parts of the code.
\end{description}


\section*{\texttt{Dt_Constant}}
\tvardef{NUMERICS}{Dt_Constant}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A \textit{constant} integration time step value
to be used for all time steps in the simulation
\item[Physical dimension:] \AUtime
\item[Type:] real
\item[Default:] none
\item[Valid values:] $(0,\infty)$
\item[Notes:] This \tvar{NUMERICS}{Dt_Constant} input variable should be used with
\textit{extreme caution}, as its specification overrules all other
time step choices that are controlled by linear stability and accuracy
considerations. In particular, \tnamelist{NUMERICS} namelist input
variables \tvar{NUMERICS}{Dt_Grow}, \tvar{NUMERICS}{Dt_Init},
\tvar{NUMERICS}{Dt_Max}, and \tvar{NUMERICS}{Dt_Min} are all ignored
if \tvar{NUMERICS}{Dt_Constant} is specified.  Use of
\tvar{NUMERICS}{Dt_Constant} is therefore advised only for controlled
numerical algorithm experiments, and not for simulations intended for
applications analysis and validation.
\end{description}


\section*{\texttt{Dt_Grow}}
\tvardef{NUMERICS}{Dt_Grow}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A factor to multiply the current integration time
step ($\delta t$) for the purpose of estimating the time step used during the
next cycle ($\delta t_g = \texttt{Dt_Grow}*\delta t$).  This
candidate time step ($\delta t_g$) is chosen only if all other
currently active time step restrictions have not already limited its
value below $\delta t_g$.
\item[Physical dimension:] dimensionless
\item[Type:] real
\item[Default:] 1.05
\item[Valid values:] $[1,\infty)$
\item[Notes:] \tvar{NUMERICS}{Dt_Grow} is \textit{ignored} if
\tvar{NUMERICS}{Dt_Constant} is specified.
\end{description}


\section*{\texttt{Dt_Init}}
\tvardef{NUMERICS}{Dt_Init}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Integration time step value used for the first
computational cycle
\item[Physical dimension:] \AUtime
\item[Type:] real
\item[Default:] $10^{-6}$
\item[Valid values:] $(0,\infty)$
\item[Notes:]  The default value of
\tvar{NUMERICS}{Dt_Init} is completely arbitrary, as it is
\textit{always} problem dependent. Unless a constant time step is
desired (via specification of \tvar{NUMERICS}{Dt_Constant}), then, a
value for \tvar{NUMERICS}{Dt_Init} should be specified (instead of
relying on the default) that is consistent with the time scales of
interest for the simulation at hand. A general rule of thumb is to set
\tvar{NUMERICS}{Dt_Init} smaller than the time step ultimately
desired, thereby allowing the time step to grow gradually and steadily
(say, over 10-20 cycles) to the desired time step value.

For restart calculations, the initial time step size is extracted from
the restart file and used instead of \tvar{NUMERICS}{Dt_Init}, unless
\tvar{RESTART}{Ignore_Dt} in the \tnamelist{RESTART} namelist has been
set to true.
 
\tvar{NUMERICS}{Dt_Init} is \textit{ignored} if \tvar{NUMERICS}{Dt_Constant}
is specified.
\end{description}


\section*{\texttt{Dt_Max}}
\tvardef{NUMERICS}{Dt_Max}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Maximum allowable value for the time step
\item[Physical dimension:] \AUtime
\item[Type:] real
\item[Default:] 10
\item[Valid values:] $(0,\infty)$
\item[Notes:] The time step is \textit{not} allowed to exceed this
value, even if other accuracy- or stability-based criteria would
permit it.  The default value of \tvar{NUMERICS}{Dt_Max} is
completely arbitrary, as it is \textit{always} problem
dependent. Unless a constant time step is desired (via specification
of \tvar{NUMERICS}{Dt_Constant}), then, a value for
\tvar{NUMERICS}{Dt_Max} should be specified (instead of relying on
the default) that is consistent with the time scales of interest for
the simulation at hand.

\tvar{NUMERICS}{Dt_Max} is \textit{ignored} if
\tvar{NUMERICS}{Dt_Constant} is specified.
\end{description}


\section*{\texttt{Dt_Min}}
\tvardef{NUMERICS}{Dt_Min}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Minimum allowable value for the time step
\item[Physical dimension:] \AUtime
\item[Type:] real
\item[Default:] $10^{-6}$
\item[Valid values:] $(0,\infty)$
\item[Notes:] If the time step falls below this value, the user is
informed as such and the simulation terminates gracefully. Termination
occurs at this condition because is it very probable that either
numerical algorithm or physical model problems have occurred and the
simulation is unable to recover. The default value of
\tvar{NUMERICS}{Dt_Min} is completely arbitrary, as it is
\textit{always} problem dependent. Unless a constant time step is
desired (via specification of \tvar{NUMERICS}{Dt_Constant}), then, a
value for \tvar{NUMERICS}{Dt_Min} should be specified (instead of relying on
the default) that is consistent with the time scales of interest for
the simulation at hand. A good rule of thumb to use for
\tvar{NUMERICS}{Dt_Min} is to use a value several orders of magnitude below
the time scale of interest.

\tvar{NUMERICS}{Dt_Min} is \textit{ignored} if
\tvar{NUMERICS}{Dt_Constant} is specified.
\end{description}


\section*{\texttt{t}}
\tvardef{NUMERICS}{t}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] The simulation time to be used at the beginning of the
first computational cycle.
\item[Physical dimension:] \AUtime
\item[Type:] real
\item[Default:] $0$
\end{description}
