\chapter{\texttt{SPECIES_BC} Namelist}
\tnamelistdef{SPECIES_BC}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The \texttt{SPECIES_BC} namelist is used to define boundary conditions for the
species diffusion model at external boundaries. Each instance of the namelist
defines a particular condition to impose on a species component over a subset
of the domain boundary. The boundary subset $\Gamma$ is specified using mesh
face sets. The namelist variable \tvar{SPECIES_BC}{face_set_ids} takes a list
of face set IDs, and the boundary condition is imposed on all faces belonging
to those face sets. Note that ExodusII mesh side sets are imported into Truchas
as face sets with the same IDs. The species component is specified using the
\tvar{SPECIES_BC}{comp} namelist variable.

The following types of boundary conditions can be defined.
The outward unit normal to the boundary $\Gamma$ is denoted $\hat{n}$.
\begin{itemize} \setlength{\itemsep}{0pt}
\item \textit{Concentration.}
  A concentration Dirichlet condition for species component $j$
  \begin{equation}
    \phi_j = c \text{ on $\Gamma$}
  \end{equation}
  is defined by setting \tvar{SPECIES_BC}{type} to \texttt{"concentration"}.
  The boundary value $c$ is specified using either \tvar{SPECIES_BC}{conc}
  for a constant value, or \tvar{SPECIES_BC}{conc_func} for a function.
\item \textit{Flux.}
  A total concentration flux condition for species component $j$
  \begin{align}
    &-D_j\nabla\phi_j\cdot\hat{n} = q \text{ on $\Gamma$}, \\
  \intertext{when the system does not include temperature as a dependent
  variable, or}
    &-D_j(\nabla\phi_j+S_j\nabla T)\cdot\hat{n}=q \text{ on $\Gamma$}
  \end{align}
  when it does, is defined by setting \tvar{SPECIES_BC}{type} to
  \texttt{"flux"}. The concentration flux q is specified using either
  \tvar{SPECIES}{flux} for a constant value, or \tvar{SPECIES_BC}{flux_func}
  for a function.
\end{itemize}
The specified species concentration boundary conditions are not allowed to
overlap, and they must completely cover the computational boundary.

\section*{\texttt{SPECIES_BC} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Required
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{SPECIES_BC}{name}
\item \tvar{SPECIES_BC}{face_set_ids}
\item \tvar{SPECIES_BC}{comp}
\item \tvar{SPECIES_BC}{type}
\item \tvar{SPECIES_BC}{conc}
\item \tvar{SPECIES_BC}{conc_func}
\item \tvar{SPECIES_BC}{flux}
\item \tvar{SPECIES_BC}{flux_func}
\end{itemize}


\section*{\texttt{name}}
\tvardef{SPECIES_BC}{name}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name used to identify a particular instance of this namelist.
\item[Type:] string (31 characters max)
\item[Default:] none
\end{description}


\section*{\texttt{comp}}
\tvardef{SPECIES_BC}{comp}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The species component this boundary condition applies to.
\item[Type:] integer
\item[Default:] 1
\end{description}


\section*{\texttt{face_set_ids}}
\tvardef{SPECIES_BC}{face_set_ids}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of face set IDs that define the portion of the boundary where the
  boundary condition will be imposed.
\item[Type:] integer list (32 max)
\item[Default:] none
\end{description}


\section*{\texttt{type}}
\tvardef{SPECIES_BC}{type}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The type of boundary condition. The available options are:
  \begin{description}\setlength{\itemsep}{0pt}
  \item[\texttt{"concentration"}]
    Concentration is prescribed on the boundary. Use \tvar{SPECIES_BC}{conc} or
    \tvar{SPECIES_BC}{conc_func} to specify its value.
  \item[\texttt{"flux"}]
    Total outward concentration flux is prescribed on the boundary. Use
    \tvar{SPECIES_BC}{flux} or \tvar{SPECIES_BC}{flux_func} to specify its value.
  \end{description}
\item[Type:] string
\item[Default:] none
\end{description}


\section*{\texttt{conc}}
\tvardef{SPECIES_BC}{conc}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of boundary concentration for a concentration-type boundary
  condition. To specify a function, use \tvar{SPECIES_BC}{conc_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{conc_func}}
\tvardef{SPECIES_BC}{conc_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the boundary concentration for a concentration-type boundary condition. The
  function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}


\section*{\texttt{flux}}
\tvardef{SPECIES_BC}{flux}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the total outward boundary concentration flux
  for a flux-type boundary condition. To specify a function, use
  \tvar{SPECIES_BC}{flux_func} instead.
\item[Default:] none
\item[Type:] real
\end{description}


\section*{\texttt{flux_func}}
\tvardef{SPECIES_BC}{flux_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist defining a function that gives
  the total outward boundary concentration flux for a flux-type boundary
  condition. The function is expected to be a function of $(t,x,y,z)$.
\item[Default:] none
\item[Type:] string
\end{description}
