\chapter{\texttt{INDUCTION_COIL} Namelist}
\tnamelistdef{INDUCTION_COIL}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}

The variables in an \texttt{INDUCTION_COIL} namelist specify the physical
characteristics of an induction coil that is to produce an external magnetic
field to drive the electromagnetic Joule heat calculation.  Figure~\ref{IC-fig1}
shows the idealized model of a coil that is used to analytically evaluate the
driving field.  The coil axis is assumed to be oriented with the problem
\hyperlink{ELECTROMAGNETICS.Symmetry_Axis}{symmetry axis} as defined in the
\tnamelist{ELECTROMAGNETICS} namelist.  Multiple coils may be specified; the net
driving field is the superposition of the fields due to the individual coils,
and a spatially \hyperlink{ELECTROMAGNETICS.Uniform_Source}{uniform field}
that can be specified in the \tnamelist{ELECTROMAGNETICS} namelist.

The coils carry a sinusoidally-varying current with a common frequency and
phase.  The \hyperlink{ELECTROMAGNETICS.Source_Frequency}{frequency} is
specified in the \tnamelist{ELECTROMAGNETICS} namelist, while the phase value
is irrelevant due to the time averaging of the calculated Joule heat.  Each
coil, however, has an independent current amplitude which is specified here.
In addition, the current and frequency may be piecewise constant functions
of time.

\begin{figure}[h]
  \begin{center}
    \includegraphics[scale=.7]{figures/ic-coil.pdf}
  \end{center}
  \caption{Physical 4-turn helical coil with extended wire cross section
    (left), and its idealized model as a stacked array of circular current
    loops (right).}
  \label{IC-fig1}
\end{figure}

\section*{\texttt{INDUCTION_COIL} Namelist Features}
\begin{description}\setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}

\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}\setlength{\itemsep}{0pt}
\item \tvar{INDUCTION_COIL}{Center}
\item \tvar{INDUCTION_COIL}{Current}
\item \tvar{INDUCTION_COIL}{Length}
\item \tvar{INDUCTION_COIL}{NTurns}
\item \tvar{INDUCTION_COIL}{Radius}
\end{itemize}


\tvardef{INDUCTION_COIL}{Center}
\section*{\texttt{Center}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] A 3-vector $\vec{x}_0$ giving the position of the center
  of the coil; cf.~Figure~\ref{IC-fig1}.
\item[Physical dimension:] \AUlength
\item[Type:] \real
\item[Default:] $(0, 0, 0)$
\item[Valid values:] any 3-vector
\end{description}


\tvardef{INDUCTION_COIL}{Current}
\section*{\texttt{Current}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Amplitude of the sinusoidally-varying current in the coil.
\item[Physical dimension:] \AUcurrent
\item[Type:] \real
\item[Default:] none
\item[Valid values:] Any single value, or any sequence of values.
\item[Notes:] A sequence of up to 32 values may be assigned to this variable
  in order to specify a time-dependent current amplitude; see
  \tvar{ELECTROMAGNETICS}{Source_Times} in the \tnamelist{ELECTROMAGNETICS}
  namelist and Fig.~\ref{EM-fig1}.
\end{description}


\tvardef{INDUCTION_COIL}{Length}
\section*{\texttt{Length}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Length $l$ of the coil; cf.~Figure~\ref{IC-fig1}.
\item[Physical dimension:] \AUlength
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $(0, \infty)$
\item[Notes:] \texttt{Length} is not required, nor meaningful, if
  \texttt{NTurns} is 1.
\end{description}


\tvardef{INDUCTION_COIL}{NTurns}
\section*{\texttt{NTurns}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Number of turns of the coil; cf.~Figure~\ref{IC-fig1}.
\item[Type:] \integer
\item[Default:] none
\item[Valid values:] Any positive integer.
\end{description}


\tvardef{INDUCTION_COIL}{Radius}
\section*{\texttt{Radius}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:] Radius $r$ of the coil; cf.~Figure~\ref{IC-fig1}.
\item[Physical dimension:] \AUlength
\item[Type:] \real
\item[Default:] none
\item[Valid values:] $(0, \infty)$
\end{description}
