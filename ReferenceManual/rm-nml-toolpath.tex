\chapter{\texttt{TOOLPATH} Namelist (Experimental)}
\tnamelistdef{TOOLPATH}

\section*{Overview}
\addcontentsline{toc}{section}{Overview}
The \texttt{TOOLPATH} namelist defines a path through space, especially
that taken by a machine tool, such as a laser or build platform, in the
course of a manufacturing process. Its use is not limited to such cases,
however.  The path is specified using a simple command language that is
adapted to common CNC machine languages. The current implementation is
limited to a path through Cartesian 3-space (3-axis).
The command language is described at the end of the chapter.

\section*{\texttt{TOOLPATH} Namelist Features}
\begin{description}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize} \setlength{\itemsep}{0pt}
\item \tvar{TOOLPATH}{Name}
\item \tvar{TOOLPATH}{Command_String}
\item \tvar{TOOLPATH}{Command_File}
\item \tvar{TOOLPATH}{Start_Time}
\item \tvar{TOOLPATH}{Start_Coord}
\item \tvar{TOOLPATH}{Time_Scale_Factor}
\item \tvar{TOOLPATH}{Coord_Scale_Factor}
\item \tvar{TOOLPATH}{Write_Plotfile}
\item \tvar{TOOLPATH}{Plotfile_Dt}
\item \tvar{TOOLPATH}{Partition_Ds}
\end{itemize}


\tvardef{TOOLPATH}{Name}
\section*{\texttt{Name}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name used to identify a particular instance of this namelist.
  Clients will reference the toolpath using this name.
\item[Type:] case sensitive string (31 characters max)
\item[Default:] none
\end{description}


\tvardef{TOOLPATH}{Command_String}
\section*{\texttt{Command_String}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A string from which to read the commands that define the toolpath.
  This is only suitable for relatively simple paths; use
  \tvar{TOOLPATH}{Command_File} instead for more complex paths.
\item[Type:] string (1000 characters max)
\item[Default:] none
\item[Notes:]
  Use single quotes to delimit the string to avoid conflicts with double
  quotes used within the commands.
\end{description}


\tvardef{TOOLPATH}{Command_File}
\section*{\texttt{Command_File}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The path to a file from which to read the commands that define the toolpath.
  If not an absolute path, it will be interpreted as a path relative to the
  Truchas input file directory.
\item[Type:] string
\item[Default:] none
\item[Notes:]
  C++ style comments may used in the file; all text from the
  `\texttt{//}' to the end of the line is ignored.
\end{description}


\tvardef{TOOLPATH}{Start_Time}
\section*{\texttt{Start_Time}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The starting time of the toolpath.
\item[Type:] real
\item[Default:] 0
\end{description}


\tvardef{TOOLPATH}{Start_Coord}
\section*{\texttt{Start_Coord}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The starting coordinates of the toolpath.
\item[Type:] real 3-vector
\item[Default:] $(0,0,0)$
\end{description}


\tvardef{TOOLPATH}{Time_Scale_Factor}
\section*{\texttt{Time_Scale_Factor}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  An optional multiplicative factor by which to scale all time values.
  This applies to all namelist variables as well as toolpath commands.
\item[Type:] real
\item[Default:] 1
\item[Valid values:] $>0$
\item[Notes:]
  This is applied appropriately to speeds and accelerations in the toolpath
  commands.
\end{description}


\tvardef{TOOLPATH}{Coord_Scale_Factor}
\section*{\texttt{Coord_Scale_Factor}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  An optional multiplicative factor by which to scale all coordinate values.
  This applies to all namelist variables as well as toolpath commands.
\item[Type:] real
\item[Default:] 1
\item[Valid values:] $>0$
\item[Notes:]
  This is applied appropriately to speeds and accelerations in the toolpath
  commands.
\end{description}


\tvardef{TOOLPATH}{Write_Plotfile}
\section*{\texttt{Write_Plotfile}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Enable this flag to have a discrete version of the toolpath written to
  a disk file.  The file will be located in the Truchas output directory
  and be named \texttt{toolpath-}\textit{name}\texttt{.dat}, where
  \textit{name} is the name assigned to the namelist.  If enabled,
  \tvar{TOOLPATH}{Plotfile_Dt} must be specified.
\item[Type:] logical
\item[Default:] \texttt{.false.}
\item[Notes:]
  The file is a multi-column text file where each line gives the toolpath
  data at a specific time.  The columns are, in order, the segment index,
  the time, the three position coordinates, and the flag settings (0~for
  clear and 1~for set).  Not all flags are written, only those that were
  set at some point.  The initial comment line starting with a `\texttt{\#}'
  labels the columns.  Data is written at the end point times of each path
  segment, and at zero or more equally spaced times within each segment interval.
  The latter frequency is determined by \tvar{TOOLPATH}{Plotfile_Dt}.
\end{description}


\tvardef{TOOLPATH}{Plotfile_Dt}
\section*{\texttt{Plotfile_Dt}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Output time frequency used when writing the toolpath to a disk file.
  See \tvar{TOOLPATH}{Write_Plotfile}.
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\end{description}


\tvardef{TOOLPATH}{Partition_Ds}
\section*{\texttt{Partition_Ds}}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  Assign a value to this parameter to generate an additional discrete
  version of the toolpath that is required by some clients.  The value
  specifies the desired spacing in path length.  Refer to the client's
  documentation on whether this is needed and for further information.
\item[Type:] real
\item[Default:] none
\item[Valid values:] $>0$
\item[Notes:]
  Some clients require a discete version of the toolpath that consists of a
  time-ordered sequence of (time, coordinate) pairs.  The sequence includes
  the end points of the segments.  In addition each segment is partitioned
  into one or more parts of equal path length approximately equal to, but
  no greater than \texttt{Partition_Ds}.
\end{description}


\section*{The Toolpath Command Language}
A toolpath is represented as a sequence of $n+2$ continuous path segments
defined on time intervals $(-\infty,t_0]$, $[t_0,t_1]$, $[t_1,t_2]$, $\ldots$,
$[t_n,\infty)$. The individual segments are simple paths (e.g., no motion or
linear motion) that are defined by path commands. A set of flags (0 through 31)
is associated with each path segment. Clients of a toolpath can use the setting
of a flag (set or clear) for a variety of purposes; for example, to indicate
that a device is on or off for the duration of the segment.

The toolpath command language is expressed using JSON text. The specification
of a toolpath takes the form

\begin{quote}\DefineShortVerb{\+}
  +[+ \emph{command}+,+ \textit{command}+,+ $\ldots$ +]+
\end{quote}
where \textit{command} is one of the following:

\newcommand{\Vitem}{%
  \SaveVerb[commandchars=\\\{\},aftersave={%
    \item[\UseVerb{Vitem}]}]{Vitem}}
\DefineShortVerb{\|}

\begin{description}
\Vitem|["dwell",\(dt\)]| \hfill\\
  Remain at the current position for the time interval \(dt\).
\Vitem|["moverel",[\(dx\),\(dy\),\(dz\)],\(s\),\(a\),\(d\)]| \hfill\\
  Linear displacement from the current position.  Motion accelerates from
  rest to a constant speed, and then decelerates to rest at the position
  \((dx,dy,dz)\) relative to the current position.  The linear speed,
  accleration, and deceleration are \(s\), \(a\), and \(d\), respectively.
  If \(d\) is omitted, its value is taken to be \(a\).  If both \(a\) and
  \(d\) are omitted then instantaneous acceleration/deceleration to speed
  \(s\) is assumed.
\Vitem|["setflag",\(n_1\),\(n_2\),\(\ldots\)]|
\Vitem|["clrflag",\(n_1\),\(n_2\),\(\ldots\)]| \hfill\\
  Sets or clears the listed flags.  32 flags (0 through 31) are available.
  The setting of a flag holds for all subsequent motions (above) until changed.
\end{description}

Except for the integer flag numbers, the real numeric values may be entered
as integer or floating point numbers; that is, "1" is an acceptable alternative
to "1.0".

The initial and final unbounded path segments are automatically generated
and are not specified.  The initial segment is a dwell at the position
given by \tvar{TOOLPATH}{Start_Coord} that ends at time $t_0$ given by
\tvar{TOOLPATH}{Start_Time}. Likewise the final segment is a dwell starting
at a time and at a position determined by the preceding sequence of path
segments.  All flags start clear in the initial segment.
