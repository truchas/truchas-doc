\chapter{\texttt{THERMAL_SOURCE} Namelist}
\tnamelistdef{THERMAL_SOURCE}

\section*{Overview}

The \texttt{THERMAL_SOURCE} namelist is used to define external volumetric
heat sources (power per unit volume). This source is in addition to any other
sources coming from other physics, such as a Joule heat source. Each instance
of this namelist defines a source, and the final source is the sum of all such
sources (subject to some limitations).

\newcommand{\x}{\textbf{x}}
Two forms of sources $q$ can be defined:
\begin{enumerate}[(1)]
\item $q(t,\x) = f(t,\x)\,\chi_S(\x)$, where $f$ is a user-defined function
    and $S$ is a subdomain corresponding to one or more user-specified mesh
    cell sets. Here $\chi_S$ is the characteristic function on $S$:
    $\chi_S=1$ for $\x\in S$ and $\chi_S=0$ for $\x\notin S$.
    Sources of this form can be summed as long as the interiors of their
    subdomains do not intersect.
\item $q(t,\x) = A(t) \sum_j q_j\,\chi_j(\x)$, where $A$ is a user-defined
    time-dependent prefactor, $q_j$ is a constant source on mesh cell $j$,
    and $\chi_j$ is the characteristic function on cell $j$. The collection
    of values $\{q_j\}$ is read from a data file.
\end{enumerate}

\section*{\texttt{THERMAL_SOURCE} Namelist Features}
\begin{description}
\setlength{\itemsep}{0pt}
\item[Required/Optional:] Optional
\item[Single/Multiple Instances:] Multiple
\end{description}


\section*{Components}
\addcontentsline{toc}{section}{Components}

\begin{itemize}
\setlength{\itemsep}{0pt}
\item \tvar{THERMAL_SOURCE}{name}
\item \tvar{THERMAL_SOURCE}{cell_set_ids}
\item \tvar{THERMAL_SOURCE}{data_file}
\item \tvar{THERMAL_SOURCE}{prefactor}
\item \tvar{THERMAL_SOURCE}{prefactor_func}
\item \tvar{THERMAL_SOURCE}{source}
\item \tvar{THERMAL_SOURCE}{source_func}
\end{itemize}


\section*{\texttt{name}}
\tvardef{THERMAL_SOURCE}{name}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A unique name used to identify a particular instance of this namelist.
\item[Type:] string (31 characters max)
\item[Default:] none
\end{description}


\section*{\texttt{cell_set_ids}}
\tvardef{THERMAL_SOURCE}{cell_set_ids}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  A list of cell set IDs that define the subdomain where the source is applied.
\item[Type:] a list of up to 32 integers
\item[Default:] none
\item[Valid values:] any valid mesh cell set ID
\item[Note:]
  Different instances of this namelist must apply to disjoint subdomains;
  overlapping of source functions of this form is not supported. \par
  Exodus II mesh element blocks are interpreted by Truchas as cell sets
  having the same IDs.
\end{description}


\section*{\texttt{source}}
\tvardef{THERMAL_SOURCE}{source}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the heat source. To specify a function, use
  \ivar{source_func} instead.
\item[Physical dimension:] \AUenergy\per\AUtime\usk\AUvolume
\item[Type:] \real
\item[Default:] none
\end{description}


\section*{\texttt{source_func}}
\tvardef{THERMAL_SOURCE}{source_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist that defines the source function.
  That function is expected to be a function of $(t, x, y, z)$.
\item[Type:] string
\item[Default:] none
\end{description}


\section*{\texttt{data_file}}
\tvardef{THERMAL_SOURCE}{data_file}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
The path to the data file. It is expected to be a raw binary file consisting
of a sequence of 8-byte floating point values, the number of which equals the
number of mesh cells. The order of the values is assumed to correspond to the
external ordering of the mesh cells. The file can be created with most any
programming language. In Fortran use an unformatted stream access file.
\end{description}


\section*{\texttt{prefactor}}
\tvardef{THERMAL_SOURCE}{prefactor}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The constant value of the prefactor $A$. For a function use
  \ivar{prefactor_func}.
\end{description}


\section*{\texttt{prefactor_func}}
\tvardef{THERMAL_SOURCE}{prefactor_func}
\begin{description}\setlength{\itemsep}{0pt}
\item[Description:]
  The name of a \tnamelist{FUNCTION} namelist that defines the function
  that computes the value of the time dependent prefactor $A(t)$.
\end{description}
