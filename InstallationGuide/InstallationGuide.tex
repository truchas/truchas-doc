\documentclass[12pt,titlepage,openright]{article}

% document identification
\newcommand{\NAME}{Truchas Installation Guide}
\newcommand{\VERSION}{3.1-dev}
\newcommand{\LACC}{LA-CC-15-097}
%\newcommand{\LAUR}{Not Approved for Public Release}

\usepackage[margin=1.0in,textheight=8.0in]{geometry}

\usepackage{lmodern}
\usepackage[T1]{fontenc}

%%% Allows the unescaped use of '_' in regular text
\usepackage{underscore}

%%% Improved verbatim commands (Verbatim environment)
\usepackage{fancyvrb}

%%% Improved tabular commands
\usepackage{tabularx}

%%% Adds extra layout control over the list environments
%%% Here the style= option to the description environment
\usepackage{enumitem}

%%% Defines the todo command for adding comments
\usepackage[draft]{todonotes}
%\usepackage[disable]{todonotes}

%%% Adds the appendices environment
\usepackage[page]{appendix}

%\input{style}

\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage[colorlinks=true,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}

\def\truchas{\textsc{Truchas}}
%\newcommand{\LSEC}[1]{\label{sec:#1}}
\newcommand{\LSECH}[1]{\hypertarget{sec:#1}{}\label{sec:#1}}
%\newcommand{\SEC}[1]{Section~\ref{sec:#1}}
\newcommand{\SECH}[1]{\hyperlink{sec:#1}{Section }\ref{sec:#1}}

\author{The Telluride Project}
\newcommand{\syscmd}[1]{{\mbox{\texttt{#1}}}}
\newcommand{\filename}[1]{{\mbox{\texttt{#1}}}}


\begin{document}

%%% START TITLE PAGE %%%
\pagenumbering{roman}
\thispagestyle{empty}
\begin{center}
  \vspace*{2.0in}
  \includegraphics[width=5in]{TLogoSmall.png} \\
  \smallskip
  \huge \NAME \\
  \medskip
  \Large Version \VERSION \\
  \bigskip\large
  The Telluride Project \\
  \bigskip
  Computational Physics and Methods Group (CCS-2) \\
  Computer, Computational, and Statistical Sciences Division \\
  Los Alamos National Laboratory
\end{center}
\vfill
{\sffamily
{\Large\LACC}}
%{\Large\LAUR}\\[-0.2\baselineskip]
%{\footnotesize Approved for public release; distribution is unlimited.}}
\cleardoublepage
\pagenumbering{arabic}
%%% END TITLE PAGE %%%

\section{Introduction}
This document describes the process for building and installing Truchas,
and for running the regression test suite to verify the build.  The process
is unchanged from Version 2.8, and continues to use CMake and CTest.  There
are a few updates to minimum version numbers, particularly for the compilers,
but no significant changes otherwise.


\section{Truchas Requirements}\label{sec:req}
\subsection{System Environment}
\subsubsection{Operating System}
Although CMake is a cross-platform build generator, the present Truchas
release assumes a UNIX-like environment.  Current development, testing,
and internal use of Truchas is done using 64-bit Linux platforms, including
Red Hat, Fedora, and Ubuntu, and these platforms are the only ones officially
supported.
\subsubsection{Disk Space}
The source for the Truchas distribution requires approximately 130~MB of disk
space, besides the compressed tar file of the distribution itself.  The disk
space required to build and install depends on the compiler used, the selected
configuration (serial/parallel, optimized/debug), and whether the build is
able to use certain existing system libraries or must build private versions.
It ranges from 140~MB to 1.4~GB to build and 65~MB to 300~MB for the
installation.  Running the test suite requires an additional 60~MB of disk
space.
\subsubsection{Compilers}
Truchas is written primarily in Fortran with a few components written in C.
Some support libraries also require C++.  Thus compatible Fortran and C/C++
compilers are required.  The Fortran compiler must correctly implement most
of the modern Fortran 2003 and 2008 standards, particularly the object oriented
features.  Only a few compilers have been found to satisfy these requirements
on the platforms Truchas supports.  The NAG 6.0 and Intel~15 and~16 Fortran
compilers are known to work.  Earlier versions of those compilers will not
work, nor will any versions to date of the PGI and GNU Fortran compilers.

\subsection{Software}
Truchas relies on a number of software packages and tools.  Some are included
in the distribution and automatically built as part of the Truchas build, and
the others are typically available as standard OS binary packages, at least
for the most popular Linux distributions.  The packages and tools listed in
this section must be installed on the system prior to building and installing
Truchas.
\subsubsection{Build Tools}
Aside from the compilers, standard software development tools are required,
including patch, perl, make, and cmake (version 2.8.12 or later).  These are
likely already installed on any machine used for software development.
A reasonably recent version of cmake is required, however; binary and source
distributions are available at \url{www.cmake.org} if needed.
\subsubsection{Python and NumPy}
Truchas uses Python to parse and post-process the Truchas output files and
to run the test suite.  Versions 2.5 through 2.7 should work.  You need the
Python development files in addition to the Python program itself, and these
are often packaged separately (e.g., the python-devel package with Fedora).
The Python package NumPy (\url{http://www.numpy.org}) is a required
add-on to your Python installation.  Version 1.4 or later should work.
To see if it is present and verify the version, start an interactive python
session and import the numpy module:
\begin{Verbatim}[commandchars=\\\{\},gobble=1,frame=single,framesep=2\fboxsep]
  $ \textbf{python}
  Python 2.7.3 (default, Jul 24 2012, 10:05:38) 
  [GCC 4.7.0 20120507 (Red Hat 4.7.0-5)] on linux2
  Type "help", "copyright", "credits" or "license" for more information.
  >>> \textbf{import numpy}
  >>> \textbf{numpy.__version__}
  '1.6.2'
\end{Verbatim}
If importing numpy results in an error message, it will need to be
installed.  First check whether an official NumPy package exists for
your OS (numpy with Fedora, e.g.)  If not, see the link above for
obtaining the package and simple instructions for installing it.
\subsubsection{MPI}
If you intend to build a parallel version of Truchas you need an
implementation of MPI.  The Truchas developers currently use Open MPI v1.6
and~v1.10 (\url{http://www.open-mpi.org}), but other implementations like
MPICH2 (\url{http://www.mpich.org}) should work as well.
Note that Truchas only uses the C language MPI bindings.

\subsection{Included Packages}\label{sec:incl}
The Truchas distribution includes the following third party packages.
These are automatically built if necessary as part of the Truchas build
process, and are identified here primarily for reference.

For the packages in the first group (external packages), the build process
will attempt to locate a suitable version installed on the system.  If
successful, it is used; otherwise, a private version of the package is built
and used.  For the libraries, the development header files are also required,
and where standard OS binary packages are available, these are usually packaged
in a separate `development' package; e.g., hdf5 and hdf5-devel with Fedora.
\begin{description}
\item[Swig] version 2.0 or later is required; version 2.0.4 is included.
  This program is used to generate code that interfaces Python and C code.
\item[Zlib] version 1.2.6 or later is required; version 1.2.8 is included.
\item[NetCDF] version 4.1.3 or later is required; version 4.1.3 is included.
  A modified version is required that includes the so-called ``large model
  modifications'' in order to read ExodusII-format mesh files.
\item[HDF5] version 1.8.8 or later is required; version 1.8.11 is included.
  The high-level hdf5_hl library is needed in particular.
\item[Hypre] version 2.6.0b is required and included.  Note that there are
  separate serial and MPI-parallel versions of the library and header files.
  Hypre is a library of high-performance preconditioners that features
  parallel multigrid methods for both structured and unstructured grid
  problems.  It is developed by the Center for Applied Scientific Computing
  (CASC) at Lawrence Livermore National Laboratory,%
  \footnote{\url{http://computation.llnl.gov/casc/linear_solvers/sls_hypre.html}}
  and licensed under the LGPL version 2.1 license.
\end{description}
The following internal third party libraries are always built as part of the
build process.
\begin{description}
\item[Chaco] version 2.2.1, from Sandia National Laboratories, is used to 
  compute a parallel decomposition of the mesh.  It is licensed under the
  LGPL version 2.1 license.
\item[Chaparral] version 3.2, from Sandia National Laboratories, is used to
  compute radiation view factors used by the enclosure radiation model.
  It is licensed under the LGPL version 2.1 license.
  The included version includes extensions developed by Los Alamos National
  Laboratory; these modifications are also licensed under the LGPL.
\item[Exodus] version 5.14, from Sandia National Laboratories,%
\footnote{\url{http://sourceforge.net/projects/exodusii}}
  is used to
  read Exodus\ II format mesh files.  It is licensed under the BSD license.
\end{description}

\section{Introduction to CMake}\label{sec:cmake}
CMake is used to generate the build system for Truchas.  This section gives
a brief introduction to CMake for those unfamiliar with it; others can jump
directly to Section~\ref{sec:truchas}.  More information can be found on the
CMake website;%
\footnote{\url{http://www.cmake.org}}
see especially the Wiki and FAQ.
\subsection{CMake Workflow}
CMake is a cross-platform system for generating build files for a number of
different environments, including UNIX makefiles, XCode, and Windows Visual
Studio.  The present Truchas release, however, is only configured to support
UNIX makefiles.

The configuration and generation of the makefiles begins with the command
\begin{Verbatim}[commandchars=\\\{\},frame=single]
 $ cmake [options] <path-to-source>
\end{Verbatim}
where \filename{<path-to-source>} is the path to the top-level project
directory that contains a \filename{CMakeLists.txt} file.  Files with this
name are processed by CMake to generate the build system.  These files may
also be located in subdirectories, and they are processed in the order the
subdirectories are referenced by parent \filename{CMakeLists.txt} files
using add_subdirectory commands.
When CMake successfully finishes processing all the \filename{CMakeLists.txt}
files, it generates the makefiles and exits.

The directory where the cmake command is executed is referred to as the build
(or binary) directory.  One key paradigm difference between CMake and GNU
autoconf is that CMake discourages, and Truchas disallows, in-source builds;
a directory other than the source directory must be used as the build directory.
A common choice is to create a subdirectory of the source directory to use as
the build directory, but a directory outside the source tree would serve
equally well.

In the build directory, CMake will create a \filename{CMakeCache.txt} file
and a \filename{CMakeFiles} directory in each subdirectory where CMake found
a \filename{CMakeLists.txt} file. The \filename{CMakeFiles} directory contains
internal logs and files used by CMake. The \filename{CMakeCache.txt} stores
configuration variables that allow CMake to quickly regenerate the build
system.  The variables that shape the build system are normally specified on
the \syscmd{cmake} command line.  However, you may edit the variable settings
in the top-level \filename{CMakeCache.txt} file and then regenerate the build
system with the command
\begin{Verbatim}[frame=single]
 $ make rebuild_cache
\end{Verbatim}
An alternative to 
directly editing the \filename{CMakeCache.txt} file is the \syscmd{ccmake}
command (`curses cmake'), which is an optional part of a CMake installation.
It presents an interactive, terminal-based GUI for viewing and changing the
variable settings.  It accepts the same arguments as \syscmd{cmake}.

After "\syscmd{cmake}" or "\syscmd{make rebuild_cache}" has generated the
makefiles, the build and installation proceeds using the standard UNIX
\syscmd{make} command.

\subsection{CMake Variables}
The build configuration is defined by the values of various CMake variables
stored in the \filename{CMakeCache.txt} file.  These variables are normally
set on the \syscmd{cmake} command line using options of the form
\syscmd{-D <var>:<type>=<value>}, where \syscmd{<var>} is the case-sensitive
variable name, \syscmd{<type>} is the variable type, and \syscmd{<value>} is
the variable value.  The variable types are \syscmd{STRING}, \syscmd{FILEPATH},
\syscmd{PATH}, and \syscmd{BOOL} for strings, file paths, directory paths,
and booleans.  Acceptable boolean values are \syscmd{TRUE}/\syscmd{FALSE},
\syscmd{YES}/\syscmd{NO}, and \syscmd{ON}/\syscmd{OFF}, and are not case
sensitive.  Because the number of configuration variables can be extensive,
an alternative to using command line options is to place the variable
settings in a CMake script file and loading it with the \syscmd{-C <script>}
option.  This script file consists of \syscmd{SET} commands with the syntax
\begin{Verbatim}[commandchars=\\\{\},frame=single]
 \textbf{SET(}<variable name> <value> \textbf{CACHE} <type> <documentation string>\textbf{)}
\end{Verbatim}
Both approaches can be used together.
A list of the variables relevant to the Truchas build are given in
Appendix~\ref{sec:vars}.

\section{Building Truchas}{\label{sec:truchas}}
This section describes how to build and install Truchas, including its
associated libraries and tools.  It assumes that the requirements outlined in
Section~\ref{sec:req} are satisfied, and that you have a copy of the Truchas
source tree (unpacked from the gzip-compressed tar archive of a Truchas
release, or checked out from the Truchas code repository).

It is recommended that you first successfully build and test the serial
version of Truchas before attempting to build the parallel version.
Experience has shown that issues with the MPI installation are the most
frequent source of configure/build errors; a serial build avoids those
potential issues and allows you concentrate first on resolving any other
configure/build problems, if any.

As described in Section~\ref{sec:cmake}, the first step is to generate the
makefiles. Begin by creating a build directory.  The discussion that follows
assumes this is a subdirectory named \texttt{build} of the top-level source
directory, but any directory other than the source directory will do.
\begin{Verbatim}[frame=single]
 $ cd <top-level-truchas-directory>
 $ mkdir build
 $ cd build
\end{Verbatim}
The build system is generated by calling \syscmd{cmake}:
\begin{Verbatim}[frame=single]
 $ cmake [options] ..
\end{Verbatim}
The options will include the setting of various CMake build configuration
variables.  The variables relevant to the Truchas build are listed in
Appendix~\ref{sec:vars}.  Settings for several pre-defined configurations
are defined the script files located in the \filename{config} directory.
These are loaded with the \syscmd{-C <script>} option.  The configurations
currently defined are
\begin{center}\begin{tabular}{>{\ttfamily}l>{\ttfamily}l}
intel-serial-opt.cmake & nag-serial-opt.cmake \\
intel-serial-dbg.cmake & nag-serial-dbg.cmake \\
intel-parallel-opt.cmake & nag-parallel-opt.cmake \\
intel-parallel-dbg.cmake & nag-parallel-dbg.cmake
\end{tabular}\end{center}
The meaning of each should be clear from its name.  You are not required
to use one of these.  You can create your own, or define all the necessary
variables on the command line with \syscmd{-D} options; whatever best suits
your situation.  As an example, the command line to generate a build system
for an optimized parallel Truchas using the Intel compilers and shared
libraries would be
\begin{Verbatim}[frame=single]
$ cmake -C ../config/intel-parallel-opt.cmake -D ENABLE_SHARED:BOOL=YES ..
\end{Verbatim}
Don't overlook the final "\texttt{..}" command line argument which gives
the path to the directory containing the top-level \texttt{CMakeLists.txt}
file---in this case the parent directory.
When \syscmd{cmake} successfully completes, you can proceed to the next
step of building and installing Truchas using \syscmd{make}:
\begin{Verbatim}[frame=single]
$ make [options] [target]
\end{Verbatim}
The list of possible targets is extensive (run "\syscmd{make help}" to get
the list), but there are only a few that are important:
\begin{description}\setlength{\itemsep}{0pt}
\item[\syscmd{all}]
  compiles all libraries and executables; this is the default when the target
  is omitted.
\item[\syscmd{install}]
  installs all the libraries and executables.  This will first make the
  \syscmd{all} target, if necessary, to compile them.  See the description
  of \syscmd{CMAKE_INSTALL_PREFIX} to see where the files are installed.
  The list of installed files is given in Appendix~\ref{sec:files}.
\item[\syscmd{rebuild_cache}]
  regenerates the build system after variables in \filename{CMakeCache.txt}
  have been modified (see Section~\ref{sec:cmake}).
\end{description}
The time to build can be greatly reduced on a system with multiple processor
cores by including the "\syscmd{-j N}" option, where \syscmd{N} is the number
of tasks to run simultaneously.  Typically, a value as much as twice the
number of cores can be used fruitfully.

\subsection{Testing Truchas}
After building Truchas you should run the test suite to verify that everything
is working properly.  This uses the \syscmd{ctest} command that is part of a
CMake installation.  From the build directory simply run
\begin{Verbatim}[frame=single]
 $ ctest
\end{Verbatim}
On a system with multiple compute cores use the "\syscmd{-j N}" option to
intelligently distribute the tests over \syscmd{N} cores, running multiple
tests simultaneously.  Note that it is not necessary to make the
\syscmd{install} target before running the tests; they use the executable
from the build tree and not the installation tree, if any.

All tests should pass. In the case of failures, examine the log file
\texttt{Testing/Temporary/\allowbreak LastTest.log} for clues to the problem.
Alternatively, re-running with the \syscmd{-V} option will produce more
verbose output to the terminal.

A common but easily fixed problem when using the Intel compiler is
exceeding the stack size.  When this happens the code aborts with a 
segmentation violation (\syscmd{SIGSEGV}, exit code 139), though be
aware that other problems can produce the same exit code.  When this
is the problem the simple solution is to unlimit the stack size.
To view your limits: "\syscmd{ulimit -a}" (bash) or \syscmd{limits} (csh).
To reset your stack size limit: "\syscmd{ulimit -s unlimited}" (bash);
or "\syscmd{limit stacksize unlimited}" (csh).

\subsection{How CMake finds your MPI installation}\label{sec:mpi}
CMake uses the MPI compiler wrappers to obtain the needed information about
the MPI installation.  It first looks in your path for these wrappers (commonly
\syscmd{mpicc}, \syscmd{mpicxx}, and \syscmd{mpif90}).  If it does not find
them there, it then looks to see if one of the environment variables
\texttt{MPIROOT}, \texttt{MPI_ROOT}, or \texttt{MPICH_ROOT} is defined, and
if so, it searches the \texttt{bin} subdirectory of that directory for the
wrappers.  If it is still unable to locate the wrappers, \syscmd{cmake} exits
with an error. The wrappers can be explicitly specified (by-passing the search
for them) by setting the \texttt{MPI_<lang>_COMPILER} variables on the
\syscmd{cmake} command line.  If the MPI installation does not provide the
compiler wrappers, you will need to define the CMake variables
\texttt{MPI_C_INCLUDE_PATH} and \texttt{MPI_C_LIBRARIES}, but note
this last-gasp approach is fraught with potential errors.  See
Appendix~\ref{sec:vars} for the CMake variables mentioned.

\section{Problems}
Please report any problems installing or running Truchas to
\href{mailto:telluride-support@lanl.gov}{truchas@lanl.gov}.
Please include details about your platform and any other information
relevant to identifying the problem.  While we do our best to support
users outside of LANL, understand that the resources for doing so is very
limited.

\begin{appendices}
\section{Truchas CMake Variables}\label{sec:vars}
\subsection{Primary variables}
These are the variables that you will most likely need to use.
\begin{description}[style=nextline]
\item[\syscmd{CMAKE_\textmd{<language>}_COMPILER}]
  Identifies the compiler to use for the different language source files:
  \syscmd{<language>} is \syscmd{C}, \syscmd{CXX}, or \syscmd{Fortran}.
  If this variable is not set, CMake will attempt to detect the compiler
  and automatically set the variable, and will use the value, if any, of the
  environment variables \syscmd{CC}, \syscmd{CXX}, and \syscmd{FC} in this
  attempt.  Note that the value of these variables cannot be changed after
  the first run of \syscmd{cmake} or \syscmd{ccmake}.
\item[\syscmd{CMAKE_\textmd{<language>}_FLAGS}]
  The compiler flags to use for compiling source files in different languages:
  \syscmd{<language>} is \syscmd{C}, \syscmd{CXX}, or \syscmd{Fortran}.
  These flags are in addition to other flags that CMake may use based on the
  value of \syscmd{CMAKE_BUILD_TYPE} and other configuration settings.
\item[\syscmd{CMAKE_BUILD_TYPE}]
  A variable that controls the type of executable to build.  The choices are
  \syscmd{RELEASE} for an optimized build, and \syscmd{DEBUG} for an
  unoptimized build that enables some runtime checking and that can be used
  with a debugger.  This option affects the C/C++ compiler flags that are used
  for optimization.  CMake currently has no built-in flag sets for Fortran;
  these must be specified with \syscmd{CMAKE_Fortran_FLAGS}.
\item[\syscmd{CMAKE_INSTALL_PREFIX}]
  The root directory for the installation.  If not specified, it is set
  to the subdirectory \filename{install/<build-id>} of the top-level
  source directory. The particular configuration determines
  \texttt{<build-id>}, which will have the form
  \begin{Verbatim}[frame=single,gobble=2]
    linux.{i686|x86_64}.{nag|intel}.{serial|parallel}.{dbg|opt}
  \end{Verbatim}
  The default generally works well for building multiple versions of Truchas.
\item[\syscmd{CMAKE_VERBOSE_MAKEFILE}]
  Set this variable to \syscmd{TRUE} if you want to see full compile and
  link commands instead of the abbreviated ones shown by default.
\item[\syscmd{ENABLE_MPI}]
  Controls whether to build an MPI-parallel or serial version of Truchas.
  Default is \texttt{TRUE}.
\item[\syscmd{ENABLE_SHARED}]
  Set this to \texttt{TRUE} to build shared libraries instead of static ones.
  Default is \texttt{FALSE}.
\item[\syscmd{ENABLE_DYNAMIC_LOADING}]
  Set this to \texttt{FALSE} to disable the capability of Truchas to
  dynamically load functions from user-created shared libraries.  It is
  necessary to do this if you need to build a purely static executable.
  The default is \texttt{TRUE}.
\end{description}

\subsection{Additional variables}
\begin{description}[style=nextline]
\item[\syscmd{ENABLE_ExternalSearch}]
  Set this to \syscmd{NO} to disable the search for installed versions of
  the libraries listed in Section~\ref{sec:incl}, and force private versions
  to be built.  The default is to search for and use installed versions when
  possible.
\item[\syscmd{TruchasExternal_INSTALL_PREFIX}]
  The root directory where the libraries listed in Section~\ref{sec:incl}
  will be installed (if built).  This defaults to
  \syscmd{\$\{CMAKE_INSTALL_PREFIX\}/external}.
\item[\syscmd{HDF5_INSTALL_PREFIX}]
  The root directory where CMake will look for a suitable HDF5 installation;
  it expects to find \texttt{include}, \texttt{lib}, and \texttt{bin}
  subdirectories of this directory.  It is not necessary to set this unless
  the system HDF5 is installed in an unusual location.
\item[\syscmd{HYPRE_INSTALL_PREFIX}]
  The root directory where CMake will look for a suitable Hypre installation;
  it expects to find \texttt{include} and \texttt{lib} subdirectories of this
  directory.
\item[\syscmd{NETCDF_INSTALL_PREFIX}]
  The root directory where CMake will look for a suitable NetCDF installation;
  it expects to find \texttt{include}, \texttt{lib}, and \texttt{bin}
  subdirectories of this directory.  It is not necessary to set this unless
  the system NetCDF is installed in an unusual location.
\item[\syscmd{ZLIB_ROOT}]
  The root directory where CMake will look for a suitable zlib installation;
  it expects to find \texttt{include} and \texttt{lib} subdirectories of this
  directory.  It is not necessary to set this unless the system zlib is
  installed in an unusual location.
\item[\syscmd{SWIG_EXECUTABLE}]
  Full path name of the \syscmd{swig} command.  Only necessary if it isn't
  in your path.
\item[\syscmd{MPI_<lang>_COMPILER}]
  The MPI compiler wrapper for language \syscmd{<lang>}:
  \syscmd{C}, \syscmd{CXX}, or \syscmd{Fortran}.
\item[\syscmd{MPI_C_INCLUDE_PATH}]
  The directory containing the MPI include files for C.  Multiple directories
  can be specified by separating them with a semicolons.
\item[\syscmd{MPI_C_LIBRARIES}]
  The MPI link libraries for C.  Multiple library paths can be specified
  (always use full paths) by separating them with semicolons.
\end{description}

\section{Installed Files}\label{sec:files}
The directory and file paths listed here are relative to the root install
directory set by the \syscmd{CMAKE_INSTALL_PREFIX} variable
(see Appendix~\ref{sec:vars}).
\begin{description}[style=nextline]
\item[\filename{external}]
  The default install root directory for the external third party packages
  described in Section~\ref{sec:incl}.  The path can be changed with the
  \syscmd{TruchasExternal_INSTALL_PREFIX} variable; see Appendix~\ref{sec:vars}.
\item[\filename{lib}]
  Contains the main truchas library and various other internal libraries.
\item[\filename{include}]
  Contains the header files and compiled Fortran module information files
  associated with the libraries in the \filename{lib} directory.
\item[\filename{bin}]
  Contains the Truchas executable and associated tools.  Includes:
  \begin{description}[style=nextline]
  \item[\filename{t-<build-id>}]
    The Truchas program; the suffix \filename{<build-id>} is automatically
    constructed from the operating system, cpu architecture, compiler, build
    mode and type, and version number.
  \item[addgaps]
    This is a mesh preprocessor that inserts so-called gap elements along
    selected internal ExodusII mesh surfaces.
  \item[Output processors]
    The programs \syscmd{xdmf-parser.py}, \syscmd{truchas-gmv-parser.py},
    \syscmd{danu_report.py},\\\syscmd{write_restart}, and \syscmd{write_probe}
    process the Truchas output in various ways.  See the Truchas Reference
    Manual for details, or use the \syscmd{{-}{-}help} option with these
    programs.
  \item[RadE tool set]
    The programs \syscmd{genre}, \syscmd{vizre}, and \syscmd{cmpre}
    (\syscmd{pgenre}, \syscmd{pvizre}, and \syscmd{pcmpre} with a parallel
    build) comprise the RadE tool set, which are tools for generating the
    radiation enclosure files that Truchas reads to initialize its view
    factor radiation physics model.
  \end{description}
\end{description}

\end{appendices}


\end{document}
