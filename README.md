The Documents
--------------------------------------------------------------------------------
#### Reference Manual
A guide to running Truchas, with a complete description of all input variables.
We strive to keep this document up to date with the Truchas master branch.

#### Physics and Algorithms
A description of the physical models and numerical algorithms used in Truchas.
**This document has not been updated since 2008 (version 2.4) and is out of
date.** The chapter on heat transfer and phase change is obsolete. Some models
differ, and the numerical algorithm used is entirely different. The model for
chemical reactions has been removed from Truchas.  The remainder (fluid
dynamics, solid mechanics, electromagnetics) remains largely correct.

#### User Manual
A tutorial that describes and demonstrates the capabilities of Truchas using
a series of simple example problems. **This document has not been updated since
2008 (version 2.4) and is badly out of date. Probably none of the examples will
work as presented.**

#### Installation Guide
A guide to compiling, testing, and installing Truchas.  This is current for
the version 3.0 release, but subsequent Truchas build changes have rendered
it obsolete.  **Current installation instructions are provided by a few README
files in the Truchas source tree.**

Building the Documents
--------------------------------------------------------------------------------
Go to the document directory of choice and type 'make'. If all goes well a pdf
of the document will emerge.  The documents are written in LaTeX, and should
build using a TeXLive 2013 installation, or newer, as long all the necessary
add-on packages have been installed. A commonly missing package is `todonotes`.
